import { NextFunction, Response } from 'express';
import axios from 'axios';
import { BYPASS_AUTH, ORG_MODULE_API } from '@config';
import { HttpException } from '@exceptions';
import { TRequestWithUser } from '@interfaces';
import { getOrganizationsAndRoles, loadUserSettings } from '.';
import { Debug } from '@utils';

const debug = Debug('aws');

export const awsAuthMiddleware = async (req: TRequestWithUser, res: Response, next: NextFunction) => {
  if (req.path.match(/^\/(admin\/token$|(?:api-)?docs)/)) {
    next();
  } else if (BYPASS_AUTH) {
    debug('Bypass the AuthService integration');
    next();
  } else {
    const token = req.cookies['Authorization'] || (req.header('Authorization') ? req.header('Authorization').split('Bearer ')[1] : null);

    if (!token) {
      next(new HttpException(401, 'Authentication token missing'));
    } else if (req.session?.userId) {
      debug('Session userId:', req.session.userId);
      // debug('Session details:', req.session);
      next();
    } else {
      debug('Running the validation');

      try {
        // Validate the token with the AWS
        const { data } = await axios.get(`${ORG_MODULE_API}/persons/requester/token`, {
          headers: { Authorization: `Bearer ${token}` },
        });
        req.userId = data.uuid;
        req.session.userId = data.uuid;

        // get getOrganizationsAndsRoles and add to the session
        await getOrganizationsAndRoles(req.session, token);

        // load user settings
        await loadUserSettings(req.session);
        next();
      } catch (err) {
        debug('Error:', err?.response?.data?.message);
        next(new HttpException(401, 'Wrong authentication token'));
      }
    }
  }
};

export default awsAuthMiddleware;
