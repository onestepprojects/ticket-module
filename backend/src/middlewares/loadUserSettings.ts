import { TicketSession as Session } from '@interfaces';
import { Debug } from '@utils';
import prisma from '@/client';

const debug = Debug('session');

export const loadUserSettings = async (session: Session) => {
  if (!session.settings) {
    debug('loading user settings');
    try {
      session.settings = await prisma.settings.findUnique({
        where: { userId: session.userId },
      });
    } catch (err) {
      debug('initializing user settings');
      session.settings = await prisma.settings.create({
        data: {
          userId: session.userId,
        },
      });
    }
  }
};
