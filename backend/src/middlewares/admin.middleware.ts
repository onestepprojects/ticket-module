import { NextFunction, Response } from 'express';
import { Debug } from '@utils';
import { TRequest } from '@/interfaces';
const debug = Debug('admin');

// eslint-disable-next-line
export const adminMiddleware = async (req: TRequest, res: Response, next: NextFunction) => {
  const userId = req.session.userId;
  debug(`${userId} accessing admin route: ${req.url}...`);
  next();
};

export default adminMiddleware;
