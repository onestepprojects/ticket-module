import axios from 'axios';
import { ORG_MODULE_API } from '@config';
import { Debug } from '@utils';
import prisma from '@/client';
import { TicketSession as Session } from '@/interfaces';
import { TicketVisibility } from '@prisma/client';

const debug = Debug('aws');

export const getOrganizationsAndRoles = async (session: Session, token: string) => {
  // session already has organizations. No need to do anything.
  if (!session.organizations) {
    debug('getting getOrganizations');

    // Validate the token with the AWS
    try {
      // get the organizations and roles from one step OPP module
      const { data } = await axios.get(`${ORG_MODULE_API}/organizations/get-org-roles-by-person/` + session.userId, {
        headers: { Authorization: `Bearer ${token}` },
      });

      // check if organization exists in postgress, create if needed
      prisma.organization.createMany({ data, skipDuplicates: true });

      session.userOrgs = data.map(x => x.id);
      session.where = {
        OR: [{ visibility: TicketVisibility.public }, { organizations: { some: { id: { in: session.userOrgs } } } }],
      };
    } catch (err) {
      debug('Error:', err?.response?.data?.message);
    }
  }
};

export default getOrganizationsAndRoles;
