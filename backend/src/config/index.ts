import { config } from 'dotenv';
export * from './notifications';
export * from './rewards';

config({ path: `.env.${process.env.NODE_ENV || 'development'}.local` });

// random seed
export const SEED = 1647142082296;

/// Environment variables

export const DEBUG_PREFIX = process.env.DEBUG_PREFIX ?? 'ticket';
export const DEBUG = process.env.DEBUG ?? process.env.NODE_ENV === 'development' ? `${DEBUG_PREFIX}:*` : undefined;
export const BYPASS_AUTH = process.env.BYPASS_AUTH !== undefined;
// 'https://test.onesteprelief.org/onestep';
export const ONESTEP_API = process.env.ONESTEP_API ?? 'https://capstone.onestepprojects.org/onestep';
export const ORG_MODULE_API = process.env.REACT_APP_ORG_MODULE_API_URL || `${ONESTEP_API}/organization`;
export const CREDENTIALS = process.env.CREDENTIALS === 'true';
export const CREATE_PROXY = process.env.TICKET_CREATE_PROXY !== undefined;

export const { // eslint-disable-line
  NODE_ENV,
  PORT,
  SECRET_KEY,
  LOG_FORMAT,
  LOG_DIR,
  ORIGIN,
} = process.env;
