// default reward points given to user when a ticket is resolved
export const DEFAULT_TICKET_REWARD = 10;
