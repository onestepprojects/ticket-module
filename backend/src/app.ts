import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import hpp from 'hpp';
import morgan from 'morgan';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { // eslint-disable-line
  ONESTEP_API,
  NODE_ENV,
  PORT,
  LOG_FORMAT,
  ORIGIN, // eslint-disable-line @typescript-eslint/no-unused-vars
  CREDENTIALS,
  CREATE_PROXY,
} from '@config';
import { Routes } from '@interfaces';
import { errorMiddleware, awsAuthMiddleware, adminMiddleware } from '@middlewares';
import { logger, stream } from '@utils';
import { websocket } from './websockets';
import { sessionParser } from './session';
import https from 'https';

// Devel
import { createProxyMiddleware } from 'http-proxy-middleware';
import fs from 'fs';

class App {
  public app: express.Application;
  public env: string;
  public port: string | number;

  constructor(routes: Routes[]) {
    this.app = express();
    this.env = NODE_ENV || 'development';
    this.port = PORT || 3000;

    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.initializeSwagger();
    this.initializeErrorHandling();
    // XXX: cache common operations
    // https://www.prisma.io/docs/guides/performance-and-optimization/connection-management#prismaclient-in-long-running-applications
  }

  public async listen() {
    const server = https.createServer(
      {
        key: fs.readFileSync('./certs/tls.key'),
        cert: fs.readFileSync('./certs/tls.crt'),
        // websocket will try to validate against known CAs- dont want this with self-signed
        rejectUnauthorized: false,
      },
      this.app,
    );
    // FIXME: websocket not working with https
    websocket(this.app, server);
    return server.listen(this.port, () => {
      logger.info(`=================================`);
      logger.info(`======= ENV: ${this.env} =======`);
      logger.info(`🚀 App listening on the port ${this.port}`);
      logger.info(`=================================`);
    });
  }

  public getServer() {
    return this.app;
  }

  private initializeMiddlewares() {
    // logging
    this.app.use(morgan(LOG_FORMAT, { stream }));
    this.app.use(
      cors({
        // true - just allow any access
        origin: true, //[/http[s]?:\/\/localhost(?::[0-9]+)?/, ORIGIN],
        credentials: CREDENTIALS,
      }),
    );
    // protect against HTTP parameter pollution attacks
    this.app.use(hpp());
    // set security HTTP headers
    this.app.use(helmet());
    // use gzip compression
    this.app.use(compression());
    // parse json request body
    this.app.use(express.json());
    // parse url encoded request body
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cookieParser());
    // dont send 304 response if data hasn't changed
    this.app.disable('etag');
    // use sessions
    this.app.use(sessionParser);
    // run Auth on all routes
    this.app.use(awsAuthMiddleware);
    // check admin
    this.app.use(/^\/admin\/.+$/, adminMiddleware);
  }

  private initializeRoutes(routes: Routes[]) {
    routes.forEach(route => {
      // XXX: use versioned API ???
      this.app.use('/', route.router);
    });
    // Development: redirects to other onestep apis
    // Known OPP APIs rooted at ${ONESTEP_API}:
    // - organization/* => organziation, persons, projects, tasks APIs
    // - accreditation/*
    // - rewards/*
    // - authorization/*
    // - fund/*
    // - resource/*
    if (NODE_ENV === 'development' || CREATE_PROXY) {
      this.app.get(
        /(a((ccredit|uthoriz)ation)|fund|organization|re(source|wards))\/*/,
        createProxyMiddleware({
          target: `${ONESTEP_API}`,
          changeOrigin: true,
        }),
      );
      // (req, res) => {
      //   res.redirect(302, `${ONESTEP_API}${req.url}`)
    }
  }

  private initializeSwagger() {
    // default openapi: '2.0.0'
    const options = {
      swaggerDefinition: {
        openapi: '3.0.3',
        info: {
          title: 'REST API',
          version: '1.0.0',
          description: 'Ticket Service docs',
        },
        security: [
          {
            bearerAuth: [],
          },
        ],
      },
      apis: [
        'swagger.yaml',
        'src/prisma/*.yaml',
        'src/interfaces/swagger/*.yaml',
        'src/routes/*route.ts',
        'src/dtos/*dto.ts',
        'src/interfaces/*interface.ts',
      ],
    };

    this.app.get(/(?:api-)?docs$/, (req, res) => {
      console.log(`handling explicit redirect to ${req.baseUrl}`);
      res.redirect(301, `${req.baseUrl}/docs/`);
    });

    const specs = swaggerJSDoc(options);
    this.app.use(
      '/docs',
      swaggerUi.serve,
      swaggerUi.setup(specs, {
        swaggerOptions: {
          docExpansion: 'none',
          deepLinking: true,
        },
      }),
    );
  }

  private initializeErrorHandling() {
    // report HTTP exceptions
    this.app.use(errorMiddleware);
  }
}

export default App;
