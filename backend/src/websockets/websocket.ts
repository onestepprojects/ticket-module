import WebSocket from 'ws';
import queryString from 'query-string';
import { Server } from 'https';
import { Application, Response } from 'express';
import { logger } from '@utils';
import { sessionParser } from '@/session';
import { IncomingMessage } from 'http';
import { Debug } from '@utils';
import { TRequest } from '@/interfaces';

const debug = Debug('ws');

const getParams = (request: IncomingMessage) => {
  // eslint-disable-next-line
  const [path, params] = request?.url?.split('?');
  return queryString.parse(params);
};

export const websocket = async (app: Application, server: Server) => {
  const wss = new WebSocket.Server({
    path: '/websockets',
    noServer: true,
  });

  server.on('upgrade', (request: TRequest, socket, head) => {
    debug('upgrading to websocket connection');
    // debug(request)

    sessionParser(request, {} as Response, () => {
      if (!request.session.userId) {
        debug('session user not found, aborting...');
        socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
        socket.destroy();
        return;
      }

      debug('Session is parsed!');

      wss.handleUpgrade(request, socket, head, function (ws) {
        wss.emit('connection', ws, request);
      });
    });
  });

  wss.on('connection', (ws, request: TRequest) => {
    // Store all connected sockets to be accessible from routes, eg.
    //   broadcast(app.locals.clients, ...);
    app.locals.clients = wss.clients;

    const params = getParams(request);
    debug(`connection params:`, JSON.stringify(params));

    const userId = request.session.userId;

    ws.on('message', async (data: string) => {
      try {
        debug(`Received message ${data} from ${userId}`);
        console.log(JSON.parse(data));
      } catch (_err) {
        console.log(data);
      }
    });
  });

  return wss;
};

// Send data to all connected sockets
export const broadcast = (clients: Set<WebSocket>, data: object) => {
  logger.debug(`[WS] Broadcasting to ${clients?.size ?? 0} clients`);

  clients?.forEach(client => {
    if (client.readyState === WebSocket.OPEN) client.send(JSON.stringify(data));
  });
};
