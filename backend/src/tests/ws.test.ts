import ws from 'ws';

const client = new ws('wss://localhost:5005/websockets?test=true', {
  rejectUnauthorized: false,
});

// eslint-disable-next-line
client.on('connection', function connection(ws) {
  console.log('client connection');
});

client.on('open', () => {
  client.send(JSON.stringify({ message: 'Hello' }));
});

client.on('message', message => {
  console.log(JSON.parse(message.toString()));
  client.close();
});
