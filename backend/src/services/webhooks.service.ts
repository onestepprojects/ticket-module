import { Webhook } from '@prisma/client';
import { CreateWebhookDto } from '@dtos';
import { HttpException } from '@exceptions';
import { isEmpty } from '@utils';
import prisma from '../client';

export class WebhookService {
  public webhooks = prisma.webhook;
  // public tickets = new PrismaClient().ticket;
  // public users = new PrismaClient().user

  public async findAllWebhooks(): Promise<Webhook[]> {
    // XXX: filter webhooks by those the current user can see
    const allWebhooks: Webhook[] = await this.webhooks.findMany();
    return allWebhooks;
  }

  public async findWebhookById(webhookId: number): Promise<Webhook> {
    if (isEmpty(webhookId)) throw new HttpException(400, 'Bad webhookId');

    const findWebhook: Webhook = await this.webhooks.findUnique({ where: { id: webhookId } });
    if (!findWebhook) throw new HttpException(409, 'Bad webhookId');

    return findWebhook;
  }

  // No user, no hook
  public async createWebhook(webhookData: CreateWebhookDto): Promise<Webhook> {
    if (isEmpty(webhookData)) throw new HttpException(400, 'Bad webhookData');

    // const findUser: User = await this.users.findUnique({ where: { id: userId } })
    // if (!findUser) throw new HttpException(409, `Must have account to create webhook`)

    const createWebhookData: Webhook = await this.webhooks.create({
      data: { ...webhookData },
    });
    return createWebhookData;
  }

  public async updateWebhook(webhookId: number, webhookData: CreateWebhookDto): Promise<Webhook> {
    if (isEmpty(webhookData)) throw new HttpException(400, 'Empty webhookData');

    const findWebhook: Webhook = await this.webhooks.findUnique({ where: { id: webhookId } });
    if (!findWebhook) throw new HttpException(409, 'Webhook not found');

    const updateWebhookData = await this.webhooks.update({
      where: { id: webhookId },
      data: { ...webhookData },
    });
    return updateWebhookData;
  }

  public async deleteWebhook(webhookId: number): Promise<Webhook> {
    if (isEmpty(webhookId)) throw new HttpException(400, 'No webhookId');

    const findWebhook: Webhook = await this.webhooks.findUnique({ where: { id: webhookId } });
    if (!findWebhook) throw new HttpException(409, 'Webhook not found');

    const deleteTodoData = await this.webhooks.delete({ where: { id: webhookId } });
    return deleteTodoData;
  }
}

export default WebhookService;
