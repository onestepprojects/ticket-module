import { Tag, TagType, Ticket, User } from '@prisma/client';
import prisma from '../client';
import { CreateTagDto } from '@dtos';
import { HttpException } from '@exceptions';
import { // eslint-disable-line
  Debug,
  findManyWatchersFor,
  findManyTicketsFor,
  isEmpty,
  checkFindManyParams,
  checkEmpty,
} from '@utils';
import { // eslint-disable-line
  FindManyQuery,
  FindManyTagsQuery,
  TagResponse,
  TagsResponse,
  TicketsResponse,
  UsersResponse,
} from '@interfaces';

const debug = Debug('tags');

interface TagData {
  id?: number;
  type?: TagType;
  name: string;
}

export class TagService {
  public tags = prisma.tag;
  public tickets = prisma.ticket;

  private async checkTag(tagId: number): Promise<Tag> {
    checkEmpty(tagId, 'missing tag Id');
    return this.tags.findUnique({ where: { id: tagId } });
  }

  // check data and convert type to type
  // Tag names are always lowercase!
  private checkTagData(tagData: CreateTagDto): TagData {
    checkEmpty(tagData, 'missing tag data');
    const { name } = tagData;
    return { ...tagData, name: name.toLowerCase() };
  }

  public async findTags(params: FindManyTagsQuery): Promise<TagsResponse> {
    if (!isEmpty(params)) debug('params:', params);
    const { countParams, ...rest } = checkFindManyParams<Tag>(params);
    if (countParams) return this.tags.count(countParams);
    return this.tags.findMany(rest);
  }

  public async findTagById(tagId: number): Promise<TagResponse> {
    return this.checkTag(tagId);
  }

  public async createTag(data: CreateTagDto): Promise<TagResponse> {
    const cleanData = this.checkTagData(data);
    let tag = undefined;
    try {
      tag = await this.tags.findUnique({ where: { name_type: { name: cleanData.name, type: cleanData.type } } });
    } catch {}
    if (tag) throw new HttpException(409, 'Tag already exists');
    return this.tags.create({ data });
  }

  public async updateTag(tagId: number, tagData: CreateTagDto): Promise<TagResponse> {
    const cleanData = this.checkTagData(tagData);
    const updateTagData = await this.tags.update({
      where: { id: tagId },
      data: cleanData,
    });
    return updateTagData;
  }

  public async deleteTag(tagId: number): Promise<TagResponse> {
    return this.tags.delete({ where: { id: tagId } });
  }

  public async findTicketsForTag(tagId: number, params: FindManyQuery<Ticket>): Promise<TicketsResponse> {
    return findManyTicketsFor<Tag>(this.tags, tagId, params);
  }

  public async findWatchersForTag(tagId: number, params: FindManyQuery<User>): Promise<UsersResponse> {
    return findManyWatchersFor<Tag>(this.tags, tagId, params);
  }
}

export default TagService;
