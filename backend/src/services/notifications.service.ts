import { Notification, NotificationType, User } from '@prisma/client';
import { CreateNotificationDto } from '@dtos';
import { HttpException } from '@exceptions';
import { // eslint-disable-line
  checkEmpty,
  daysAgo,
  Debug,
  getUsersToNotifyForTicket,
  getWatchersToNotify,
  idsToHash,
  isEmpty,
  notificationRelationsQ,
} from '@utils';
import prisma from '../client';
import { TicketEvent } from '@types';
import { FindManyQuery } from '@interfaces';
import { NOTIFICATIONS_MAX_AGE } from '@config';
import { partition } from 'lodash';

const debug = Debug('notifications');

// Notification data after conversion from raw input
interface NotificationData extends CreateNotificationDto {
  type: NotificationType;
}

export class NotificationService {
  public notifications = prisma.notification;
  public notifsOn = prisma.notificationsOnReceivers;
  public tickets = prisma.ticket;
  public users = prisma.user;

  private async checkUser(userId: string): Promise<User> {
    checkEmpty(userId, 'missing userId');
    return this.users.findUnique({ where: { id: userId } });
  }

  /**
   * Check validity of notification data, and return processed result.
   */
  private async checkData(notificationData: CreateNotificationDto): Promise<NotificationData> {
    checkEmpty(notificationData, 'missing notification data');
    const { type, senderId, ticketId, points } = notificationData;

    // Depending on notification type, some different required fields
    switch (type) {
      case 'message':
      case 'user':
        this.checkUser(senderId);
        break;
      case 'organization':
        break;
      case 'rewards':
        checkEmpty(points, 'Reward notification missing points');
      default:
        checkEmpty(ticketId, 'Notification missing ticket Id');
        await this.tickets.findUnique({ where: { id: ticketId } });
    }
    return { ...notificationData, type: NotificationType[type] };
  }

  public async findNotifications(params: FindManyQuery<Notification>): Promise<Notification[]> {
    // eslint-disable-next-line
    const { skip, take, cursor, where, orderBy } = params;
    return this.notifications.findMany({
      include: {
        sender: { select: { id: true, name: true } },
        receivers: {
          select: {
            read: true,
            receiver: {
              select: { id: true, name: true },
            },
          },
        },
      },
      orderBy: { updatedAt: 'desc' },
    });
  }

  public async findNotificationById(notificationId: number): Promise<Notification> {
    if (isEmpty(notificationId)) throw new HttpException(400, 'Missing notificationId');
    return this.notifications.findUnique({ where: { id: notificationId } });
  }

  /**
   * Send notifications to users watching tags added to new ticket
   */
  public async notifyForNewTicket(data: Partial<CreateNotificationDto>): Promise<void> {
    const { tags, organizations, message, ...rest } = data;
    if (isEmpty(tags) && isEmpty(organizations)) return;

    const notifyIds = await getWatchersToNotify(tags, organizations);
    debug('notifying on new ticket:', notifyIds);

    if (!isEmpty(notifyIds)) {
      this.createNotification({
        ...rest,
        message: message ?? 'ticketCreate',
        type: NotificationType.ticketCreate,
        receivers: notifyIds,
        tags,
        organizations,
      });
    }
  }

  /**
   * Send notifications to users watching ticket
   *
   * If event is a status update, `TicketEvent.ticketStatusUpdate`, and points
   * are defined, then users is `assignedTo` will be sent
   * `NotificationType.rewards` notifications, and remaining users will be sent
   * `NotificationType.ticketStatusChange`.
   *
   * Otherwise, the same notifications are sent to all receivers.
   *
   * @param data - notification data
   * @param [assignedTo] - array of user Ids assigned to ticket
   */
  public async notifyForTicket(
    data: Partial<CreateNotificationDto> & {
      event: TicketEvent;
    },
    assignedTo?: { id: string }[],
  ): Promise<void> {
    const { ticketId, event, message, points, status, ...rest } = data;
    let notifyIds = await getUsersToNotifyForTicket(ticketId, event);
    if (isEmpty(notifyIds)) return;

    // If status change, send rewards notifications to assignees
    if (event === TicketEvent['ticketStatusChange'] && points) {
      const hash = idsToHash(assignedTo);
      const [assignees, others] = partition(notifyIds, id => id in hash);
      debug(`rewards:`, assignees);
      notifyIds = others;

      // send rewards notifications
      this.createNotification({
        ...rest,
        message: message ?? 'rewards',
        type: NotificationType.rewards,
        receivers: assignees,
        ticketId,
        points,
        status,
      });
    }

    if (!isEmpty(notifyIds)) {
      debug(`${TicketEvent[event]}:`, notifyIds);
      this.createNotification({
        ...rest,
        message: message ?? TicketEvent[event],
        type: NotificationType[TicketEvent[event]],
        receivers: notifyIds,
        ticketId,
        status,
      });
    }
  }

  // TODO: push notificatons to websocket listeners
  public async createNotification(notificationData: CreateNotificationDto): Promise<Notification> {
    const { senderId, receivers, ticketId, tags, organizations, ...rest } = await this.checkData(notificationData);
    return this.notifications.create({
      data: {
        ...rest,
        receivers: {
          create: receivers.map(recId => ({
            read: false,
            receiver: { connect: { id: recId } },
          })),
        },
        ...notificationRelationsQ(senderId, ticketId, tags, organizations),
      },
    });
  }

  // Allow updates to notifications to:
  // - change the message
  // - add to the receiver list (if this happens, should push to added receivers as well)
  public async updateNotification(notificationId: number, notificationData: Partial<CreateNotificationDto>): Promise<Notification> {
    const { receivers, message } = notificationData;

    return this.notifications.update({
      where: { id: notificationId },
      data: {
        message,
        receivers: {
          connectOrCreate: receivers?.map(receiverId => ({
            where: { notificationId_receiverId: { notificationId, receiverId } },
            create: { read: false, receiver: { connect: { id: receiverId } } },
          })),
        },
      },
    });
  }

  public async deleteNotification(notificationId: number): Promise<Notification> {
    try {
      await this.notifications.findUnique({ where: { id: notificationId } });
    } catch {
      throw new HttpException(400, 'Notification not found');
    }
    return this.notifications.delete({ where: { id: notificationId } });
  }

  /**
   * Delete all notifications older than given days
   * @param {integer} [days = NOTIFICATIONS_MAX_AGE] - max age notifications to keep
   */
  public async deleteExpiredNotifications(days = NOTIFICATIONS_MAX_AGE): Promise<void> {
    debug(days);
    this.notifications.deleteMany({
      where: {
        createdAt: {
          gt: daysAgo(days),
        },
      },
    });
  }
}

export default NotificationService;
