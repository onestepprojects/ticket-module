import { Settings, Comment, Ticket, TicketChange, TicketStatus } from '@prisma/client';
import { CreateTicketDto, UpdateTicketDto } from '@dtos';
import {
  Debug,
  isEmpty,
  unassignedTicketsQ,
  assignedTicketsQ,
  ticketRelationsQ,
  includeTicketRelationsQ,
  checkUserAccess,
  checkEmpty,
  addToChangelogQ,
  selectChangelogQ,
  checkFindManyParams,
  composeWhereAnd,
  selectWithRelationsQ,
  pointsFromStatus,
  selectTicketQ,
} from '@utils';
import prisma from '../client';
import { PSelectWithRelations, TicketEvent } from '@types';
import {
  AddRemove,
  ChangeLogQuery,
  ChangeLogResponse,
  CommentsResponse,
  DEFAULT_TICKET_REWARD,
  FindManyQuery,
  FindManyTicketsQuery,
  GroupByQuery,
  SelectQuery,
  TicketRelationFieldEnum,
  TicketResponse,
  TicketSelectWithRelations,
  TicketsResponse,
} from '@interfaces';
import { NotificationService } from '@services';
import { HttpException } from '@exceptions';

const debug = Debug('tickets:service');

export class TicketService {
  public tickets = prisma.ticket;
  public users = prisma.user;
  public assignments = prisma.assignment;
  public tags = prisma.tag;
  public changelog = prisma.ticketChangeLog;
  public comments = prisma.comment;
  public notificationService = new NotificationService();

  /**
   * convert tags to lower case in create ticket / update ticket queries
   */
  private processTagsNames<T extends string[] | AddRemove<string>>(tags: T, update?: boolean) {
    if (!tags) return undefined;
    if (update) {
      return {
        add: (tags as AddRemove<string>)?.add?.map(t => t.toLowerCase()) ?? undefined,
        remove: (tags as AddRemove<string>)?.remove?.map(t => t.toLowerCase()) ?? undefined,
      };
    }
    return (tags as string[]).map(t => t.toLowerCase());
  }

  /**
   * Check create ticket data
   * - Downcase all tag names
   * - check visibility requirements
   * - check unsupported
   */
  private checkData<T extends CreateTicketDto | UpdateTicketDto>(ticketData: T, update?: boolean): Omit<T, 'files'> {
    checkEmpty(ticketData, 'Ticket data empty');
    const { // eslint-disable-line
      files,
      physicians,
      educators,
      topics,
      organizationIds,
      visibility,
      ...data
    } = ticketData;

    if (!isEmpty(files)) throw new HttpException(400, "files isn't currently supported");
    if (visibility === 'organization') {
      // TODO: check that organizations are defined if visibility is updated
      // to organization
      if (!update && !(organizationIds as string[])?.length) {
        throw new HttpException(400, "If visibility is 'organization', then organizationIds must be present");
      }
    }
    return {
      ...data,
      organizationIds,
      visibility,
      physicians: this.processTagsNames(physicians, update),
      educators: this.processTagsNames(educators, update),
      topics: this.processTagsNames(topics, update),
    } as T;
  }

  // Make sure user is allowed to modify ticket
  // - update ticket status / delete ticket :: author only
  // - edit ticket :: author / assignees
  private async checkAccess(userId: string, ticketId: number, allowAssigned?: boolean): Promise<Ticket> {
    const ticket = await this.tickets.findUnique({
      where: { id: ticketId },
      include: allowAssigned
        ? {
            assignedTo: { select: { id: true } },
          }
        : undefined,
    });
    checkUserAccess(userId, ticket.userId, ticket.assignedTo);
    return ticket;
  }

  // Convert arrays of ticket relations, { id: x }[], to arrays of ids
  private processResult(res, select?): TicketResponse {
    Object.entries(TicketRelationFieldEnum).forEach(([k, fn]) => {
      if ((!select || select[k]) && res[k]) res[k] = res[k].map(({ id }) => fn(id));
      else if (select) delete res[k];
    });
    return res;
  }

  /**
   * Get tickets
   *
   * Supports all FindManyTicketsQuery parameters.
   * @return number if count, otherwise an Array of Ticket objects
   */
  public async findTickets(params?: FindManyTicketsQuery): Promise<TicketsResponse> {
    if (!isEmpty(params)) debug('find tickets query params:', params);
    const { countParams, ...rest } = checkFindManyParams<Ticket>(params);
    if (countParams) return this.tickets.count(countParams);
    if (!rest.select) rest.select = selectTicketQ as PSelectWithRelations<Ticket>;
    const res = await this.tickets.findMany(rest);
    res.forEach(ticket => this.processResult(ticket));
    return res;
  }

  /**
   * Get ticket by Id
   * @returns Ticket data, including ids of relations
   */
  public async findTicketById(ticketId: number, params: SelectQuery<Ticket, TicketSelectWithRelations>): Promise<Partial<TicketResponse>> {
    if (!isEmpty(params)) debug('find ticket query params:', params);
    const { select } = params;

    const res = select
      ? await this.tickets.findUnique({
          where: { id: ticketId },
          select: selectWithRelationsQ(select, includeTicketRelationsQ),
        })
      : await this.tickets.findUnique({
          where: { id: ticketId },
          include: {
            _count: true,
            ...includeTicketRelationsQ,
          },
        });

    return this.processResult(res, select);
  }

  /**
   * Create new ticket
   *
   * Rewards: new tickets are created with a default number of rewards points,
   * `DEFAULT_TICKET_REWARD`. This value is configurable in
   * `@config/rewards.ts`.
   *
   * Events: users watching organizations + tags (notification types `withTags`,
   * `withOrgs`) will be notified.
   *
   * @return created ticket
   */
  public async createTicket(data: CreateTicketDto, settings?: Settings): Promise<TicketResponse> {
    const cleanData = this.checkData(data, false);
    /* eslint-disable @typescript-eslint/no-unused-vars */
    const { userId, tagIds, organizationIds, linkedTo, educators, physicians, topics, ...scalarData } = cleanData;
    /* eslint-enable @typescript-eslint/no-unused-vars */

    const ticket: Ticket & { [_: string]: any } = await this.tickets.create({
      data: {
        user: { connect: { id: userId } },
        ...scalarData,
        points: DEFAULT_TICKET_REWARD,
        ...ticketRelationsQ(cleanData),
        ...addToChangelogQ(userId, TicketChange.created),
      },
      include: includeTicketRelationsQ,
    });

    // Notify users watching organizations + tags of newly created ticket
    const { tags, ...res } = ticket;
    this.notificationService.notifyForNewTicket({
      ticketId: res.id,
      tags: tags.map(({ id }) => id),
      organizations: organizationIds,
      senderId: userId,
    });

    // Subscribe user to ticket
    if (settings?.subscribeOnCreate) {
      debug(`subscribing ${userId} to ${ticket.id}`);
      await this.users.update({
        where: { id: userId },
        data: {
          subscriptions: { connect: { id: ticket.id } },
        },
      });
    }

    return this.processResult(ticket);
  }

  /**
   * Update ticket
   *
   * Access:
   * - Only ticket creators/assignees can update tickets (anyone else allowed?)
   *
   * Events:
   * - notifications: `ticketUpdate`
   * - changelog: `modified`
   *
   * @return updated ticket
   */
  public async updateTicket(ticketId: number, ticketData: UpdateTicketDto): Promise<TicketResponse> {
    /* eslint-disable @typescript-eslint/no-unused-vars */
    debug(JSON.stringify(ticketData, null, 2));
    const { userId, ...data } = this.checkData(ticketData, /*update=*/ true);
    const { tagIds, organizationIds, linkedTo, educators, physicians, topics, ...scalarData } = data;
    /* eslint-enable @typescript-eslint/no-unused-vars */
    await this.checkAccess(userId, ticketId, /*allowAssigned=*/ true);
    if (data?.linkedTo?.add) data.linkedTo.add = linkedTo.add.filter(id => id !== ticketId);
    debug(JSON.stringify(ticketRelationsQ(data, true), null, 2));

    const res = await this.tickets.update({
      where: { id: ticketId },
      data: {
        ...scalarData,
        ...ticketRelationsQ(data, /*update=*/ true),
        ...addToChangelogQ(userId, TicketChange.modified),
      },
      include: includeTicketRelationsQ,
    });

    this.notificationService.notifyForTicket({
      ticketId,
      event: TicketEvent.ticketUpdate,
      senderId: userId,
    });

    return this.processResult(res);
  }

  /**
   * Update ticket status
   *
   * Access:
   * - Only ticket creator can update ticket status (anyone else allowed?)
   *
   * Events:
   * - notifications: `ticketStatusChange`, `rewards` on change to `completed`/`open`
   * - changelog: `statusChange`
   *
   * @return updated ticket
   */
  public async updateTicketStatus(ticketId: number, userId: string, status: TicketStatus): Promise<TicketResponse> {
    const ticket = await this.checkAccess(userId, ticketId);
    if (ticket.status === status) return ticket;

    const { assignedTo, ...res } = (await this.tickets.update({
      where: { id: ticketId },
      data: {
        status,
        ...addToChangelogQ(userId, TicketChange.statusChange, /*log=*/ status),
      },
      include: includeTicketRelationsQ,
    })) as Ticket & { assignedTo: { id: string }[] };

    // Notify watchers of status change
    // Watchers who are also assigned to ticket receive rewards notifications
    // if status changed to completed or was reopened after being marked completed
    this.notificationService.notifyForTicket(
      {
        ticketId,
        event: TicketEvent.ticketStatusChange,
        senderId: userId,
        points: pointsFromStatus(res.points, /*prevStatus=*/ ticket.status, status),
        status,
      },
      assignedTo,
    );

    return this.processResult(res);
  }

  public async deleteTicket(ticketId: number, userId: string): Promise<TicketResponse> {
    await this.checkAccess(userId, ticketId);
    return this.tickets.delete({ where: { id: ticketId } });
  }

  /**
   * Aggregated ticket queries using Prisma.groupBy
   *
   * Supported parameters:
   * - all GroupByQuery parameters
   *
   * @return Promise of aggregates
   */
  public async groupByTickets(params: GroupByQuery<Ticket>): Promise<any> {
    return this.tickets.groupBy(params);
  }

  /**
   * Make queries against tickets grouped by assigned/unassigned
   *
   * Supported parameters:
   * - count	return counts of assigned/unassigned
   *
   * @return Promise of counts if count, otherwise tickets by assigned
   */
  public async ticketsByAssigned(params): Promise<any> {
    const { count } = params;
    const unassigned = await (count ? this.tickets.count({ where: unassignedTicketsQ }) : this.tickets.findMany({ where: unassignedTicketsQ }));
    const assigned = await (count ? this.tickets.count({ where: assignedTicketsQ }) : this.tickets.findMany({ where: assignedTicketsQ }));
    return { assigned, unassigned };
  }

  /**
   * Comments on ticket
   * @returns {CommentsResponse[]} Array of Comment objects
   */
  public async ticketComments(ticketId: number, params: FindManyQuery<Comment>): Promise<CommentsResponse> {
    if (!isEmpty(params)) debug('comments query params:', params);
    const { countParams, ...rest } = checkFindManyParams<Comment>(composeWhereAnd<Comment>({ ticketId }, params));
    if (countParams) return this.comments.count(countParams);
    return this.comments.findMany(rest);
  }

  /**
   * Changelog for ticket
   *
   * By default, results are ordered by creation date, ascending.
   *
   * @param {ChangeLogQuery} params - Parsed query parameters
   * @returns {ChangeLogResponse[]} Array of changelog events or count
   */
  public async ticketChangelog(params: ChangeLogQuery): Promise<ChangeLogResponse> {
    if (!isEmpty(params)) debug('changelog query params:', params);
    const { count, orderBy = { createdAt: 'asc' }, where, ...paginate } = params;

    if (count) return this.changelog.count({ ...paginate, where, orderBy });

    const res = await this.changelog.findMany({
      ...paginate,
      where,
      select: selectChangelogQ,
      orderBy,
    });

    return res.map(x => ({ ...x, user: x.user.name }));
  }
}

export default TicketService;
