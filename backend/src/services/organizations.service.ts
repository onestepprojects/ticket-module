import { Organization, Ticket, User } from '@prisma/client';
import { findManyTicketsFor, findManyWatchersFor } from '@utils';
import prisma from '../client';
import { FindManyQuery, TicketsResponse, UsersResponse } from '@/interfaces';

export class OrganizationsService {
  public orgs = prisma.organization;
  public tickets = prisma.ticket;
  public users = prisma.user;

  public async findTicketsForOrg(orgId: string, params: FindManyQuery<Ticket>): Promise<TicketsResponse> {
    return findManyTicketsFor<Organization>(this.orgs, orgId, params);
  }

  public async findWatchersForOrg(orgId: string, params: FindManyQuery<User>): Promise<UsersResponse> {
    return findManyWatchersFor<Organization>(this.orgs, orgId, params);
  }
}

export default OrganizationsService;
