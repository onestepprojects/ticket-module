import { Comment } from '@prisma/client';
import { CreateCommentDto } from '@dtos';
import { HttpException } from '@exceptions';
import prisma from '../client';
import { TicketEvent } from '@/types';
import { CommentResponse, CommentsResponse, FindManyQuery } from '@/interfaces';
import { NotificationService } from '@services';
import { truncate } from 'lodash';

export class CommentService {
  public comments = prisma.comment;
  public tickets = prisma.ticket;
  public users = prisma.user;
  public notificationService = new NotificationService();

  private async checkAccess(userId: string, commentId: number): Promise<Comment> {
    const comment: Comment = await this.comments.findUnique({ where: { id: commentId } });
    if (!comment) throw new HttpException(409, 'Comment not found');
    if (comment.userId !== userId) throw new HttpException(401, 'Unauthorized data access');
    return comment;
  }

  public async findComments(params: FindManyQuery<Comment> | null): Promise<CommentsResponse> {
    const { count, select } = params;
    if (count !== undefined) return this.comments.count();
    return this.comments.findMany({ select });
  }

  public async findCommentById(commentId: number): Promise<CommentResponse> {
    return this.comments.findUnique({ where: { id: commentId } });
  }

  /**
   * Create comment on ticket
   * - notifications: ticketComment
   */
  public async createComment(commentData: CreateCommentDto): Promise<CommentResponse> {
    const { userId, ticketId, ...data } = commentData;
    const res = await this.comments.create({
      data: {
        ...data,
        user: { connect: { id: userId } },
        ticket: { connect: { id: Number(ticketId) } },
      },
    });
    this.notificationService.notifyForTicket({
      ticketId,
      event: TicketEvent.ticketComment,
      message: truncate(data.comment, { length: 30 }),
      senderId: userId,
    });
    return res;
  }

  public async updateComment(commentId: number, commentData: CreateCommentDto): Promise<CommentResponse> {
    // eslint-disable-next-line
    const { userId, ticketId, ...data } = commentData;
    await this.checkAccess(userId, commentId);
    return this.comments.update({
      where: { id: commentId },
      data: { ...data },
    });
  }

  public async deleteComment(commentId: number, userId: string): Promise<CommentResponse> {
    await this.checkAccess(userId, commentId);
    return this.comments.delete({ where: { id: commentId } });
  }
}

export default CommentService;
