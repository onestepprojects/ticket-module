import { Assignment, Ticket } from '@prisma/client';
import { CreateAssignmentDto } from '@dtos';
import { HttpException } from '@exceptions';
import { isEmpty } from '@utils';
import prisma from '../client';

type AssignmentData = CreateAssignmentDto;

export class AssignmentService {
  public assignments = prisma.assignment;
  public tickets = prisma.ticket;
  public users = prisma.user;

  private async checkData(assignmentData: CreateAssignmentDto): Promise<AssignmentData> {
    if (isEmpty(assignmentData)) throw new HttpException(400, 'Bad assignmentData');
    const { ticketId } = assignmentData;
    const findTicket: Ticket = await this.tickets.findUnique({ where: { id: ticketId } });
    if (!findTicket) throw new HttpException(409, `Ticket must exist`);
    // XXX: could check users exist/aren't assigned to same ticket multiple times?
    return assignmentData;
  }

  public async findAssignments(): Promise<Assignment[]> {
    return this.assignments.findMany();
  }

  public async findAssignmentById(assignmentId: number): Promise<Assignment> {
    if (isEmpty(assignmentId)) throw new HttpException(400, 'Bad assignmentId');
    return this.assignments.findUnique({ where: { id: assignmentId } });
  }

  public async createAssignment(assignmentData: CreateAssignmentDto): Promise<Assignment> {
    const { assignedTo, assignedBy, ticketId, ...createData } = await this.checkData(assignmentData);
    return this.assignments.create({
      data: {
        ...createData,
        assignedTo: { connect: assignedTo.map(id => ({ id })) },
        assignedBy: { connect: { id: assignedBy } },
        ticket: { connect: { id: ticketId } },
      },
    });
  }

  public async updateAssignment(assignmentId: number, assignmentData: CreateAssignmentDto): Promise<Assignment> {
    const { assignedTo, assignedBy, ticketId, ...data } = assignmentData;
    return this.assignments.update({
      where: { id: assignmentId },
      data: {
        ...data,
        assignedTo: { connect: assignedTo.map(id => ({ id })) },
        assignedBy: { connect: { id: assignedBy } },
        ticket: { connect: { id: ticketId } },
      },
    });
  }

  public async deleteAssignment(assignmentId: number): Promise<Assignment> {
    return this.assignments.delete({ where: { id: assignmentId } });
  }
}
export default AssignmentService;
