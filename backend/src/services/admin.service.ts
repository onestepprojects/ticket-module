import prisma from '../client';
import { Debug } from '@utils';
import { groupBy } from 'lodash';

const debug = Debug('admin:service');

export class AdminService {
  public tickets = prisma.ticket;
  public users = prisma.user;
  public assignments = prisma.assignment;
  public tags = prisma.tag;

  /**
   * Get SQL table names from database
   * @return Object with pivots table and model table names
   */
  public async getSQLTables(): Promise<{ pivots: string[]; models: string[] }> {
    const tables: { tablename: string }[] = await prisma.$queryRaw`
select tablename from pg_tables
where (schemaname = 'public')`;
    const res = groupBy(
      tables.map(t => t.tablename).filter(t => t !== '_prisma_migrations'),
      x => x.startsWith('_'),
    );
    return {
      pivots: res['true'],
      models: res['false'],
    };
  }

  /**
   * Raw SQL Query (unsafe)
   *
   * @return any result from raw SQL query
   */
  public async rawQuery(sql: string): Promise<any> {
    debug(`Raw query: ${sql}`);
    return prisma.$queryRawUnsafe(sql);
  }
}

export default AdminService;
