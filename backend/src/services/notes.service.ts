import { Note } from '@prisma/client';
import { CreateNoteDto } from '@dtos';
import { Debug, checkEmpty, checkUserAccess, composeWhereAnd, isEmpty, checkFindManyParams } from '@utils';
import prisma from '../client';
import { FindManyQuery, NoteResponse, NotesResponse } from '@interfaces';

const debug = Debug('notes');

export class NoteService {
  public notes = prisma.note;
  public users = prisma.user;

  private async checkAccess(userId: string, noteId: number): Promise<Note> {
    const note: Note = await this.notes.findUnique({ where: { id: noteId } });
    checkUserAccess(userId, note.userId);
    return note;
  }

  public async findNotes(userId: string, params: FindManyQuery<Note> | null): Promise<NotesResponse> {
    if (!isEmpty(params)) debug(params);
    const { countParams, ...rest } = checkFindManyParams<Note>(composeWhereAnd<Note>({ userId }, params));
    if (countParams) return this.notes.count(countParams);
    return this.notes.findMany(rest);
  }

  public async findNoteById(noteId: number, userId: string): Promise<NoteResponse> {
    const note = await this.notes.findUnique({ where: { id: noteId } });
    checkUserAccess(userId, note.userId);
    return note;
  }

  public async createNote(noteData: CreateNoteDto): Promise<NoteResponse> {
    const { ticketId, userId, ...data } = noteData;
    return this.notes.create({
      data: {
        ...data,
        user: { connect: { id: userId } },
        ticket: { connect: { id: ticketId } },
      },
    });
  }

  public async updateNote(noteId: number, noteData: CreateNoteDto): Promise<NoteResponse> {
    checkEmpty(noteData);
    await this.checkAccess(noteData.userId, noteId);
    return this.notes.update({
      where: { id: noteId },
      data: { ...noteData },
    });
  }

  public async deleteNote(noteId: number, userId: string): Promise<NoteResponse> {
    await this.checkAccess(userId, noteId);
    return this.notes.delete({ where: { id: noteId } });
  }
}

export default NoteService;
