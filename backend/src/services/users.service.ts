import { hash } from 'bcrypt';
import { User, Ticket, Comment, Settings } from '@prisma/client';
import { CreateSettingsDto, CreateUserDto, UpdateWatchDto } from '@dtos';
import { HttpException } from '@exceptions';
import {
  Debug,
  completedTicketsQ,
  isEmpty,
  selectWatchingQ,
  expandSettings,
  unexpandSettings,
  selectNotificationQ,
  updateMarkedQ,
  sinceDaysAgoQ,
  includeUserRelationsQ,
  checkEmpty,
  selectHistoryQ,
  checkFindManyParams,
  userSelectDefault,
  selectWithRelationsQ,
} from '@utils';
import prisma from '../client';
import { TicketEvent } from '@types';
import {
  SimpleAssignResponse,
  CommentsResponse,
  TicketsResponse,
  WatchingResponse,
  SettingsResponse,
  SubscriptionResponse,
  NotificationsResponse,
  MarkNotificationRequestBody,
  FindManyNotificationsQuery,
  SelectManyQuery,
  FindManyQuery,
  ChangeLogQuery,
  HistoryResponse,
  UserResponse,
  UsersResponse,
  SelectQuery,
  UserSelectWithRelations,
} from '@interfaces';
import { NotificationService } from '@services';

const debug = Debug('users:service');

export class UserService {
  public users = prisma.user;
  public tickets = prisma.ticket;
  public settings = prisma.settings;
  public notifsOn = prisma.notificationsOnReceivers;
  public notifs = prisma.notification;
  public changelog = prisma.ticketChangeLog;
  public comments = prisma.comment;
  public notificationService = new NotificationService();

  /**
   * Query users
   * @return Array of user objects or count
   */
  public async findUsers(params: FindManyQuery<User>): Promise<UsersResponse> {
    if (!isEmpty(params)) debug(params);
    const { countParams, ...rest } = checkFindManyParams<User>(params);
    if (countParams) return this.users.count(countParams);
    return this.users.findMany(rest);
  }

  /**
   * Get User by Id
   * Returns user relations by default
   */
  public async findUserById(userId: string, params: SelectQuery<User, UserSelectWithRelations>): Promise<Partial<UserResponse>> {
    checkEmpty(userId, 'no userId');
    const { select } = params;
    return this.users.findUnique({
      where: { id: userId },
      select: selectWithRelationsQ(select, includeUserRelationsQ) || {
        id: true,
        ...includeUserRelationsQ,
      },
    });
  }

  // eslint-disable-next-line
  public async findUserOrCreate({ roles, ...userData }: CreateUserDto): Promise<UserResponse> {
    checkEmpty(userData, 'Empty user data');
    const { id } = userData;
    const findUser: UserResponse = await this.users.upsert({
      where: { id },
      create: { ...userData },
      update: {},
      select: userSelectDefault,
    });
    if (!findUser) throw new HttpException(409, `Invalid user data`);

    return findUser;
  }

  // eslint-disable-next-line
  public async createUser({ roles, ...userData }: CreateUserDto): Promise<UserResponse> {
    checkEmpty(userData, 'Empty user data');
    try {
      const findUser: User = await this.users.findUnique({ where: { id: userData.id } });
      if (findUser) throw new HttpException(409, `User already already exists`);
    } catch {} // ignore error if not found
    // const isAdmin = roles.includes('admin');
    const hashedPassword = await hash(userData.password, 10);
    const createUserData: UserResponse = await this.users.create({
      data: { ...userData, password: hashedPassword },
      select: userSelectDefault,
    });
    return createUserData;
  }

  public async updateUser(userId: string, userData: CreateUserDto): Promise<UserResponse> {
    checkEmpty(userData, 'Empty user data');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "You're not user");

    const hashedPassword = await hash(userData.password, 10);
    const updateUserData = await this.users.update({
      where: { id: userId },
      data: { ...userData, password: hashedPassword },
      select: userSelectDefault,
    });
    return updateUserData;
  }

  public async deleteUser(userId: string): Promise<UserResponse> {
    checkEmpty(userId, 'Missing userId');

    const findUser: User = await this.users.findUnique({ where: { id: userId } });
    if (!findUser) throw new HttpException(409, "You're not user");

    const deleteUserData = await this.users.delete({
      where: { id: userId },
      select: userSelectDefault,
    });
    return deleteUserData;
  }

  /**
   * Created Tickets
   *
   * @return Promise array of tickets created by user
   */
  public async ticketsCreatedByUser(userId: string, params: SelectManyQuery<Ticket>): Promise<TicketsResponse> {
    const { count, select } = params;
    const res = await this.users.findUnique({ where: { id: userId } }).ticketsBy({ select });
    return count ? res.length : res;
  }

  /**
   * Completed Tickets
   *
   * @return Promise array of tickets completed by user
   */
  public async ticketsCompletedByUser(userId: string, params: SelectManyQuery<Ticket>): Promise<TicketsResponse> {
    const { count, select } = params;
    const res = await this.users.findUnique({ where: { id: userId } }).ticketsFor({ where: completedTicketsQ, select });
    return count ? res.length : res;
  }

  // Other people that this users has assigned to tickets
  public async usersAssignedByUser(userId: string): Promise<UsersResponse> {
    const users = await this.users
      .findUnique({
        where: { id: userId },
      })
      .assignmentsBy({
        include: {
          assignedTo: {
            select: userSelectDefault,
          },
        },
      });
    return users.reduce((acc, user) => acc.concat(user.assignedTo), []);
  }

  /**
   * Simple Assignment - user un/assigning themselves to tickets, doesn't track
   * who assigned who
   * - notifications: ticketAssignment
   *
   * @return Promise of ticket Ids users is assigned to
   */
  public async assignTickets(userId: string, ticketIds: number[], settings: Settings): Promise<SimpleAssignResponse> {
    const assns = await this.users.update({
      where: { id: userId },
      data: {
        ticketsFor: { connect: ticketIds.map(id => ({ id })) },
        subscriptions: settings?.subscribeOnAssign
          ? {
              connect: ticketIds.map(id => ({ id })),
            }
          : undefined,
      },
      select: { ticketsFor: { select: { id: true } } },
    });
    ticketIds.forEach(id =>
      this.notificationService.notifyForTicket({
        ticketId: id,
        event: TicketEvent.ticketAssignment,
        message: 'assign',
        senderId: userId,
      }),
    );
    return { ticketIds: assns.ticketsFor.map(({ id }) => id) };
  }

  /**
   * Unassign user from ticket(s)
   * - notifications: ticketAssignment
   */
  public async unassignTickets(userId: string, ticketIds: number[], settings: Settings): Promise<SimpleAssignResponse> {
    // unsubscribe users according to settings & if isn't a ticket they created
    let unsubIds: { id: number }[] = undefined;
    if (settings?.subscribeOnAssign) {
      unsubIds = await this.tickets.findMany({
        where: { NOT: { userId }, id: { in: ticketIds } },
        select: { id: true },
      });
    }
    if (unsubIds) debug(`unsubscribing ${userId} from ${unsubIds}`);

    const assns = await this.users.update({
      where: { id: userId },
      data: {
        ticketsFor: { disconnect: ticketIds.map(id => ({ id })) },
        subscriptions: unsubIds ? { disconnect: unsubIds } : undefined,
      },
      select: { ticketsFor: { select: { id: true } } },
    });

    ticketIds.forEach(id =>
      this.notificationService.notifyForTicket({
        ticketId: id,
        event: TicketEvent.ticketAssignment,
        message: 'unassign',
        senderId: userId,
      }),
    );

    return { ticketIds: assns.ticketsFor.map(({ id }) => id) };
  }

  /**
   * Subscriptions
   *
   * @return Promise array of ticket Ids user is subscribed to
   */
  public async subscribeTickets(userId: string, ticketIds: number[]): Promise<SubscriptionResponse> {
    const subs = await this.users.update({
      where: { id: userId },
      data: { subscriptions: { connect: ticketIds.map(id => ({ id })) } },
      select: { subscriptions: { select: { id: true } } },
    });
    return { ticketIds: subs.subscriptions.map(({ id }) => id) };
  }

  public async unsubscribeTickets(userId: string, ticketIds: number[]): Promise<SubscriptionResponse> {
    const subs = await this.users.update({
      where: { id: userId },
      data: { subscriptions: { disconnect: ticketIds.map(id => ({ id })) } },
      select: { subscriptions: { select: { id: true } } },
    });
    return { ticketIds: subs.subscriptions.map(({ id }) => id) };
  }

  public async ticketsSubscribedTo(userId: string): Promise<SubscriptionResponse> {
    const { subscriptions } = await this.users.findUnique({
      where: { id: userId },
      select: { subscriptions: { select: { id: true } } },
    });
    return { ticketIds: subscriptions.map(({ id }) => id) };
  }

  /**
   * Comments
   *
   * @return Promise array of user's comments
   */
  public async userComments(userId: string, params?: FindManyQuery<Comment>): Promise<CommentsResponse> {
    if (!isEmpty(params)) debug(params);
    params.where = params.where ? { AND: { ...params.where, userId } } : { userId };
    const { countParams, ...rest } = checkFindManyParams<Comment>(params);
    if (countParams) return this.comments.count(countParams);
    return this.comments.findMany(rest);
  }

  /**
   * Watched orgs/tags
   *
   * @return Promise array of watched orgs/tags
   */
  private watchingToResponse = (data: { watchedOrgs: { id: string }[]; watchedTags: { id: number }[] }): WatchingResponse => {
    return {
      orgIds: data.watchedOrgs.map(({ id }) => id),
      tagIds: data.watchedTags.map(({ id }) => id),
    };
  };

  public async userWatching(userId: string): Promise<WatchingResponse> {
    const res = await this.users.findUnique({
      where: { id: userId },
      select: selectWatchingQ,
    });
    return this.watchingToResponse(res);
  }

  public async updateWatching(userId: string, data: UpdateWatchDto): Promise<WatchingResponse> {
    const { orgIds = {}, tagIds = {} } = data;

    const res = await this.users.update({
      where: { id: userId },
      data: {
        watchedOrgs: {
          disconnect: orgIds?.remove?.map(id => ({ id })),
          connect: orgIds?.add?.map(id => ({ id })),
        },
        watchedTags: {
          disconnect: tagIds?.remove?.map(id => ({ id })),
          connect: tagIds?.add?.map(id => ({ id })),
        },
      },
      select: selectWatchingQ,
    });
    return this.watchingToResponse(res);
  }

  /**
   * Settings
   * @return User settings object
   */
  public async getSettings(userId: string): Promise<SettingsResponse> {
    const user = await this.users.findUnique({
      where: { id: userId },
      select: { settings: {} },
    });
    return expandSettings(user.settings);
  }

  public async updateSettings(userId: string, settings: CreateSettingsDto): Promise<SettingsResponse> {
    const userSettings = await this.settings.update({
      where: { userId },
      data: unexpandSettings(settings),
    });
    return expandSettings(userSettings);
  }

  /**
   * Notifications
   *
   * By default returns notifications from the last 30 days, sorted
   * ascending by created time, most recent first.
   *
   * @return array of Notifications or count
   */
  public async getNotifications(userId: string, params: FindManyNotificationsQuery): Promise<NotificationsResponse> {
    const { count, orderBy, read, days = 30, take, skip } = params;

    if (count) {
      return this.notifsOn.count({
        where: { receiverId: userId, read, notification: sinceDaysAgoQ(days) },
        take,
        skip,
      });
    }

    const notifs = await this.notifsOn.findMany({
      where: {
        notification: sinceDaysAgoQ(days),
        receiverId: userId,
        read,
      },
      orderBy: { notification: orderBy || { updatedAt: 'desc' } },
      select: selectNotificationQ,
      take,
      skip,
    });

    return notifs.map(({ read, notification }) => {
      const { tags = [], organizations = [], ...fields } = notification;
      return {
        read,
        tags: tags?.map(({ id }) => id),
        organizations: organizations?.map(({ id }) => id),
        ...fields,
      };
    });
  }

  /**
   * Delete all read notifications for user
   */
  public async deleteReadNotifications(userId: string): Promise<NotificationsResponse> {
    const res = await this.notifsOn.deleteMany({
      where: { read: true, receiverId: userId },
    });
    return res.count;
  }

  /**
   * Toggle read status of notifications
   * @return updated notifications
   */
  public async markNotifications(userId: string, body: MarkNotificationRequestBody): Promise<NotificationsResponse> {
    checkEmpty(body, 'Mark body is empty');
    const { read, unread } = body;

    if ((read === 'all' && !isEmpty(unread)) || (unread === 'all' && !isEmpty(read)))
      throw new HttpException(400, 'If one of read/unread is "all", the other should be empty');

    if (!isEmpty(read)) await this.notifsOn.updateMany({ ...updateMarkedQ(userId, read), data: { read: true } });

    if (!isEmpty(unread)) await this.notifsOn.updateMany({ ...updateMarkedQ(userId, unread), data: { read: false } });

    return this.getNotifications(userId, {});
  }

  /**
   * Get user history/activity
   * @returns Array of changelog events instigated by user, or count
   */
  public async getHistory(params: ChangeLogQuery): Promise<HistoryResponse> {
    const { count, orderBy = { createdAt: 'asc' }, where, ...paginate } = params;

    if (count) return this.changelog.count({ ...paginate, where, orderBy });

    return this.changelog.findMany({
      ...paginate,
      where,
      select: selectHistoryQ,
      orderBy,
    });
  }
}
export default UserService;
