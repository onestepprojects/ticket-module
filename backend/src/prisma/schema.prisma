///
// Markup format:
//
// Any comments starting with '///' with be used as description
// for a field in swagger docs.
///
generator client {
  provider        = "prisma-client-js"
  previewFeatures = ["fullTextSearch"]
}

generator jsonSchema {
  provider                 = "prisma-json-schema-generator"
  keepRelationScalarFields = true
}

// Note: seems to have problems on WSL (4/24/22)
// generator erd {
//   provider = "prisma-erd-generator"
// }

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

/// Internal notes about tickets (private to user)
model Note {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  note      String
  /// Note creator's Id
  userId    String
  user      User     @relation(fields: [userId], references: [id])
  /// Ticket id
  ticket    Ticket   @relation(fields: [ticketId], references: [id])
  ticketId  Int
}

// Note: changes to these enums should be reflected in frontend/src/types/Ticket.ts
enum TicketStatus {
  open /// Active ticket: either assigned or unassigned (default)
  closed /// Closed without completion
  completed /// Closed and completed
}

enum TicketPriority {
  low
  normal /// default
  high
}

enum TicketVisibility {
  organization /// Limited to organization(s) (default)
  public /// Anyone can see
  private /// Limited to individual(s)
  restricted /// TODO: etc.
}

/// Ticket changelog events
enum TicketChange {
  created /// ticket created
  modified /// ticket title/details modified
  statusChange /// ticket status changed
}

/// Simple implementation for immutable ticket changelog
model TicketChangeLog {
  id        Int          @id @default(autoincrement())
  createdAt DateTime     @default(now())
  type      TicketChange /// changes applied
  log       String? /// Log with details, if any, about what happened
  ticketId  Int /// Id of ticket changed
  ticket    Ticket       @relation(fields: [ticketId], references: [id], onDelete: Cascade)
  userId    String /// Id of User making changes
  user      User         @relation(fields: [userId], references: [id])
}

// Triggers notification events:
// - on status/priority/visibility change
// - on content (title, detail) change
model Ticket {
  id            Int               @id @default(autoincrement())
  /// Date-time when ticket was created
  createdAt     DateTime          @default(now())
  /// Date-time when ticket was last updated
  updatedAt     DateTime          @updatedAt
  /// Ticket title
  title         String
  /// Ticket description/details
  detail        String
  /// Ticket status
  status        TicketStatus      @default(open)
  /// Ticket priority
  priority      TicketPriority    @default(normal)
  /// Ticket visibility (TODO: determine possible visibilities)
  visibility    TicketVisibility  @default(public)
  /// UserId who created ticket
  userId        String
  /// Historical changes to ticket
  changelog     TicketChangeLog[]
  /// Referenced User who created ticket
  user          User              @relation(fields: [userId], references: [id])
  /// Assignments for ticket
  assignments   Assignment[]
  /// Users ticket is assigned to
  assignedTo    User[]            @relation("TicketsFor")
  /// Files attached to ticket
  files         TicketFile[]
  /// notifications referencing ticket
  notifications Notification[]
  /// tags referencing ticket
  tags          Tag[]
  /// users subscribed to ticket
  subscribers   User[]            @relation("TicketSubscription")
  /// organizations ticket is linked to
  organizations Organization[]
  /// Linked/related tickets
  linkedTo      Ticket[]          @relation("TicketToTicket")
  linkedBy      Ticket[]          @relation("TicketToTicket")
  /// Users who have contributed to ticket
  contributors  User[]            @relation("TicketContribution")
  /// Comments on ticket
  comments      Comment[]
  /// Personal notes on ticket
  notes         Note[]
  /// Reward points for resolving ticket
  points        Int               @default(0)
  /// Referral url, eg. a link to the health case referencing ticket
  referralUrl   String?
}

model Comment {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  user      User     @relation(fields: [userId], references: [id], onDelete: Cascade, onUpdate: Cascade)
  ticket    Ticket   @relation(fields: [ticketId], references: [id], onDelete: Cascade, onUpdate: Cascade)
  userId    String
  ticketId  Int
  comment   String
}

enum TagType {
  topic /// Normal tag: just a category name
  educator /// Designates an educator speciality, eg. English, Biology
  physician /// Designates a physician speciality, eg. Cardiology, Psychiatry
}

// Notification events:
// - ticketCreate: if ticket is tagged with requested specialities, notify users
//   watching the tag who have those specialities
model Tag {
  id            Int            @id @default(autoincrement())
  createdAt     DateTime       @default(now())
  updatedAt     DateTime       @updatedAt
  name          String /// Tag name
  type          TagType /// Tag type
  tickets       Ticket[] /// Relation to tagged Tickets
  watchers      User[] /// Users watching tag
  notifications Notification[] /// notifications referencing tag

  @@unique(fields: [name, type], name: "name_type")
}

model TicketFile {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  filename  String
  path      String /// Path to access file
  ticketId  Int
  ticket    Ticket   @relation(fields: [ticketId], references: [id], onDelete: Cascade, onUpdate: Cascade)
}

enum RoleType {
  admin
}

model UserRole {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  type      RoleType
  userId    String
  user      User     @relation(fields: [userId], references: [id], onDelete: Cascade, onUpdate: Cascade)
}

model User {
  /// Mirrors Person.uuid from AWS
  id               String                     @id
  /// user creation date
  createdAt        DateTime                   @default(now())
  /// Mirrors Person.name from AWS
  name             String?
  /// TODO: remove this
  password         String?
  /// Mirrors Person.email from AWS (some are null on AWS)
  email            String?
  /// User has admin access to database
  isAdmin          Boolean                    @default(false)
  /// Tickets created by user
  ticketsBy        Ticket[]
  /// Tickets assigned to user
  ticketsFor       Ticket[]                   @relation("TicketsFor")
  /// Assignments for user, eg. assigned by other uesr
  assignmentsFor   Assignment[]               @relation("AssignmentsFor")
  /// Assignments created by user, eg. users assigned by user
  assignmentsBy    Assignment[]
  /// Notes created by user
  notes            Note[]
  /// Files uploaded by user
  files            UserFile[]
  /// Roles associated with user
  roles            UserRole[]
  /// Notifications for user
  notificationsFor NotificationsOnReceivers[]
  /// Notifications created by user
  notificationsBy  Notification[]
  /// Tickets subscribed to
  subscriptions    Ticket[]                   @relation("TicketSubscription")
  /// Organizations watched by user
  watchedOrgs      Organization[]
  /// Tags watched by user
  watchedTags      Tag[]
  /// User's ticket activity
  activity         TicketChangeLog[]
  /// Tickets contributed to by user
  contributedTo    Ticket[]                   @relation("TicketContribution")
  /// Comments
  comments         Comment[]
  settings         Settings?
}

/// Type of ticket subscription
// enum SubscriptionType {
//   manual /// User manually subscribed to ticket
//   created /// User was subscribed when they created ticket
//   assigned /// User was subscribed when they were assigned to ticket
// }

// model TicketsOnSubscribers {
//   id           Int              @id @default(autoincrement())
//   /// Type of subscription
//   type         SubscriptionType
//   subscriber   User             @relation(fields: [subscriberId], references: [id], onDelete: Cascade, onUpdate: Cascade)
//   subscriberId String
//   ticket       Ticket           @relation(fields: [ticketId], references: [id], onDelete: Cascade, onUpdate: Cascade)
//   ticketId     Int

//   @@unique([subscriberId, ticketId])
// }

// Assignments track when a user assigns other users to a ticket
// only necessary if wanting to keep track of who assigned who
model Assignment {
  id           Int      @id @default(autoincrement())
  createdAt    DateTime @default(now())
  updatedAt    DateTime @updatedAt
  /// Users assigned to ticket
  assignedTo   User[]   @relation("AssignmentsFor")
  /// Assignment creator
  assignedBy   User     @relation(fields: [assignedById], references: [id])
  /// Assignment creator Id
  assignedById String
  /// Id of assigned ticket
  ticketId     Int
  ticket       Ticket   @relation(fields: [ticketId], references: [id], onDelete: Cascade, onUpdate: Cascade)

  @@unique([assignedById, ticketId])
}

model UserFile {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  filename  String
  path      String
  userId    String
  user      User     @relation(fields: [userId], references: [id], onDelete: Cascade, onUpdate: Cascade)
}

enum WebhookType {
  ticketCreate
  ticketStatusChange
}

// Something like this could potentially be used to link additional
// push services, eg. email notifications
model Webhook {
  id        Int         @id @default(autoincrement())
  createdAt DateTime    @default(now())
  updatedAt DateTime    @updatedAt
  name      String
  url       String?
  email     String?
  type      WebhookType
  active    Boolean     @default(false)
  secret    String?
  createdBy String
}

model Organization {
  /// uuid mapping Organization to AWS database
  id            String         @id
  /// mirror AWS organization name
  name          String?
  /// Users watching organization
  watchers      User[]
  /// Organization's tickets
  tickets       Ticket[]
  /// Notifications referencing organization
  notifications Notification[]
}

// -------------------------------------------------------------------
/// Notifications

enum NotificationType {
  ticketCreate /// new ticket: 'with(Tags|Orgs|Roles)' notification types are subtypes of ticketCreate
  ticketUpdate /// ticket modified
  ticketAssignment /// ticket un/assigned
  ticketStatusChange /// ticket status change
  ticketComment /// comment on ticket
  withTags /// ticket created with tags
  withOrgs /// ticket created with orgs
  rewards /// Rewards given to/taken from users when ticket is completed/re-opened
  withRoles /// (unused) ticket created with requested roles
  message /// (unused) simple message
  user /// (unused) eg. mentor/mentee
  organization /// (unused) organization related
}

model Notification {
  id            Int                        @id @default(autoincrement())
  createdAt     DateTime                   @default(now())
  updatedAt     DateTime                   @updatedAt
  /// Message displayed to receivers of notification
  message       String
  /// Notification type
  type          NotificationType
  /// Relation to notified users
  receivers     NotificationsOnReceivers[]
  /// Ticket referenced by notification
  ticket        Ticket?                    @relation(fields: [ticketId], references: [id])
  /// Id of ticket referenced by notification
  ticketId      Int?
  /// User who created/triggered notification
  sender        User?                      @relation(fields: [senderId], references: [id])
  /// Id of user who created/triggered notification
  senderId      String?
  /// tag Ids referenced by notification
  tags          Tag[]
  /// Organization Ids referenced by notification
  organizations Organization[]
  /// Points given to/taken from receievers (type == rewards)
  points        Int?
  /// New ticket status (type == ticketStatusChange)
  status        TicketStatus?
}

// Pivot table for notification messages to receivers
// On create: make one Notification and a NotificationsOnReceivers linking
// it to every receiver
model NotificationsOnReceivers {
  id             Int          @id @default(autoincrement())
  /// Whether this notification has been read by receiver
  read           Boolean      @default(false)
  notification   Notification @relation(fields: [notificationId], references: [id], onDelete: Cascade, onUpdate: Cascade)
  notificationId Int
  receiver       User         @relation(fields: [receiverId], references: [id], onDelete: Cascade)
  receiverId     String

  @@unique([notificationId, receiverId])
}

// -------------------------------------------------------------------
/// User Settings/Preferences
// Notification settings are stored as bitset

// Ticket notification events:
// - update :: when ticket details are modified
// - assignment :: when ticket is un/assigned
// - status change :: when ticket status changes: open<->closed<->completed
// - comment :: when comments made on ticket
// New ticket notification events:
// - with tags :: when ticket is created with watched tag
// - with orgs :: when ticket is created from watched org
// - with roles :: when ticket is created requesting watched role
model Settings {
  id                Int     @id @default(autoincrement())
  userId            String  @unique
  user              User    @relation(fields: [userId], references: [id])
  /// Automatically subscribe to created tickets
  subscribeOnCreate Boolean @default(true)
  /// Automatically subscribe to/unsubscribe from assigned/unassigned tickets
  subscribeOnAssign Boolean @default(true)
  /// Notification settings for subscribed tickets (default all)
  onSubscribed      Int     @default(15)
  /// Notification settings for new tickets (default all)
  onWatched         Int     @default(7)
}
