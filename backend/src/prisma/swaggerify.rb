#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'yaml'

# Additional json-schema metadata to merge into schema
def composite_keys(a, b)
  {'properties' => {"#{a}_#{b}" => {'type' => 'integer', 'readOnly' => true}}}
end

EXTRA = {
  'definitions' => {
    'User' => {
      'required' => ['id'],
      'properties' => {
        'id' => { 'format' => 'uuid'  },
        'email' => { 'format' => 'email' }
      },
    },
    'Notification' => {
      'required' => ['receivers'],
      'properties' => {
        'receivers' => {
          'items' => { 'type' => 'string', 'format' => 'uuid' },
        }
      }
    },
    # 'Assignmnet' => {
    #   'required' => ['assignedTo'],
    #   'properties' => {
    #     'assignedTo' => {
    #       'items' => { 'type' => 'string', 'format' => 'uuid' },
    #     }
    #   }
    # },
  }
}
[['NotificationsOnReceivers', 'notificationId', 'receiverId']]
  .each { |m,a,b| EXTRA['definitions'][m] = composite_keys(a,b) }


YAML_SCHEMA = File.expand_path('schema.yaml', __dir__)
JSON_SCHEMA = File.expand_path('json-schema/json-schema.json', __dir__)

class Hash
  def deep_merge!(other)
    other.keys.each do |k| 
      if !self.key? k
        self[k] = other[k]
      elsif self[k].is_a? Hash
        self[k].deep_merge! other[k]
      elsif self[k].is_a? Array
        self[k] << other[k]
      else
        self[k] = other[k]
      end
    end
  end
end

# Add additional json-schema type information
def process_types(model, props)
  relations = []
  props.each do |k, v|
    case k
    when /^((updated|created)At)|token$/          # internal
      v['readOnly'] = true
    when /^id$/                                   # integral primary keys
      if v['type'] == 'integer'
        v['autoIncrement'] = true
        v['initialOffset'] = 0
        v['readOnly'] = true
      end
    # Foreign keys: fix references to point to Ids of correct types
    when /^(.*)Id$/
      next if v.has_key?('readOnly')
      m = /^(.*)Id$/.match(k)
      if m[1] =~ /(assignedBy|user|receiver|sender|subscriber|creator)/
        props[k] = { '$ref' => '#/definitions/User/properties/id' }
      end
      fmod = m[1]
      STDERR.puts "#{fmod} not found on #{model}!" unless props.has_key? fmod
      relations << fmod
    end
    # Array relations
    if v.has_key?('items') and v['items'].has_key?('$ref')
      if v['items'].has_key?('type')
        v['items'].delete('$ref')
      else
        v['readOnly'] = true
      end
    # Optional relation
    elsif v.has_key? 'anyOf'
      v['readOnly'] = true
    end
  end
  # remove foreign key relation
  relations.each { |k| props.delete k }
end

# Determine required fields:
# - No fields where 'null' is a type option
# - internal fields not required (with exceptions in EXTRA):
#   - (created|updated)At
#   - id
# - relations aren't required, but foreign keys are (unless 'null' is an option)
def required(model, props)
  props.keys.select do |k|
    next if props[k]['readOnly'] or props[k].has_key?('default')
    meta = props[k]
    type = meta['type']
    next k if type.nil?
    next if type.include?('null')
    next if type == 'array' && meta['items'].include?('$ref')
    k
  end
end

if __FILE__ == $PROGRAM_NAME
  STDERR.puts "#{JSON_SCHEMA} not found" && exit(1) unless File.exist? JSON_SCHEMA
  json = JSON.load(File.read(JSON_SCHEMA))
  json.deep_merge! EXTRA

  # add required fields and type metadata
  defs = json['definitions']
  defs.each do |model, v|
    process_types(model, v['properties'])

    # add required fields
    defs[model]['required'] ||= []
    defs[model]['required'].concat(required(model, v['properties'])).uniq! rescue nil

    # sort required properties first, rest by alpha
    v['properties'] =
      v['properties']
        .sort_by { |k, val| v['required'] && v['required'].include?(k) ? -1 : k[0].ord }
        .to_h
  end

  # dump json schema with additional metadata to generate better seed data
  File.write(JSON_SCHEMA, JSON.pretty_generate(json))

  File.write(
    YAML_SCHEMA,
    YAML.dump({ 'components' => { 'schemas' => defs } })
      .gsub!('"$ref"', '$ref')
      .gsub!('#/definitions', '#/components/schemas')
      .gsub!("- 'null'", ''))
end
