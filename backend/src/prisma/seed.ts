import seedrandom from 'seedrandom';
import { faker } from '@faker-js/faker';
import { // eslint-disable-line
  TagType,
  TicketStatus,
  TicketVisibility,
  TicketPriority,
  User,
  Organization,
  Tag,
  Ticket,
  TicketChange,
} from '@prisma/client';
import axios from 'axios';
import prisma from '../client';
import { DEFAULT_TICKET_REWARD } from '../config';
import Debug from 'debug';

// const { DEBUG = 'seed:*' } = process.env;
const debug = Debug('seed');
const dbg_users = Debug('seed:user');
const dbg_orgs = Debug('seed:org');
Debug.enable(process.env.DEBUG || 'seed');

// Settings
const useAwsData = true; // seed with Person/Organization uuids from AWS

// Random test case parameters
const NUM_USER = 10; // not used with aws seeding
const NUM_ORGANIZATIONS = 10; // not used with aws seeding
const NUM_TOPIC_TAGS = 50;
const TICKETS_PER_USER = 8;
const NOTES_PER_USER = [0, 20];
const USER_ASSIGNS = [0, 40];
const USER_SUBS = [0, 20];
const USER_COMMENTS = [0, 20];
const WATCHED_TAGS = [5, 15];
const WATCHED_ORGS = [5, 10];
const TICKET_LINKS = [0, 10];
const TAGS_PER_TICKET = 5;
const TICKET_DETAIL_SIZE = [1, 5];

// match those hardcoded in health module
const physicians = [
  'Cardiology',
  'Infectious Disease',
  'Dermatology',
  'Hematology',
  'Neurology',
  'Gastroenterology',
  'OBGYN',
  'Ophthalmology',
  'Pediatrics',
  'Family',
  'Psychiatry',
  'Pain',
  'Internal',
  'Allergy',
  'Radiology',
  'Endocrinology',
  'Nephrology',
  'Rheumatology',
].map(x => x.toLowerCase());

const educators = ['English', 'Biology', 'Mathematics', 'Chemistry', 'Physics', 'Personal Finance', 'Computer Science', 'Data Science'].map(x =>
  x.toLowerCase(),
);

// Reproducible randoms
const SEED = 1647142082296;
const useSeededRNG = true;
const seedDate = new Date(SEED);
const randomTimestampSeed = seedDate.toISOString();

let rng = seedrandom();
if (useSeededRNG) {
  rng = seedrandom(randomTimestampSeed);
  // setRandom(rng);
  faker.seed(seedDate.getTime());
}

const randomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  return Math.floor(rng() * (Math.floor(max) - min + 1)) + min;
};
const rand = (range: number[], max = Infinity) =>
  range.length === 1 ? randomInt(0, Math.min(max, range[0])) : randomInt(range[0], Math.min(max, range[1]));

const randomIntArray = (min: number, max: number, n = 1) => {
  min = Math.ceil(min);
  return Array.from({ length: n }, () => Math.floor(rng() * (max - min + 1)) + min);
};

const maptimes = (n, fn) => Array.from({ length: n }, (x, i) => i).map(i => fn(i));

const recent = (days = 30) => faker.date.recent(days);

// take a random selection from ARR
const takeSample = (arr: any[], n = Infinity) => {
  return randomIntArray(0, arr.length - 1, randomInt(0, Math.min(arr.length - 1, n))).map(i => arr[i]);
};

const randomKey = typ => {
  const keys = Object.keys(typ);
  return keys[randomInt(0, keys.length - 1)];
};

// Test markdown rendering
const randomTr = (n: number) => '|' + faker.random.words(n).replace(/ /g, '|') + '|';
const randomMdTable = () => {
  const n = randomInt(1, 10);
  const m = randomInt(1, 10);
  const header = randomTr(n);
  const delim = '|'
    .repeat(n + 1)
    .split('')
    .join('---');
  return [header, delim, maptimes(m, () => randomTr(n))].join('\n');
};

// AWS data
const OPP_URI = 'https://capstone.onestepprojects.org/onestep';
const ORG_URI = `${OPP_URI}/organization`;
const PERSON_URI = `${ORG_URI}/persons`;
const ORGANIZATION_URI = `${ORG_URI}/organizations`;

// These routes are buggy - in some cases the one route works for
// a given user but not the other.  And, neither route works for all users
// - ${ORGANIZATION_URI}/get-org-by-person/${id}
// - ${ORGANIZATION_URI}/get-org-roles-by-person/${id}
const orgs_by_person_uri = id => `${ORGANIZATION_URI}/get-org-roles-by-person/${id}`;
const org_roles_by_person_uri = id => `${ORGANIZATION_URI}/get-org-roles-by-person/${id}`;

interface ResponseData {
  id: string;
  name: string;
  [key: string]: any;
}

interface UserData extends ResponseData {
  email: string;
}

const fetchUsers = async (): Promise<UserData[]> => {
  try {
    const res = await axios.get<UserData>(PERSON_URI);
    return res.data.map(({ name, uuid, email }) => ({ name, id: uuid, email }));
  } catch (err) {
    dbg_users(`[ERROR] fetching users:`, err?.response?.data?.message);
    return [];
  }
};

const fetchOrgs = async (): Promise<ResponseData[]> => {
  try {
    const res = await axios.get<ResponseData>(ORGANIZATION_URI);
    return res.data.map(({ id, name }) => ({ id, name }));
  } catch (err) {
    dbg_orgs(`[ERROR] fetching orgs:`, err?.response?.data?.message);
    return [];
  }
};

// Since org service routes are buggy (5/4/22), try to get user orgs
// from either org-by-person and org-roles-by-person, whichever works
const fetchUserOrgs = async (id): Promise<ResponseData[]> => {
  try {
    const res = await axios.get<ResponseData>(orgs_by_person_uri(id));
    return res.data.map(({ id, name }) => ({ id, name }));
  } catch (err) {
    dbg_orgs(`[ERROR] fetching orgs for ${id}, trying org-roles instead:`, err?.response?.data?.message);
    try {
      const res = await axios.get<ResponseData>(org_roles_by_person_uri(id));
      return res.data.map(({ id, name }) => ({ id, name }));
    } catch (err) {
      dbg_orgs(`[ERROR] couldnt fetch user orgs for ${id}:`, err?.response?.data?.message);
      return [];
    }
  }
};

interface AllUserOrgs {
  [_: string]: Organization[];
}

interface AllVisibleTickets {
  [_: string]: Ticket[];
}

/**
 * Get mapping of userId => userOrgsIds
 */
const fetchAllUserOrgs = async (users: User[]): Promise<AllUserOrgs> => {
  const h = {};
  const vals = await Promise.allSettled(
    users.map(async user => {
      const orgs = await fetchUserOrgs(user.id);
      return {
        userId: user.id,
        result: orgs,
      };
    }),
  );
  const data = vals.filter(res => res.status === 'fulfilled') as PromiseFulfilledResult<{
    userId: string;
    result: Organization[];
  }>[];
  data.forEach(({ value }) => {
    const { userId, result } = value;
    h[userId] = result;
  });
  dbg_orgs(`allSettled: fetched all user orgs`);
  return h;
};

/**
 * Get unique org ids from all user organizations
 */
const uniqueOrgs = (allOrgs: AllUserOrgs): string[] => {
  const h = {};
  Object.values(allOrgs).forEach(orgs => orgs.forEach(org => (h[org.id] = org)));
  return Object.values(h);
};

// eslint-disable-next-line
const fetchOrgsFromUsers = async (users: User[]): Promise<ResponseData[]> => {
  const h = {};
  const vals = await Promise.allSettled(users.map(user => fetchUserOrgs(user.id)));
  const data = vals.filter(res => res.status === 'fulfilled') as PromiseFulfilledResult<ResponseData[]>[];
  data.forEach(({ value }) => value.forEach(({ id, name }) => (h[id] = { id, name })));
  dbg_orgs(`allSettled: fetched orgs from users`);
  return Object.values(h);
};

// eslint-disable-next-line
const getOrgs = async () => {
  return useAwsData
    ? await fetchOrgs()
    : maptimes(NUM_ORGANIZATIONS, () => ({
        id: faker.datatype.uuid(),
        name: faker.company.companyName(),
      }));
};

// eslint-disable-next-line
const getUserOrgs = async (id, orgs = []) => {
  return useAwsData ? await fetchUserOrgs(id) : takeSample(orgs, 3);
};

const getUsers = async () => {
  return useAwsData
    ? await fetchUsers()
    : maptimes(NUM_USER, () => ({
        id: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
      }));
};

const getAllUserOrgs = async (users: User[]) => {
  return useAwsData
    ? fetchAllUserOrgs(users)
    : users.reduce(
        (acc, { id }) => ({
          ...acc,
          [id]: maptimes(randomInt(1, NUM_ORGANIZATIONS), () => ({
            id: faker.datatype.uuid(),
            name: faker.company.companyName(),
          })),
        }),
        {},
      );
};

// Work around bugged out '/organizations' route
// eslint-disable-next-line
const getOrgsFromUsers = (allUserOrgs: AllUserOrgs): Organization[] => {
  return useAwsData
    ? uniqueOrgs(allUserOrgs)
    : maptimes(NUM_ORGANIZATIONS, () => ({
        id: faker.datatype.uuid(),
        name: faker.company.companyName(),
      }));
};

// Create tags for each tag type
const tags = [
  ...educators.map(x => ({ name: x, type: TagType.educator })),
  ...physicians.map(x => ({ name: x, type: TagType.physician })),
  ...maptimes(NUM_TOPIC_TAGS, () => ({
    name: faker.word.noun().toLowerCase(),
    type: TagType.topic,
  })),
];

const report = (users: User[], orgs: Organization[]) => {
  const userIds = users.map(u => u.id);
  const pr = (v: number[]) => `[${v[0]}-${v[1]}]`;
  console.log(`=================================`);
  console.log('= Seeding database\n');
  console.log(`= Users(${users.length}):\n\t${userIds.join('\n\t')}`);
  console.log(`= Each with:`);
  console.log(`\t- default user settings`);
  console.log(`\t- ${TICKETS_PER_USER} tickets`);
  console.log(`\t- ${pr(USER_COMMENTS)} comments`);
  console.log(`\t- ${pr(NOTES_PER_USER)} personal notes on tickets`);
  console.log(`\t- ${pr(WATCHED_TAGS)} watched tags`);
  console.log(`\t- ${pr(WATCHED_ORGS)} watched organizations`);
  console.log(`\t- ${pr(USER_SUBS)} subscribed tickets`);
  console.log(`\t- ${pr(USER_ASSIGNS)} assigned tickets`);
  console.log(`= Tickets(${TICKETS_PER_USER * users.length}) with:`);
  console.log(`\t- ${pr(TICKET_LINKS)} linked tickets`);
  console.log(`= Tags(${tags.length}): ${tags.map(({ name, type }) => type + ':' + name).join(', ')}\n`);
  console.log(`= Orgs(${orgs.length}): ${orgs.map(({ name }) => name).join(', ')}`);
  console.log(`=================================`);
};

const createOrgs = async (orgs: Organization[]) => {
  return prisma.organization.createMany({ data: orgs, skipDuplicates: true });
};

const createTags = async (tags: Tag[]) => {
  return prisma.tag.createMany({ data: tags, skipDuplicates: true });
};

/**
 * Seed users + tickets
 * - initialize with default settings
 * - seeds tickets created by each user
 * - tickets have random tags
 * - tickets have random organizations selected from organizations the ticket creator's
 *   organizations
 */
const createUsers = async (users: User[], allUserOrgs: AllUserOrgs) => {
  return Promise.all(
    users.map(user => {
      const userOrgs = allUserOrgs[user.id];
      return prisma.user.upsert({
        where: { id: user.id },
        update: {},
        create: {
          ...user,
          settings: { create: {} },
          password: faker.internet.password(),
          isAdmin: false,
          ticketsBy: {
            create: maptimes(TICKETS_PER_USER, () => {
              const vis = userOrgs?.length > 0 ? randomKey(TicketVisibility) : TicketVisibility.public;
              return {
                createdAt: recent(),
                title: faker.lorem.sentence(),
                detail: faker.lorem.paragraphs(rand(TICKET_DETAIL_SIZE)),
                status: TicketStatus.open,
                priority: randomKey(TicketPriority),
                visibility: vis,
                points: DEFAULT_TICKET_REWARD,
                changelog: {
                  create: { type: TicketChange.created, userId: user.id },
                },
                tags: {
                  connectOrCreate: takeSample(tags, TAGS_PER_TICKET)?.map(({ name, type }) => ({
                    where: { name_type: { name, type } },
                    create: { name, type },
                  })),
                },
                organizations: {
                  connectOrCreate: userOrgs?.map(({ id, name }) => ({
                    where: { id },
                    create: { id, name },
                  })),
                },
              };
            }),
          },
        },
      });
    }),
  );
};

/**
 * Get tickets visible to user
 *
 * Visible tickets are:
 * - created by user
 * - visibility: public
 * - from organization user is member of
 */
const visibleTicketsForUser = async (user: User, userOrgs: string[]) => {
  const visibleIds = await prisma.ticket.findMany({
    where: {
      OR: [{ userId: user.id }, { visibility: TicketVisibility.public }, { organizations: { some: { id: { in: userOrgs } } } }],
    },
    select: { id: true },
  });
  return visibleIds.map(x => x.id);
};

/**
 * Map users to their visible tickets
 */
const getAllVisibleTickets = async (users: User[], allUserOrgs: AllUserOrgs): Promise<AllVisibleTickets> => {
  const allVisible = await Promise.all(
    users.map(async (user: User) => ({
      userId: user.id,
      tickets: await visibleTicketsForUser(
        user,
        allUserOrgs[user.id]?.map(({ id }) => id),
      ),
    })),
  );
  return allVisible.reduce(
    (acc, { userId, tickets }) => ({
      ...acc,
      [userId]: tickets,
    }),
    {},
  );
};

/**
 * Seed user relations
 * - Assignments: assign random (visible) tickets to users
 * - subscriptions: subscribe users to random (visible) tickets + their created
 *   tickets + their assigned tickets (default settings)
 * - watchedTags/Orgs: connect users to random tags + their member organizations to
 *   receive notifications about
 * - create random personal notes on (visible) tickets
 */
const createUserRelations = async (users: User[], tagIds: number[], allUserOrgs: AllUserOrgs, allVisibleTickets: AllVisibleTickets) => {
  return await Promise.all(
    users.map(async (user: User) => {
      const userTickets = await prisma.ticket.findMany({ where: { userId: user.id } });
      const userTicketIds = userTickets.map(({ id }) => id);
      const orgIds = allUserOrgs[user.id]?.map(({ id }) => id);
      const visibleIds = allVisibleTickets[user.id];
      const assns = takeSample(visibleIds, rand(USER_ASSIGNS));
      // subscribe user to all created + assigned tickets, plus some random other
      // visible tickets
      const subs = userTicketIds.concat(assns).concat(takeSample(visibleIds, rand(USER_SUBS)));
      const watchedTags = takeSample(tagIds, rand(WATCHED_TAGS, tagIds.length));
      const watchedOrgs = orgIds; //takeSample(orgIds, rand(WATCHED_ORGS, orgIds.length));
      return prisma.user.update({
        where: { id: user.id },
        data: {
          ticketsFor: { connect: assns.map(id => ({ id })) },
          subscriptions: { connect: subs.map(id => ({ id })) },
          watchedOrgs: { connect: watchedOrgs.map(id => ({ id })) },
          watchedTags: { connect: watchedTags.map(id => ({ id })) },
          notes: {
            create: maptimes(rand(NOTES_PER_USER), () => {
              const createdAt = recent();
              return {
                createdAt,
                updatedAt: createdAt,
                note: faker.hacker.phrase(),
                ticket: { connect: { id: faker.random.arrayElement(visibleIds) } },
              };
            }),
          },
        },
      });
    }),
  );
};

/**
 * Seed ticket relations
 * - link tickets to other tickets
 */
const createTicketRelations = async (tickets: Ticket[]) => {
  const ids = tickets.map(({ id }) => id);
  return Promise.all(
    tickets.map((ticket: Ticket) => {
      const links = takeSample(ids, rand(TICKET_LINKS));
      return prisma.ticket.update({
        where: { id: ticket.id },
        data: {
          linkedTo: { connect: links.map(id => ({ id })) },
        },
      });
    }),
  );
};

/**
 * Seed comments
 * - adds user comments to (visible) tickets
 * - some comments are seeded with markdown to test rendering
 */
const createComments = async (users: User[], allVisibleTickets: AllVisibleTickets) => {
  return Promise.all(
    users.map((user: User) => {
      return prisma.user.update({
        where: { id: user.id },
        data: {
          comments: {
            create: takeSample(allVisibleTickets[user.id] ?? [], rand(USER_COMMENTS)).map(ticketId => {
              const date = recent();
              return {
                createdAt: date,
                updatedAt: date,
                ticketId,
                comment: randomInt(1, 10) <= 5 ? randomMdTable() : faker.lorem.paragraph(),
              };
            }),
          },
        },
      });
    }),
  );
};

async function main() {
  const users = await getUsers();
  debug('got users');
  // const orgs = await getOrgs();
  const allUserOrgs = await getAllUserOrgs(users);
  debug('got all user orgs');
  // (5/6/22) organization endpoint working again
  const orgs = await getOrgs(); // getOrgsFromUsers(allUserOrgs);
  debug('got orgs:', orgs?.length);

  await createOrgs(orgs);
  debug('created orgs');
  await createTags(tags);
  debug('created tags');
  await createUsers(users, allUserOrgs);
  debug('created users:', users?.length);

  const dbTags = await prisma.tag.findMany();
  const tickets = await prisma.ticket.findMany();
  const allVisibleTickets = await getAllVisibleTickets(users, allUserOrgs);
  debug('got all visible tickets for users');

  await createComments(users, allVisibleTickets);
  debug('created comments on tickets');

  await createUserRelations(
    users,
    dbTags.map(({ id }) => id),
    allUserOrgs,
    allVisibleTickets,
  );
  debug('created user relations');

  await createTicketRelations(tickets);
  debug('created ticket relations');

  const dbUsers = await prisma.user.findMany();
  const dbOrgs = await prisma.organization.findMany();
  report(dbUsers, dbOrgs);
}

main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
