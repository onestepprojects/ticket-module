import { Response } from 'express';
import { CreateTagDto } from '@dtos';
import { TagService } from '@services';
import { // eslint-disable-line
  catchAsync,
  parseFindManyQuery,
  parseFindManyTagsQuery,
} from '@utils';
import { Ticket, User } from '@prisma/client';
import { TRequest as Request, FindManyQuery } from '@interfaces';

export class TagsController {
  public tagService = new TagService();

  public getTags = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseFindManyTagsQuery(req.query);
    res.status(200).json(await this.tagService.findTags(params));
  });

  public getTagById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const tagId = Number(req.params.tagId);
    res.status(200).json(await this.tagService.findTagById(tagId));
  });

  public createTag = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const tagData: CreateTagDto = req.body;
    res.status(201).json(await this.tagService.createTag(tagData));
  });

  public updateTag = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const tagId = Number(req.params.tagId);
    const tagData: CreateTagDto = req.body;
    res.status(200).json(await this.tagService.updateTag(tagId, tagData));
  });

  public deleteTag = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const tagId = Number(req.params.tagId);
    res.status(200).json(await this.tagService.deleteTag(tagId));
  });

  /**
   * Tag tickets
   * @return Array of ticket objects or count
   */
  public getTicketsForTag = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { tagId } = req.params;
    const params: FindManyQuery<Ticket> = parseFindManyQuery(req.query, { model: 'Ticket' });
    res.status(200).json(await this.tagService.findTicketsForTag(Number(tagId), params));
  });

  /**
   * Tag followers
   * @return Array of user objects or count
   */
  public getWatchersForTag = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { tagId } = req.params;
    const params: FindManyQuery<User> = parseFindManyQuery(req.query, { model: 'User' });
    res.status(200).json(await this.tagService.findWatchersForTag(Number(tagId), params));
  });
}

export default TagsController;
