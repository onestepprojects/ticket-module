import { Request, Response } from 'express';
import { CreateAssignmentDto } from '@dtos';
import { AssignmentService } from '@services';
import { catchAsync } from '@utils';

interface AssignmentParams {
  assignmentId: number;
}

export class AssignmentsController {
  public assignmentService = new AssignmentService();

  private getParams = (req: Request): AssignmentParams => {
    const assignmentId = Number(req.params.assignmentId);
    return { assignmentId };
  };

  // eslint-disable-next-line
  public getAssignments = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.assignmentService.findAssignments());
  });

  public getAssignmentById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { assignmentId } = this.getParams(req);
    res.status(200).json(await this.assignmentService.findAssignmentById(assignmentId));
  });

  public createAssignment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const data: CreateAssignmentDto = req.body;
    res.status(201).json(await this.assignmentService.createAssignment(data));
  });

  public updateAssignment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const newData: CreateAssignmentDto = req.body;
    const { assignmentId } = this.getParams(req);
    res.status(200).json(await this.assignmentService.updateAssignment(assignmentId, newData));
  });

  public deleteAssignment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { assignmentId } = this.getParams(req);
    res.status(200).json(await this.assignmentService.deleteAssignment(assignmentId));
  });
}

export default AssignmentsController;
