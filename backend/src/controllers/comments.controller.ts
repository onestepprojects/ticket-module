import { Response } from 'express';
import { CreateCommentDto } from '@dtos';
import { CommentService } from '@services';
import { catchAsync, parseFindManyQuery } from '@utils';
import { Comment } from '@prisma/client';
import { TRequest as Request } from '@/interfaces';

export class CommentsController {
  public commentService = new CommentService();

  public getComments = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseFindManyQuery<Comment>(req.query, { model: 'Comment' });
    res.status(200).json(await this.commentService.findComments(params));
  });

  public getCommentById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const commentId = Number(req.params.commentId);
    res.status(200).json(await this.commentService.findCommentById(commentId));
  });

  public createComment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const commentData: CreateCommentDto = { ...req.body, userId: req.session.userId };
    res.status(201).json(await this.commentService.createComment(commentData));
  });

  public updateComment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const commentId = Number(req.params.commentId);
    const commentData: CreateCommentDto = { ...req.body, userId: req.session.userId };
    res.status(200).json(await this.commentService.updateComment(commentId, commentData));
  });

  public deleteComment = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const commentId = Number(req.params.commentId);
    res.status(200).json(await this.commentService.deleteComment(commentId, req.session.userId));
  });
}

export default CommentsController;
