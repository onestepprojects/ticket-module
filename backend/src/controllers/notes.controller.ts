import { Response } from 'express';
import { CreateNoteDto } from '@dtos';
import { NoteService } from '@services';
import { catchAsync, parseFindManyQuery } from '@utils';
import { TRequest as Request } from '@/interfaces';
import { Note } from '@prisma/client';

export class NotesController {
  public noteService = new NoteService();

  public getNotes = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseFindManyQuery<Note>(req.query, { model: 'Note' });
    res.status(200).json(await this.noteService.findNotes(req.session.userId, params));
  });

  public getNoteById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const noteId = Number(req.params.id);
    res.status(200).json(await this.noteService.findNoteById(noteId, req.session.userId));
  });

  public createNote = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const noteData: CreateNoteDto = { ...req.body, userId: req.session.userId };
    res.status(201).json(await this.noteService.createNote(noteData));
  });

  public updateNote = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const noteId = Number(req.params.id);
    const noteData: CreateNoteDto = { ...req.body, userId: req.session.userId };
    res.status(200).json(await this.noteService.updateNote(noteId, noteData));
  });

  public deleteNote = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const noteId = Number(req.params.id);
    res.status(200).json(await this.noteService.deleteNote(noteId, req.session.userId));
  });
}

export default NotesController;
