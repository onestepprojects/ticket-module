import { Response } from 'express';
import { Ticket, User } from '@prisma/client';
import { OrganizationsService } from '@services';
import { catchAsync, parseFindManyQuery } from '@utils';
import { TRequest as Request } from '@/interfaces';

export class OrganizationsController {
  public orgsService = new OrganizationsService();

  /**
   * Organization tickets
   * @return Array of ticket objects or count
   */
  public getTicketsForOrg = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { orgId } = req.params;
    const params = parseFindManyQuery<Ticket>(req.query, { model: 'Ticket' });
    res.status(200).json(await this.orgsService.findTicketsForOrg(orgId, params));
  });

  /**
   * Organization watchers
   * @return Array of user objects or count
   */
  public getWatchersForOrg = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { orgId } = req.params;
    const params = parseFindManyQuery<User>(req.query, { model: 'User' });
    res.status(200).json(await this.orgsService.findWatchersForOrg(orgId, params));
  });
}

export default OrganizationsController;
