import { Response } from 'express';
import { CreateNotificationDto } from '@dtos';
import { NotificationService } from '@services';
import { catchAsync, parseFindManyQuery } from '@utils';
import { TRequest as Request } from '@interfaces';
import { Notification } from '@prisma/client';

export class NotificationsController {
  public notificationService = new NotificationService();

  public getNotifications = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseFindManyQuery<Notification>(req.query);
    res.status(200).json(await this.notificationService.findNotifications(params));
  });

  public getNotificationById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.notificationService.findNotificationById(Number(req.params.notificationId)));
  });

  public createNotification = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const notificationData: CreateNotificationDto = req.body;
    res.status(201).json(await this.notificationService.createNotification(notificationData));
  });

  public updateNotification = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const notificationData: CreateNotificationDto = req.body;
    res.status(200).json(await this.notificationService.updateNotification(Number(req.params.notificationId), notificationData));
  });

  public deleteNotification = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.notificationService.deleteNotification(Number(req.params.notificationId)));
  });

  public deleteExpiredNotifications = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { daysOld = undefined } = req.params;
    res.status(200).json(await this.notificationService.deleteExpiredNotifications(daysOld ? Number(daysOld) : undefined));
  });
}

export default NotificationsController;
