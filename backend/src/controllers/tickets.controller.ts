import { Response } from 'express';
import { Comment, Ticket } from '@prisma/client';
import { CreateTicketDto, UpdateTicketDto } from '@dtos';
import { TicketService } from '@services';
import { // eslint-disable-line
  TRequest as Request,
  GroupByRequest,
  TicketResponse,
  TicketsResponse,
  TicketSelectWithRelations,
  FindManyQuery,
  FindManyTicketsQuery,
} from '@interfaces';
import { // eslint-disable-line
  parseGroupByQuery,
  assignedTicketsQ,
  catchAsync,
  closedTicketsQ,
  completedTicketsQ,
  openTicketsQ,
  unassignedTicketsQ,
  parseSelectQuery,
  parseFindManyTicketsQuery,
  parseSearchTicketsQuery,
  parseChangeLogQuery,
  assignedToUserQ,
  parseFindManyQuery,
  composeWhereAnd,
  cleanQuery,
} from '@utils';

export class TicketsController {
  public ticketService = new TicketService();

  public createTicket = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const ticketData: CreateTicketDto = req.body;
    res.status(201).json(await this.ticketService.createTicket(ticketData, req.session.settings));
  });

  public updateTicket = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const ticketId = Number(req.params.ticketId);
    const ticketData: UpdateTicketDto = { ...req.body, userId: req.session.userId };
    const updateTicketData: TicketResponse = await this.ticketService.updateTicket(ticketId, ticketData);
    res.status(200).json(updateTicketData);
  });

  public updateTicketStatus = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { status } = req.body;
    res.status(200).json(await this.ticketService.updateTicketStatus(Number(req.params.ticketId), req.session.userId, status));
  });

  public deleteTicket = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const ticketId = Number(req.params.ticketId);
    const deleteTicketData: TicketResponse = await this.ticketService.deleteTicket(ticketId, req.session.userId);
    res.status(200).json(deleteTicketData);
  });

  public getTicketById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const ticketId = Number(req.params.ticketId);
    const { select: fields } = req.query;
    const select = fields ? parseSelectQuery<Ticket, TicketSelectWithRelations>(String(fields), { model: 'Ticket' }) : undefined;
    res.status(200).json(await this.ticketService.findTicketById(ticketId, cleanQuery({ select })));
  });

  /**
   * Ticket changelog
   * @returns {ChangeLogResponse} Array of changelog events or count
   */
  public getChangelog = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseChangeLogQuery(null, { ...req.query, ticketIds: req.params.ticketId });
    res.status(200).json(await this.ticketService.ticketChangelog(params));
  });

  /**
   * Comments on ticket
   * @returns {CommentsResponse} Array of Comment objects or count
   */
  public getComments = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const ticketId = Number(req.params.ticketId);
    const params: FindManyQuery<Comment> = parseFindManyQuery<Comment>(req.query, {
      model: 'Comment',
    });
    res.status(200).json(await this.ticketService.ticketComments(ticketId, params));
  });

  /**
   * Search for tickets, combining condition with parsed where clause from query
   * parameters, if present.
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  private findWithCondition = (cond: object, req: Request): Promise<TicketsResponse> => {
    const params: FindManyQuery<Ticket> = parseFindManyTicketsQuery(req.query);
    params.where = params.where ? { AND: { ...params.where, ...cond } } : cond;
    return this.ticketService.findTickets(params);
  };

  /**
   * Get tickets - unrestricted by user organizations / ticket visibility
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getTicketsAdmin = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params: FindManyTicketsQuery = parseFindManyTicketsQuery(req.query);
    res.status(200).json(await this.ticketService.findTickets(params));
  });

  /**
   * Get tickets visible to current user
   *
   * Visible tickets are those with public visibility or within organizations
   * the current user is a member of.
   *
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params: FindManyTicketsQuery = parseFindManyTicketsQuery(req.query);
    res.status(200).json(await this.ticketService.findTickets(composeWhereAnd<Ticket>(req.session.where, params)));
  });

  /**
   * Get tickets assigned to user
   * @returns {TicketsResponse} Array of tickets or count
   */
  public getTicketsAssignedToUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.params.userId;
    res.status(200).json(await this.findWithCondition(assignedToUserQ(userId), req));
  });

  /**
   * Get tickets created by user
   * @returns {TicketsResponse} Array of tickets or count
   */
  public getTicketsOpenedByUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.params.userId;
    res.status(200).json(await this.findWithCondition({ userId: userId }, req));
  });

  /**
   * Search for tickets
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public searchTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseSearchTicketsQuery(req.query);
    res.status(200).json(await this.ticketService.findTickets(params));
  });

  /**
   * Get open tickets that are assigned to at least one person
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getAssignedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.findWithCondition(assignedTicketsQ, req));
  });

  /**
   * Get unassigned and open tickets
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getUnassignedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.findWithCondition(unassignedTicketsQ, req));
  });

  /**
   * Get open tickets
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getOpenTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.findWithCondition(openTicketsQ, req));
  });

  /**
   * Get completed tickets
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getCompletedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.findWithCondition(completedTicketsQ, req));
  });

  /**
   * Get closed tickets
   * @returns {TicketsResponse} Array of Ticket objects or count
   */
  public getClosedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.findWithCondition(closedTicketsQ, req));
  });

  /**
   * Aggregate queries
   *
   * @returns object (TODO: type this better)
   */
  public getAggregateTickets = catchAsync(async (req: GroupByRequest, res: Response): Promise<void> => {
    if (!('by' in req.query)) throw new Error('by missing from aggregate');
    const params = parseGroupByQuery<Ticket>(req.query);
    console.dir(params);
    res.status(200).json(await this.ticketService.groupByTickets(params));
  });

  public getTicketsByAssigned = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { count } = req.query;
    res.status(200).json(
      await this.ticketService.ticketsByAssigned({
        count: count !== undefined,
      }),
    );
  });
}

export default TicketsController;
