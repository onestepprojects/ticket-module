import { Response } from 'express';
import { AdminService, UserService, TicketService } from '@services';
import { getAccessToken, catchAsync, parseFindManyQuery } from '@utils';
import { Comment } from '@prisma/client';
import { TQuery, TRequest as Request } from '@/interfaces';
import { isEmpty } from 'class-validator';
import { HttpException } from '@/exceptions';

export class AdminController {
  public adminService = new AdminService();
  public userService = new UserService();
  public ticketService = new TicketService();

  // -----------------------------------------------------------------
  /// Comments

  public getComments = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { userId } = req.params;
    const params = parseFindManyQuery<Comment>(req.query);
    res.status(200).json(await this.userService.userComments(userId, params));
  });

  /**
   * (unsafe) Raw SQL Queries
   * @return any results from SQL query
   */
  public getRawQuery = catchAsync(async (req: TQuery<{ sql: string }>, res: Response): Promise<void> => {
    const { sql } = req.query;
    res.status(200).json(await this.adminService.rawQuery(sql));
  });

  // eslint-disable-next-line
  public getSQLTables = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.adminService.getSQLTables());
  });

  /**
   * Get user access token for testing
   */
  public accessToken = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { username, password } = req.query;
    if (isEmpty(username) || isEmpty(password)) throw new HttpException(400, 'missing username/password');
    res.status(200).json(await getAccessToken(String(username), String(password)));
  });
}

export default AdminController;
