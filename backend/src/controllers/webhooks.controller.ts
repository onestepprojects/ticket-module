import { Response } from 'express';
import { Webhook } from '@prisma/client';
import { CreateWebhookDto } from '@dtos';
import { WebhookService } from '@services';
import { catchAsync } from '@utils';
import { TRequest as Request } from '@interfaces';

export class WebhooksController {
  public webhookService = new WebhookService();

  // eslint-disable-next-line
  public getWebhooks = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const findAllWebhooksData: Webhook[] = await this.webhookService.findAllWebhooks();

    res.status(200).json({ data: findAllWebhooksData, message: 'findAll' });
  });

  public getWebhookById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const webhookId = Number(req.params.id);
    const findOneWebhookData: Webhook = await this.webhookService.findWebhookById(webhookId);

    res.status(200).json({ data: findOneWebhookData, message: 'findOne' });
  });

  public createWebhook = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const webhookData: CreateWebhookDto = req.body;
    const createWebhookData: Webhook = await this.webhookService.createWebhook(webhookData);

    res.status(201).json({ data: createWebhookData, message: 'created' });
  });

  public updateWebhook = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const webhookId = Number(req.params.id);
    const webhookData: CreateWebhookDto = req.body;
    const updateWebhookData: Webhook = await this.webhookService.updateWebhook(webhookId, webhookData);

    res.status(200).json({ data: updateWebhookData, message: 'updated' });
  });

  public deleteWebhook = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const webhookId = Number(req.params.id);
    const deleteWebhookData: Webhook = await this.webhookService.deleteWebhook(webhookId);

    res.status(200).json({ data: deleteWebhookData, message: 'deleted' });
  });
}

export default WebhooksController;
