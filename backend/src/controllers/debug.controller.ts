import { Response } from 'express';
import { // eslint-disable-line
  Debug,
  catchAsync,
  getUsersToNotifyForTicket,
  getWatchersToNotify,
} from '@utils';
import { TRequest as Request } from '@interfaces';
import { TicketEvent } from '@types';

const debug = Debug('debug');

export class DebugController {
  public slow = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { ms = 10000 } = req.query;
    setTimeout(() => {
      debug('done sitting around');
      res.status(201).json([]);
    }, Number(ms));
  });

  /**
   * Get users watching tags
   */
  public watchers = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { tagIds } = req.query;
    const ids = String(tagIds)?.split(',').map(Number);
    res.status(200).json(await getWatchersToNotify(ids));
  });

  /**
   * Get users to notify for a ticket on an event
   */
  public notifyForTicket = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { ticketId = 1, event = 'ticketUpdate' } = req.query;
    const n = TicketEvent[String(event)];
    res.status(200).json(await getUsersToNotifyForTicket(Number(ticketId), n));
  });
}

export default DebugController;
