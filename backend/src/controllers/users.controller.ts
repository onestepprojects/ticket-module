import { Response } from 'express';
import { Comment, Ticket, User } from '@prisma/client';
import { CreateSettingsDto, CreateUserDto } from '@dtos';
import { UserService, TicketService } from '@services';
import { // eslint-disable-line
  assignedToUserQ,
  catchAsync,
  parseChangeLogQuery,
  parseFindManyNotificationsQuery,
  parseFindManyTicketsQuery,
  parseSelectManyQuery,
  parseFindManyQuery,
  checkInvalid,
  parseSelectQuery,
} from '@utils';
import type { // eslint-disable-line
  FindManyQuery,
  MarkNotificationRequestBody,
  TRequest as Request,
  UserResponse,
  UserSelectWithRelations,
} from '@interfaces';

export class UsersController {
  public userService = new UserService();
  public ticketService = new TicketService();

  public getUsers = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const params = parseFindManyQuery<User>(req.query, { model: 'User' });
    res.status(200).json(await this.userService.findUsers(params));
  });

  /**
   * Get user response, allowing relations to selected in query
   */
  private getUser = async (userId: string, req: Request): Promise<Partial<UserResponse>> => {
    const { select: fields, ...rest } = req.query;
    checkInvalid(rest);
    const select = fields ? parseSelectQuery<User, UserSelectWithRelations>(String(fields), { model: 'User' }) : undefined;
    return this.userService.findUserById(userId, { select });
  };

  public getCurrentUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.getUser(userId, req));
  });

  public getUserById = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { userId } = req.params;
    res.status(200).json(await this.getUser(userId, req));
  });

  public getOrCreateUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { userId } = req.params;
    const userData: CreateUserDto = req.body;
    userData.id = userId;

    const user = await this.userService.findUserOrCreate(userData);
    res.status(201).json(user);
  });

  public getOrCreateUserTemp = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { userId } = req.params;
    const userData: CreateUserDto = req.body;
    userData.id = userId;

    await this.userService.findUserOrCreate(userData);
    res.status(200).json(await this.userService.findUserById(userId, {}));
  });

  public createUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userData: CreateUserDto = req.body;
    res.status(201).json(await this.userService.createUser(userData));
  });

  public updateUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const { userId } = req.params;
    const userData: CreateUserDto = req.body;
    res.status(200).json(await this.userService.updateUser(userId, userData));
  });

  public deleteUser = catchAsync(async (req: Request, res: Response): Promise<void> => {
    res.status(200).json(await this.userService.deleteUser(String(req.params.userId)));
  });

  /**
   * Tickets created by current user
   * @returns Array of ticket objects
   */
  public getOpenedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params = parseSelectManyQuery<Ticket>(req.query, { model: 'Ticket' });
    res.status(200).json(await this.userService.ticketsCreatedByUser(userId, params));
  });

  /**
   * Tickets completed by current user
   */
  public getCompletedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params = parseSelectManyQuery<Ticket>(req.query, { model: 'Ticket' });
    res.status(200).json(await this.userService.ticketsCompletedByUser(userId, params));
  });

  // -----------------------------------------------------------------
  /// Assignments

  public assignedTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params: FindManyQuery<Ticket> = parseFindManyTicketsQuery(req.query);
    params.where = params.where ? { AND: { ...assignedToUserQ(userId), ...params.where } } : assignedToUserQ(userId);
    res.status(200).json(await this.ticketService.findTickets(params));
  });

  public assignTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const { ticketIds } = req.body;
    res.status(200).json(await this.userService.assignTickets(userId, ticketIds, req.session.settings));
  });

  public unassignTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const { ticketIds } = req.body;
    res.status(200).json(await this.userService.unassignTickets(userId, ticketIds, req.session.settings));
  });

  // -----------------------------------------------------------------
  /// Subscriptions

  public subscribeToTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const { ticketIds } = req.body;
    res.status(200).json(await this.userService.subscribeTickets(userId, ticketIds));
  });

  public unsubscribeFromTickets = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const { ticketIds } = req.body;
    res.status(200).json(await this.userService.unsubscribeTickets(userId, ticketIds));
  });

  public ticketsSubscribedTo = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.userService.ticketsSubscribedTo(String(userId)));
  });

  // -----------------------------------------------------------------
  /// Comments

  public getComments = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params = parseFindManyQuery<Comment>(req.query, { model: 'Comment' });
    res.status(200).json(await this.userService.userComments(userId, params));
  });

  /**
   * Get watched orgs/tags for current user
   * @return Object with orgIds and tagIds
   */
  public getWatching = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.userService.userWatching(userId));
  });

  public updateWatching = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.userService.updateWatching(userId, req.body));
  });

  /**
   * Get user settings
   * @return Settings object with current settings
   */
  public getSettings = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.userService.getSettings(userId));
  });

  /**
   * Update user settings
   * @return Settings object with updated settings
   */
  public updateSettings = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const settings: CreateSettingsDto = req.body;
    res.status(200).json(await this.userService.updateSettings(userId, settings));
  });

  /**
   * Get notifications for user
   * @return Array of Notification objects
   */
  public getNotifications = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params = parseFindManyNotificationsQuery(req.query);
    res.status(200).json(await this.userService.getNotifications(userId, params));
  });

  /**
   * Delete all read notifications
   */
  public deleteReadNotifications = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    res.status(200).json(await this.userService.deleteReadNotifications(userId));
  });

  /**
   * Mark all notifications as un/read
   */
  public markNotifications = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const body: MarkNotificationRequestBody = req.body;
    res.status(200).json(await this.userService.markNotifications(userId, body));
  });

  /**
   * Get user history/activity for logged in user
   * @returns Array of changelog events instigated by user, or count
   */
  public getHistory = catchAsync(async (req: Request, res: Response): Promise<void> => {
    const userId = req.session.userId;
    const params = parseChangeLogQuery(userId, req.query);
    res.status(200).json(await this.userService.getHistory(params));
  });
}

export default UsersController;
