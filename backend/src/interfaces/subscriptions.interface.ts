/**
 * Subscribe/Unsubscribe from tickets body
 * @examples require('./examples').exampleSubscribeRequest
 */
export interface SubscriptionRequestBody {
  /**
   * Ticket Ids to subscribe / unsubscribe to
   */
  ticketIds: number[];
}

/**
 * Ticket Ids for tickets user is subscribed to
 * @examples require('./examples').exampleSubscribeResponse
 */
export interface SubscriptionResponse {
  /**
   * Ticket Ids for subscribed tickets
   */
  ticketIds: number[];
}
