/** Notification settings for tickets
 * @examples require('./examples').exampleTicketNotificationSettings
 */
export interface TicketNotificationSettings {
  /**
   * Receive notifications when ticket is updated
   * @default true
   */
  ticketUpdate: boolean;
  /**
   * Receive notifications when ticket assignment changes
   * @default true
   */
  ticketAssignment: boolean;
  /**
   * Receive notifications when ticket status changes
   * @default true
   */
  ticketStatusChange: boolean;
  /**
   * Receive notifications when ticket is commented on
   * @default true
   */
  ticketComment: boolean;
}

/**
 * Notification settings for newly created tickets
 * @examples require('./examples').exampleWatchedNotificationSettings
 */
export interface WatchedNotificationSettings {
  /**
   * Receive notifications when ticket is created with a watched tag
   * @default true
   */
  withTags: boolean;
  /**
   * Receive notifications when ticket is created by a watched organization
   * @default true
   */
  withOrgs: boolean;
  /**
   * Receive notifications when ticket is created requesting a watched role
   * @default true
   */
  withRoles: boolean;
}

/**
 * Notifications settings
 * @examples require('./examples').exampleNotificationSettings
 */
export interface NotificationSettings {
  /**
   * Notification settings for subscribed tickets
   */
  onSubscribed: TicketNotificationSettings;
  /**
   * Notification settings for newly created tickets
   */
  onWatched: WatchedNotificationSettings;
}

/**
 * Settings response object
 * @examples require("./examples").exampleSettings
 */
export interface SettingsResponse {
  /**
   * Subscribe to tickets when created
   *
   * @default true
   */
  subscribeOnCreate: boolean;

  /**
   * Subscribe to/unsubscribe from tickets when assigned/unassigned
   *
   * @default true
   */
  subscribeOnAssign: boolean;

  /**
   * Notification settings
   */
  notifications: NotificationSettings;
}

/**
 * Settings request body is same as response, except fields are optional.
 * Absent settings are treated as <b>false</b>.
 *
 * @examples require('./examples').exampleSettings
 */
export interface SettingsRequestBody extends Partial<SettingsResponse> {} // eslint-disable-line
