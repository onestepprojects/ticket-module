import { PWhere } from '@/types';
import { Tag, TagType } from '@prisma/client';
import { FindManyResponse, FindOneResponse, RawSelectManyQuery, SelectManyQuery } from '.';
/**
 * Tag data
 * @examples require('./examples').exampleTags
 */
export type TagRequestBody = Pick<Tag, 'name' | 'type'>;

/**
 * Tag Object
 */
export type TagResponse = FindOneResponse<Tag>;

/**
 * Array of tag objects or count
 */
export type TagsResponse = FindManyResponse<Tag>;

/**
 * Fields available to select from returned Tag objects
 */
export type TagSelectField = keyof TagResponse;

export type TagOrderBy = `-${keyof Tag}` | `${keyof Tag}`;

/**
 * Query parameters unique to Tag queries
 */
export interface TagQueryParameters {
  /**
   * Comma-separated list of tag types to collect into results.
   *
   * @default null
   */
  type?: TagType[];

  /**
   * Comma-separated list of tag names to collect into results.
   *
   * @default null
   */
  name?: string[];

  /**
   * Select fields to collect in array of returned Tag objects.
   * Available fields are defined in the schema TagSelectField.
   *
   * @default null
   */
  select?: TagSelectField[];

  /**
   * Order results by Tag column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-name"
   */
  orderBy?: TagOrderBy;
}

/**
 * Query parameters available to GET many Tag routes
 * @examples "'?skip=0&take=50&type=topic,educator'"
 */
export interface RawFindManyTagsQuery extends RawSelectManyQuery, Omit<TagQueryParameters, 'select' | 'orderBy'> {}

/**
 * Parsed find many tags query
 */
export interface FindManyTagsQuery extends Omit<SelectManyQuery<Tag>, 'dateFilter'> {
  /**
   * @ignore
   */
  where?: PWhere<Tag>;
}
