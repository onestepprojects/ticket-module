/**
 * Default find many queries return arrays of objects
 */
export type FindOneResponse<T> = Partial<T>;

/**
 * Find many queries can return either counts or arrays of objects
 */
export type FindManyResponse<T> = FindOneResponse<T>[] | number;
