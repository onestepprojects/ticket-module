import { RawChangeLogQuery, FindManyResponse, ChangeLogEntry } from '.';

// eslint-disable-next-line
interface HistoryDummy extends Partial<Omit<ChangeLogEntry,'user'>> {}

/**
 * User history
 */
export type HistoryResponse = FindManyResponse<HistoryDummy>;

export interface HistoryQueryParameters {
  /**
   * Comma-separated list of ticket Ids.
   * Restricts results to those related to given tickets.
   * @default null
   */
  ticketIds?: number[];
}

export interface RawHistoryQuery extends RawChangeLogQuery, Omit<HistoryQueryParameters, 'ticketIds'> {
  ticketIds?: string;
}
