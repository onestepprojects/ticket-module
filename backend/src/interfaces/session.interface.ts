import { PWhere } from '@/types';
import { Settings, Ticket, User } from '@prisma/client';
import { Session } from 'express-session';

export interface TicketSession extends Session {
  userId: User['id'];
  organizations: {
    id: string;
    name: string;
    // TODO: role types: 'ADMIN',...
    roles: string[];
  };
  where: PWhere<Ticket>;
  userOrgs: string[];
  settings: Settings;
}
