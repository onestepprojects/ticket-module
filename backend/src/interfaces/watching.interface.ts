/**
 * Data to bulk update watched tags/organizations
 * @examples require('./examples').exampleUpdateWatched
 */
export interface WatchingRequestBody {
  /** Organization Ids */
  orgIds?: {
    /**
     * Organization Ids to add
     *
     * @items {"type":"string","format":"uuid"}
     */
    add?: string[];
    /**
     * Organization Ids to remove
     *
     * @items {"type":"string","format":"uuid"}
     */
    remove?: string[];
  };
  /** Tag Ids */
  tagIds?: {
    /** Tag Ids to add */
    add?: number[];
    /** Tag Ids to remove */
    remove?: number[];
  };
}

/**
 * Response data containing Ids of organizations/tags being watched
 * @examples require('./examples').exampleWatchingResponse
 */
export interface WatchingResponse {
  /**
   * Organization Ids
   *
   * @items {"type":"string","format":"uuid"}
   */
  orgIds: string[];
  /**
   * Tag Ids
   * @items {"type":"integer"}
   */
  tagIds: number[];
}
