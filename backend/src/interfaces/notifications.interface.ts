import { CreateNotificationDto } from '@/dtos';
import { Notification, NotificationType } from '@prisma/client';
import { FindManyResponse } from '.';
import { BaseManyQuery } from './queries.interface';

/**
 * Notification data
 * @examples require('./examples').exampleNotifications
 */
export interface NotificationRequestBody extends CreateNotificationDto {
  /**
   * Notification type
   */
  type: NotificationType;

  /**
   * Message displayed to receivers of notification
   */
  message: string;

  /**
   * User Ids of users to notify
   * @items {"type":"string","format":"uuid"}
   */
  receivers: string[];

  /**
   * Id of sender or user who initiated event that spawned notification
   * @format uuid
   */
  senderId?: string;

  /**
   * Tag Ids added to new ticket
   *
   * When a ticket is created, users watching tags on new ticket
   * will be notified.
   */
  tags?: number[];

  /**
   * Organization Ids added to new ticket
   *
   * When a ticket is created, users who are members of/watching organizations
   * will be notified.
   *
   * @items {"type":"string","format":"uuid"}
   */
  organizations?: string[];

  /**
   * Id of ticket where notification originated during a ticket
   * update/status change/comment event.
   */
  ticketId?: number;
}

/**
 * Notification object
 * @examples require('./examples').exampleNotificationResponse
 */
export interface NotificationResponse extends Partial<Notification> {
  /**
   * True if notification has been read by receiver
   */
  read: boolean;
  /**
   * Id of ticket referenced by notification
   */
  ticketId: number;
  /**
   * Tag Ids referenced by notification
   * @items {"type":"integer"}
   */
  tags?: number[];
  /**
   * Organization Ids referenced by notification
   * @items {"type":"string","format":"uuid"}
   */
  organizations?: string[];
}

/**
 * Array of notification objects or count
 */
export type NotificationsResponse = FindManyResponse<NotificationResponse>;

/**
 * The number of notifications deleted
 */
export type DeleteReadNotificationsResponse = number;

type MarkType = 'all' | number[];

/**
 * Request to mark notifications as read/unread
 * @examples require('./examples').exampleMarkNotification
 */
export interface MarkNotificationRequestBody {
  /**
   * Notification Ids, or 'all', to mark as read.
   */
  read?: MarkType;
  /**
   * Notification Ids, or 'all', to mark as unread.
   */
  unread?: MarkType;
}

/**
 * Notification query parameters
 */
export interface NotificationQueryParameters {
  /**
   * Include notifications that have been read by current user
   * in results, if true.
   *
   * @default true
   */
  read?: boolean;

  /**
   * The maximum age (days previous from current date) notifications
   * to collect into results
   *
   * Valid days must be in range [1, 60].
   *
   * @TJS-title daysOld:days
   * @minimum 1
   * @maximum require('./examples').NOTIFICATIONS_MAX_AGE
   * @default 30
   */
  days?: number;
}

/**
 * Delete expired notifications query parameters
 */
export interface DeleteExpiredNotificationsQueryParameters {
  /**
   * The maximum age (days previous from current date) notifications
   * to keep.
   *
   * Valid days must be in range [1, 60].
   *
   * @TJS-title deleteDaysOld:days
   * @minimum 1
   * @maximum require('./examples').NOTIFICATIONS_MAX_AGE
   * @default require('./examples').NOTIFICATIONS_MAX_AGE
   */
  days?: number;
}

/**
 * Query parameters available to GET /notifications
 * @examples '?skip=10&take=10&days=60&read=false'
 */
export interface FindManyNotificationsQuery extends Omit<BaseManyQuery<Notification>, 'dateFilter'>, NotificationQueryParameters {}
