import { CreateCommentDto } from '@/dtos';
import { Comment } from '@prisma/client';
import { FindManyResponse, FindOneResponse } from '.';

/**
 * Comment data
 * @examples require('./examples').exampleComments
 */
export interface CommentRequestBody extends CreateCommentDto {
  /**
   * @$ref "#/components/schemas/User/properties/id"
   */
  userId: string;
  /**
   * Id of ticket being commented on
   */
  ticketId: number;
  /**
   * Comment to post to ticket
   * Markdown is supported with Github-flavored markdown extensions.
   *
   * See <a href='https://github.github.com/gfm'>GFM documentation</a> for
   * supported syntax.
   */
  comment: string;
}

/**
 * Comment object
 */
export type CommentResponse = FindOneResponse<Comment>;

/**
 * Array of comment objects or count
 */
export type CommentsResponse = FindManyResponse<Comment>;

/**
 * Fields available to select from returned comments
 */
export type CommentSelectField = keyof CommentResponse;

/**
 * Fields available to order returned comments by
 */
export type CommentOrderBy = `-${keyof CommentResponse}` | `${keyof CommentResponse}`;

/**
 * Query parameters for comment queries
 */
export interface CommentQueryParameters {
  /**
   * Select fields to collect in array of returned Comment objects.
   * @default null
   */
  select?: CommentSelectField[];

  /**
   * Order results by Comment column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-updatedAt"
   */
  orderBy?: CommentOrderBy;
}
