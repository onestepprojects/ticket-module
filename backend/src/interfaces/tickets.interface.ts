import { CreateTicketDto } from '@/dtos';
import { PWhere } from '@/types';
import { Ticket, TicketPriority, TicketStatus, TicketVisibility } from '@prisma/client';
import { AddRemove, FindManyResponse, FindOneResponse, RawFindManyQuery, SelectManyQuery } from '.';

/**
 * Ticket update status data
 * @examples require('./examples').exampleUpdateTicketStatus
 */
export interface TicketUpdateStatusRequestBody {
  /**
   * @$ref "#/components/schemas/Ticket/properties/status"
   */
  status: keyof typeof TicketStatus;
}

/**
 * Updaate ticket data
 * @examples require('./examples').exampleUpdateTickets
 */
export interface TicketUpdateRequestBody extends Partial<Pick<TicketRequestBody, 'title' | 'detail' | 'referralUrl' | 'priority' | 'visibility'>> {
  /**
   * Add or remove physician specialities from ticket
   */
  physicians?: AddRemove<string>;
  /**
   * Add or remove educator specialities from ticket
   */
  educators?: AddRemove<string>;
  /**
   * Add or remove topics specialities from ticket
   */
  topics?: AddRemove<string>;
  /**
   * Add or remove tags by Ids from ticket
   */
  tagIds?: AddRemove<number>;
  /**
   * Add or remove organization by Ids from ticket
   */
  organizationIds?: AddRemove<string>;
  /**
   * Add or remove linked tickets by Ids
   */
  linkedTo?: AddRemove<number>;
}

/**
 * Create ticket data
 * @examples require('./examples').exampleCreateTickets
 */
export interface TicketRequestBody extends Pick<Ticket, 'userId' | 'title' | 'detail'>, CreateTicketDto {
  /**
   * Id of user who created ticket
   *
   * @$ref "#/components/schemas/User/properties/id"
   */
  userId: string;

  /**
   * Ticket description
   *
   * Markdown is supported with Github-flavored markdown extensions.
   *
   * See <a href='https://github.github.com/gfm'>GFM documentation</a> for
   * supported syntax.
   */
  detail: string;

  /**
   * Referral URL
   * For example, a health ticket might have a referall URL linking
   * to the relevant case.
   *
   * @default null
   */
  referralUrl?: string;

  /**
   * @$ref "#/components/schemas/Ticket/properties/priority"
   */
  priority?: keyof typeof TicketPriority;

  /**
   * @$ref "#/components/schemas/Ticket/properties/visibility"
   */
  visibility?: keyof typeof TicketVisibility;

  /**
   * @ignore
   */
  files?: string[];

  /**
   * List of physician speciality names requested by or associated with
   * the ticket.  These must already exist in the database (case-insensitive).
   *
   * People with any of the given specialities, and who are subscribed to
   * receive notifications, will be notified when the ticket is created.
   */
  physicians?: string[];

  /**
   * List of education specilities requested by or associated with
   * the ticket.  These must already exist in the database (case-insensitive).
   *
   * People with any of the given specialities, and who are subscribed to
   * receive notifications, will be notified when the ticket is created.
   */
  educators?: string[];

  /**
   * List of topics associated with the ticket.
   * These must already exist in the database (case-insensitive).
   *
   * People watching any of the given topics, and who are subscribed to
   * receive notifications, will be notified when the ticket is created.
   */
  topics?: string[];

  /**
   * List of Tag Ids to attach to ticket.
   *
   * People watching any of the given tags, and who are subscribed to
   * receive notifications, will be notified when the ticket is created.
   */
  tagIds?: number[];

  /**
   * List of Organization Ids that the ticket belongs to.
   *
   * Members of the organizations will be able to see the ticket if
   * they are otherwise qualified.
   *
   * This field is required if ticket visibility is 'organization'.
   *
   * @items {"type":"string","format":"uuid"}
   */
  organizationIds?: string[];

  /**
   * List of related ticket Ids.  An association will be created linking
   * the tickets to this ticket.
   */
  linkedTo?: number[];
}

/**
 * Ticket relations that can be selected and returned in ticket request
 */
export const TicketRelationFieldEnum = {
  contributors: String,
  assignedTo: String,
  organizations: String,
  tags: Number,
  linkedTo: Number,
  linkedBy: Number,
  files: Number,
  changelog: Number,
};

/**
 * Arrays of Ids in ticket relations
 * @TJS-hide
 */
export interface TicketRelations {
  /**
   * Array of user ids who contfibuted to ticket
   * @items {"type":"string","format":"uuid"}
   */
  contributors?: string[];
  /**
   * Array of organization ids that ticket belongs to
   * @items {"type":"string","format":"uuid"}
   */
  organizations?: string[];
  /**
   * Array of user ids who are assigned to ticket
   * @items {"type":"string","format":"uuid"}
   */
  assignedTo?: string[];
  /**
   * Array of changelog ids for ticket
   * @items {"type":"integer"}
   */
  changelog?: number[];
  /**
   * Array of file ids attached to ticket
   * @items {"type":"integer"}
   */
  files?: number[];
  /**
   * Array of tag ids that ticket is tagged with
   * @items {"type":"integer"}
   */
  tags?: number[];
  /**
   * Array of ticket ids that ticket is linked to
   * @items {"type":"integer"}
   */
  linkedTo?: number[];
  /**
   * Array of ticket ids that linked ticket
   * @items {"type":"integer"}
   */
  linkedBy?: number[];
}

type TicketRelationCounts = {
  [K in keyof TicketRelations]: number;
};

/**
 * Detailed ticket object including ids of related records
 */
export interface TicketResponse extends Ticket, TicketRelations {
  /**
   * Counts of ticket relations
   */
  _count?: TicketRelationCounts;
}
type TicketResponseDummy = Omit<TicketResponse, '_count'>;

/**
 * Ticket object without relation ids
 */
export type SimpleTicketResponse = FindOneResponse<Ticket>;

/**
 * Array of Ticket Objects or count
 */
export type TicketsResponse = FindManyResponse<TicketResponseDummy>;

/**
 * Fields available to select in GET ticket routes.
 *
 * These fields include arrays of Ids for relations and relation counts,
 * in addition to scalar Ticket fields.
 */
export type TicketSelectField = keyof TicketResponse;

type TicketsOrderField = keyof Ticket;

/**
 * OrderBy fields available for Tickets.
 */
export type TicketsOrderBy = `-${TicketsOrderField}` | `${TicketsOrderField}`;

/**
 * Fields available to select in GET many tickets routes
 */
export type TicketsSelectField = keyof TicketResponseDummy;

/**
 * Ticket select type including arrays of Ids for relations, relation
 * counts, and scalar Ticket fields.
 * @TJS-ignore
 * @ignore
 */
export type TicketSelectWithRelations = { [K in keyof TicketResponseDummy]: boolean };

/**
 * Ticket select type including arrays of Ids for relations, relation
 * counts, and scalar Ticket fields.
 * @TJS-ignore
 * @ignore
 */
export type TicketSelectWithRelationsAndCount = { [K in keyof TicketResponse]: boolean };

/**
 * @TJS-ignore
 * @ignore
 */
export type TicketStatusList = `${TicketStatus}` | `${TicketStatus},${TicketStatus}` | `${TicketStatus},${TicketStatus},${TicketStatus}`;

/**
 * @TJS-ignore
 * @ignore
 */
export type TicketPriorityList =
  | `${TicketPriority}`
  | `${TicketPriority},${TicketPriority}`
  | `${TicketPriority},${TicketPriority},${TicketPriority}`;

/**
 * @TJS-ignore
 * @ignore
 */
export type TicketVisibilityList =
  | `${TicketVisibility}`
  | `${TicketVisibility},${TicketVisibility}`
  | `${TicketVisibility},${TicketVisibility},${TicketVisibility}`
  | `${TicketVisibility},${TicketVisibility},${TicketVisibility},${TicketVisibility}`;

/**
 * Query parameters unique to GET ticket routes
 */
export interface TicketQueryParameters {
  /**
   * Comma-separated list of ticket Ids.
   * Restricts results to those related to given tickets.
   *
   * @TJS-title ticketIds:ticketIds
   * @default null
   */
  ticketIds?: number[];

  /**
   * Select fields to collect in returned Ticket object.
   * When relations are selected, they are returned as arrays of
   * record Ids.
   *
   * @TJS-title ticketSelectWithRelation:select
   * @default null
   */
  selectWithRelation?: TicketsSelectField[];

  /**
   * Select fields to collect in returned Ticket object include counts of
   * relations. When relations are selected, they are returned as arrays of
   * record Ids.
   *
   * @TJS-title ticketSelectWithRelationAndCount:select
   * @default null
   */
  selectWithRelationAndCount?: TicketSelectField[];

  /**
   * Select fields to collect in array of returned Ticket objects.
   *
   * @default null
   */
  select?: TicketsSelectField[];

  /**
   * Order results by Ticket column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-updatedAt"
   */
  orderBy?: TicketsOrderBy;

  /**
   * Restrict results to tickets that have been assigned to at
   * least one person, if true, or tickets that have not been
   * assigned to anyone, if false.
   *
   * By default, results include both assigned and unassigned
   * tickets.
   *
   * @default null
   */
  isAssigned?: boolean;

  /**
   * A comma-separated list of ticket statuses.  Restricts search to
   * only tickets with status in given values.
   *
   * @default null
   */
  status?: TicketStatus[];

  /**
   * A comma-separated list of ticket priorities.  Restricts search to
   * only tickets with priority in given values.
   *
   * @default null
   */
  priority?: TicketPriority[];

  /**
   * A comma-separated list of ticket visibilities.  Restricts search to
   * only tickets with visibility in given values.
   *
   * @default null
   */
  visibility?: TicketVisibility[];

  /**
   * A comma-separated list of specialites.  Restricts search to tickets
   * marked with at least one of the provided specialites.
   *
   * @default null
   */
  speciality?: string[];

  /**
   * A comma-separated list of topics.  Restricts search to tickets
   * marked with at least one of the provided topics.
   *
   * @default null
   */
  topic?: string[];
}

/**
 * Query parameters available to GET many Ticket routes
 * @examples "?skip=0&take=50&assigned=true&status=open&priority=normal"
 */
export interface RawFindManyTicketsQuery
  extends Omit<TicketQueryParameters, 'status' | 'priority' | 'visibility' | 'ticketIds' | 'select' | 'orderBy' | 'selectWithRelation'>,
    RawFindManyQuery {
  status?: TicketStatusList;
  priority?: TicketPriorityList;
  visibility?: TicketVisibilityList;
}

export interface FindManyTicketsQuery extends Omit<SelectManyQuery<Ticket>, 'dateFilter'> {
  /**
   * Where query to filter collected results.
   * Created from parsed query parameters.
   *
   * @ignore
   */
  where?: PWhere<Ticket>;
}

type GroupByTicketsCount = 'all' | TicketsSelectField | null;

/**
 * Query parameters available to aggregate/groupBy tickets routes
 */
export interface GroupByTicketsQueryParameters {
  /**
   * Fields to group tickets by before applying aggregation operation.
   */
  by: TicketsSelectField[];

  /**
   * Count aggregation operator
   *
   * If empty or 'all', count all fields in <code>by</code>.
   * Otherwise, a comma-separated list of fields to count in aggregation.
   *
   * @default all
   */
  count: GroupByTicketsCount[];

  /**
   * Field to order results by, default ascending.
   * If prefixed by <code>-</code>, order by descending.
   *
   * @default null
   */
  orderBy?: TicketsOrderBy;

  /**
   * JSON-stringified Prisma where clause.
   *
   * @default null
   */
  where?: string;
}
