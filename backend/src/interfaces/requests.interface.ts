import { User } from '@prisma/client';
import { Request, RequestHandler } from 'express';
import { RawGroupByQuery, TicketSession } from '.';

export interface TRequest extends Request {
  userId?: string;
  session: TicketSession;
}

export interface TRequestWithUser extends TRequest {
  user: User;
}

export interface TQuery<T> extends RequestHandler {
  userId?: string;
  session?: TicketSession;
  query: T;
}

export type GroupByRequest = TQuery<RawGroupByQuery>;

export type AddRemove<T> = {
  add?: T[];
  remove?: T[];
};
