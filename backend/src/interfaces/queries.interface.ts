import {
  Models,
  PCountAggregateType,
  PInclude,
  POrderByWithAggregation,
  POrderByWithRelation,
  PRelations,
  PScalarFieldEnum,
  PSelect,
  PWhere,
  PWhereUnique,
} from '@types';
import { Prisma } from '@prisma/client';

/**
 * Pagination query parameters
 */
export interface PaginationQueryParameters {
  /**
   * The number of items to skip before collecting the result set (>= 0).
   *
   * @TJS-title skip:skip
   * @minimum 0
   * @default null
   */
  skip?: number;

  /**
   * The maximum number of items to collect into the result set (>= 0).
   *
   * @TJS-title take:take
   * @minimum 0
   * @default null
   */
  take?: number;
}

/**
 * Base query parameters for routes returning many results.
 * Supports pagination, count, date filtering, and orderBy.
 * @examples '?skip=0&take=50&orderBy=updatedAt'
 */
export interface BaseManyQueryParameters<T> {
  /**
   * Return only count of result set
   *
   * @TJS-title count:count
   * @TSJ-value boolean
   * @default null
   */
  count?: any;

  /**
   * Order the results array by column ascending/descending
   *
   * This query takes a column name, optionally
   * preceded by a '-' to indicate results should be sorted in descending order
   * instead of the default ascending order.
   *
   * The given column name must exist as a field in the database model of
   * he returned objects.
   *
   * @ignore
   */
  orderBy?: T;

  /**
   * Restrict results to those created after given date.
   *
   * Simplified extended ISO format (ISO 8601), ie. `Date.prototype.toIsoString`.
   *
   * @TJS-title createdAfter:createdAfter
   * @TJS-type string
   * @format date-time
   * @examples 2022-03-12T21:09:43.129Z
   * @default null
   */
  createdAfter?: Date;

  /**
   * Restrict results to those created before given date.
   *
   * Simplified extended ISO format (ISO 8601), ie. `Date.prototype.toIsoString`.
   *
   * @TJS-title createdBefore:createdBefore
   * @TJS-type string
   * @format date-time
   * @examples 2022-04-11T20:08:28.954Z
   * @default null
   */
  createdBefore?: Date;
}

/**
 * Query parameters for routes returning many results that allow selecting which
 * data fields should be collected into results.
 *
 * Available fields to select are dependent on the type of objects returned by route.
 */
export interface SelectManyQueryParameters<T extends Models> {
  /**
   * Select fields to collect in array of returned objects
   *
   * @ignore
   * @default null
   */
  select?: PSelect<T>[];

  /**
   * Select fields, including relations, to collect in returned objects.
   * When relations are selected, they are returned as arrays of record Ids.
   *
   * @ignore
   * @default null
   */
  selectWithRelation?: PSelect<T> & PRelations<T>[];

  /**
   * Include relation fields to collect in array of returned objects
   * Included relations are returned as arrays of Ids.
   *
   * The available relations must be present in the data model, and are
   * dependent on the type of object being returned.
   *
   * @examples 'tags,assignees'
   */
  // include?: string;
}

/**
 * Query parameters for routes returning many results.
 */
export interface FindManyQueryParameters<T extends Models> extends SelectManyQueryParameters<T> {
  /**
   * A JSON-stringified object of type Prisma.WhereUnique for the
   * object model being collected into the results set.
   *
   * @ignore
   * @default null
   */
  cursor?: PWhereUnique<T>;
  /**
   * A JSON-stringified object of type Prisma.Where for the object
   * model being collected in the results set.
   *
   * @ignore
   * @default null
   */
  where?: PWhere<T>;
}

// -------------------------------------------------------------------
/// Raw queries

export type RawPaginationQuery = {
  [K in keyof PaginationQueryParameters]?: string;
};
export type RawBaseManyQuery = RawPaginationQuery & {
  [K in keyof BaseManyQueryParameters<any>]?: string;
};

export type RawSelectManyQuery = RawBaseManyQuery & {
  [K in keyof SelectManyQueryParameters<any>]?: string;
};

export type RawFindManyQuery = RawSelectManyQuery & {
  [K in keyof FindManyQueryParameters<any>]?: string;
};

/**
 * Query parameters for group by/aggregation routes.
 * @ignore
 */
export interface RawGroupByQuery extends RawPaginationQuery {
  by: string;
  // ?count=all or ?count  both count all
  // ?count=a,b,c  counts fields [a,b,c]
  count?: 'all' | null | string;
  where?: string;
  orderBy?: string;
}

// -------------------------------------------------------------------
/// Parsed queries

/**
 * Parsed Pagination query parameters
 */
export type PaginationQuery = PaginationQueryParameters;

/**
 * Parsed base query parameters for routes returning many results.
 * Supports pagination, count, and orderBy.
 */
export interface BaseManyQuery<T extends Models, S = PSelect<T>> extends PaginationQuery {
  count?: any;
  /**
   * @ignore
   */
  orderBy?: Prisma.Subset<POrderByWithRelation<T>, S>;
  /**
   * A Prisma where query to filter results by creation dates, created from
   * input query parameters: createdBefore, createdAfter
   *
   * @ignore
   */
  dateFilter?: PWhere<T>;
}

/**
 * Parsed query parameters to select fields from results
 * @ignore
 */
// eslint-disable-next-line
export interface SelectQuery<T extends Models, S = PSelect<T>, I = Partial<PInclude<T>>> {
  /**
   * Prisma select query from parsed select string
   * @ignore
   */
  select?: S;
  /**
   * Prisma include query from parsed include string
   * @ignore
   */
  // include?: I;
}

/**
 * Parsed query parameters for routes returning many results that allow selecting
 * which data fields should be collected into results.
 *
 * Available fields to select are dependent on the type, T, of objects returned by route,
 * or the fields of type, S, if given.
 * @ignore
 */
export interface SelectManyQuery<T extends Models, S = PSelect<T>, I = PInclude<T>> extends BaseManyQuery<T, S>, SelectQuery<T, S, I> {}

/**
 * Parsed find many query
 */
export interface FindManyQuery<T extends Models, S = PSelect<T>, I = PInclude<T>> extends SelectManyQuery<T, S, I> {
  /**
   * Prisma where unique
   * @ignore
   */
  cursor?: PWhereUnique<T>;
  /**
   * Prisma where clauses
   * @ignore
   */
  where?: PWhere<T>;
}

// -------------------------------------------------------------------
/// GroupBy queries
// - cant't take 'select'
// - if take/skip are present, then orderBy must also be

/**
 * GroupBy aggregation operators
 *
 * Currently, only supporting <code>count</code>, but other
 * potential operators are <code>sum</code>, <code>avg</code>,
 * <code>min</code>, and <code>max</code>.
 */
export type GroupByActionType = 'count'; //|'sum'|'avg'|'min'|'max'

/**
 * @ignore
 */
interface GroupByNoTakeSkipQuery<T extends Models> {
  /**
   * @ignore
   */
  by: Array<PScalarFieldEnum<T>>;
  /**
   * @ignore
   */
  _count?: true | PCountAggregateType<T>;
  /**
   * @ignore
   */
  where?: PWhere<T>;
  /**
   * @ignore
   */
  orderBy?: Prisma.Enumerable<POrderByWithAggregation<T>>;
}

/**
 * @ignore
 */
export interface GroupByWithTakeSkipQuery<T extends Models> extends GroupByNoTakeSkipQuery<T>, PaginationQueryParameters {
  /**
   * @ignore
   */
  orderBy: Prisma.Enumerable<POrderByWithAggregation<T>>;
}

/**
 * @ignore
 */
export type GroupByQuery<T extends Models> = GroupByNoTakeSkipQuery<T> | GroupByWithTakeSkipQuery<T>;
