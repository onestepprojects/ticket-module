import { CreateNoteDto } from '@/dtos';
import { Note } from '@prisma/client';
import { FindManyResponse, FindOneResponse } from '.';

/**
 * Note Data
 */
export interface NoteRequestBody extends CreateNoteDto {
  /**
   * @$ref "#/components/schemas/User/properties/id"
   */
  userId: string;
  /**
   * Note body
   * Markdown is supported with Github-flavored markdown extensions.
   *
   * See <a href='https://github.github.com/gfm'>GFM documentation</a> for
   * supported syntax.
   */
  note: string;
  /**
   * Id of ticket to add note to
   */
  ticketId: number;
}

/**
 * Note object
 */
export type NoteResponse = FindOneResponse<Note>;

/**
 * Array of note objects or count
 */
export type NotesResponse = FindManyResponse<Note>;

/**
 * Fields available to select from returned notes
 */
export type NoteSelectField = keyof NoteResponse;

/**
 * Fields available to order returned notes by
 */
export type NoteOrderBy = `-${keyof NoteResponse}` | `${keyof NoteResponse}`;

/**
 * Query parameters for note queries
 */
export interface NoteQueryParameters {
  /**
   * Select fields to collect in array of returned Note objects.
   * @default null
   */
  select?: NoteSelectField[];

  /**
   * Order results by Note column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-updatedAt"
   */
  orderBy?: NoteOrderBy;
}
