/**
 * Query parameters available for the '/schema' route.
 */
export interface SchemaQueryParameters {
  /**
   * Return schemas for data models.
   *
   * If absent, or 'all', result is a JSON schema for all database models.
   *
   * Otherwise, a comma-separated, <b>case-sensitive</b> list of models, each of
   * which can be followed by a '.' separated path to restrict the results
   * to specified schema.
   *
   * Each element in the list takes the format (in simplified EBNF):
   * <pre>
   *   model ( '.' field )*
   * </pre>
   *
   * For example, to get the JSON-schema for Ticket priority:
   *   model=Ticket.properties.priority
   *
   * @default null
   * @TJS-type string
   * @examples require('./examples').exampleModelQueries
   */
  model?: string;

  /**
   * Return data model names only
   *
   * @default null
   */
  names?: any;
}
