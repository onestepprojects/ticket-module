import { RawFindManyTicketsQuery, TicketsResponse } from '.';

/**
 * Array of ticket objects or count
 */
export type SearchResponse = TicketsResponse;

/**
 * Query parameters available in /search ticket endpoints.
 */
export interface SearchTicketsQueryParameters {
  /**
   * Search ticket titles for matches
   *
   * @TJS-title searchTitle:title
   * @default null
   * @examples require('./examples').exampleSearchText
   */
  title?: string;

  /**
   * Search ticket details for matches
   *
   * @TJS-title searchDetail:detail
   * @default null
   * @examples require('./examples').exampleSearchText
   */
  detail?: string;

  /**
   * If true, then search text fields for literal matches.
   * Otherwise, treat query as extended full-text search.
   *
   * For further details of search DSL:
   * <ul>
   *   <li>
   *     <a href='https://www.prisma.io/docs/concepts/components/prisma-client/full-text-search'>prisma docs</a>
   *   </li>
   *   <li>
   *     <a href='https://www.postgresql.org/docs/12/functions-textsearch.html'>postgresql textsearch</a>
   *   </li>
   * </ul>
   *
   * @TJS-title searchLiteral:literal
   * @default null
   */
  literal?: any;

  /**
   * If defined, use case-sensitive matching when searching literally.
   * Otherwise, and by default, matching is case-insensitive.
   *
   * @TJS-title caseSensitive:caseSensitive
   * @default null
   */
  caseSensitive?: any;
}

/**
 * @ignore
 */
export interface RawSearchTicketsQuery extends RawFindManyTicketsQuery, SearchTicketsQueryParameters {}
