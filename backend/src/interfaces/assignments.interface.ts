/**
 * Assign or unassign current user to tickets.
 * @examples require('./examples').exampleSimpleAssign
 */
export interface SimpleAssignRequestBody {
  /**
   * Ticket Ids to un/assign to current logged in user
   */
  ticketIds: number[];
}

/**
 * Ids of tickets user is currently assigned
 * @examples require('./examples').exampleSimpleAssignResponse
 */
export interface SimpleAssignResponse {
  /**
   * Ids of tickets user is currently assigned to
   */
  ticketIds: number[];
}

// -------------------------------------------------------------------
/// Assign other users/groups of users to tickets
// Note: not using this currently

/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     assignmentBody:
 *       description: Assignment data
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Assignment'
 *
 *   responses:
 *     assignmentObject:
 *       description: Assignment object
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Assignment'
 *
 *     assignments:
 *       description: Array of Assignment objects
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/Assignment'
 */
