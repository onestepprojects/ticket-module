import { PWhere } from '@/types';
import { TicketChange, TicketChangeLog } from '@prisma/client';
import { BaseManyQuery, FindManyResponse, RawBaseManyQuery } from '.';

export interface ChangeLogEntry extends Omit<TicketChangeLog, 'user'> {
  /**
   * Username of user associated with the change
   *
   * @TJS-type string
   * @examples "Doretta Gore"
   */
  user: string;
}

/**
 * Array of changelog events or count
 * @examples require('./examples').exampleChangeLogResponse
 */
export type ChangeLogResponse = FindManyResponse<ChangeLogEntry>;

export type ChangeLogOrderBy = `-${keyof ChangeLogEntry}` | `${keyof ChangeLogEntry}`;

export interface ChangeLogQueryParameters {
  /**
   * Comma-separated list of changelog event types to collect
   * into the result set
   *
   * @default null
   */
  type?: TicketChange[];

  /**
   * Order results by ChangeLog column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-createdAt"
   */
  orderBy?: ChangeLogOrderBy;
}

/**
 * Query parameters available to GET many Tag routes
 * @examples "'?skip=0&take=50&type=topic,educator'"
 */
export interface RawChangeLogQuery extends RawBaseManyQuery, Omit<ChangeLogQueryParameters, 'orderBy'> {}

export interface ChangeLogQuery extends Omit<BaseManyQuery<TicketChangeLog>, 'dateFilter'> {
  /**
   * @ignore
   */
  where?: PWhere<TicketChangeLog>;
}
