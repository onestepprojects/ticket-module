import { User } from '@prisma/client';
import { FindManyResponse } from '.';

/**
 * User data
 * @examples require('./examples').exampleCreateUsers
 */
export type UserRequestBody = Pick<User, 'id' | 'email' | 'password'>;

/**
 * User scalar fields that can be selected and returned
 */
export const UserScalarFieldEnum = {
  id: 'id',
  name: 'name',
  email: 'email',
  isAdmin: 'isAdmin',
};

/**
 * User relations that can be selected and returned
 */
export const UserRelationFieldEnum = {
  subscriptions: Number,
  watchedOrgs: String,
  watchedTags: Number,
  ticketsFor: Number,
  ticketsBy: Number,
  activity: Number,
  contributedTo: Number,
};

export interface UserRelations {
  /**
   * Array of ticket Ids user is subscribed to
   */
  subscriptions?: number[];
  /**
   * Array of organization Ids user is watching
   * @items {"type":"string","format":"uuid"}
   */
  watchedOrgs?: string[];
  /**
   * Array of tag Ids user is watching
   */
  watchedTags?: number[];
  /**
   * Array of ticket Ids that user is assigned to
   */
  ticketsFor?: number[];
  /**
   * Array of ticket Ids that user created
   */
  ticketsBy?: number[];
  /**
   * Array of changelog Ids associated with changes user initiated
   */
  activity?: number[];
  /**
   * Array of ticket Ids that user contributed to
   */
  contributedTo?: number[];
}

// eslint-disable-next-line
interface UserScalarResult extends Partial<Omit<User,'password'>> {}

/**
 * User object
 */
export interface UserResponse extends UserScalarResult, UserRelations {}

/**
 * Fields available to select from routes returning user objects.
 */
export type UserSelectField = keyof UserScalarResult;

/**
 * User select type including arrays of Ids for relations, relation
 * counts, and scalar User fields.
 * @TJS-ignore
 * @ignore
 */
export type UserSelectWithRelations = { [K in keyof UserResponse]: boolean };

/**
 * OrderBy fields available for Users.
 */
export type UserOrderBy = `-${UserSelectField}` | `${UserSelectField}`;

/**
 * Array of user objects or count
 */
export type UsersResponse = FindManyResponse<UserScalarResult>;

/**
 * Query parameters for GET users requests
 */
export interface UserQueryParameters {
  /**
   * Select fields to collect in returned User objects.
   * When relations are selected, they are returned as arrays of
   * record Ids.
   *
   * @default null
   */
  select?: UserSelectField[];

  /**
   * Order results by User column
   *
   * Default ordering is ascending. If column name is prefixed by '-',
   * order descending instead.
   *
   * @TJS-type string
   * @default null
   * @examples "-name"
   */
  orderBy?: UserOrderBy;
}
