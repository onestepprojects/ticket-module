---
components:
  parameters:
    commentOrderBy:
      default: 
      description: |-
        Order results by Comment column

        Default ordering is ascending. If column name is prefixed by '-',
        order descending instead.
      example: "-updatedAt"
      in: query
      required: false
      name: orderBy
      schema:
        $ref: "#/components/definitions/CommentOrderBy"
    commentSelect:
      default: 
      description: Select fields to collect in array of returned Comment objects.
      in: query
      required: false
      name: select
      schema:
        type: array
        items:
          enum:
          - comment
          - createdAt
          - id
          - ticketId
          - updatedAt
          - userId
          type: string
      style: form
      explode: false
  requestBodies:
    commentBody:
      description: Comment data
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/CommentRequestBody"
          examples:
            ticketComment:
              $ref: "#/components/examples/ticketComment"
  responses:
    commentResponse:
      description: Comment object
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/CommentResponse"
          examples: 
    commentsResponse:
      description: Array of comment objects or count
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/CommentsResponse"
          examples: 
  examples:
    ticketComment:
      summary: Comment on a ticket
      value:
        comment: This is the body of the comment. **Markdown** will be rendered by
          the ticket UI.
        ticketId: 1
        userId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
  definitions:
    CommentOrderBy:
      description: Fields available to order returned comments by
      enum:
      - "-comment"
      - "-createdAt"
      - "-id"
      - "-ticketId"
      - "-updatedAt"
      - "-userId"
      - comment
      - createdAt
      - id
      - ticketId
      - updatedAt
      - userId
      type: string
    CommentRequestBody:
      description: Comment data
      examples:
        ticketComment:
          $ref: "#/components/examples/ticketComment"
      properties:
        comment:
          description: |-
            Comment to post to ticket
            Markdown is supported with Github-flavored markdown extensions.

            See <a href='https://github.github.com/gfm'>GFM documentation</a> for
            supported syntax.
          type: string
        ticketId:
          description: Id of ticket being commented on
          type: integer
        userId:
          $ref: "#/components/schemas/User/properties/id"
          type: string
      required:
      - comment
      - ticketId
      - userId
      type: object
    CommentResponse:
      description: Comment object
      properties:
        comment:
          type: string
        createdAt:
          format: date-time
          type: string
        id:
          type: integer
        ticketId:
          type: integer
        updatedAt:
          format: date-time
          type: string
        userId:
          type: string
      type: object
    CommentSelectField:
      enum:
      - comment
      - createdAt
      - id
      - ticketId
      - updatedAt
      - userId
      type: string
    CommentsResponse:
      anyOf:
      - items:
          $ref: "#/components/definitions/CommentObject"
        type: array
      - type: integer
      description: Array of comment objects or count
    CommentObject:
      properties:
        comment:
          type: string
        createdAt:
          format: date-time
          type: string
        id:
          type: integer
        ticketId:
          type: integer
        updatedAt:
          format: date-time
          type: string
        userId:
          type: string
      type: object
