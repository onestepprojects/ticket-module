---
components:
  parameters:
    groupByTicketsBy:
      description: Fields to group tickets by before applying aggregation operation.
      in: query
      required: false
      default: 
      name: by
      schema:
        type: array
        items:
          enum:
          - assignedTo
          - changelog
          - contributors
          - createdAt
          - detail
          - files
          - id
          - linkedBy
          - linkedTo
          - organizations
          - points
          - priority
          - referralUrl
          - status
          - tags
          - title
          - updatedAt
          - userId
          - visibility
          type: string
      style: form
      explode: false
    groupByTicketsCount:
      default: all
      description: |-
        Count aggregation operator

        If empty or 'all', count all fields in <code>by</code>.
        Otherwise, a comma-separated list of fields to count in aggregation.
      in: query
      required: false
      name: count
      schema:
        type: array
        items:
          enum:
          - all
          - assignedTo
          - changelog
          - contributors
          - createdAt
          - detail
          - files
          - id
          - linkedBy
          - linkedTo
          - organizations
          - points
          - priority
          - referralUrl
          - status
          - tags
          - title
          - updatedAt
          - userId
          - visibility
          type: string
      style: form
      explode: false
    groupByTicketsOrderBy:
      default: 
      description: |-
        Field to order results by, default ascending.
        If prefixed by <code>-</code>, order by descending.
      in: query
      required: false
      name: orderBy
      schema:
        $ref: "#/components/definitions/TicketsOrderBy"
    groupByTicketsWhere:
      default: 
      description: JSON-stringified Prisma where clause.
      in: query
      required: false
      name: where
      schema:
        type: string
    ticketIsAssigned:
      default: 
      description: |-
        Restrict results to tickets that have been assigned to at
        least one person, if true, or tickets that have not been
        assigned to anyone, if false.

        By default, results include both assigned and unassigned
        tickets.
      in: query
      required: false
      name: isAssigned
      schema:
        type: boolean
    ticketOrderBy:
      default: 
      description: |-
        Order results by Ticket column

        Default ordering is ascending. If column name is prefixed by '-',
        order descending instead.
      example: "-updatedAt"
      in: query
      required: false
      name: orderBy
      schema:
        $ref: "#/components/definitions/TicketsOrderBy"
    ticketPriority:
      default: 
      description: |-
        A comma-separated list of ticket priorities.  Restricts search to
        only tickets with priority in given values.
      in: query
      required: false
      name: priority
      schema:
        type: array
        items:
          $ref: "#/components/definitions/TicketPriority"
      style: form
      explode: false
    ticketSelect:
      default: 
      description: Select fields to collect in array of returned Ticket objects.
      in: query
      required: false
      name: select
      schema:
        type: array
        items:
          enum:
          - assignedTo
          - changelog
          - contributors
          - createdAt
          - detail
          - files
          - id
          - linkedBy
          - linkedTo
          - organizations
          - points
          - priority
          - referralUrl
          - status
          - tags
          - title
          - updatedAt
          - userId
          - visibility
          type: string
      style: form
      explode: false
    ticketSelectWithRelation:
      default: 
      description: |-
        Select fields to collect in returned Ticket object.
        When relations are selected, they are returned as arrays of
        record Ids.
      in: query
      required: false
      name: select
      schema:
        type: array
        items:
          enum:
          - assignedTo
          - changelog
          - contributors
          - createdAt
          - detail
          - files
          - id
          - linkedBy
          - linkedTo
          - organizations
          - points
          - priority
          - referralUrl
          - status
          - tags
          - title
          - updatedAt
          - userId
          - visibility
          type: string
      style: form
      explode: false
    ticketSelectWithRelationAndCount:
      default: 
      description: |-
        Select fields to collect in returned Ticket object include counts of
        relations. When relations are selected, they are returned as arrays of
        record Ids.
      in: query
      required: false
      name: select
      schema:
        type: array
        items:
          enum:
          - _count
          - assignedTo
          - changelog
          - contributors
          - createdAt
          - detail
          - files
          - id
          - linkedBy
          - linkedTo
          - organizations
          - points
          - priority
          - referralUrl
          - status
          - tags
          - title
          - updatedAt
          - userId
          - visibility
          type: string
      style: form
      explode: false
    ticketSpeciality:
      default: 
      description: |-
        A comma-separated list of specialites.  Restricts search to tickets
        marked with at least one of the provided specialites.
      in: query
      required: false
      name: speciality
      schema:
        type: array
        items:
          type: string
      style: form
      explode: false
    ticketStatus:
      default: 
      description: |-
        A comma-separated list of ticket statuses.  Restricts search to
        only tickets with status in given values.
      in: query
      required: false
      name: status
      schema:
        type: array
        items:
          $ref: "#/components/definitions/TicketStatus"
      style: form
      explode: false
    ticketIds:
      default: 
      description: |-
        Comma-separated list of ticket Ids.
        Restricts results to those related to given tickets.
      in: query
      required: false
      name: ticketIds
      schema:
        type: array
        items:
          type: integer
      style: form
      explode: false
    ticketTopic:
      default: 
      description: |-
        A comma-separated list of topics.  Restricts search to tickets
        marked with at least one of the provided topics.
      in: query
      required: false
      name: topic
      schema:
        type: array
        items:
          type: string
      style: form
      explode: false
    ticketVisibility:
      default: 
      description: |-
        A comma-separated list of ticket visibilities.  Restricts search to
        only tickets with visibility in given values.
      in: query
      required: false
      name: visibility
      schema:
        type: array
        items:
          $ref: "#/components/definitions/TicketVisibility"
      style: form
      explode: false
  requestBodies:
    ticketBody:
      description: Create ticket data
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/TicketRequestBody"
          examples:
            educationTicket:
              $ref: "#/components/examples/educationTicket"
            healthTicket:
              $ref: "#/components/examples/healthTicket"
    ticketUpdateBody:
      description: Updaate ticket data
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/TicketUpdateRequestBody"
          examples:
            updateTicketBody:
              $ref: "#/components/examples/updateTicketBody"
            updateTicketLinkedTo:
              $ref: "#/components/examples/updateTicketLinkedTo"
            updateTicketOrganizations:
              $ref: "#/components/examples/updateTicketOrganizations"
            updateTicketSpecialities:
              $ref: "#/components/examples/updateTicketSpecialities"
    ticketUpdateStatusBody:
      description: Ticket update status data
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/TicketUpdateStatusRequestBody"
          examples:
            completeTicket:
              $ref: "#/components/examples/completeTicket"
            reopenTicket:
              $ref: "#/components/examples/reopenTicket"
  responses:
    simpleTicketResponse:
      description: Ticket object without relation ids
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/SimpleTicketResponse"
          examples: 
    ticketResponse:
      description: Detailed ticket object including ids of related records
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/TicketResponse"
          examples: 
    ticketsResponse:
      description: Array of Ticket Objects or count
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/TicketsResponse"
          examples: 
  examples:
    educationTicket:
      summary: Ticket tagged with educator specialities
      value:
        detail: This is the body of the ticket. **Markdown** will be rendered by the
          ticket UI.
        educators:
        - mathematics
        - computer science
        organizationIds:
        - 1c8368f5-4250-4d86-8fcf-0f12c48cd586
        priority: low
        title: Education ticket example
        topics: []
        userId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        visibility: organization
    healthTicket:
      summary: Ticket tagged with physician specialities
      value:
        detail: This is the body of the ticket. **Markdown** will be rendered by the
          ticket UI.
        organizationIds:
        - 1c8368f5-4250-4d86-8fcf-0f12c48cd586
        physicians:
        - cardiology
        - hematology
        priority: low
        title: Health ticket example
        topics: []
        userId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        visibility: organization
    updateTicketBody:
      summary: Update ticket title/details
      value:
        detail: Some new description
        title: Some new title
    updateTicketLinkedTo:
      summary: Update linked tickets
      value:
        linkedTo:
          remove:
          - 1
    updateTicketOrganizations:
      summary: Update ticket organization
      value:
        organizationIds:
          add:
          - 1c8368f5-4250-4d86-8fcf-0f12c48cd586
    updateTicketSpecialities:
      summary: Update ticket specialities
      value:
        educators:
          add:
          - computer science
          remove:
          - mathematics
        physicians:
          add:
          - obgyn
          remove:
          - cardiologist
    completeTicket:
      summary: Mark ticket completed
      value:
        status: completed
    reopenTicket:
      summary: Re-open ticket
      value:
        status: open
  definitions:
    GroupByTicketsCount:
      enum:
      - all
      - assignedTo
      - changelog
      - contributors
      - createdAt
      - detail
      - files
      - id
      - linkedBy
      - linkedTo
      - organizations
      - points
      - priority
      - referralUrl
      - status
      - tags
      - title
      - updatedAt
      - userId
      - visibility
      type: string
    TicketResponseObject:
      properties:
        assignedTo:
          description: Array of user ids who are assigned to ticket
          items:
            format: uuid
            type: string
          type: array
        changelog:
          description: Array of changelog ids for ticket
          items:
            type: integer
          type: array
        contributors:
          description: Array of user ids who contfibuted to ticket
          items:
            format: uuid
            type: string
          type: array
        createdAt:
          description: Date-time when ticket was created
          format: date-time
          type: string
        detail:
          description: Ticket description/details
          type: string
        files:
          description: Array of file ids attached to ticket
          items:
            type: integer
          type: array
        id:
          type: integer
        linkedBy:
          description: Array of ticket ids that linked ticket
          items:
            type: integer
          type: array
        linkedTo:
          description: Array of ticket ids that ticket is linked to
          items:
            type: integer
          type: array
        organizations:
          description: Array of organization ids that ticket belongs to
          items:
            format: uuid
            type: string
          type: array
        points:
          description: Reward points for resolving ticket
          type: integer
        priority:
          $ref: "#/components/definitions/TicketPriority"
          description: Ticket priority
        referralUrl:
          description: Referral url, eg. a link to the health case referencing ticket
          type: string
        status:
          $ref: "#/components/definitions/TicketStatus"
          description: Ticket status
        tags:
          description: Array of tag ids that ticket is tagged with
          items:
            type: integer
          type: array
        title:
          description: Ticket title
          type: string
        updatedAt:
          description: Date-time when ticket was last updated
          format: date-time
          type: string
        userId:
          description: UserId who created ticket
          type: string
        visibility:
          $ref: "#/components/definitions/TicketVisibility"
          description: 'Ticket visibility (TODO: determine possible visibilities)'
      type: object
    SimpleTicketResponse:
      description: Ticket object without relation ids
      properties:
        createdAt:
          description: Date-time when ticket was created
          format: date-time
          type: string
        detail:
          description: Ticket description/details
          type: string
        id:
          type: integer
        points:
          description: Reward points for resolving ticket
          type: integer
        priority:
          $ref: "#/components/definitions/TicketPriority"
          description: Ticket priority
        referralUrl:
          description: Referral url, eg. a link to the health case referencing ticket
          type: string
        status:
          $ref: "#/components/definitions/TicketStatus"
          description: Ticket status
        title:
          description: Ticket title
          type: string
        updatedAt:
          description: Date-time when ticket was last updated
          format: date-time
          type: string
        userId:
          description: UserId who created ticket
          type: string
        visibility:
          $ref: "#/components/definitions/TicketVisibility"
          description: 'Ticket visibility (TODO: determine possible visibilities)'
      type: object
    TicketPriority:
      enum:
      - high
      - low
      - normal
      type: string
    TicketRelationCounts:
      properties:
        assignedTo:
          description: Array of user ids who are assigned to ticket
          items:
            format: uuid
            type: string
          type: integer
        changelog:
          description: Array of changelog ids for ticket
          items:
            type: integer
          type: integer
        contributors:
          description: Array of user ids who contfibuted to ticket
          items:
            format: uuid
            type: string
          type: integer
        files:
          description: Array of file ids attached to ticket
          items:
            type: integer
          type: integer
        linkedBy:
          description: Array of ticket ids that linked ticket
          items:
            type: integer
          type: integer
        linkedTo:
          description: Array of ticket ids that ticket is linked to
          items:
            type: integer
          type: integer
        organizations:
          description: Array of organization ids that ticket belongs to
          items:
            format: uuid
            type: string
          type: integer
        tags:
          description: Array of tag ids that ticket is tagged with
          items:
            type: integer
          type: integer
      type: object
    TicketRequestBody:
      description: Create ticket data
      examples:
        educationTicket:
          $ref: "#/components/examples/educationTicket"
        healthTicket:
          $ref: "#/components/examples/healthTicket"
      properties:
        detail:
          description: |-
            Ticket description

            Markdown is supported with Github-flavored markdown extensions.

            See <a href='https://github.github.com/gfm'>GFM documentation</a> for
            supported syntax.
          type: string
        title:
          description: Ticket title
          type: string
        userId:
          $ref: "#/components/schemas/User/properties/id"
          description: Id of user who created ticket
          type: string
        educators:
          description: |-
            List of education specilities requested by or associated with
            the ticket.  These must already exist in the database (case-insensitive).

            People with any of the given specialities, and who are subscribed to
            receive notifications, will be notified when the ticket is created.
          items:
            type: string
          type: array
        linkedTo:
          description: |-
            List of related ticket Ids.  An association will be created linking
            the tickets to this ticket.
          items:
            type: integer
          type: array
        organizationIds:
          description: |-
            List of Organization Ids that the ticket belongs to.

            Members of the organizations will be able to see the ticket if
            they are otherwise qualified.

            This field is required if ticket visibility is 'organization'.
          items:
            format: uuid
            type: string
          type: array
        physicians:
          description: |-
            List of physician speciality names requested by or associated with
            the ticket.  These must already exist in the database (case-insensitive).

            People with any of the given specialities, and who are subscribed to
            receive notifications, will be notified when the ticket is created.
          items:
            type: string
          type: array
        priority:
          $ref: "#/components/schemas/Ticket/properties/priority"
          enum:
          - high
          - low
          - normal
          type: string
        referralUrl:
          default: 
          description: |-
            Referral URL
            For example, a health ticket might have a referall URL linking
            to the relevant case.
          type: string
        tagIds:
          description: |-
            List of Tag Ids to attach to ticket.

            People watching any of the given tags, and who are subscribed to
            receive notifications, will be notified when the ticket is created.
          items:
            type: integer
          type: array
        topics:
          description: |-
            List of topics associated with the ticket.
            These must already exist in the database (case-insensitive).

            People watching any of the given topics, and who are subscribed to
            receive notifications, will be notified when the ticket is created.
          items:
            type: string
          type: array
        visibility:
          $ref: "#/components/schemas/Ticket/properties/visibility"
          enum:
          - organization
          - private
          - public
          - restricted
          type: string
      required:
      - detail
      - title
      - userId
      type: object
    TicketResponse:
      description: Detailed ticket object including ids of related records
      properties:
        _count:
          $ref: "#/components/definitions/TicketRelationCounts"
          description: Counts of ticket relations
        assignedTo:
          description: Array of user ids who are assigned to ticket
          items:
            format: uuid
            type: string
          type: array
        changelog:
          description: Array of changelog ids for ticket
          items:
            type: integer
          type: array
        contributors:
          description: Array of user ids who contfibuted to ticket
          items:
            format: uuid
            type: string
          type: array
        createdAt:
          description: Date-time when ticket was created
          format: date-time
          type: string
        detail:
          description: Ticket description/details
          type: string
        files:
          description: Array of file ids attached to ticket
          items:
            type: integer
          type: array
        id:
          type: integer
        linkedBy:
          description: Array of ticket ids that linked ticket
          items:
            type: integer
          type: array
        linkedTo:
          description: Array of ticket ids that ticket is linked to
          items:
            type: integer
          type: array
        organizations:
          description: Array of organization ids that ticket belongs to
          items:
            format: uuid
            type: string
          type: array
        points:
          description: Reward points for resolving ticket
          type: integer
        priority:
          $ref: "#/components/definitions/TicketPriority"
          description: Ticket priority
        referralUrl:
          description: Referral url, eg. a link to the health case referencing ticket
          type: string
        status:
          $ref: "#/components/definitions/TicketStatus"
          description: Ticket status
        tags:
          description: Array of tag ids that ticket is tagged with
          items:
            type: integer
          type: array
        title:
          description: Ticket title
          type: string
        updatedAt:
          description: Date-time when ticket was last updated
          format: date-time
          type: string
        userId:
          description: UserId who created ticket
          type: string
        visibility:
          $ref: "#/components/definitions/TicketVisibility"
          description: 'Ticket visibility (TODO: determine possible visibilities)'
      type: object
    TicketSelectWithRelationsAndCount:
      description: |-
        Ticket select type including arrays of Ids for relations, relation
        counts, and scalar Ticket fields.
      ignore: ''
      properties:
        _count:
          description: Counts of ticket relations
          type: boolean
        assignedTo:
          description: Array of user ids who are assigned to ticket
          items:
            format: uuid
            type: string
          type: boolean
        changelog:
          description: Array of changelog ids for ticket
          items:
            type: integer
          type: boolean
        contributors:
          description: Array of user ids who contfibuted to ticket
          items:
            format: uuid
            type: string
          type: boolean
        createdAt:
          description: Date-time when ticket was created
          type: boolean
        detail:
          description: Ticket description/details
          type: boolean
        files:
          description: Array of file ids attached to ticket
          items:
            type: integer
          type: boolean
        id:
          type: boolean
        linkedBy:
          description: Array of ticket ids that linked ticket
          items:
            type: integer
          type: boolean
        linkedTo:
          description: Array of ticket ids that ticket is linked to
          items:
            type: integer
          type: boolean
        organizations:
          description: Array of organization ids that ticket belongs to
          items:
            format: uuid
            type: string
          type: boolean
        points:
          description: Reward points for resolving ticket
          type: boolean
        priority:
          description: Ticket priority
          type: boolean
        referralUrl:
          description: Referral url, eg. a link to the health case referencing ticket
          type: boolean
        status:
          description: Ticket status
          type: boolean
        tags:
          description: Array of tag ids that ticket is tagged with
          items:
            type: integer
          type: boolean
        title:
          description: Ticket title
          type: boolean
        updatedAt:
          description: Date-time when ticket was last updated
          type: boolean
        userId:
          description: UserId who created ticket
          type: boolean
        visibility:
          description: 'Ticket visibility (TODO: determine possible visibilities)'
          type: boolean
      required:
      - createdAt
      - detail
      - id
      - points
      - priority
      - referralUrl
      - status
      - title
      - updatedAt
      - userId
      - visibility
      type: object
    TicketStatus:
      enum:
      - closed
      - completed
      - open
      type: string
    TicketUpdateRequestBody:
      description: Updaate ticket data
      examples:
        updateTicketBody:
          $ref: "#/components/examples/updateTicketBody"
        updateTicketLinkedTo:
          $ref: "#/components/examples/updateTicketLinkedTo"
        updateTicketOrganizations:
          $ref: "#/components/examples/updateTicketOrganizations"
        updateTicketSpecialities:
          $ref: "#/components/examples/updateTicketSpecialities"
      properties:
        detail:
          description: |-
            Ticket description

            Markdown is supported with Github-flavored markdown extensions.

            See <a href='https://github.github.com/gfm'>GFM documentation</a> for
            supported syntax.
          type: string
        educators:
          description: Add or remove educator specialities from ticket
          properties:
            add:
              items:
                type: string
              type: array
            remove:
              items:
                type: string
              type: array
          type: object
        linkedTo:
          description: Add or remove linked tickets by Ids
          properties:
            add:
              items:
                type: integer
              type: array
            remove:
              items:
                type: integer
              type: array
          type: object
        organizationIds:
          description: Add or remove organization by Ids from ticket
          properties:
            add:
              items:
                type: string
              type: array
            remove:
              items:
                type: string
              type: array
          type: object
        physicians:
          description: Add or remove physician specialities from ticket
          properties:
            add:
              items:
                type: string
              type: array
            remove:
              items:
                type: string
              type: array
          type: object
        priority:
          $ref: "#/components/schemas/Ticket/properties/priority"
          enum:
          - high
          - low
          - normal
          type: string
        referralUrl:
          default: 
          description: |-
            Referral URL
            For example, a health ticket might have a referall URL linking
            to the relevant case.
          type: string
        tagIds:
          description: Add or remove tags by Ids from ticket
          properties:
            add:
              items:
                type: integer
              type: array
            remove:
              items:
                type: integer
              type: array
          type: object
        title:
          description: Ticket title
          type: string
        topics:
          description: Add or remove topics specialities from ticket
          properties:
            add:
              items:
                type: string
              type: array
            remove:
              items:
                type: string
              type: array
          type: object
        visibility:
          $ref: "#/components/schemas/Ticket/properties/visibility"
          enum:
          - organization
          - private
          - public
          - restricted
          type: string
      type: object
    TicketUpdateStatusRequestBody:
      description: Ticket update status data
      examples:
        completeTicket:
          $ref: "#/components/examples/completeTicket"
        reopenTicket:
          $ref: "#/components/examples/reopenTicket"
      properties:
        status:
          $ref: "#/components/schemas/Ticket/properties/status"
          enum:
          - closed
          - completed
          - open
          type: string
      required:
      - status
      type: object
    TicketVisibility:
      enum:
      - organization
      - private
      - public
      - restricted
      type: string
    TicketsOrderBy:
      description: OrderBy fields available for Tickets.
      enum:
      - "-createdAt"
      - "-detail"
      - "-id"
      - "-points"
      - "-priority"
      - "-referralUrl"
      - "-status"
      - "-title"
      - "-updatedAt"
      - "-userId"
      - "-visibility"
      - createdAt
      - detail
      - id
      - points
      - priority
      - referralUrl
      - status
      - title
      - updatedAt
      - userId
      - visibility
      type: string
    TicketsResponse:
      anyOf:
      - items:
          $ref: "#/components/definitions/TicketResponseObject"
        type: array
      - type: integer
      description: Array of Ticket Objects or count
