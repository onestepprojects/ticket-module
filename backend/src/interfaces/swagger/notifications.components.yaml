---
components:
  parameters:
    deleteDaysOld:
      description: |-
        The maximum age (days previous from current date) notifications
        to keep.

        Valid days must be in range [1, 60].
      in: query
      required: false
      name: days
      schema:
        type: integer
        minimum: 1
        maximum: 60
        default: 60
    daysOld:
      description: |-
        The maximum age (days previous from current date) notifications
        to collect into results

        Valid days must be in range [1, 60].
      in: query
      required: false
      name: days
      schema:
        type: integer
        minimum: 1
        maximum: 60
        default: 30
    notificationRead:
      description: |-
        Include notifications that have been read by current user
        in results, if true.
      in: query
      required: false
      name: read
      schema:
        type: boolean
        default: true
  requestBodies:
    markNotificationBody:
      description: Request to mark notifications as read/unread
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/MarkNotificationRequestBody"
          examples:
            markSomeNotifications:
              $ref: "#/components/examples/markSomeNotifications"
            readAllNotifications:
              $ref: "#/components/examples/readAllNotifications"
            unreadAllNotifications:
              $ref: "#/components/examples/unreadAllNotifications"
    notificationBody:
      description: Notification data
      required: true
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/NotificationRequestBody"
          examples:
            ticketCommentNotification:
              $ref: "#/components/examples/ticketCommentNotification"
            ticketCreatedNotification:
              $ref: "#/components/examples/ticketCreatedNotification"
            ticketStatusChangeNotification:
              $ref: "#/components/examples/ticketStatusChangeNotification"
            ticketUpdatedNotification:
              $ref: "#/components/examples/ticketUpdatedNotification"
  responses:
    deleteReadNotificationsResponse:
      description: The number of notifications deleted
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/DeleteReadNotificationsResponse"
          examples: 
    notificationResponse:
      description: Notification object
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/NotificationResponse"
          examples:
            newTicketNotification:
              $ref: "#/components/examples/newTicketNotification"
    notificationsResponse:
      description: Array of notification objects or count
      content:
        application/json:
          schema:
            $ref: "#/components/definitions/NotificationsResponse"
          examples: 
  examples:
    markSomeNotifications:
      summary: Mark some notifications as read
      value:
        read:
        - 1
        - 2
        - 3
    readAllNotifications:
      summary: Mark all notifications as read
      value:
        read: all
    unreadAllNotifications:
      summary: Mark all notifications as read
      value:
        unread: all
    ticketCommentNotification:
      summary: Ticket commented on
      value:
        message: This is a new comment about...
        senderId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        ticketId: 1
        type: ticketComment
    ticketCreatedNotification:
      summary: New ticket created
      value:
        message: New ticket
        organizations:
        - 1c8368f5-4250-4d86-8fcf-0f12c48cd586
        senderId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        tags:
        - 1
        - 2
        ticketId: 1
        type: ticketCreate
    ticketStatusChangeNotification:
      summary: Ticket status changed
      value:
        message: assign
        senderId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        ticketId: 1
        type: ticketStatusChange
    ticketUpdatedNotification:
      summary: Ticket updated
      value:
        message: Ticket updated
        senderId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        ticketId: 1
        type: ticketUpdate
    newTicketNotification:
      summary: New ticket with tag
      value:
        createdAt: '2022-04-06T01:15:51.589Z'
        id: 1
        message: New ticket
        organizations: 
        read: false
        senderId: 6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f
        tags:
        - 1
        ticketId: 1
        type: ticketCreate
        updatedAt: '2022-04-06T01:15:51.589Z'
  definitions:
    DeleteReadNotificationsResponse:
      description: The number of notifications deleted
      type: integer
    MarkNotificationRequestBody:
      description: Request to mark notifications as read/unread
      examples:
        markSomeNotifications:
          $ref: "#/components/examples/markSomeNotifications"
        readAllNotifications:
          $ref: "#/components/examples/readAllNotifications"
        unreadAllNotifications:
          $ref: "#/components/examples/unreadAllNotifications"
      properties:
        read:
          anyOf:
          - items:
              type: integer
            type: array
          - enum:
            - all
            type: string
          description: Notification Ids, or 'all', to mark as read.
        unread:
          anyOf:
          - items:
              type: integer
            type: array
          - enum:
            - all
            type: string
          description: Notification Ids, or 'all', to mark as unread.
      type: object
    MarkType:
      anyOf:
      - items:
          type: integer
        type: array
      - enum:
        - all
        type: string
    NotificationRequestBody:
      description: Notification data
      examples:
        ticketCommentNotification:
          $ref: "#/components/examples/ticketCommentNotification"
        ticketCreatedNotification:
          $ref: "#/components/examples/ticketCreatedNotification"
        ticketStatusChangeNotification:
          $ref: "#/components/examples/ticketStatusChangeNotification"
        ticketUpdatedNotification:
          $ref: "#/components/examples/ticketUpdatedNotification"
      properties:
        message:
          description: Message displayed to receivers of notification
          type: string
        receivers:
          description: User Ids of users to notify
          items:
            format: uuid
            type: string
          type: array
        type:
          $ref: "#/components/definitions/NotificationType"
          description: Notification type
        organizations:
          description: |-
            Organization Ids added to new ticket

            When a ticket is created, users who are members of/watching organizations
            will be notified.
          items:
            format: uuid
            type: string
          type: array
        points:
          type: integer
        senderId:
          description: Id of sender or user who initiated event that spawned notification
          format: uuid
          type: string
        status:
          enum:
          - closed
          - completed
          - open
          type: string
        tags:
          description: |-
            Tag Ids added to new ticket

            When a ticket is created, users watching tags on new ticket
            will be notified.
          items:
            type: integer
          type: array
        ticketId:
          description: |-
            Id of ticket where notification originated during a ticket
            update/status change/comment event.
          type: integer
      required:
      - message
      - receivers
      - type
      type: object
    NotificationResponse:
      description: Notification object
      examples:
        newTicketNotification:
          $ref: "#/components/examples/newTicketNotification"
      properties:
        createdAt:
          format: date-time
          type: string
        id:
          type: integer
        message:
          description: Message displayed to receivers of notification
          type: string
        organizations:
          description: Organization Ids referenced by notification
          items:
            format: uuid
            type: string
          type: array
        points:
          description: Points given to/taken from receievers (type == rewards)
          type: integer
        read:
          description: True if notification has been read by receiver
          type: boolean
        senderId:
          description: Id of user who created/triggered notification
          type: string
        status:
          description: New ticket status (type == ticketStatusChange)
          enum:
          - closed
          - completed
          - open
          type: string
        tags:
          description: Tag Ids referenced by notification
          items:
            type: integer
          type: array
        ticketId:
          description: Id of ticket referenced by notification
          type: integer
        type:
          $ref: "#/components/definitions/NotificationType"
          description: Notification type
        updatedAt:
          format: date-time
          type: string
      type: object
    NotificationType:
      enum:
      - message
      - organization
      - rewards
      - ticketAssignment
      - ticketComment
      - ticketCreate
      - ticketStatusChange
      - ticketUpdate
      - user
      - withOrgs
      - withRoles
      - withTags
      type: string
    NotificationsResponse:
      anyOf:
      - items:
          $ref: "#/components/definitions/NotificationResponseObject"
        type: array
      - type: integer
      description: Array of notification objects or count
    NotificationResponseObject:
      properties:
        createdAt:
          format: date-time
          type: string
        id:
          type: integer
        message:
          description: Message displayed to receivers of notification
          type: string
        organizations:
          description: Organization Ids referenced by notification
          items:
            format: uuid
            type: string
          type: array
        points:
          description: Points given to/taken from receievers (type == rewards)
          type: integer
        read:
          description: True if notification has been read by receiver
          type: boolean
        senderId:
          description: Id of user who created/triggered notification
          type: string
        status:
          description: New ticket status (type == ticketStatusChange)
          enum:
          - closed
          - completed
          - open
          type: string
        tags:
          description: Tag Ids referenced by notification
          items:
            type: integer
          type: array
        ticketId:
          description: Id of ticket referenced by notification
          type: integer
        type:
          $ref: "#/components/definitions/NotificationType"
          description: Notification type
        updatedAt:
          format: date-time
          type: string
      type: object
