// import seedrandom from 'seedrandom';
import { faker } from '@faker-js/faker';
import { NotificationType, TicketChange, TicketStatus, TicketVisibility } from '@prisma/client';
import { ChangeLogResponse } from '.';
import { SEED } from '../config';
export * from '../config';

export const exampleUserId = '6b18ae51-b8d0-4216-89a7-e5de9b9c9d9f';
export const exampleOrgId = '1c8368f5-4250-4d86-8fcf-0f12c48cd586';
export const exampleUserName = 'Doretta Gore';
export const exampleTicketId = 1;

// Generate reproducible random data
const seedDate = new Date(SEED);
faker.seed(seedDate.getTime());
// const rng = seedrandom(seedDate.toISOString());
// const recent = (days = 30) => faker.date.recent(days);

// Create user
export const exampleCreateUsers = [
  {
    basicUser: {
      summary: 'Create user',
      value: {
        id: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
      },
    },
  },
];

// Create tickets
export const exampleCreateTickets = [
  {
    healthTicket: {
      summary: 'Ticket tagged with physician specialities',
      value: {
        userId: exampleUserId,
        title: 'Health ticket example',
        detail: 'This is the body of the ticket. **Markdown** will be rendered by the ticket UI.',
        priority: 'low',
        physicians: ['cardiology', 'hematology'],
        visibility: TicketVisibility.organization,
        organizationIds: [exampleOrgId],
        topics: [],
      },
    },
    educationTicket: {
      summary: 'Ticket tagged with educator specialities',
      value: {
        userId: exampleUserId,
        title: 'Education ticket example',
        detail: 'This is the body of the ticket. **Markdown** will be rendered by the ticket UI.',
        priority: 'low',
        educators: ['mathematics', 'computer science'],
        visibility: TicketVisibility.organization,
        organizationIds: [exampleOrgId],
        topics: [],
      },
    },
  },
];

// Update tickets
export const exampleUpdateTickets = [
  {
    updateTicketBody: {
      summary: 'Update ticket title/details',
      value: {
        detail: 'This is the updated body of the ticket. **Markdown** will be rendered by the ticket UI.',
        title: 'Updated Health ticket example',
      },
    },
    updateTicketSpecialities: {
      summary: 'Update ticket specialities',
      value: {
        physicians: {
          add: ['obgyn'],
          remove: ['cardiologist'],
        },
        educators: {
          add: ['computer science'],
          remove: ['mathematics'],
        },
      },
    },
    updateTicketOrganizations: {
      summary: 'Update ticket organization',
      value: {
        organizationIds: {
          add: [exampleOrgId],
        },
      },
    },
    updateTicketLinkedTo: {
      summary: 'Update linked tickets',
      value: {
        linkedTo: {
          remove: [exampleTicketId],
        },
      },
    },
  },
];

// change ticket status
export const exampleUpdateTicketStatus = [
  {
    completeTicket: {
      summary: 'Mark ticket completed',
      value: {
        status: TicketStatus.completed,
      },
    },
    reopenTicket: {
      summary: 'Re-open ticket',
      value: {
        status: TicketStatus.open,
      },
    },
  },
];

// Create tags
export const exampleTags = [
  {
    physicianTag: {
      summary: 'Physician speciality',
      value: {
        name: 'OBGYN',
        type: 'physician',
      },
    },
    educatorTag: {
      summary: 'Educator speciality',
      value: {
        name: 'Computer Science',
        type: 'educator',
      },
    },
    topicTag: {
      summary: 'Topic',
      value: {
        name: 'gray matter',
        type: 'topic',
      },
    },
  },
];

// Get ticket changelog
export const exampleChangeLogResponse: ChangeLogResponse[] = [
  [
    {
      id: 1,
      type: TicketChange.created,
      createdAt: new Date('2022-04-14T01:52:54.318Z'),
      userId: exampleUserId,
      user: exampleUserName,
      ticketId: exampleTicketId,
      log: null,
    },
    {
      id: 2,
      type: TicketChange.modified,
      createdAt: new Date('2022-04-14T01:58:54.318Z'),
      userId: exampleUserId,
      user: exampleUserName,
      ticketId: exampleTicketId,
      log: null,
    },
    {
      id: 3,
      type: TicketChange.statusChange,
      createdAt: new Date('2022-04-14T01:58:54.318Z'),
      userId: exampleUserId,
      user: exampleUserName,
      ticketId: exampleTicketId,
      log: 'completed',
    },
  ],
];

// Assign/unassign tickets
export const exampleSimpleAssign = [
  {
    ticketAssignment: {
      summary: 'Assign or unassign current user from tickets',
      value: {
        ticketIds: [1, 2, 3],
      },
    },
  },
];

// Assign/unassign tickets response
export const exampleSimpleAssignResponse = [
  {
    assignedTicketIds: {
      summary: 'Array of ticket Ids user is currently assigned',
      value: {
        ticketIds: [1, 2, 3],
      },
    },
  },
];

// Create comments
export const exampleComments = [
  {
    ticketComment: {
      summary: 'Comment on a ticket',
      value: {
        userId: exampleUserId,
        ticketId: exampleTicketId,
        comment: 'This is the body of the comment. **Markdown** will be rendered by the ticket UI.',
      },
    },
  },
];

// Get notifications
export const exampleNotificationResponse = [
  {
    newTicketNotification: {
      summary: 'New ticket with tag',
      value: {
        read: false,
        id: 1,
        createdAt: new Date('2022-04-06T01:15:51.589Z'),
        updatedAt: new Date('2022-04-06T01:15:51.589Z'),
        message: 'New ticket',
        type: NotificationType.ticketCreate,
        senderId: exampleUserId,
        tags: [1],
        ticketId: 1,
        organizations: null,
      },
    },
  },
];

// Create notifications
export const exampleNotifications = [
  {
    ticketCreatedNotification: {
      summary: 'New ticket created',
      value: {
        message: 'New ticket',
        type: NotificationType.ticketCreate,
        senderId: exampleUserId,
        tags: [1, 2],
        organizations: [exampleOrgId],
        ticketId: exampleTicketId,
      },
    },
    ticketUpdatedNotification: {
      summary: 'Ticket updated',
      value: {
        message: 'Ticket updated',
        type: NotificationType.ticketUpdate,
        senderId: exampleUserId,
        ticketId: exampleTicketId,
      },
    },
    ticketStatusChangeNotification: {
      summary: 'Ticket status changed',
      value: {
        message: 'assign',
        type: NotificationType.ticketStatusChange,
        senderId: exampleUserId,
        ticketId: exampleTicketId,
      },
    },
    ticketCommentNotification: {
      summary: 'Ticket commented on',
      value: {
        message: 'This is a new comment about...',
        type: NotificationType.ticketComment,
        senderId: exampleUserId,
        ticketId: exampleTicketId,
      },
    },
  },
];

// mark notifications read/unread
export const exampleMarkNotification = [
  {
    readAllNotifications: {
      summary: 'Mark all notifications as read',
      value: {
        read: 'all',
      },
    },
    unreadAllNotifications: {
      summary: 'Mark all notifications as read',
      value: {
        unread: 'all',
      },
    },
    markSomeNotifications: {
      summary: 'Mark some notifications as read',
      value: {
        read: [1, 2, 3],
      },
    },
  },
];

// schema queries
export const exampleModelQueries = [
  {
    allModelsSchema: {
      summary: 'All database models',
      value: 'all',
    },
    multipleModelsSchema: {
      summary: 'Ticket and Comment models',
      value: 'Ticket,Comment',
    },
    ticketPrioritySchema: {
      summary: 'Ticket priority enum',
      value: 'Ticket.properties.priority.enum',
    },
    multiplePropertiesSchema: {
      summary: 'Multiple properties',
      value: 'Ticket.properties.priority,Ticket.properties.status',
    },
  },
];

// Search tickets
export const exampleSearchText = [
  {
    searchTextOr: {
      summary: "match entries containing 'human' OR 'animal'",
      value: 'human | animal',
    },
    searchTextAnd: {
      summary: "match entries containing 'human' AND 'animal'",
      value: 'human & animal',
    },
    searchTextFollowedBy: {
      summary: "match entries containing 'south' followed by 'america'",
      value: 'south <-> america',
    },
  },
];

// Settings
const ticketNotificationSettings = {
  ticketUpdate: true,
  ticketAssignment: false,
  ticketStatusChange: false,
  ticketComment: true,
};

const watchedSettings = {
  withTags: true,
  withOrgs: false,
  withRoles: true,
};

export const exampleTicketNotificationSettings = [
  {
    ticketNotificationSettings: {
      summary: 'Ticket notification settings',
      value: ticketNotificationSettings,
    },
  },
];

export const exampleWatchedNotificationSettings = [
  {
    watchedNotificationSettings: {
      summary: 'Watched notification settings',
      value: watchedSettings,
    },
  },
];

export const exampleNotificationSettings = [
  {
    notificationSettings: {
      summary: 'Notification settings',
      value: {
        onSubscribed: ticketNotificationSettings,
        onWatched: watchedSettings,
      },
    },
  },
];

export const exampleSettings = [
  {
    userSettings: {
      summary: 'User settings',
      value: {
        subscribeOnCreate: true,
        subscribeOnAssign: true,
        notifications: exampleNotificationSettings[0].notificationSettings.value,
      },
    },
  },
];

// Subscribe or unsubscribe from tickets
export const exampleSubscribeRequest = [
  {
    subscribeToTickets: {
      summary: 'Tickets to subscribe to/unsubscribe from',
      value: {
        ticketIds: [1, 2, 3],
      },
    },
  },
];

export const exampleSubscribeResponse = [
  {
    subscribedTickets: {
      summary: 'Tickets currently subscribed to',
      value: {
        ticketIds: [1, 2, 3],
      },
    },
  },
];

// Watching
export const exampleWatchingResponse = [
  {
    watchedResponse: {
      summary: 'Organizations and tags currently being watched',
      value: {
        orgIds: [exampleOrgId],
        tagIds: [1, 2, 3],
      },
    },
  },
];

export const exampleUpdateWatched = [
  {
    watchedRequest: {
      summary: 'Update watched organizations and tags',
      value: {
        orgIds: {
          add: [exampleOrgId],
        },
        tagIds: {
          add: [1],
          remove: [2, 3],
        },
      },
    },
  },
];
