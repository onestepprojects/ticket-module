import prisma from '@/client';
import { TicketSelectWithRelations, UserSelectWithRelations } from '@/interfaces';
import { Assignment, Notification, Organization, Prisma, Tag, Ticket, User, Comment, Note, TicketChangeLog } from '@prisma/client';

export type Models = Ticket | User | Notification | Assignment | Organization | Tag | Comment | Note | TicketChangeLog;

export type PDelegate<T extends Models> = T extends Ticket
  ? typeof prisma.ticket
  : T extends User
  ? typeof prisma.user
  : T extends Notification
  ? typeof prisma.notification
  : T extends Assignment
  ? typeof prisma.assignment
  : T extends Organization
  ? typeof prisma.organization
  : T extends Tag
  ? typeof prisma.tag
  : T extends Comment
  ? typeof prisma.comment
  : T extends Note
  ? typeof prisma.note
  : 'Unknown Select Type';

// Fields available to select
export type PSelect<T extends Models> = { [K in keyof T]?: boolean };

export type PSelectWithRelations<T extends Models> = T extends Ticket
  ? TicketSelectWithRelations
  : T extends User
  ? UserSelectWithRelations
  : 'Unsupported SelectWithRelations type';

// Relation fields available to select
export type PRelations<T extends Models> = {
  [K in keyof PInclude<T>]?: T['id'] extends string ? String : Number;
};

// Fields available to include
export type PInclude<T extends Models> = T extends Ticket
  ? Prisma.TicketInclude
  : T extends User
  ? Prisma.UserInclude
  : T extends Notification
  ? Prisma.NotificationInclude
  : T extends Assignment
  ? Prisma.AssignmentInclude
  : T extends Organization
  ? Prisma.OrganizationInclude
  : T extends Tag
  ? Prisma.TagInclude
  : T extends Comment
  ? Prisma.CommentInclude
  : T extends Note
  ? Prisma.NoteInclude
  : 'Unknown Include Type';

export type PWhere<T extends Models> = T extends Ticket
  ? Prisma.TicketWhereInput
  : T extends TicketChangeLog
  ? Prisma.TicketChangeLogWhereInput
  : T extends User
  ? Prisma.UserWhereInput
  : T extends Notification
  ? Prisma.NotificationWhereInput
  : T extends Assignment
  ? Prisma.AssignmentWhereInput
  : T extends Organization
  ? Prisma.OrganizationWhereInput
  : T extends Tag
  ? Prisma.TagWhereInput
  : T extends Comment
  ? Prisma.CommentWhereInput
  : T extends Note
  ? Prisma.NoteWhereInput
  : 'Unknown Where Type';

export type PWhereUnique<T extends Models> = T extends Ticket
  ? Prisma.TicketWhereUniqueInput
  : T extends User
  ? Prisma.UserWhereUniqueInput
  : T extends Notification
  ? Prisma.NotificationWhereUniqueInput
  : T extends Assignment
  ? Prisma.AssignmentWhereUniqueInput
  : T extends Organization
  ? Prisma.OrganizationWhereUniqueInput
  : T extends Tag
  ? Prisma.TagWhereUniqueInput
  : T extends Comment
  ? Prisma.CommentWhereUniqueInput
  : T extends Note
  ? Prisma.NoteWhereUniqueInput
  : 'Unknown WhereUnique Type';

export type POrderByWithRelation<T extends Models> = T extends Ticket
  ? Prisma.TicketOrderByWithRelationAndSearchRelevanceInput
  : T extends TicketChangeLog
  ? Prisma.TicketChangeLogOrderByWithRelationAndSearchRelevanceInput
  : T extends User
  ? Prisma.UserOrderByWithRelationAndSearchRelevanceInput
  : T extends Notification
  ? Prisma.NotificationOrderByWithRelationAndSearchRelevanceInput
  : T extends Assignment
  ? Prisma.AssignmentOrderByWithRelationAndSearchRelevanceInput
  : T extends Organization
  ? Prisma.OrganizationOrderByWithRelationAndSearchRelevanceInput
  : T extends Tag
  ? Prisma.TagOrderByWithRelationAndSearchRelevanceInput
  : T extends Comment
  ? Prisma.CommentOrderByWithRelationAndSearchRelevanceInput
  : T extends Note
  ? Prisma.NoteOrderByWithRelationAndSearchRelevanceInput
  : 'Unknown OrderByWithRelation Type';

export type POrderByWithAggregation<T extends Models> = T extends Ticket
  ? Omit<Prisma.TicketOrderByWithAggregationInput, 'max' | 'min' | 'sum'>
  : T extends User
  ? Prisma.UserOrderByWithAggregationInput
  : T extends Notification
  ? Prisma.NotificationOrderByWithAggregationInput
  : T extends Assignment
  ? Prisma.AssignmentOrderByWithAggregationInput
  : T extends Organization
  ? Prisma.OrganizationOrderByWithAggregationInput
  : T extends Tag
  ? Prisma.TagOrderByWithAggregationInput
  : T extends Comment
  ? Prisma.CommentOrderByWithAggregationInput
  : T extends Note
  ? Prisma.NoteOrderByWithAggregationInput
  : 'Unknown OrderByWithAggregation Type';

export type PScalarFieldEnum<T extends Models> = T extends Ticket
  ? Prisma.TicketScalarFieldEnum
  : T extends User
  ? Prisma.UserScalarFieldEnum
  : T extends Notification
  ? Prisma.NotificationScalarFieldEnum
  : T extends Assignment
  ? Prisma.AssignmentScalarFieldEnum
  : T extends Organization
  ? Prisma.OrganizationScalarFieldEnum
  : T extends Tag
  ? Prisma.TagScalarFieldEnum
  : T extends Comment
  ? Prisma.CommentScalarFieldEnum
  : T extends Note
  ? Prisma.NoteScalarFieldEnum
  : 'Unknown ScalarFieldEnum Type';

export type PCountAggregateType<T extends Models> = T extends Ticket
  ? Prisma.TicketCountAggregateInputType
  : T extends User
  ? Prisma.UserCountAggregateInputType
  : T extends Notification
  ? Prisma.NotificationCountAggregateInputType
  : T extends Assignment
  ? Prisma.AssignmentCountAggregateInputType
  : T extends Organization
  ? Prisma.OrganizationCountAggregateInputType
  : T extends Tag
  ? Prisma.TagCountAggregateInputType
  : T extends Comment
  ? Prisma.CommentCountAggregateInputType
  : T extends Note
  ? Prisma.NoteCountAggregateInputType
  : 'Unknown CountAggregate Type';
