export enum TicketEvent {
  ticketUpdate = 0,
  ticketAssignment,
  ticketStatusChange,
  ticketComment,
  // mark end of enum
  end,
}

export enum NewTicketEvent {
  withTags = 0,
  withOrgs,
  withRoles,
  // mark end of enum
  end,
}
