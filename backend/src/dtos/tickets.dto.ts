import { IsArray, IsString, IsOptional, IsUUID, IsEnum, IsInt, IsNotEmpty, Validate } from 'class-validator';
import { TicketVisibility, TicketPriority, TicketStatus } from '@prisma/client';
import { IsAddRemoveNumbers, IsAddRemoveStrings, IsAddRemoveUuids } from '.';
import { AddRemove } from '@/interfaces';

export class UpdateTicketStatusDto {
  @IsOptional()
  @IsString()
  @IsEnum(TicketStatus, {
    message: `status must be one of: ${Object.keys(TicketStatus).join(', ')}`,
  })
  public status: keyof typeof TicketStatus;
}

export class CreateTicketDto {
  @IsUUID()
  public userId: string;

  @IsString()
  public title: string;

  @IsString()
  public detail: string;

  @IsOptional()
  @IsString()
  public referralUrl?: string;

  @IsOptional()
  @IsString()
  @IsEnum(TicketPriority, {
    message: `priority must be one of: ${Object.keys(TicketPriority).join(', ')}`,
  })
  public priority?: keyof typeof TicketPriority;

  @IsOptional()
  @IsString()
  @IsEnum(TicketVisibility, {
    message: `visibility must be one of: ${Object.keys(TicketVisibility).join(', ')}`,
  })
  public visibility?: keyof typeof TicketVisibility;

  @IsOptional({
    message: 'files are not implemented',
  })
  @IsArray()
  public files?: string[];

  @IsOptional()
  @IsArray()
  @IsString({
    each: true,
    message: 'Physician specialities must be strings',
  })
  public physicians?: string[];

  @IsOptional()
  @IsArray()
  @IsString({
    each: true,
    message: 'Educator specialities must be strings',
  })
  public educators?: string[];

  @IsOptional()
  @IsArray()
  @IsString({
    each: true,
    message: 'Topics must be strings',
  })
  public topics?: string[];

  @IsOptional()
  @IsArray()
  @IsInt({
    each: true,
    message: 'tagIds must be a list of Tag Ids',
  })
  public tagIds?: number[];

  @IsOptional()
  @IsArray()
  @IsUUID('all', {
    each: true,
    message: 'organizationIds must be a list of Organization Ids',
  })
  public organizationIds?: string[];

  @IsOptional()
  @IsArray()
  @IsInt({
    each: true,
    message: 'linkedTo must be a list of Ticket Ids',
  })
  public linkedTo?: number[];
}

export class UpdateTicketDto {
  @IsOptional()
  @IsUUID()
  public userId?: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  public title?: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  public detail?: string;

  @IsOptional()
  @IsString()
  public referralUrl?: string;

  @IsOptional()
  @IsString()
  @IsEnum(TicketPriority, {
    message: `priority must be one of: ${Object.keys(TicketPriority).join(', ')}`,
  })
  public priority?: keyof typeof TicketPriority;

  @IsOptional()
  @IsString()
  @IsEnum(TicketVisibility, {
    message: `visibility must be one of: ${Object.keys(TicketVisibility).join(', ')}`,
  })
  public visibility?: keyof typeof TicketVisibility;

  @IsOptional({
    message: 'files are not implemented',
  })
  @IsArray()
  public files?: string[];

  @IsOptional()
  @Validate(IsAddRemoveStrings)
  public physicians?: AddRemove<string>;

  @IsOptional()
  @Validate(IsAddRemoveStrings)
  public educators?: AddRemove<string>;

  @IsOptional()
  @Validate(IsAddRemoveStrings)
  public topics?: AddRemove<string>;

  @IsOptional()
  @Validate(IsAddRemoveNumbers)
  public tagIds?: AddRemove<number>;

  @IsOptional()
  @Validate(IsAddRemoveUuids)
  public organizationIds?: AddRemove<string>;

  @IsOptional()
  @Validate(IsAddRemoveNumbers)
  public linkedTo?: AddRemove<number>;
}
