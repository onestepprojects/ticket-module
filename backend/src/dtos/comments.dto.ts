import { IsInt, IsString, IsUUID } from 'class-validator';

export class CreateCommentDto {
  @IsString()
  public comment: string;

  @IsUUID()
  public userId: string;

  @IsInt()
  public ticketId: number;
}
