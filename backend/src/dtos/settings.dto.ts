import { // eslint-disable-line
  IsOptional,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidatorConstraint,
  Validate,
  IsBoolean,
} from 'class-validator';
import { // eslint-disable-line
  WatchedNotificationSettings,
  NotificationSettings,
  TicketNotificationSettings,
} from '@/interfaces';
import { isEmpty } from '@/utils';

const checkSetting = (setting?: boolean) => setting === undefined || typeof setting === 'boolean';

const isTicketNotificationSettings = (obj?: TicketNotificationSettings): boolean => {
  if (!obj) return true;
  const { // eslint-disable-line
    ticketAssignment = undefined,
    ticketUpdate = undefined,
    ticketStatusChange = undefined,
    ticketComment = undefined,
    ...rest
  } = obj;

  return (
    isEmpty(rest) && [ // eslint-disable-line
      ticketAssignment,
      ticketUpdate,
      ticketStatusChange,
      ticketComment,
    ].every(checkSetting)
  );
};

const isWatchedNotificationSettings = (obj?: WatchedNotificationSettings): boolean => {
  if (!obj) return true;
  const { withTags, withOrgs, withRoles, ...rest } = obj;
  return isEmpty(rest) && [withTags, withOrgs, withRoles].every(checkSetting);
};

@ValidatorConstraint({ name: 'notificationSettings', async: false })
class IsNotificationSettings implements ValidatorConstraintInterface {
  validate(settings: NotificationSettings): boolean | Promise<boolean> {
    const { onWatched, onSubscribed, ...invalid } = settings;
    return isEmpty(invalid) && isTicketNotificationSettings(onSubscribed) && isWatchedNotificationSettings(onWatched);
  }
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Invalid notification settings: ${JSON.stringify(validationArguments.value, null, 2)}`;
  }
}

export class CreateSettingsDto {
  @IsOptional()
  @Validate(IsNotificationSettings)
  public notifications?: NotificationSettings;

  @IsOptional()
  @IsBoolean()
  public subscribeOnCreate?: boolean;

  @IsOptional()
  @IsBoolean()
  public subscribeOnAssign?: boolean;
}
