import { IsInt, IsString, IsUUID } from 'class-validator';

export class CreateNoteDto {
  @IsString()
  public note: string;

  @IsInt()
  public ticketId: number;

  @IsUUID()
  public userId: string;
}
