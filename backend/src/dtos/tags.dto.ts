import { TagType } from '@prisma/client';
import { IsEnum, IsOptional, IsString, Length } from 'class-validator';

export class CreateTagDto {
  @Length(3, 20, {
    message: 'Tag length must be >= 3 and <= 20',
  })
  @IsString()
  public name: string;

  @IsOptional()
  @IsString()
  @IsEnum(TagType, {
    message: `tag type must be one of: ${Object.keys(TagType).join(', ')}`,
  })
  public type: keyof typeof TagType;
}
