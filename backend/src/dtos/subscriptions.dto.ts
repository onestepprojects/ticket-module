import { IsArray, IsInt } from 'class-validator';

export class CreateSubscriptionDto {
  @IsArray()
  @IsInt({ each: true })
  public ticketIds: number[];
}
