import { IsString, IsUUID } from 'class-validator';

export class CreateUserFileDto {
  @IsString()
  public filename: string;

  @IsString()
  public path: string;

  @IsUUID()
  public userId: string;
}
