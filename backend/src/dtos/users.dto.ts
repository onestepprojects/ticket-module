import { IsOptional, IsBoolean, IsArray, IsEmail, IsString, IsUUID } from 'class-validator';

export class CreateUserDto {
  @IsUUID()
  public id: string;

  @IsOptional()
  @IsString()
  public name: string;

  @IsOptional()
  @IsEmail()
  public email: string;

  @IsOptional()
  @IsString()
  public password: string;

  @IsOptional()
  @IsArray()
  public roles: string[];

  @IsOptional()
  @IsBoolean()
  public isAdmin: boolean;
}
