import { IsInt, IsString } from 'class-validator';

export class CreateUserFileDto {
  @IsString()
  public filename: string;

  @IsString()
  public path: string;

  @IsInt()
  public ticketId: number;
}
