import { IsBoolean, IsEnum, IsEmail, IsString, IsOptional, IsUUID } from 'class-validator';
import { WebhookType } from '@prisma/client';

export class CreateWebhookDto {
  @IsUUID()
  public createdBy: string;

  @IsString()
  public name: string;

  @IsString()
  @IsEnum(WebhookType, {
    message: `status must be one of: ${Object.keys(WebhookType).join(', ')}`,
  })
  public type: keyof typeof WebhookType;

  @IsOptional()
  @IsEmail()
  public email: string;

  @IsOptional()
  @IsString()
  public url: string;

  @IsOptional()
  @IsBoolean()
  public active: boolean;

  @IsOptional()
  @IsString()
  public secret: string;
}
