import { IsArray, IsInt, IsUUID } from 'class-validator';

export class CreateAssignmentDto {
  @IsArray()
  @IsUUID('all', { each: true })
  public assignedTo: string[];

  @IsInt()
  public ticketId: number;

  @IsUUID()
  public assignedBy: string;
}

export class AssignmentRequestDto {
  @IsArray()
  public ticketIds: number[];
}
