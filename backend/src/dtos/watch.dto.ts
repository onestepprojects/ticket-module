import { WatchingRequestBody } from '@interfaces';
import { isEmpty } from '@utils';
import {
  IsArray,
  IsInt,
  IsOptional,
  IsUUID,
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  isUUID,
  isInt,
  Validate,
  isArray,
  isNumber,
  isString,
} from 'class-validator';

export class CreateWatchDto {
  @IsOptional()
  @IsArray()
  @IsUUID('all', { each: true })
  public orgIds: string[];

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  public tagIds: number[];
}

@ValidatorConstraint({ name: 'addRemoveUuids', async: false })
export class IsAddRemoveUuids implements ValidatorConstraintInterface {
  validate(data: WatchingRequestBody['orgIds']): boolean | Promise<boolean> {
    const { add = [], remove = [], ...invalid } = data;
    return isEmpty(invalid) && isArray(add) && add.every(x => isUUID(x, 'all')) && isArray(remove) && remove.every(x => isUUID(x, 'all'));
  }
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Invalid add/remove uuids: ${JSON.stringify(validationArguments.object)}`;
  }
}

@ValidatorConstraint({ name: 'addRemoveStrings', async: false })
export class IsAddRemoveStrings implements ValidatorConstraintInterface {
  validate(data: { add?: string[]; remove?: string[] }): boolean | Promise<boolean> {
    const { add = [], remove = [], ...invalid } = data;
    return isEmpty(invalid) && isArray(add) && add.every(x => isString(x)) && isArray(remove) && remove.every(x => isString(x));
  }
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Invalid add/remove strings: ${JSON.stringify(validationArguments.object)}`;
  }
}

@ValidatorConstraint({ name: 'addRemoveNumbers', async: false })
export class IsAddRemoveNumbers implements ValidatorConstraintInterface {
  validate(data: WatchingRequestBody['tagIds']): boolean | Promise<boolean> {
    const { add = [], remove = [], ...invalid } = data;
    return isEmpty(invalid) && isArray(add) && add.every(x => isInt(x)) && isArray(remove) && remove.every(x => isNumber(x));
  }
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Invalid add/remove ids: ${JSON.stringify(validationArguments.object)}`;
  }
}

export class UpdateWatchDto {
  @IsOptional()
  @Validate(IsAddRemoveUuids)
  public orgIds: WatchingRequestBody['orgIds'];

  @IsOptional()
  @Validate(IsAddRemoveNumbers)
  public tagIds: WatchingRequestBody['tagIds'];
}
