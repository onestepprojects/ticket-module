import {
  isInt,
  Validate,
  isArray,
  IsArray,
  IsString,
  IsOptional,
  IsUUID,
  ArrayNotEmpty,
  IsEnum,
  IsInt,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';
import { NotificationType, TicketStatus } from '@prisma/client';
import { MarkNotificationRequestBody } from '@interfaces';

export class CreateNotificationDto {
  @IsString()
  @IsEnum(NotificationType, {
    message: `notification type must be one of: ${Object.keys(NotificationType).join(', ')}`,
  })
  public type: keyof typeof NotificationType;

  @IsString()
  public message: string;

  @IsArray()
  @ArrayNotEmpty()
  @IsUUID('all', { each: true })
  public receivers: string[];

  @IsOptional()
  @IsUUID()
  public senderId?: string;

  @IsOptional()
  @IsInt()
  public ticketId?: number;

  @IsOptional()
  @IsArray()
  public tags?: number[];

  @IsOptional()
  @IsArray()
  @IsUUID('all', { each: true })
  public organizations?: string[];

  // FIXME: points not optional when type === rewards
  @IsOptional()
  @IsInt()
  public points?: number;

  // FIXME: not optional when type === ticketStatusChange
  @IsOptional()
  @IsEnum(TicketStatus, {
    message: `status must be one of: ${Object.keys(TicketStatus).join(', ')}`,
  })
  public status?: keyof typeof TicketStatus;
}

@ValidatorConstraint({ name: 'isMarkBody', async: false })
class IsMarkBody implements ValidatorConstraintInterface {
  validate(data: MarkNotificationRequestBody['read']): boolean | Promise<boolean> {
    return data === 'all' || (isArray(data) && data.every(isInt));
  }
  defaultMessage(validationArguments?: ValidationArguments): string {
    return `Invalid mark notification: ${JSON.stringify(validationArguments.object)}`;
  }
}

export class MarkNotificationDto {
  @IsOptional()
  @Validate(IsMarkBody)
  public read?: 'all' | number[];

  @IsOptional()
  @Validate(IsMarkBody)
  public unread?: 'all' | number[];
}
