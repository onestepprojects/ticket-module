import App from '@/app';
import {
  AdminRoute,
  AggregateRoute,
  AssignmentsRoute,
  AuthRoute,
  CommentsRoute,
  WatchingRoute,
  IndexRoute,
  NotesRoute,
  NotificationsRoute,
  OrganizationsRoute,
  SearchRoute,
  SubscriptionsRoute,
  TagsRoute,
  TicketsRoute,
  UsersRoute,
  WebhooksRoute,
  SettingsRoute,
  HistoryRoute,
  SchemaRoute,
  // BEGIN development
  DebugRoute,
  // END development
} from '@routes';
import { validateEnv } from '@utils';

validateEnv();

const app = new App([
  new IndexRoute(),
  new SubscriptionsRoute(),
  new UsersRoute(),
  new AuthRoute(),
  new TicketsRoute(),
  new NotesRoute(),
  new NotificationsRoute(),
  new WebhooksRoute(),
  new TagsRoute(),
  new AssignmentsRoute(),
  new AdminRoute(),
  new CommentsRoute(),
  new SearchRoute(),
  new AggregateRoute(),
  new OrganizationsRoute(),
  new WatchingRoute(),
  new SettingsRoute(),
  new HistoryRoute(),
  new SchemaRoute(),

  // BEGIN development
  new DebugRoute(),
  // END development
]);

app.listen();
