import { NextFunction, Request, Response } from 'express';
/**
 * @method isEmpty
 * @param {String | Number | Object} value
 * @returns {Boolean} true & false
 * @description this value is Empty Check
 */
export const isEmpty = (value: string | number | object): boolean => {
  if (value === null) {
    return true;
  } else if (typeof value !== 'number' && value === '') {
    return true;
  } else if (typeof value === 'undefined' || value === undefined) {
    return true;
  } else if (value !== null && typeof value === 'object' && !Object.keys(value).length) {
    return true;
  } else {
    return false;
  }
};

export const catchAsync = fn => (req: Request, res: Response, next: NextFunction) => {
  Promise.resolve(fn(req, res, next)).catch(err => next(err));
};

export const zip = (...arrays) => {
  const maxLength = Math.max(...arrays.map(x => x.length));
  return Array.from({ length: maxLength }).map((_, i) => {
    return Array.from({ length: arrays.length }, (_, k) => arrays[k][i]);
  });
};

export const uuidRe = (param: string) => `:${param}([A-Fa-f0-9-]{36})`;

export const mapi = (n: number, fn: (i: number) => any) => Array.from({ length: n }, (_x, i) => i).map(i => fn(i));

export function reducei<T>(n: number, fn: (acc: T, i: number) => T, init: T) {
  return Array.from({ length: n }, (_x, i) => i).reduce(fn, init);
}

export const daysAgo = (n: number) => {
  const d = new Date();
  d.setDate(d.getDate() - n);
  return d.toISOString();
};

export const idsToArray = (ids: { id: true }[], fn = Number): any[] => ids.map(({ id }) => fn(id));

export const idsToHash = (ids: { id: string | number }[]): { [_: string | number]: true } => ids.reduce((acc, x) => ({ ...acc, [x.id]: true }), {});
