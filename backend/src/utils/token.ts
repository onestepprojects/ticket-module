const aws = require('aws-amplify');
const { Amplify, Auth } = aws;
import { HttpException } from '@/exceptions';

const amplifyConfig = {
  userPoolId: '' + process.env.REACT_APP_USER_POOL_ID,
  region: '' + (process.env.REACT_APP_USER_POOL_REGION || 'us-west-2'),
  identityPoolRegion: '' + (process.env.REACT_APP_USER_POOL_IDENTITY_POOL_REGION || 'us-west-2'),
  userPoolWebClientId: '' + process.env.REACT_APP_USER_POOL_USER_POOL_WEB_CLIENT_ID,
  oauth: {
    domain: '' + process.env.REACT_APP_USER_POOL_DOMAIN,
    scope: ['phone', 'email', 'openid', 'profile', 'aws.cognito.signin.user.admin'],
    redirectSignIn: '' + (process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_IN || 'https://localhost:3000'),
    redirectSignOut: '' + (process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_OUT || 'https://localhost:3000'),
    responseType: 'code',
  },
};

Amplify.configure(amplifyConfig);

/**
 * Get user access token from AWS
 */
export const getAccessToken = async (username: string, password: string) => {
  try {
    const auth = await Auth.signIn(username, password);
    return auth?.signInUserSession.accessToken.jwtToken;
  } catch (err) {
    throw new HttpException(400, `Invalid aws-amplify config/login credentials`);
  }
};
