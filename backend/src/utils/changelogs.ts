import { ChangeLogQuery, RawChangeLogQuery, RawHistoryQuery } from '@/interfaces';
import { TicketChange, TicketChangeLog } from '@prisma/client';
import { checkEnum, checkInvalid, cleanQuery, parseBaseManyQuery } from '.';

/**
 * Parse raw changelog query.
 *
 * @param {string|null} userId - restrict results to those initiated by user.
 *   No restriction if null.
 * @param {RawChangeLogQuery|RawHistoryQuery} params - parameters to parse
 *
 * @returns {ChangeLogQuery} parsed query
 */
export const parseChangeLogQuery = (userId: string | null, params: RawChangeLogQuery | RawHistoryQuery): ChangeLogQuery => {
  const values = parseBaseManyQuery<TicketChangeLog>(params);
  const { skip, take, orderBy, dateFilter, type, count, ticketIds, ...invalid } = values;
  checkInvalid(invalid);
  const types = checkEnum(type, TicketChange, 'changelog event type');
  let where = cleanQuery({
    userId: userId ? userId : undefined,
    ticketId: ticketIds ? { in: ticketIds.split(',').map(Number) } : undefined,
    type: types ? { in: types } : undefined,
  });
  if (dateFilter) where = { ...where, ...dateFilter };
  return cleanQuery({
    skip,
    take,
    orderBy,
    count,
    where: Object.keys(where).length > 0 ? where : undefined,
  });
};
