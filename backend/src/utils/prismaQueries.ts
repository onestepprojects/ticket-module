import { CreateTicketDto, UpdateTicketDto } from '@dtos';
import { // eslint-disable-line
  FindManyQuery,
  TicketRelationFieldEnum,
  TicketsResponse,
  UserRelationFieldEnum,
  UsersResponse,
  AddRemove,
} from '@interfaces';
import { // eslint-disable-line
  Organization,
  Prisma,
  Tag,
  TagType,
  Ticket,
  TicketChange,
  TicketStatus,
  User,
} from '@prisma/client';
import { daysAgo, checkFindManyParams, cleanQuery } from '.';
import { Models, PSelect } from '@types';

export const openTicketsQ = { status: TicketStatus.open };
export const completedTicketsQ = { status: TicketStatus.completed };
export const closedTicketsQ = { status: TicketStatus.closed };

export const assignedTicketsQ = { AND: [openTicketsQ, { assignedTo: { none: {} } }] };
export const unassignedTicketsQ = { AND: [openTicketsQ, { assignedTo: { some: {} } }] };
export const assignedToUserQ = (userId: string): Prisma.TicketWhereInput => ({
  AND: [openTicketsQ, { assignedTo: { some: { id: userId } } }],
});

export const selectWatchingQ = {
  watchedOrgs: { select: { id: true } },
  watchedTags: { select: { id: true } },
};

// Ticket create/update tag fields are TagType + 's'
const ticketTagFields = Object.keys(TagType).map(v => [v, v + 's']);

/**
 * Create array of tag queries to connect/disconnect ticket tags
 * During a ticket update, fields will be { add: [], remove: [] }
 * where 'add' will connect and 'remove' will disconnect relations
 * @param obj - create/update ticket data
 * @param update - true if operation is update
 * @param addRemove - 'add' to connect, 'remove' to disconnect
 * @return Prisma connect or disconnect clause for tags
 */
const connectTicketTags = (obj: { [_: string]: any }, update?: boolean, addRemove?: 'add' | 'remove'): Prisma.TagWhereUniqueInput[] => {
  const { tagIds } = obj;
  return ticketTagFields
    .reduce(
      (acc, [field, inputField]) =>
        !obj[inputField]
          ? acc
          : acc.concat(
              (update ? obj[inputField][addRemove] : obj[inputField]).map(name => ({
                name_type: { name, type: TagType[field] },
              })),
            ),
      [] as Prisma.TagWhereUniqueInput[],
    )
    .concat(!tagIds ? [] : (update ? tagIds[addRemove] : tagIds)?.map(id => ({ id })));
};

/**
 * Create / Update ticket relations
 *
 * Converts tag inputs to necessary prisma connect queries
 * @param data - Create or update ticket data
 * @param update - true if operation is update
 * @return prisma connect + disconnect (if operation is update) clause
 */
export function ticketRelationsQ<T extends Partial<CreateTicketDto> | UpdateTicketDto>(data: T, update?: boolean) {
  const { organizationIds, linkedTo } = data;
  const connectTags = connectTicketTags(data, update, 'add');
  if (!update) {
    // CreateTicketDto
    return cleanQuery({
      tags: !connectTags?.length ? undefined : { connect: connectTags },
      organizations: !organizationIds ? undefined : { connect: (organizationIds as string[])?.map(id => ({ id })) },
      linkedTo: !linkedTo ? undefined : { connect: (linkedTo as number[])?.map(id => ({ id })) },
    });
  }
  // Updating relations - UpdateTicketDto
  const disconnectTags = update ? connectTicketTags(data, update, 'remove') : undefined;
  return cleanQuery({
    tags: connectTags?.length || disconnectTags?.length ? { connect: connectTags, disconnect: disconnectTags } : undefined,
    organizations: !organizationIds
      ? undefined
      : {
          connect: (organizationIds as AddRemove<string>)?.add?.map(id => ({ id })),
          disconnect: (organizationIds as AddRemove<string>)?.remove?.map(id => ({ id })),
        },
    linkedTo: !linkedTo
      ? undefined
      : {
          connect: (linkedTo as AddRemove<number>)?.add?.map(id => ({ id })),
          disconnect: (linkedTo as AddRemove<number>)?.remove?.map(id => ({ id })),
        },
  });
}

// add to changelog
export const addToChangelogQ = (userId: string, event: TicketChange, log?: string) => ({
  changelog: {
    create: {
      userId,
      type: event,
      log,
    },
  },
});

// fields to return from changelog entries
export const selectChangelogQ = {
  id: true,
  type: true,
  createdAt: true,
  userId: true,
  user: { select: { name: true } },
  ticketId: true,
  log: true,
};

// fields to return from changelog entries for user history calls
export const selectHistoryQ = {
  id: true,
  type: true,
  createdAt: true,
  userId: true,
  ticketId: true,
  log: true,
};

export const notificationRelationsQ = (senderId?: string, ticketId?: number, tags?: number[], orgs?: string[]) => ({
  sender: senderId ? { connect: { id: senderId } } : undefined,
  tags: { connect: tags?.map(id => ({ id })) },
  ticket: ticketId ? { connect: { id: ticketId } } : undefined,
  organizations: { connect: orgs?.map(id => ({ id })) },
});

// select fields to return in get users notification queries
export const selectNotificationQ = {
  read: true,
  notification: {
    select: {
      id: true,
      createdAt: true,
      updatedAt: true,
      message: true,
      type: true,
      senderId: true,
      ticketId: true,
      status: true,
      points: true,
      tags: { select: { id: true } },
      organizations: { select: { id: true } },
    },
  },
};

/**
 * Toggle notifications read/unread
 * Find notificationsOnReceivers belonging to user and in ids
 */
export const updateMarkedQ = (userId: string, ids: 'all' | number[]) => ({
  where: ids === 'all' ? { receiverId: userId } : { AND: [{ receiverId: userId }, { notificationId: { in: ids } }] },
});

/** Filter results more recent than DAYS ago (defaul createdAt) */
export const sinceDaysAgoQ = (days: number, col = 'createdAt') => ({ [col]: { gte: daysAgo(days) } });

// include Ids only from relations
export const includeTicketRelationsQ: Prisma.TicketInclude = Object.keys(TicketRelationFieldEnum).reduce(
  (acc, k) => ({
    ...acc,
    [k]: { select: { id: true } },
  }),
  {},
);

// select query for all scalar fields of ticket
export const selectTicketScalarsQ = Object.keys(Prisma.TicketScalarFieldEnum).reduce(
  (acc, val) => ({
    ...acc,
    [val]: true,
  }),
  {},
);

// default ticket select, all scalars + arrays of relations ids
export const selectTicketQ = {
  ...selectTicketScalarsQ,
  ...includeTicketRelationsQ,
};

/**
 * Default User fields to return in queries
 */
export const userSelectDefault = {
  id: true,
  name: true,
  email: true,
};

export const includeUserRelationsQ = Object.keys(UserRelationFieldEnum).reduce(
  (acc, k) => ({
    ...acc,
    [k]: { select: { id: true } },
  }),
  {},
);

/**
 * Return tickets from relations, allowing for all find many query parameters
 */
export async function findManyTicketsFor<T extends Organization | Tag, V = T['id']>(
  service /*: PDelegate<T>*/,
  id: V,
  params: FindManyQuery<Ticket>,
): Promise<TicketsResponse> {
  const { countParams, ...rest } = checkFindManyParams<Ticket>(params);
  if (countParams) {
    const res = await service.findUnique({ where: { id } }).tickets(countParams);
    return res.length;
  }
  return service.findUnique({ where: { id } }).tickets(rest);
}

/**
 * Return watchers from relations, allowing for all find many query parameters
 */
export async function findManyWatchersFor<T extends Organization | Tag, V = T['id']>(
  service /*: PDelegate<T> */,
  id: V,
  params: FindManyQuery<User>,
): Promise<UsersResponse> {
  if (!params.select) params.select = userSelectDefault;
  const { countParams, ...rest } = checkFindManyParams<User>(params);
  if (countParams) {
    const res = await service.findUnique({ where: { id } }).watchers(countParams);
    return res.length;
  }
  return service.findUnique({ where: { id } }).watchers(rest);
}

/**
 * Return select clause that may include relations
 * If relations are present, only return the Ids of objects from relation
 */
export function selectWithRelationsQ<T extends Models>(select: PSelect<T>, relations: object): PSelect<T> {
  return select
    ? Object.keys(select).reduce(
        (acc, x) => ({
          ...acc,
          [x]: relations[x] ? { select: { id: true } } : true,
        }),
        {},
      )
    : undefined;
}
