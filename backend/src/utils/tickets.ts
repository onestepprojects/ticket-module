import { HttpException } from '@exceptions';
import { FindManyTicketsQuery, RawFindManyTicketsQuery, RawSearchTicketsQuery } from '@interfaces';
import { Prisma, Ticket, TicketPriority, TicketStatus, TicketVisibility } from '@prisma/client';
import { Debug, cleanQuery, isEmpty, parseFindManyQuery, checkEnum } from '.';

const debug = Debug('tickets:parse');

// parse comma-separated speciality,topic lists to queries
const parseTags = (speciality?: string, topic?: string) => {
  const specs = speciality
    ? {
        AND: [
          {
            name: {
              in: speciality.split(','),
              mode: 'insensitive',
            },
          },
          { type: { in: ['educator', 'physician'] } },
        ],
      }
    : undefined;

  const topics = topic
    ? {
        AND: [{ name: { in: topic.split(','), mode: 'insensitive' } }, { type: 'topic' }],
      }
    : undefined;

  return topics && specs ? { some: { OR: [topics, specs] } } : { some: topics || specs };
};

/**
 * Parse find many tickets query parameters
 */
export const parseFindManyTicketsQuery = (params: RawFindManyTicketsQuery): FindManyTicketsQuery => {
  if (isEmpty(params)) return {};
  debug('params:', params);

  const {
    isAssigned,
    status,
    priority,
    visibility,
    speciality,
    topic,
    where: manyWhere,
    ...rest
  } = parseFindManyQuery<Ticket>(params, { model: 'Ticket' });

  const whereQueries = cleanQuery({
    tags: speciality || topic ? parseTags(speciality, topic) : undefined,
    status: status ? { in: checkEnum(status, TicketStatus, 'status') } : undefined,
    priority: priority ? { in: checkEnum(priority, TicketPriority, 'priority') } : undefined,
    visibility: visibility ? { in: checkEnum(visibility, TicketVisibility, 'visibility') } : undefined,
    ...manyWhere,
  });

  let where: Prisma.TicketWhereInput = isEmpty(whereQueries)
    ? undefined
    : Object.keys(whereQueries).length === 1
    ? whereQueries
    : { OR: whereQueries };

  if (isAssigned !== undefined) {
    if (!(isAssigned === 'true' || isAssigned === 'false')) throw new HttpException(400, `unrecognized 'isAssigned' value: ${isAssigned}`);
    const assQ = { [isAssigned === 'true' ? 'some' : 'none']: {} };
    where = where ? { AND: { assignedTo: assQ, ...where } } : { assignedTo: assQ };
  }

  debug('parsed where:', JSON.stringify(where, null, 2));

  return cleanQuery({ ...rest, where });
};

export const parseSearchTicketsQuery = (params: RawSearchTicketsQuery): FindManyTicketsQuery => {
  if (isEmpty(params)) return {};
  const { literal, caseSensitive, title, detail, ...findManyParams } = params;
  const { where: findWhere, ...rest } = parseFindManyTicketsQuery(findManyParams);
  const titleQ = title
    ? {
        title: {
          [literal ? 'contains' : 'search']: title,
          mode: caseSensitive ? 'default' : 'insensitive',
        },
      }
    : undefined;
  const detailQ = detail
    ? {
        detail: {
          [literal ? 'contains' : 'search']: detail,
          mode: caseSensitive ? 'default' : 'insensitive',
        },
      }
    : undefined;
  const search = titleQ && detailQ ? { OR: [titleQ, detailQ] } : titleQ || detailQ;
  const where = findWhere && search ? { AND: { ...findWhere, ...search } } : findWhere || search;

  debug('parsed search:', JSON.stringify(where, null, 2));
  return cleanQuery({ ...rest, where });
};
