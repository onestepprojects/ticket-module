import { FindManyTagsQuery, RawFindManyTagsQuery } from '@interfaces';
import { Tag, TagType } from '@prisma/client';
import { checkEnum, cleanQuery, isEmpty, parseSelectManyQuery } from '.';

/**
 * Parse raw find many tags query.
 */
export const parseFindManyTagsQuery = (params: RawFindManyTagsQuery): FindManyTagsQuery => {
  const { type, name, dateFilter, ...rest } = parseSelectManyQuery<Tag>(params, { model: 'Tag' });
  const types = checkEnum(type, TagType, 'tag type');
  let whereQ: any = {};
  if (types) whereQ.type = { in: type.split(',') };
  if (name) whereQ.name = { in: name.split(','), mode: 'insensitive' };
  if (isEmpty(whereQ)) whereQ = undefined;
  return cleanQuery({
    ...rest,
    where: dateFilter && whereQ ? { ...dateFilter, ...whereQ } : dateFilter || whereQ,
  });
};
