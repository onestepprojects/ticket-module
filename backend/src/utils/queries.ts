import { HttpException } from '@/exceptions/HttpException';
import { // eslint-disable-line
  BaseManyQuery,
  PaginationQuery,
  RawBaseManyQuery,
  RawPaginationQuery,
  SelectManyQuery,
  SelectQuery,
  RawSelectManyQuery,
  RawFindManyQuery,
  FindManyQuery,
  GroupByActionType,
  RawGroupByQuery,
  GroupByQuery,
  UserScalarFieldEnum,
  UserRelationFieldEnum,
  TicketRelationFieldEnum,
} from '@interfaces';
import { Prisma } from '@prisma/client';
import { // eslint-disable-line
  Models,
  // PDelegate,
  // PInclude,
  POrderByWithRelation,
  PRelations,
  PScalarFieldEnum,
  PSelect,
  PWhere,
} from '@types';
import { // eslint-disable-line
  checkEmpty,
  checkInvalid,
  isEmpty,
} from '.';
import { Debug } from './debug';
const debug = Debug('parse'); // eslint-disable-line

/**
 * Remove undefined entries
 * Causes a problem in groupBy, despite Prisma docs saying 'undefined' should be
 * treated as non-existent in queries.
 *
 * https://stackoverflow.com/questions/286141/remove-blank-attributes-from-an-object-in-javascript
 */
export const cleanQuery = (obj: object) =>
  // eslint-disable-next-line
  Object.fromEntries(Object.entries(obj).filter(([_, v]) => v != undefined && v != null));

// pass additional properties through parsers
type Leftover = { [k: string]: any };

/**
 *  Parse pagination query parameters: skip, take
 */
export const parsePaginationQuery = (params: RawPaginationQuery & Leftover): PaginationQuery & Leftover => {
  const { skip, take, ...rest } = params;
  return {
    ...rest,
    skip: skip ? Number(skip) : undefined,
    take: take ? Number(take) : undefined,
  };
};

/**
 * Parse orderBy query
 */
export function parseOrderByQuery<T extends Models, S = PSelect<T>>(params: RawBaseManyQuery & Leftover): BaseManyQuery<T, S> & Leftover {
  const { orderBy: raw, ...rest } = parsePaginationQuery(params);
  if (!raw || raw.length === 0) return rest;
  const res = raw.split(',').reduce(
    (acc: POrderByWithRelation<T>, x: string) => ({
      ...acc,
      [x[0] === '-' ? x.slice(1) : x]: x[0] === '-' ? 'desc' : 'asc',
    }),
    {},
  );
  return {
    ...rest,
    orderBy: res,
  };
}

/**
 * Parse createdSince, createdAfter parameters
 */
export function parseDateQuery<T extends Models>(params: { createdAfter?: string; createdBefore?: string }): PWhere<T> | undefined {
  if (isEmpty(params)) return undefined;
  const { createdAfter, createdBefore } = params;
  const res = cleanQuery({
    gt: createdAfter ? new Date(createdAfter) : undefined,
    lt: createdBefore ? new Date(createdBefore) : undefined,
  });
  return isEmpty(res) ? undefined : ({ createdAt: res } as PWhere<T>);
}

/**
 * Parse pagination, count, createdBefore, createdAfter, and orderBy parameters
 */
export function parseBaseManyQuery<T extends Models, S = PSelect<T>>(params: RawBaseManyQuery & Leftover): BaseManyQuery<T, S> & Leftover {
  if (isEmpty(params)) return {};
  const { count, createdAfter, createdBefore, ...rest } = parseOrderByQuery<T, S>(params);
  return cleanQuery({
    ...rest,
    count: count !== undefined || undefined,
    dateFilter: parseDateQuery({ createdAfter, createdBefore }),
  });
}

/**
 * Fields available to [select,include] in models
 */
export const selectFields = (model?: Prisma.ModelName) => {
  if (!model) return [undefined, undefined];
  switch (model) {
    case 'Ticket':
      return [Prisma.TicketScalarFieldEnum, TicketRelationFieldEnum];
    case 'User':
      return [UserScalarFieldEnum, UserRelationFieldEnum];
    case 'Note':
      return [Prisma.NoteScalarFieldEnum, undefined];
    case 'Notification':
      return [Prisma.NotificationScalarFieldEnum, undefined];
    case 'Comment':
      return [Prisma.CommentScalarFieldEnum, undefined];
    case 'Assignment':
      return [Prisma.AssignmentScalarFieldEnum, undefined];
    case 'Organization':
      return [Prisma.OrganizationScalarFieldEnum, undefined];
    case 'Tag':
      return [Prisma.TagScalarFieldEnum, undefined];
    default:
      throw new Error(`invalid model: ${model}`);
  }
};

export type SelectQueryOptions<T extends Models> = {
  model?: Prisma.ModelName;
  // Array with [ scalar fields allowed, relation fields allowed ]
  fieldsAllowed?: [{ [K in PScalarFieldEnum<T>]?: string }, Partial<PRelations<T>>];
};

/**
 * Convert select field to Primsa select
 * query '?select=a,b,c' => select { a: true, ... }
 * @return Prisma.<model>Select compatible { [x]: true, [y]:true, ... } | undefined
 */
export function parseSelectQuery<T extends Models, S = PSelect<T>, R = SelectQuery<T, S>['select']>(
  rawSelect: string | null,
  opts?: SelectQueryOptions<T>,
): R {
  if (!rawSelect || rawSelect.length === 0) return undefined;
  const fields = rawSelect.split(',');
  const [scalars, relations] = opts?.fieldsAllowed || selectFields(opts?.model);

  const res = {};
  fields.forEach(x => {
    if (relations && relations[x]) res[x] = { select: { id: true } };
    else {
      if (scalars && !scalars[x]) throw new HttpException(400, `invalid select field: ${x}`);
      res[x] = true;
    }
  });

  return res as R;
}

/**
 * Parse pagination + date + count + select query parameters
 */
export function parseSelectManyQuery<T extends Models, S = PSelect<T>>(
  params: RawSelectManyQuery & Leftover,
  opts?: SelectQueryOptions<T>,
): SelectManyQuery<T, S> & Leftover {
  if (isEmpty(params)) return {};
  const { select, ...rest } = parseBaseManyQuery(params);
  return cleanQuery({
    ...rest,
    select: select ? parseSelectQuery<T, S>(select, opts) : undefined,
  });
}

/**
 * Convert raw findMany queries to forms expected by Prisma's findMany
 */
export function parseFindManyQuery<T extends Models, S = PSelect<T>>(
  params: RawFindManyQuery & Leftover,
  opts?: SelectQueryOptions<T>,
): FindManyQuery<T, S> & Leftover {
  if (isEmpty(params)) return {};
  const {   // eslint-disable-line
    where,
    cursor, // eslint-disable-line
    dateFilter,
    ...rest
  } = parseSelectManyQuery<T, S>(params, opts);
  const parsedWhere: PWhere<T> = where ? JSON.parse(where) : undefined;
  return cleanQuery({
    where: parsedWhere && dateFilter ? { ...parsedWhere, ...dateFilter } : parsedWhere || dateFilter,
    ...rest,
  });
}

/**
 * Update WHERE clause as intersection of current, if any, and new condition
 */
export function composeWhereAnd<T extends Models, S = PSelect<T>>(
  where: PWhere<T>,
  params: FindManyQuery<T, S> & Leftover,
): FindManyQuery<T, S> & Leftover {
  if (isEmpty(params.where)) return { ...params, where };
  const { where: manyWhere, ...rest } = params;
  return { ...rest, where: { ...manyWhere, ...where } };
}

/**
 * Update WHERE clause as union of current, if any, and new condition
 */
export function composeWhereOr<T extends Models, S = PSelect<T>>(
  where: PWhere<T>,
  params: FindManyQuery<T, S> & Leftover,
): FindManyQuery<T, S> & Leftover {
  if (isEmpty(params.where)) return { ...params, where };
  const { where: manyWhere, ...rest } = params;
  return { ...rest, where: { OR: [manyWhere, where] } as PWhere<T> };
}

/**
 * Split parsed find many params into those for count, and for findMany.
 * Throw invalid error if there are unrecognized params.
 */
export function checkFindManyParams<T extends Models, S = PSelect<T>>(
  params: FindManyQuery<T, S>,
): Omit<FindManyQuery<T, S>, 'count' | 'dateFilter'> & {
  countParams?: Pick<FindManyQuery<T, S>, 'skip' | 'take' | 'orderBy' | 'where'>;
} {
  if (isEmpty(params)) return {};
  const { count, ...rest } = params;
  // eslint-disable-next-line
  const { select, skip, take, orderBy, where, ...invalid } = rest;
  checkInvalid(invalid);
  return {
    countParams: count ? { skip, take, orderBy, where } : undefined,
    ...rest,
  };
}

/**
 * GroupBy actions: count, sum, etc.
 *
 * *Note*: Only supporting count
 *
 * Input queries result in the following:
 * - ?count      => { _count: true }
 * - ?count=a,b  => { _count: { select: { a: true, b: true, ... } } }
 */
const parseGroupByAction = (action?: GroupByActionType, values?: string) => {
  if (!action) return {};
  if (!values || values === 'all') {
    if (action !== 'count') throw new Error(`GroupBy ${action} not supported`);
    return { ['_' + action]: true };
  }
  return {
    ['_' + action]: values.split(',').reduce((acc, x) => ({ ...acc, [x]: true }), {}),
  };
};

export function parseGroupByQuery<T extends Models>(params: RawGroupByQuery): GroupByQuery<T> {
  checkEmpty(params, 'empty groupBy query');
  const { skip, take, count, by, orderBy, where, ...invalid } = parsePaginationQuery(params);
  checkInvalid(invalid);
  if ((skip || take) && !orderBy)
    throw new HttpException(400, `If 'take' or 'skip' are defined in aggregate query, then 'orderBy' must also be defined.`);

  return {
    by: by.split(',') as Array<PScalarFieldEnum<T>>,
    ...cleanQuery({
      orderBy:
        orderBy?.split(',').reduce(
          (acc, x: string) => ({
            ...acc,
            [x[0] === '-' ? x.slice(1) : x]: x[0] === '-' ? 'desc' : 'asc',
          }),
          {},
        ) ?? undefined,

      ...parseGroupByAction('count', count),
      where: where ? JSON.parse(where) : undefined,
    }),
  };
}
