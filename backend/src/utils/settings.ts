import { CreateSettingsDto } from '@/dtos';
import { // eslint-disable-line
  WatchedNotificationSettings,
  NotificationSettings,
  SettingsResponse,
  TicketNotificationSettings,
} from '@/interfaces';
import { TicketEvent, NewTicketEvent } from '@/types';
import { Settings } from '@prisma/client';
import { mapi, reducei } from '.';

export const notificationSettings = ['onSubscribed', 'onWatched'] as const;
export const newTicketSettings = mapi(NewTicketEvent.end, i => NewTicketEvent[i]);
export const ticketSettings = mapi(TicketEvent.end, i => TicketEvent[i]);

const emptyTicketSettings = reducei(TicketEvent.end, (acc, i) => ({ ...acc, [TicketEvent[i]]: false }), {}) as TicketNotificationSettings;

const emptyWatchedSettings = reducei(NewTicketEvent.end, (acc, i) => ({ ...acc, [NewTicketEvent[i]]: false }), {}) as WatchedNotificationSettings;

const emptyNotificationSettings = notificationSettings.reduce(
  (acc, s) => ({
    ...acc,
    [s]: s === 'onWatched' ? emptyWatchedSettings : emptyTicketSettings,
  }),
  {},
) as NotificationSettings;

const expandTicketSettings = (n: number): TicketNotificationSettings => {
  const res = {} as TicketNotificationSettings;
  for (let i = 0; i < TicketEvent.end; i++) res[TicketEvent[i]] = (n & (1 << i)) != 0;
  return res;
};

const expandWatchedSettings = (n: number): WatchedNotificationSettings => {
  const res = {} as WatchedNotificationSettings;
  for (let i = 0; i < NewTicketEvent.end; i++) res[NewTicketEvent[i]] = (n & (1 << i)) != 0;
  return res;
};

// Convert settings to human-readable format
export const expandSettings = (settings: Settings): SettingsResponse => {
  const { onWatched, onSubscribed, ...rest } = settings;
  return {
    ...rest,
    notifications: {
      onSubscribed: expandTicketSettings(onSubscribed),
      onWatched: expandWatchedSettings(onWatched),
    },
  };
};

export const collapseTicketSetting = (obj?: Partial<TicketNotificationSettings>): number | undefined =>
  obj ? Object.entries(obj).reduce((acc, [k, v]) => acc | (v ? 1 << TicketEvent[k] : 0), 0) : undefined;
export const collapseFollowSetting = (obj?: Partial<WatchedNotificationSettings>): number | undefined =>
  obj ? Object.entries(obj).reduce((acc, [k, v]) => acc | (v ? 1 << NewTicketEvent[k] : 0), 0) : undefined;

// Convert human-readable to integers
export const unexpandSettings = (settings: CreateSettingsDto): Partial<Settings> => {
  const { notifications = emptyNotificationSettings, ...otherSettings } = settings;
  const { onWatched, onSubscribed, ...rest } = notifications;
  return {
    ...otherSettings,
    ...rest,
    onSubscribed: collapseTicketSetting(onSubscribed),
    onWatched: collapseFollowSetting(onWatched),
  };
};
