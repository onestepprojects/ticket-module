import { TicketStatus } from '@prisma/client';

/**
 * Determine points to award receivers from ticket status change type
 *
 * If ticket was re-opened, and was previously marked completed, points are negative.
 * @returns points
 */
export const pointsFromStatus = (points?: number, prevStatus?: TicketStatus, status?: TicketStatus): number | undefined => {
  if (!(points && prevStatus && status)) return undefined;
  return prevStatus === TicketStatus.completed && status === TicketStatus.open ? -points : status === TicketStatus.completed ? points : undefined;
};
