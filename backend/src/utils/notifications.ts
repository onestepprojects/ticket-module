import { // eslint-disable-line
  FindManyNotificationsQuery,
  NotificationSettings,
  RawBaseManyQuery,
} from '@/interfaces';
import { TicketEvent, NewTicketEvent } from '@/types';
import { User } from '@prisma/client';
import { cleanQuery, parseBaseManyQuery } from '.';
import prisma from '../client';

/**
 * Queries
 */
export interface RawFindManyNotificationsQuery extends RawBaseManyQuery {
  read?: string;
  days?: string;
}

export const parseFindManyNotificationsQuery = (params: RawFindManyNotificationsQuery): FindManyNotificationsQuery => {
  // eslint-disable-next-line
  const { read, days = 30, dateFilter, ...rest } = parseBaseManyQuery(params);
  return cleanQuery({ ...rest, read: read ? read !== 'false' : undefined, days: Number(days) });
};

/*
 * Prisma doesn't current support bitwise operations, so need to write the raw SQL
 */

// Pivot tables
const tagsToUsers = '_TagToUser';
// const assignedToUsers = '_TicketsFor';
const subbedToUsers = '_TicketSubscription';
const orgsToUsers = '_OrganizationToUser';

type NotifyIds = { id: User['id']; [key: string]: any }[];

const cleanRes = (objs: NotifyIds) => objs.map(x => x.id);

/**
 * Wrap SQL query to filter users whose notification preferences allow notification
 * on event
 * @return SQL query
 */
function withSettings<T extends keyof NotificationSettings, E = T extends 'onWatched' ? NewTicketEvent : TicketEvent>(
  setting: T,
  event: E,
  userTable: string,
): string {
  return `
SELECT users.id FROM ( ${userTable} ) as users
INNER JOIN "Settings" s
ON (users.id = s."userId")
WHERE (s."${setting}" & ${1 << Number(event)} != 0)`;
}

/**
 * Get users to notify for new ticket event, taking into account their notification
 * preferences
 * @return Array of userIds to notify
 */
export async function getWatchersToNotify(tagIds?: number[], orgIds?: string[]): Promise<User['id'][]> {
  const tags = !tagIds?.length
    ? undefined
    : withSettings<'onWatched'>('onWatched', NewTicketEvent.withTags, `SELECT "B" AS id FROM "${tagsToUsers}" WHERE "A" IN (${tagIds})`);
  const orgStr = orgIds?.map(id => `'${id}'`).join(',');
  const orgs = !orgIds?.length
    ? undefined
    : withSettings<'onWatched'>('onWatched', NewTicketEvent.withOrgs, `SELECT "B" AS id FROM "${orgsToUsers}" WHERE "A" IN (${orgStr})`);
  const query = !(tags && orgs) ? tags || orgs : `${tags} UNION ${orgs}`;
  return cleanRes(await prisma.$queryRawUnsafe(query));
}

/**
 * Get users to notify for ticket actions, accounting for user preferences
 * @return Array of userIds to notify
 */
export async function getUsersToNotifyForTicket(ticketId: number, event: TicketEvent): Promise<User['id'][]> {
  // const creator = withSettings('onCreated', event, `SELECT "userId" AS id FROM "Ticket" t WHERE t.id = ${ticketId}`);
  // const assigned = withSettings('onAssigned', event, `SELECT "B" AS id FROM "${assignedToUsers}" WHERE "A" = ${ticketId}`);
  const subbed = withSettings('onSubscribed', event, `SELECT "B" AS id FROM "${subbedToUsers}" WHERE "A" = ${ticketId}`);
  // return cleanRes(await prisma.$queryRawUnsafe(`${creator} UNION ${assigned} UNION ${subbed}`));
  return cleanRes(await prisma.$queryRawUnsafe(subbed));
}
