import { HttpException } from '@exceptions';
import { isEmpty } from '.';

/**
 * Report unrecognized args, if not empty
 */
export const checkInvalid = (args?: object, message?: string) => {
  if (!isEmpty(args)) throw new HttpException(400, `${message || 'unrecognized query parameters'}: ${JSON.stringify(args)}`);
};

/**
 * Check user can access data
 * @param userId - user Id to check access
 * @param authorId - user Id of author
 * @param [others] - optional array of relation ids to check against
 */
export function checkUserAccess<T extends string | number>(userId: T, authorId: T, others?: { id: T }[]): void {
  if (userId !== authorId && !others?.find(x => x.id === userId)) throw new HttpException(401, 'Unauthorized data access');
}

/**
 * Check data isn't empty
 */
export const checkEmpty = (data: any, message?: string) => {
  if (isEmpty(data)) throw new HttpException(400, message || 'Empty data');
};

/**
 * Check comma-separated values are valid enum values
 * @returns array of checked values
 */
export const checkEnum = (values: string, enumObj: object, name?: string): string[] => {
  if (!values || values.length === 0) return undefined;
  const vals = values.split(',');
  vals.forEach(v => {
    if (!enumObj[v]) {
      throw new HttpException(400, `Invalid ${name || 'enum value'}: '${v}'.  Valid options are: ${Object.keys(enumObj).join(', ')}`);
    }
  });
  return vals;
};
