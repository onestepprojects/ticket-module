import { Router } from 'express';
import { Routes } from '@interfaces';
import TicketsController from '@/controllers/tickets.controller';

export class SearchRoute implements Routes {
  public path = '/search';
  public router = Router();
  public ticketsController = new TicketsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Search
   *   description: Ticket Search API
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /search:
     *     get:
     *       summary: Search for tickets
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/searchTitle'
     *         - $ref: '#/components/parameters/searchDetail'
     *         - $ref: '#/components/parameters/searchLiteral'
     *         - $ref: '#/components/parameters/caseSensitive'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *       tags:
     *         - Search
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/searchResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.ticketsController.searchTickets);
  }
}

export default SearchRoute;
