import { Router } from 'express';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';
import { UsersController } from '@controllers';
import { CreateSettingsDto } from '@/dtos/settings.dto';

export class SettingsRoute implements Routes {
  public path = '/settings';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Settings
   *   description: User settings API
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /settings:
     *     get:
     *       summary: User settings
     *       tags:
     *         - Settings
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/settingsResponse'
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: Update User settings
     *       tags:
     *         - Settings
     *       requestBody:
     *         $ref: '#/components/requestBodies/settingsBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/settingsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.usersController.getSettings);
    this.router.put(`${this.path}`, validationMiddleware(CreateSettingsDto, 'body'), this.usersController.updateSettings);
  }
}

export default SettingsRoute;
