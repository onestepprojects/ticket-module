import { Router } from 'express';
import { UsersController, TicketsController } from '@controllers';
import { CreateTicketDto, UpdateTicketDto, UpdateTicketStatusDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class TicketsRoute implements Routes {
  public path = '/tickets';
  public router = Router();
  public ticketsController = new TicketsController();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  /**
   * @swagger
   *
   * tags:
   * - name: Tickets
   *   description: tickets API
   *
   * paths:
   *   /tickets/{ticketId}:
   *     parameters:
   *       - $ref: '#/components/parameters/ticketId'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /tickets:
     *     get:
     *       summary: Get tickets for current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       tags:
     *         - Tickets
     *       summary: Create ticket
     *       requestBody:
     *         $ref: '#/components/requestBodies/ticketBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketResponse'
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.ticketsController.getTickets);
    this.router.post(`${this.path}`, validationMiddleware(CreateTicketDto, 'body'), this.ticketsController.createTicket);
    /**
     * @swagger
     *
     * paths:
     *   /tickets/opened:
     *     get:
     *       summary: Get tickets opened (created) by current user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/opened`, this.usersController.getOpenedTickets);
    /**
     * @swagger
     *
     * paths:
     *   /tickets/completed:
     *     get:
     *       summary: Get tickets completed by current user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/completed`, this.usersController.getCompletedTickets);

    /**
     * @swagger
     *
     * paths:
     *   /tickets/{ticketId}:
     *     get:
     *       summary: Find ticket by Id
     *       parameters:
     *         - $ref: '#/components/parameters/ticketSelectWithRelationAndCount'
     *       tags:
     *         - Tickets
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketResponse'
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: Update Ticket by Id
     *       tags:
     *         - Tickets
     *       requestBody:
     *         $ref: '#/components/requestBodies/ticketUpdateBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketResponse'
     *         200:
     *           description: 'OK'
     *
     *     delete:
     *       tags:
     *         - Tickets
     *       summary: Delete ticket by Id
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:ticketId(\\d+)`, this.ticketsController.getTicketById);
    this.router.put(`${this.path}/:ticketId(\\d+)`, validationMiddleware(UpdateTicketDto, 'body', true), this.ticketsController.updateTicket);
    this.router.delete(`${this.path}/:ticketId(\\d+)`, this.ticketsController.deleteTicket);
    /**
     * @swagger
     *
     * paths:
     *   /tickets/{ticketId}/status:
     *     put:
     *       summary: Update Ticket status
     *       tags:
     *         - Tickets
     *       requestBody:
     *         $ref: '#/components/requestBodies/ticketUpdateStatusBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.put(
      `${this.path}/:ticketId(\\d+)/status`,
      validationMiddleware(UpdateTicketStatusDto, 'body', true),
      this.ticketsController.updateTicketStatus,
    );

    /**
     * @swagger
     *
     * paths:
     *   /tickets/{ticketId}/comments:
     *     get:
     *       summary: Get comments for ticket
     *       parameters:
     *         - $ref: '#/components/parameters/ticketId'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/commentSelect'
     *         - $ref: '#/components/parameters/commentOrderBy'
     *       tags:
     *         - Comments
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:ticketId(\\d+)/comments`, this.ticketsController.getComments);
    /**
     * @swagger
     *
     * paths:
     *   /tickets/{ticketId}/changelog:
     *     get:
     *       summary: Get changelog for ticket
     *       parameters:
     *         - $ref: '#/components/parameters/ticketId'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/changeLogOrderBy'
     *         - $ref: '#/components/parameters/changeLogType'
     *       tags:
     *         - History
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/changeLogResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:ticketId(\\d+)/changelog`, this.ticketsController.getChangelog);
  }
}

export default TicketsRoute;
