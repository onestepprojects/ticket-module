import { Router } from 'express';
import {
  TicketsController,
  CommentsController,
  AssignmentsController,
  UsersController,
  AdminController,
  NotificationsController,
} from '@controllers';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';
import { CreateAssignmentDto, CreateSubscriptionDto, CreateUserDto } from '@dtos';
import { uuidRe } from '@utils';
const userRe = uuidRe('userId');

export class AdminRoute implements Routes {
  public path = '/admin';
  public router = Router();
  public adminController = new AdminController();
  public usersController = new UsersController();
  public assignmentsController = new AssignmentsController();
  public commentsController = new CommentsController();
  public ticketsController = new TicketsController();
  public notificationsController = new NotificationsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    /**
     * @swagger
     *
     * tags:
     * - name: Admin
     *   description: Admin API
     * - name: SQL
     *   description: API to make raw (unsafe) SQL queries
     *
     * paths:
     *   /admin/{userId}/subscriptions:
     *     parameters:
     *       - $ref: '#/components/parameters/userId'
     *     get:
     *       summary: 'Get ticket subscriptions for user'
     *       tags:
     *         - Admin
     *         - Subscriptions
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         200:
     *           description: 'OK'
     *     post:
     *       tags:
     *         - Admin
     *         - Subscriptions
     *       summary: Subscribe user to tickets
     *       requestBody:
     *         $ref: '#/components/requestBodies/subscriptionBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         200:
     *           description: 'OK'
     *     delete:
     *       tags:
     *         - Admin
     *         - Subscriptions
     *       summary: 'Unsubscribe user from tickets'
     *       requestBody:
     *         $ref: '#/components/requestBodies/subscriptionBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/${userRe}/subscriptions`, this.usersController.ticketsSubscribedTo);
    this.router.post(
      `${this.path}/${userRe}/subscriptions`,
      validationMiddleware(CreateSubscriptionDto, 'body'),
      this.usersController.subscribeToTickets,
    );
    this.router.delete(
      `${this.path}/${userRe}/subscriptions`,
      validationMiddleware(CreateSubscriptionDto, 'body'),
      this.usersController.unsubscribeFromTickets,
    );

    /**
     * @swagger
     *
     * paths:
     *   /admin/notifications/delete-expired:
     *     delete:
     *       summary: Delete expired notifications
     *       parameters:
     *         - $ref: '#/components/parameters/deleteDaysOld'
     *       tags:
     *         - Admin
     *         - Notifications
     *       responses:
     *         200:
     *           description: 'OK'
     */
    this.router.delete(`${this.path}/notifications/delete-expired`, this.notificationsController.deleteExpiredNotifications);

    /**
     * @swagger
     *
     * paths:
     *   /admin/assignments:
     *     get:
     *       summary: Get assignments
     *       tags:
     *         - Admin
     *         - Assignments
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/assignments'
     *         200:
     *           description: 'OK'
     *
     *   /admin/{userId}/assignments:
     *     parameters:
     *       - $ref: '#/components/parameters/userId'
     *
     *     get:
     *       tags:
     *         - Admin
     *         - Assignments
     *       summary: Get assignments for user
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/assignments'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       tags:
     *         - Admin
     *         - Assignments
     *       summary: Assign users
     *       requestBody:
     *         $ref: '#/components/requestBodies/assignmentBody'
     *       responses:
     *         201:
     *           $ref: '#/components/responses/assignmentObject'
     *
     *     delete:
     *       tags:
     *         - Admin
     *         - Assignments
     *       summary: Unassign users
     *       requestBody:
     *         $ref: '#/components/requestBodies/assignmentBody'
     *       responses:
     *         201:
     *           $ref: '#/components/responses/assignmentObject'
     */
    this.router.get(`${this.path}/assignments`, this.assignmentsController.getAssignments);
    // TODO:
    this.router.get(`${this.path}/${userRe}/assignments`, this.assignmentsController.getAssignments);
    this.router.post(
      `${this.path}/${userRe}/assignments`,
      validationMiddleware(CreateAssignmentDto, 'body'),
      this.assignmentsController.createAssignment,
    );

    /**
     * @swagger
     *
     * paths:
     *   /admin/{userId}/comments:
     *     get:
     *       summary: Get user comments
     *       parameters:
     *         - $ref: '#/components/parameters/userId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *       tags:
     *         - Admin
     *         - Comments
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/${userRe}/comments`, this.adminController.getComments);

    /**
     * @swagger
     *
     * paths:
     *   /admin/tickets:
     *     get:
     *       summary: Get tickets
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Admin
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     *
     *   /admin/{userId}/tickets/assigned:
     *     get:
     *       summary: Get tickets assigned to user
     *       parameters:
     *         - $ref: '#/components/parameters/userId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Admin
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     *
     *   /admin/{userId}/tickets/opened:
     *     get:
     *       summary: Get tickets opened (created) by user
     *       parameters:
     *         - $ref: '#/components/parameters/userId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketIsAssigned'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Admin
     *         - Tickets
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/tickets`, this.ticketsController.getTicketsAdmin);
    this.router.get(`${this.path}/${userRe}/tickets/assigned`, this.ticketsController.getTicketsAssignedToUser);
    this.router.get(`${this.path}/${userRe}/tickets/opened`, this.ticketsController.getTicketsOpenedByUser);

    /**
     * @swagger
     *
     * paths:
     *   /admin/users:
     *     get:
     *       summary: Get users
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/userSelect'
     *         - $ref: '#/components/parameters/userOrderBy'
     *       tags:
     *         - Admin
     *         - Users
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/usersResponse'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       summary: Create user
     *       tags:
     *         - Admin
     *         - Users
     *       operationId: createUser
     *       requestBody:
     *         $ref: '#/components/requestBodies/userBody'
     *       responses:
     *         201:
     *           $ref: '#/components/responses/userResponse'
     */
    this.router.get(`${this.path}/users`, this.usersController.getUsers);
    this.router.post(`${this.path}/users`, validationMiddleware(CreateUserDto, 'body'), this.usersController.createUser);
    /**
     * @swagger
     *
     * paths:
     *   /admin/{userId}:
     *     parameters:
     *       - $ref: '#/components/parameters/userId'
     *     get:
     *       summary: Find User by Id
     *       tags:
     *         - Admin
     *         - Users
     *       responses:
     *         200:
     *           $ref: '#/components/responses/userResponse'
     *
     *     put:
     *       summary: Update User by Id
     *       tags:
     *         - Admin
     *         - Users
     *       requestBody:
     *         $ref: '#/components/requestBodies/userBody'
     *       responses:
     *         200:
     *           $ref: '#/components/responses/userResponse'
     *
     *     delete:
     *       summary: Delete User by Id
     *       tags:
     *         - Admin
     *         - Users
     *       responses:
     *         200:
     *           $ref: '#/components/responses/userResponse'
     */
    this.router.get(`${this.path}/${userRe}`, this.usersController.getOrCreateUserTemp);
    this.router.put(`${this.path}/${userRe}`, validationMiddleware(CreateUserDto, 'body', true), this.usersController.updateUser);
    this.router.delete(`${this.path}/${userRe}`, this.usersController.deleteUser);

    /**
     * @swagger
     *
     * paths:
     *   /admin/sql/query:
     *     get:
     *       tags:
     *         - Admin
     *         - SQL
     *       summary: Execute unsafe raw SQL query
     *       responses:
     *         200:
     *           'OK'
     *   /admin/sql/tables:
     *     get:
     *       tags:
     *         - Admin
     *         - SQL
     *       summary: Get SQL tables
     *       responses:
     *         200:
     *           'OK'
     */
    this.router.get(`${this.path}/sql/query`, this.adminController.getRawQuery);
    this.router.get(`${this.path}/sql/tables`, this.adminController.getSQLTables);

    /**
     * Testing: return user access token to run postman tests
     * FIXME: restrict access
     */
    this.router.get(`${this.path}/token`, this.adminController.accessToken);
  }
}

export default AdminRoute;
