import { Router } from 'express';
import { OrganizationsController } from '@controllers';
import { Routes } from '@interfaces';
import { uuidRe } from '@utils';
import { adminMiddleware } from '@/middlewares';
const orgRe = uuidRe('orgId');

export class OrganizationsRoute implements Routes {
  public path = '/organizations';
  public router = Router();
  public orgsController = new OrganizationsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Organizations
   *   description: 'Organization Ticket API'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /organizations/{orgId}/tickets:
     *     get:
     *       parameters:
     *         - $ref: '#/components/parameters/orgId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *       tags:
     *         - Organizations
     *         - Tickets
     *       summary: Get tickets for organization
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/${orgRe}/tickets`, this.orgsController.getTicketsForOrg);
    /**
     * @swagger
     *
     * paths:
     *   /organizations/{orgId}/watchers:
     *     get:
     *       summary: "[admin] Get watchers of organization's tickets"
     *       parameters:
     *         - $ref: '#/components/parameters/orgId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/userSelect'
     *         - $ref: '#/components/parameters/userOrderBy'
     *       tags:
     *         - Organizations
     *         - Watching
     *         - Admin
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/usersResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/${orgRe}/watchers`, adminMiddleware, this.orgsController.getWatchersForOrg);
  }
}

export default OrganizationsRoute;
