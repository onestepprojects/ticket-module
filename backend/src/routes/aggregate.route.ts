import { Router } from 'express';
import { Routes } from '@interfaces';
import TicketsController from '@/controllers/tickets.controller';

export class AggregateRoute implements Routes {
  public path = '/aggregate';
  public router = Router();
  public ticketsController = new TicketsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Aggregate
   *   description: API to make groupBy / aggregated ticket queries
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /aggregate:
     *     get:
     *       summary: Get aggregate results from ticket groupBy queries
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/groupByTicketsBy'
     *         - $ref: '#/components/parameters/groupByTicketsCount'
     *         - $ref: '#/components/parameters/groupByTicketsOrderBy'
     *         - $ref: '#/components/parameters/groupByTicketsWhere'
     *       tags:
     *         - Aggregate
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           type: object
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.ticketsController.getAggregateTickets);

    /**
     * Undocument routes subject to change
     */
    this.router.get(`${this.path}/by-assigned`, this.ticketsController.getTicketsByAssigned);
  }
}
export default AggregateRoute;
