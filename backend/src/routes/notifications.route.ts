import { Router } from 'express';
import { NotificationsController, UsersController } from '@controllers';
import { CreateNotificationDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class NotificationsRoute implements Routes {
  public path = '/notifications';
  public router = Router();
  public notificationsController = new NotificationsController();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Notifications
   *   description: Notifications API
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /notifications:
     *     get:
     *       summary: Get Notifications for current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/notificationRead'
     *         - $ref: '#/components/parameters/daysOld'
     *       tags:
     *         - Notifications
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationsResponse'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       summary: Create new notification
     *       tags:
     *         - Notifications
     *       requestBody:
     *         $ref: '#/components/requestBodies/notificationBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationResponse'
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.usersController.getNotifications);
    this.router.post(`${this.path}`, validationMiddleware(CreateNotificationDto, 'body'), this.notificationsController.createNotification);
    /**
     * @swagger
     *
     * paths:
     *   /notifications/mark:
     *     put:
     *       tags:
     *         - Notifications
     *       summary: Mark notifications as un/read
     *       requestBody:
     *         $ref: '#/components/requestBodies/markNotificationBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.put(`${this.path}/mark`, this.usersController.markNotifications);

    /**
     * @swagger
     *
     * paths:
     *   /notifications/read:
     *     delete:
     *       summary: Delete all user notifications that have been read
     *       tags:
     *         - Notifications
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/deleteReadNotificationsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.delete(`${this.path}/read`, this.usersController.deleteReadNotifications);

    /**
     * @swagger
     *
     * paths:
     *   /notifications/{notificationId}:
     *     parameters:
     *       - $ref: '#/components/parameters/notificationId'
     *
     *     get:
     *       summary: Get Notification by Id
     *       tags:
     *         - Notifications
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationResponse'
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: Update Notification by Id
     *       tags:
     *         - Notifications
     *       requestBody:
     *         $ref: '#/components/requestBodies/notificationBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationResponse'
     *         200:
     *           description: 'OK'
     *
     *     delete:
     *       summary: Delete Notification by Id
     *       tags:
     *         - Notifications
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notificationResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:notificationId(\\d+)`, this.notificationsController.getNotificationById);
    this.router.put(
      `${this.path}/:notificationId(\\d+)`,
      validationMiddleware(CreateNotificationDto, 'body', true),
      this.notificationsController.updateNotification,
    );
    this.router.delete(`${this.path}/:notificationId(\\d+)`, this.notificationsController.deleteNotification);
  }
}

export default NotificationsRoute;
