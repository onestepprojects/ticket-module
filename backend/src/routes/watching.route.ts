import { Router } from 'express';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';
import { UsersController } from '@controllers';
import { UpdateWatchDto } from '@dtos';

export class WatchingRoute implements Routes {
  public path = '/watching';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Watching
   *   description: |-
   *     API for users to watch/subscribe to tickets they want to
   *     be informed/notified about.
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /watching:
     *     get:
     *       summary: Get watched tags + organizations for current user
     *       tags:
     *         - Watching
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/watchingResponse'
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: Update watched tags/organizations for current user
     *       tags:
     *         - Watching
     *       requestBody:
     *         $ref: '#/components/requestBodies/watchingBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/watchingResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.usersController.getWatching);
    this.router.put(`${this.path}`, validationMiddleware(UpdateWatchDto, 'body'), this.usersController.updateWatching);
  }
}

export default WatchingRoute;
