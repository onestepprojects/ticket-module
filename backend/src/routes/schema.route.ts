import { Router } from 'express';
import { SchemaController } from '@controllers';
import { Routes } from '@interfaces';

export class SchemaRoute implements Routes {
  public path = '/schema';
  public router = Router();
  public schemaController = new SchemaController();

  constructor() {
    this.initializeRoutes();
  }

  /**
   * @swagger
   *
   * tags:
   * - name: Schema
   *   description: API to access model schemas
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /schema:
     *     get:
     *       summary: Get JSON-schemas for models, responses, and requests
     *       parameters:
     *         - $ref: '#/components/parameters/schemaModel'
     *         - $ref: '#/components/parameters/schemaNames'
     *       tags:
     *         - Schema
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           type: object
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.schemaController.getSchema);
  }
}

export default SchemaRoute;
