import { Router } from 'express';
import { UsersController } from '@controllers';
import { Routes } from '@interfaces';

export class HistoryRoute implements Routes {
  public path = '/history';
  public router = Router();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: History
   *   description: |-
   *     API to access ticket changelogs and historical user activity
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /history:
     *     get:
     *       summary: Get history for current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/historyTicketIds'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/changeLogOrderBy'
     *         - $ref: '#/components/parameters/changeLogType'
     *       tags:
     *         - History
     *         - Users
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/historyResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}`, this.usersController.getHistory);
  }
}

export default HistoryRoute;
