import { Router } from 'express';
import { CommentsController } from '@controllers';
import { UsersController } from '@controllers';
import { CreateCommentDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class CommentsRoute implements Routes {
  public path = '/comments';
  public router = Router();
  public commentsController = new CommentsController();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Comments
   *   description: API to manage ticket comments
   *
   * paths:
   *   /comments/{commentId}:
   *     parameters:
   *       - $ref: '#/components/parameters/commentId'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /comments:
     *     get:
     *       summary: Get comments for current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/commentSelect'
     *         - $ref: '#/components/parameters/commentOrderBy'
     *       tags:
     *         - Comments
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentsResponse'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       summary: Create a new comment
     *       tags:
     *         - Comments
     *       requestBody:
     *         $ref: '#/components/requestBodies/commentBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentResponse'
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.usersController.getComments);
    this.router.post(`${this.path}`, validationMiddleware(CreateCommentDto, 'body'), this.commentsController.createComment);
    /**
     * @swagger
     *
     * paths:
     *   /comments/{commentId}:
     *     get:
     *       summary: Get Comment by Id
     *       tags:
     *         - Comments
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentResponse'
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: Update Comment by Id
     *       tags:
     *         - Comments
     *       requestBody:
     *         $ref: '#/components/requestBodies/commentBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentResponse'
     *         200:
     *           description: 'OK'
     *
     *     delete:
     *       summary: Delete Comment by Id
     *       tags:
     *         - Comments
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/commentResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:commentId(\\d+)`, this.commentsController.getCommentById);
    this.router.put(`${this.path}/:commentId(\\d+)`, validationMiddleware(CreateCommentDto, 'body', true), this.commentsController.updateComment);
    this.router.delete(`${this.path}/:commentId(\\d+)`, this.commentsController.deleteComment);
  }
}

export default CommentsRoute;
