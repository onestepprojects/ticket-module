import { Router } from 'express';
import { UsersController } from '@controllers';
import { AssignmentsController } from '@controllers';
import { Routes } from '@interfaces';

export class UsersRoute implements Routes {
  public path = '/users';
  public router = Router();
  public usersController = new UsersController();
  public assignmentsController = new AssignmentsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Users
   *   description: 'Users API'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /users/current:
     *     get:
     *       summary: Get current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/userSelect'
     *       tags:
     *         - Users
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/userResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/current`, this.usersController.getCurrentUser);

    /**
     * @swagger
     *
     * paths:
     *   /users/login:
     *     post:
     *       summary: Login or create user
     *       tags:
     *         - Users
     *       requestBody:
     *         $ref: '#/components/requestBodies/userBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/userResponse'
     *         201:
     *           description: Created
     */
    this.router.post(`${this.path}/login`, this.usersController.getOrCreateUser);
  }
}

export default UsersRoute;
