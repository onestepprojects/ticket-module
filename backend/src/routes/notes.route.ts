import { Router } from 'express';
import { NotesController } from '@controllers';
import { CreateNoteDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class NotesRoute implements Routes {
  public path = '/notes';
  public router = Router();
  public notesController = new NotesController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Notes
   *   description: API to manage user's ticket notes
   * paths:
   *   /notes/{noteId}:
   *     parameters:
   *       - $ref: '#/components/parameters/noteId'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /notes:
     *     get:
     *       summary: Get notes for current user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/noteSelect'
     *         - $ref: '#/components/parameters/noteOrderBy'
     *       tags:
     *         - Notes
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/notesResponse'
     *         200:
     *           description: 'OK'
     *     post:
     *       tags:
     *         - Notes
     *       summary: Create note
     *       requestBody:
     *         $ref: '#/components/requestBodies/noteBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/noteResponse'
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.notesController.getNotes);
    this.router.post(`${this.path}`, validationMiddleware(CreateNoteDto, 'body'), this.notesController.createNote);
    /**
     * @swagger
     *
     * paths:
     *   /notes/{id}:
     *     get:
     *       summary: Find Note by Id
     *       tags:
     *         - Notes
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/noteResponse'
     *         200:
     *           description: 'OK'
     *     put:
     *       summary: Update Note by Id
     *       tags:
     *         - Notes
     *       requestBody:
     *         $ref: '#/components/requestBodies/noteBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/noteResponse'
     *         200:
     *           description: 'OK'
     *     delete:
     *       summary: Delete Note by Id
     *       tags:
     *         - Notes
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/noteResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:id(\\d+)`, this.notesController.getNoteById);
    this.router.put(`${this.path}/:id(\\d+)`, validationMiddleware(CreateNoteDto, 'body', true), this.notesController.updateNote);
    this.router.delete(`${this.path}/:id(\\d+)`, this.notesController.deleteNote);
  }
}

export default NotesRoute;
