import { Router } from 'express';
import { TagsController } from '@controllers';
import { CreateTagDto } from '@dtos';
import { Routes } from '@interfaces';
import { adminMiddleware, validationMiddleware } from '@middlewares';

export class TagsRoute implements Routes {
  public path = '/tags';
  public router = Router();
  public tagsController = new TagsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Tags
   *   description: Tags API
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /tags:
     *     get:
     *       summary: Get tags
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/tagSelect'
     *         - $ref: '#/components/parameters/tagType'
     *         - $ref: '#/components/parameters/tagOrderBy'
     *         - $ref: '#/components/parameters/tagName'
     *       tags:
     *         - Tags
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/tagsResponse'
     *         200:
     *           description: 'OK'
     *     post:
     *       tags:
     *         - Tags
     *         - Admin
     *       summary: '[admin] Create new tags'
     *       requestBody:
     *         $ref: '#/components/requestBodies/tagBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/tagResponse'
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.tagsController.getTags);
    this.router.post(`${this.path}`, adminMiddleware, validationMiddleware(CreateTagDto, 'body'), this.tagsController.createTag);
    /**
     * @swagger
     *
     * paths:
     *   /tags/{tagId}:
     *     parameters:
     *       - $ref: '#/components/parameters/tagId'
     *     get:
     *       tags:
     *         - Tags
     *       summary: Find Tag by Id
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/tagObject'
     *         200:
     *           description: 'OK'
     *     put:
     *       tags:
     *         - Tags
     *         - Admin
     *       summary: '[admin] Update Tag by Id'
     *       requestBody:
     *         $ref: '#/components/requestBodies/tagObject'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/tagObject'
     *         200:
     *           description: 'OK'
     *     delete:
     *       tags:
     *         - Tags
     *         - Admin
     *       summary: '[admin] Delete Tag by Id'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/tagObject'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:tagId(\\d+)`, this.tagsController.getTagById);
    this.router.put(`${this.path}/:tagId(\\d+)`, adminMiddleware, validationMiddleware(CreateTagDto, 'body', true), this.tagsController.updateTag);
    this.router.delete(`${this.path}/:tagId(\\d+)`, adminMiddleware, this.tagsController.deleteTag);
    /**
     * @swagger
     *
     * paths:
     *   /tags/{tagId}/tickets:
     *     get:
     *       parameters:
     *         - $ref: '#/components/parameters/tagId'
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *       tags:
     *         - Tags
     *         - Tickets
     *       summary: Get tickets tagged with tag
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/ticketsResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:tagId(\\d+)/tickets`, this.tagsController.getTicketsForTag);
    /**
     * @swagger
     *
     * paths:
     *   /tags/{tagId}/watchers:
     *     parameters:
     *       - $ref: '#/components/parameters/tagId'
     *     get:
     *       tags:
     *         - Tags
     *         - Watching
     *         - Admin
     *       summary: '[admin] Get watchers of tag'
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/usersResponse'
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:tagId(\\d+)/watchers`, adminMiddleware, this.tagsController.getWatchersForTag);
  }
}

export default TagsRoute;
