import { Router } from 'express';
import { WebhooksController } from '@controllers';
import { CreateWebhookDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class WebhooksRoute implements Routes {
  public path = '/webhooks';
  public router = Router();
  public webhooksController = new WebhooksController();

  constructor() {
    this.initializeRoutes();
  }

  /**
   * @swagger
   *
   * tags:
   * - name: Webhooks
   *   description: Webhooks API
   * paths:
   *   /webhooks/{webhookId}:
   *     parameters:
   *       - $ref: '#/components/parameters/webhookId'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /webhooks:
     *     get:
     *       summary: Find all webhooks for current user
     *       tags:
     *         - Webhooks
     *       responses:
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       summary: Add webhook
     *       tags:
     *         - Webhooks
     *       requestBody:
     *         description: 'Webhook Data'
     *         required: true
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/Webhook'
     *       responses:
     *         201:
     *           description: 'Created'
     */
    this.router.get(`${this.path}`, this.webhooksController.getWebhooks);
    this.router.post(`${this.path}`, validationMiddleware(CreateWebhookDto, 'body'), this.webhooksController.createWebhook);
    /**
     * @swagger
     *
     * paths:
     *   /webhooks/{webhookId}:
     *     get:
     *       summary: Find Webhook by Id
     *       tags:
     *         - Webhooks
     *       responses:
     *         200:
     *           description: 'OK'
     *
     *     put:
     *       summary: 'Update Webhook by Id'
     *       tags:
     *         - Webhooks
     *       requestBody:
     *         description: Webhook Data
     *         required: true
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/Webhook'
     *       responses:
     *         200:
     *           description: 'OK'
     *
     *     delete:
     *       summary: Delete Webhook by Id
     *       tags:
     *         - Webhooks
     *       responses:
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:webhookId(\\d+)`, this.webhooksController.getWebhookById);
    this.router.put(`${this.path}/:webhookId(\\d+)`, validationMiddleware(CreateWebhookDto, 'body', true), this.webhooksController.updateWebhook);
    this.router.delete(`${this.path}/:webhookId(\\d+)`, this.webhooksController.deleteWebhook);
  }
}

export default WebhooksRoute;
