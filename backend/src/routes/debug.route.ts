import { Router } from 'express';
import { DebugController } from '@controllers';
import { Routes } from '@interfaces';

export class DebugRoute implements Routes {
  public path = '/debug';
  public router = Router();
  public debugController = new DebugController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/slow`, this.debugController.slow);
    this.router.get(`${this.path}/watchers`, this.debugController.watchers);
    this.router.get(`${this.path}/to-notify`, this.debugController.notifyForTicket);
  }
}

export default DebugRoute;
