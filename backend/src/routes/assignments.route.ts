import { Router } from 'express';
import { AssignmentsController } from '@controllers';
import { UsersController } from '@controllers';
import { CreateAssignmentDto, AssignmentRequestDto } from '@dtos';
import { Routes } from '@interfaces';
import { validationMiddleware } from '@middlewares';

export class AssignmentsRoute implements Routes {
  public path = '/assignments';
  public router = Router();
  public assignmentsController = new AssignmentsController();
  public usersController = new UsersController();

  constructor() {
    this.initializeRoutes();
  }

  /**
   * @swagger
   *
   * tags:
   * - name: Assignments
   *   description: API to manage user's assigned tickets
   *
   * paths:
   *   /assignments/{userId}:
   *     parameters:
   *       - $ref: '#/components/parameters/userId'
   *   /assignments/{assignmentId}:
   *     parameters:
   *       - $ref: '#/components/parameters/assignmentId'
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /assignments:
     *     get:
     *       summary: Get assigned tickets for current logged in user
     *       parameters:
     *         - $ref: '#/components/parameters/skip'
     *         - $ref: '#/components/parameters/take'
     *         - $ref: '#/components/parameters/count'
     *         - $ref: '#/components/parameters/createdAfter'
     *         - $ref: '#/components/parameters/createdBefore'
     *         - $ref: '#/components/parameters/ticketOrderBy'
     *         - $ref: '#/components/parameters/ticketSelectWithRelation'
     *         - $ref: '#/components/parameters/ticketPriority'
     *         - $ref: '#/components/parameters/ticketVisibility'
     *         - $ref: '#/components/parameters/ticketStatus'
     *         - $ref: '#/components/parameters/ticketSpeciality'
     *         - $ref: '#/components/parameters/ticketTopic'
     *       tags:
     *         - Assignments
     *       produces:
     *         - application/json
     *       responses:
     *         200:
     *           $ref: '#/components/responses/ticketsResponse'
     *
     *     post:
     *       summary: Assign current user to tickets
     *       tags:
     *         - Assignments
     *       requestBody:
     *         $ref: '#/components/requestBodies/simpleAssignBody'
     *       responses:
     *         200:
     *           $ref: '#/components/responses/simpleAssignResponse'
     *
     *     delete:
     *       summary: Unassign current user from tickets
     *       tags:
     *         - Assignments
     *       requestBody:
     *         $ref: '#/components/requestBodies/simpleAssignBody'
     *       responses:
     *         200:
     *           $ref: '#/components/responses/simpleAssignResponse'
     */
    this.router.get(`${this.path}`, this.usersController.assignedTickets);
    this.router.post(`${this.path}`, validationMiddleware(AssignmentRequestDto, 'body'), this.usersController.assignTickets);
    this.router.delete(`${this.path}`, validationMiddleware(AssignmentRequestDto, 'body'), this.usersController.unassignTickets);

    /**
     * Note using these currently:
     * they are for assigning other users, or groups of users to tickets.
     *
     * @
     *
     * paths:
     *   /assignments/{assignmentId}:
     *     get:
     *       tags:
     *         - Assignments
     *       summary: 'Find Assignment by Id'
     *       responses:
     *         200:
     *           $ref: '#/components/responses/assignmentObject'
     *     put:
     *       tags:
     *         - Assignments
     *       summary: 'Update Assignment by Id'
     *       requestBody:
     *         $ref: '#/components/requestBodies/assignmentBody'
     *       responses:
     *         200:
     *           $ref: '#/components/responses/assignmentObject'
     *     delete:
     *       tags:
     *         - Assignments
     *       summary: 'Delete Assignment by Id'
     *       responses:
     *         200:
     *           description: 'OK'
     */
    this.router.get(`${this.path}/:assignmentId(\\d+)`, this.assignmentsController.getAssignmentById);
    this.router.put(
      `${this.path}/:assignmentId(\\d+)`,
      validationMiddleware(CreateAssignmentDto, 'body', true),
      this.assignmentsController.updateAssignment,
    );
    this.router.delete(`${this.path}/:assignmentId(\\d+)`, this.assignmentsController.deleteAssignment);
  }
}

export default AssignmentsRoute;
