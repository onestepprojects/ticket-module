import { Router } from 'express';
import { UsersController } from '@controllers';
import { AssignmentsController } from '@controllers';
import { Routes } from '@interfaces';
import { CreateSubscriptionDto } from '@dtos';
import { validationMiddleware } from '@middlewares';

export class SubscriptionsRoute implements Routes {
  public path = '/subscriptions';
  public router = Router();
  public usersController = new UsersController();
  public assignmentsController = new AssignmentsController();

  constructor() {
    this.initializeRoutes();
  }
  /**
   * @swagger
   *
   * tags:
   * - name: Subscriptions
   *   description: Ticket subscription API
   *
   */
  private initializeRoutes() {
    /**
     * @swagger
     *
     * paths:
     *   /subscriptions:
     *     get:
     *       summary: Get ticket subscriptions for logged in user
     *       tags:
     *         - Subscriptions
     *       produces:
     *         - application/json
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         200:
     *           description: 'OK'
     *
     *     post:
     *       tags:
     *         - Subscriptions
     *       summary: Subscribe to to tickets
     *       requestBody:
     *         $ref: '#/components/requestBodies/subscriptionBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         201:
     *           $ref: '#/components/responses/201'
     *
     *     delete:
     *       tags:
     *         - Subscriptions
     *       summary: Unsubscribe from tickets
     *       requestBody:
     *         $ref: '#/components/requestBodies/subscriptionBody'
     *       responses:
     *         schema:
     *           $ref: '#/components/responses/subscriptionResponse'
     *         200:
     *           $ref: '#/components/responses/200'
     */
    this.router.get(`${this.path}`, this.usersController.ticketsSubscribedTo);
    this.router.post(`${this.path}`, validationMiddleware(CreateSubscriptionDto, 'body'), this.usersController.subscribeToTickets);
    this.router.delete(`${this.path}`, validationMiddleware(CreateSubscriptionDto, 'body'), this.usersController.unsubscribeFromTickets);
  }
}

export default SubscriptionsRoute;
