import session from 'express-session';
import { SECRET_KEY } from '@config';

const sessionStore = new session.MemoryStore();

export const sessionMap = new Map();

export const sessionParser = session({
  store: sessionStore,
  // this can't be userId b/c users can have multiple sessions, eg. different hosts
  // genid: (_req) => v4(),
  saveUninitialized: false,
  secret: SECRET_KEY,
  resave: false,
  cookie: {
    httpOnly: false,
    secure: true,
    maxAge: 1000 * 60 * 60 * 24,
  },
});
export type ParserType = typeof sessionParser;
