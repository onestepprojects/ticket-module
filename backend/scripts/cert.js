#! /usr/bin/env node
'use strict';

const path = require('path');
const fs = require('fs');
const devcert = require('devcert');
const CERT_DIR = path.join(__dirname, '../certs');

async function main() {
  return devcert.certificateFor('localhost');
}

main()
  .then(({key, cert}) => {
    fs.writeFileSync(`${CERT_DIR}/tls.key`, key);
    fs.writeFileSync(`${CERT_DIR}/tls.crt`, cert);
  })
  .catch(console.error);
