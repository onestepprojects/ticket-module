#! /usr/bin/env node
'use strict';

import fs from 'fs';
import path from 'path';
import * as TJS from 'typescript-json-schema';

const p = path.resolve(__dirname, '../src/interfaces');
const files = fs.readdirSync(p).map(f => path.resolve(__dirname, '../src/interfaces', f))

console.log(files);

const compilerOptions = TJS.programFromConfig;
const program = TJS.getProgramFromFiles(
  files,
  {},
  '..'
);

const schema = TJS.generateSchema(program, '*');

console.log(schema);
