#!/usr/bin/env bash

# shellcheck disable=SC2155
export TICKET_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd)"
export HELM_DIR="$TICKET_DIR/../deploy/helm/ticket"

export DEPLOY_POD='ticket-ticket'
export INGRESS_POD='nginx-ingress-ingress-nginx-controller'
export DOCKER_IMAGE='registry.gitlab.com/onestepprojects/ticket-module/ticket-service:latest'
