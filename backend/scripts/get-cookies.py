#!/usr/bin/env python

"""Retrieve Chrome cookies"""

import sys
import browser_cookie3

def main():
    cookiejar = None

    try:
        cookiejar = browser_cookie3.chrome(domain_name="https://test.onesteprelief.org")
    except Exception:
        print("failed to get cookie from Chrome", file=sys.stderr)

    # print(list(browser_cookie3.chrome()))
    print(cookiejar)
    for cookie in cookiejar:
        print(cookie)

if __name__ == '__main__':
    main()    
