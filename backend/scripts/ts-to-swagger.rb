#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'yaml'

##
# Conventions
#
# Swagger parameters, responses, and requestBodies are created
# from types/interfaces when there are types matching the following naming
# conventions:
# - parameters: from types named '*QueryParameters'
# - responses: from types named '*Response'
# - requestBodies: from types named '*RequestBody'
#
# See `process_examples` for format of examples.
##

# Remove any types matching any of these patterns from generated YAML
IGNORE_TYPE_PATTERNS = [
  /.*List$/,            # Expanded enum comma-separated lists
  /^Raw[A-Z]/,          # Raw input types
  /.*Dummy$/,           # placeholder types/interfaces
  /.*Query(<.*>.*)?$/,  # Queries
  /\{.*\}/,
  /Ticket.*Relations$/,
  /Ticket.*Field$/,
]

# https://stackoverflow.com/questions/6227600/how-to-remove-a-key-from-hash-and-get-the-remaining-hash-in-ruby-rails
class Hash
  # Returns a hash that includes everything but the given keys.
  #   hash = { a: true, b: false, c: nil}
  #   hash.except(:c) # => { a: true, b: false}
  #   hash # => { a: true, b: false, c: nil}
  #
  # This is useful for limiting a set of parameters to everything but a few
  #   known toggles: @person.update(params[:person].except(:admin))
  def except(*keys)
    dup.except!(*keys)
  end

  # Replaces the hash without the given keys.
  #   hash = { a: true, b: false, c: nil}
  #   hash.except!(:c) # => { a: true, b: false}
  #   hash # => { a: true, b: false }
  def except!(*keys)
    keys.each { |key| delete(key) }
    self
  end
end

class String
  def uncapitalize 
    self[0, 1].downcase + self[1..-1]
  end
  # dont downcase rest of word
  def capitalize
    self[0, 1].upcase + self[1..-1]
  end
end


def deep_dup(h)
  Hash[h.map{|k, v| [k,
    if v.is_a?(Hash)
      deep_dup(v)
    else
      v.dup rescue v
    end
  ]}]
end

##
# Examples format in json-schema isnt right for swagger
#
# If examples isn't an array, or it is an array but the elements arent objects:
#   rename 'examples' to 'example' and continue
# 
# Otherwise:
#  - Add named examples to top-level components/examples
#  - Replace them in the object they were pulled from with '$ref's to
#    their top-level definitions.
#  
#  Examples will be formatted such that the key becomes their tag name
#  in components/examples and the value remains the same.
#  Each example should have a 'summary' and 'value' field to be recognized
#  properly by swagger.
# 
#  The top-level 'examples' field will be a hash of { tagName => value, ... },
#  and the transformed 'examples' field in the object will be a hash of
#  { tagName => { '$ref' => '#/components/examples/tagName' } }
##
def process_examples(top, hash)
  top['examples'] ||= {}
  if !hash['examples'].is_a? Array
    hash['example'] = hash['examples']
    hash.delete 'examples'
  elsif !hash['examples'][0].is_a? Hash
    hash['example'] = hash['examples'][0]
    hash.delete 'examples'
  else
    examples = {}
    cnt = 0
    hash['examples'].each do |ex|
      if !ex.respond_to?(:has_key?)
        examples[cnt] = ex
        cnt += 1
      else
        ex.each do |name, vals| 
          if !(vals.has_key?('summary') and vals.has_key?('value'))
            examples[cnt] << vals
            cnt += 1
          else
            top['examples'][name] = deep_dup(vals)
            examples[name] = { '$ref' => "#/components/examples/#{name}" }
          end
        end
      end
    end
    hash['examples'] = examples
  end
end

# Process top-level examples in properties and examples in sub-keys
def process_properties(top, h)
  return h unless h.has_key? 'properties'
  h['properties'].each do |k, v|
    next unless v.has_key? 'examples'
    process_examples(top, v)
  end
end

# Add objects matching '*RequestBody' to top-level components/requestBodies
# Note: Removes processed object from schema
def process_request(top, name, h)
  bodies = top['requestBodies'] ||= {}
  # Sort required properties first, rest alpha
  h['properties'] =
    h['properties']
      .sort_by { |k, v| h['required'] && h['required'].include?(k) ? -1 : k[0].ord }
      .to_h
  bodies["#{name.uncapitalize}Body"] = {
    "description" => h['description'] || "#{name.capitalize} data",
    "required" => h['tjs-required'] || true,
    "content" => {
      'application/json' => {
        "schema" => {
          '$ref' => "#/components/definitions/#{name}RequestBody",
          # "type" => 'object',
          # "properties" => deep_dup(h['properties']),
        },
        "examples" => h['examples'] ? deep_dup(h['examples']) : nil
      }
    }
  }
end

# Add objects matching '*Response' to top-level components/responses
def process_response(top, name, model, h)
  responses = top['responses'] ||= {}
  responses["#{name.uncapitalize}Response"] = {
    "description" => h['description'] || "#{name.capitalize} response",
    "content" => {
      'application/json' => {
        "schema" => {
          '$ref' => "#/components/definitions/#{model}",
        },
        "examples" => h['examples'] ? deep_dup(h['examples']) : nil
      },
    }
  }
  # remove any fields starting with 'tjs-' from processed object
  h.except! h.keys.select { |k| k.start_with? 'tjs-' }
  h.delete 'required'
end

##
# Process parameters from Query
# @param {string} prefix - prefix for top-level parameter names
# Adds fields to top-level components/parameters
# 
# @TJS-title property can be added to specify the swagger
#   parameter name and its corresponding query parameter name. It must be
#   of format: <swagger parameter name>:<query parameter name>
##
def process_parameters(top, prefix, h)
  return unless h['properties']
  pars = top['parameters'] ||= {}
  defaults = {
    'in' => 'query',
    'required' => false,
  }
  h['properties'].each do |k, v|
    parName, queryName = v.has_key?('title') ? v['title'].split(':')
                : [
      k.start_with?(prefix.uncapitalize) ? k : "#{prefix.uncapitalize}#{k.capitalize}",
      k
    ]
    # STDERR.puts "DBG: h = #{h.inspect}" if k == 'count'

    v.delete 'title'
    vals = pars["#{parName}"] = v.merge(defaults)
    vals['default'] ||= nil
    vals['name'] = queryName
    schema = vals['schema'] ||= {}
    
    # Format comma-separated lists and '$ref's
    isArray = vals['type'] == 'array'
    if isArray or vals['$ref']
      schema['$ref'] = vals['$ref'] unless isArray
      if isArray
        vals['style'] = 'form'
        vals['explode'] = false
        vals.delete 'type'
        schema['type'] = 'array'
        schema['items'] = vals.has_key?('items') ? deep_dup(vals['items']) : {
          'type' => 'string'
        }
        vals.delete 'items'
      end
      vals.delete '$ref'
    elsif vals['anyOf']
      schema['anyOf'] = deep_dup(vals['anyOf'])
      vals.delete 'anyOf'
    else
      vals['allowEmptyValue'] = true unless vals['type']
      ['type', 'minimum', 'maximum', 'format', 'default'].each do |key| 
        next unless vals[key]
        schema[key] = vals[key]
        vals.delete(key)
      end
    end
  end
end

def to_json file
  # TODO: process whole directory?
  # files = Dir.entries(File.expand_path('../src/interfaces', __dir__))
  #           .reject { |f| f.start_with? '.' }
  #           .map { |f| 'src/interfaces/' + f }
  json = JSON.load(
    `npx typescript-json-schema tsconfig.json '*' --required --tsNodeRegister --defaultNumberType 'integer' --include '#{file}'`)

  # Format examples, responses, requestBodies, and parameters
  defs = json['definitions']
  defs.each do |model, h|
    process_properties(json, h)
    process_examples(json, h) if h.has_key? 'examples'
    if model.match(/(.+)RequestBody$/)
      process_request(json, $1, h)
    elsif model.match(/(.+)Response$/)
      process_response(json, $1, model, h)
    elsif model.match(/(.+)QueryParameters$/)
      process_parameters(json, $1, h)
      defs.delete model
    elsif IGNORE_TYPE_PATTERNS.any? { |patt| model.match(patt) }
      defs.delete model
    end
  end

  json
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length < 1
    STDERR.puts "usage: #{__FILE__} <filename>"
    exit 1
  end

  json = to_json ARGV[0]
  # puts JSON.pretty_generate json
  yaml = YAML.dump(
    {
      'components' => {
        'parameters' => json['parameters'],
        'requestBodies' => json['requestBodies'],
        'responses' => json['responses'],
        'examples' => json['examples'],
        'definitions' => json['definitions'],
      }
    })
  yaml.gsub!('#/definitions', '#/components/definitions') rescue nil
  yaml.gsub!(/Partial<([^>]+)>(_[0-9]+)?/) { "#{$~[1]}Object" } rescue nil
  yaml.gsub!(/DummyObject/, 'Object') rescue nil
  yaml.gsub!('"$ref"', '$ref') rescue nil
  
  STDOUT.write(yaml)
end
