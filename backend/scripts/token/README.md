<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Access Token](#access-token)
- [Install](#install)
- [Run](#run)

<!-- markdown-toc end -->

# Access Token

This script gets user access token from AWS and prints it to stdout.

# Install

    npm install
    
Copy the `example.env` to `.env` and 
- fill in the correct username/password for an AWS user
- copy the other environment variables for the site-wide .env

# Run

    npm run token
