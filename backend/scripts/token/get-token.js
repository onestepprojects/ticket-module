#! /usr/bin/env node
'use strict';

const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.join(__dirname, '.env') });
const aws = require('aws-amplify');
const { Amplify, Auth } = aws;
const amplifyConfig = require('./aws-amplify.config');

Amplify.configure(amplifyConfig);

/**
 * Get user access token from AWS
 */
const getToken = async () => {
  try {
    const res = await Auth.signIn(process.env.AWS_USERNAME, process.env.AWS_PASSWORD);
    console.log(res?.signInUserSession.accessToken.jwtToken);
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

getToken();
