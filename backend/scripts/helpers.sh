#!/usr/bin/env bash

# Some aliases to help with deployment/debugging
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
# shellcheck disable=SC1090
. "$DIR/envvars.sh"

# Print user AWS access token
# If xclip is available, also copy it to clipboard
access_token() {
    token="$("$TICKET_DIR/scripts/token/get-token.js")"
    hash xclip 2>/dev/null &&
        echo -n "$token" | xclip -sel clip &&
        printf '=====\ncopied access token to clipboard\n=====\n'
    echo "$token"
}
alias token='access_token'

### Docker

# login to registry
# shellcheck disable=SC2120
d_login() {
    if [ "$#" -lt 2 ]; then
        docker login registry.gitlab.com
    else
        docker login registry.gitlab.com -u "$1" -p "$2"
    fi
}

# build image
d_build() {
    docker-compose -f "$TICKET_DIR/docker-compose.prod.yml" build
}

# push image to gitlab
d_push() {
    docker push "$DOCKER_IMAGE"
}

# build and deploy image
d_deploy() {
    d_login && d_build && d_push
}

### helm

helm_install() {
    helm install -f "$HELM_DIR/values/prod.yaml" ticket "$HELM_DIR"
}
helm_upgrade() {
    helm upgrade -f "$HELM_DIR/values/prod.yaml" ticket "$HELM_DIR"
}
helm_uninstall() {
    helm uninstall ticket
}

### Kubernetes

# Get the name of the ticket pod running on EKS
# usage: get_pod <prefix> [args passed to kubectl get all]
get_pod() {
    ! hash kubectl 2>/dev/null && return 0
    local name="$1" && shift
    kubectl get all "$@" |
        awk -v name="$name" \
            'match($0, "^pod/(" name "-\\S*)", l) { print l[1]; exit }'
}

# Run debug environment in EKS
export DEPLOY_POD_
kube_debug()
{
    # shellcheck disable=SC2153
    [ -z "$DEPLOY_POD_" ] && DEPLOY_POD_="$(get_pod "$DEPLOY_POD")"
    kubectl exec -it "$DEPLOY_POD_" -- sh
}

# Check ingress logs
# nginx-ingress-ingress-nginx-controller-54dc8fc8b6-wg7tx
# nginx-ingress-ingress-nginx-controller-54dc8fc8b6-hlx9j
export INGRESS_POD_=nginx-ingress-ingress-nginx-controller-54dc8fc8b6-l64fq
kube_logs() {
    # shellcheck disable=SC2153
    [ -z "$INGRESS_POD_" ] && INGRESS_POD_="$(get_pod "$INGRESS_POD" -n kube-system)"
    kubectl logs -f "$INGRESS_POD_" -n kube-system
}

# Configure
alias kube_config='aws eks update-kubeconfig --region us-east-1 --name capstone-us-east-1'

# Run curl inside EKS (call APIs without worrying about ingress control)
alias kube_curl='kubectl run -it --rm --image=curlimages/curl curly -- sh'

alias kube_sys='kubectl get all -n kube-system'
