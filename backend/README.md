<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Install](#install)
- [Development Setup](#development-setup)
    - [Generate CA certificates](#generate-ca-certificates)
    - [Starting Backend](#starting-backend)
    - [Getting user AWS access token (Authorization bearer token)](#getting-user-aws-access-token-authorization-bearer-token)
    - [Database Management](#database-management)
        - [Rebuilding Database / Database errors](#rebuilding-database--database-errors)
    - [Logging](#logging)
    - [Linting](#linting)
- [Build Configuration](#build-configuration)
- [Deployment](#deployment)
    - [Deployment helpers](#deployment-helpers)
    - [Docker](#docker)
        - [Build Notes](#build-notes)
    - [EKS](#eks)
    - [Helm](#helm)
- [Environment Variables](#environment-variables)
    - [BYPASS AUTH with the OneStep Authentication Service](#bypass-auth-with-the-onestep-authentication-service)
    - [Debug](#debug)
    - [Testing](#testing)
- [Project Layout](#project-layout)
- [Middleware](#middleware)
- [Swagger](#swagger)
    - [Model Schema Definitions](#model-schema-definitions)
    - [Examples, responses, requestBodes, parameters, and definitions](#examples-responses-requestbodes-parameters-and-definitions)
    - [Unused](#unused)

<!-- markdown-toc end -->

# Install

The backend runs on node v16. Install locally with

    npm install
  

# Development Setup

Make sure to setup your environment variables!

    cp ./example.env .env
    cp ./example.env.development.local .env.development.local

The `NODE_ENV` variable determines which local environment file will be sourced
when the app starts. For example, when the app is running under development, eg.
`npm run dev`, `NODE_ENV` is set to 'development' and the
`.env.development.local` file will be used.

**Note**: the postgres in the container exposes a non-standard port, 5434, to
hopefully not interfere with other host postgres processes.  The
`example.env.development.local` accounts for this.

## Generate CA certificates

Devcert certificates are needed to run the server using HTTPs.  To generate
them, run 

    npm run certs


## Starting Backend

Use docker-compose to start the database, proxy, and server

    docker-compose up

The server binds the project directory and runs `nodemon`, so changes on the
host will be reflected in the container.

## Getting user AWS access token (Authorization bearer token)

To get user AWS access token, first install dev scripts and add the variables
required by the README in `scripts/token/README`.

    npm run dev:install
    
Then, to get the current user AWS access token, run

    npm run token


## Database Management

From the host, `prisma` can connect to and modify the database in the container.
It will use the `DATABASE_URL` in `.env`.

To populate the database with basic data to start

    npx prisma db seed

After changing the schema, you can relect changes in the container with

    npm run prisma:generate && npm run prisma:migrate

You can pull/push the schema from/to the container with

    npx prisma db push                    # sync container with host schema
    # Don't do this- it overwrites all comments in schema!
    npx prisma db pull                    # sync host schema with container (DANGER)

## Database Configuration

In production environment, we use RDS instead of a postgres container. The `DATABASE_URL` environment variable is stored in AWS secrets manager as a secret called `capstone/ticket/env`, and passed to the backend container in EKS.

When debugging database connection issue, you can create/modify `.env` file in the ticket-module container to override the `DATABASE_URL`.

### Rebuilding Database / Database errors

When the database changes, you may see errors in the backend console, and
sometimes it is necessary to rebuild it. The following target will try to
rebuild the docker images, destroy the database, and rebuild from scratch 

    npm run dev:rebuild

It runs the following steps:

- destroy the docker volumes: `docker-compose down --volumes`
- rebuild docker images: `docker-compose up --build`
- generate prisma: `npm run prisma:generate`
- run migrations: `npm run prisma:migrate`
- seed: `npm run seed`
- reattach to server: `docker-compose up server`

## Logging

Error and debug logs should be available in `src/logs`.

## Linting

To lint the source code, run `npm run lint` or `npm run lint:fix` to both lint
and attempt to updated the source with lint fixes.

# Build Configuration

Configuration variables are in the `src/config` directory.

# Deployment

To manage the EKS cluster and deploy with helm locally, the following tools
must be installed:

- `aws` - [aws cli](https://aws.amazon.com/cli/)
- `kubectl` - [kubernetes cluster manager](https://kubernetes.io/docs/reference/kubectl/kubectl/)
- `helm` - [kubernetes package manager](https://helm.sh/docs/intro/install/)

## Deployment helpers

There are some helper functions/aliases defined in `scripts/helpers.sh`, which can be
sourced into bash

    source scripts/helpers.sh

A manual deployment process looks like this:

1. Build and push docker image to gitlab container registry;
2. Use Helm to deploy the docker image to EKS cluster;
3. Interacte with ticket pod to check logs and debug.

## Docker

To build the production container and push it to the gitlab registry run the
following target. It will use variables defined in the `.env` file in this
directory. 

    npm run deploy:image

The environment variables `GITLAB_IMAGE` and `VERSION` can be passed
to override the defaults and tag the image as `GITLAB_IMAGE`:`VERSION` (defaults
to `registry.gitlab.com/onestepprojects/ticket-module/ticket-service:latest`).

### Build Notes

As of 5/2/22, there were issues using node v17 in the EKS cluster, so we
downgraded node to the latest LTS release, v16.

## EKS

`kubectl` must be installed and configured for the EKS cluster. There are a
couple helper functions/aliases in `scripts/helpers.sh`:

- `kube_config` - configure kubectl for the EKS using `aws`
- `kube_debug` - launch a shell in the deployed container
- `kube_logs` - check the EKS ingress logs

## Helm

Helm is used to manage the deployed container in the EKS cluster. Relevant helpers:

- `helm_install` - deploy latest build from gitlab registry to EKS cluster
- `helm_upgrade` - upgrade container in EKS cluster
- `helm_uninstall` - stop deployed EKS container

# Environment Variables

## BYPASS AUTH with the OneStep Authentication Service

Add the following to .env.development.local under backend-express

    BYPASS_AUTH = true

## Debug

This app uses the `debug` package, so debugging output can be restricted by the
environment variable `DEBUG` to only ouput certain messages matching the given
prefix, eg. to restrict debugging output to messages from the ticket service:

    DEBUG=ticket:* npm run start

## Proxy

To make the backend proxy requests to other onestep backend services, define
`TICKET_CREATE_PROXY`

    TICKET_CREATE_PROXY = true

# Project Layout

For each model you want to expose via a REST API, there are four things to
update (after changing the model in the Prisma schema):

-   **`src/dtos/<model>.dto.ts`:** fields of model to verify, eg. during a POST
    request.
-   **`src/controllers/<model>.controller.ts`:** boilerplate methods to handle
    requests: GET, POST, PUT, DELETE
-   **`src/services/<model>.service.ts`:** methods wrapping Prisma client calls
    to interact with database.
-   **`src/routes/<model>.routes.ts`:** define the routes related to the model


# Middleware

In `src/app.ts` all the middleware is initialized.  The middleware processes
requests in succession, passing its processed result on to the next piece of
middleware - just a linear functional progression.

# Swagger

Adding to `swagger.yaml`, or inline in code using the `@swagger` in comments
(see `src/routes/tickets.route.ts`) will be generated into interactive
documentation, accessable to API users at `/docs` endpoint.

## Model Schema Definitions

*Note* This command both require an available ruby (>= v2.5) to compile the
swagger yaml schema.

Swagger model schemas can be auto-generated from the `src/prisma/schema.prisma`
file to `src/prisma/schema.yaml` using the command 
    
    npm run swag:schema
    
Any comments starting with '///' in `src/prisma/schema.prisma` will be included
as descriptions of the following field, or the current field if comment comes
afterward on the same line.

## Examples, responses, requestBodes, parameters, and definitions

Swagger examples, responses, requestBodies, parameters, and definitions can be
compiled to `src/interfaces/swagger` from the typescript interfaces in 
`src/interfaces` using the command

    npm run swag:interfaces
    
When adding new interfaces that should be compiled to swagger, there are some
type naming conventions required by the script to determine why type of
component the type should be compiled to. See the comments in
`scripts/ts-to-swagger.rb` for configuration and details about the naming
conventions and supported annotations.

The generated yaml files will be included in the swagger UI and visible in the 
API docs.

## Unused

There are some currently unused tools and configurations:

- `nginx.conf` and the nginx image in `docker-compose.yml` - setup to work as a
proxy, potentially for websockets, but not being used
