# Ticket Module

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Ticket Module](#ticket-module)
    - [Team Members](#team-members)
    - [Structure](#structure)
        - [Frontend](#frontend)
        - [Backend](#backend)
        - [Deployment](#deployment)
- [Development](#development)
    - [Development Lifecycle](#development-lifecycle)
        - [Dependencies](#dependencies)
        - [CI/CD](#cicd)
        - [Branching Model](#branching-model)
    - [Additional Resources](#additional-resources)

<!-- markdown-toc end -->

## Team Members
* Noah Peart
* Nathan Hunt
* Neeraj Aggarwal
* Parker Sullins
* Aromal Prasannan Lilly
* Hongchao Wang
* Jiajia Chen


## Structure

The module is made up of 3 main directories: `frontend/`, `backend/`, and `deploy`.

### Frontend

The `frontend` directory contains code for the Ticket Module frontend React
application. See `frontend/README.md` for details.

### Backend

The `backend` directory containers code for the ticket service express backend
application. It also has a `docker-compose.yml` file that can be used for
testing and deployment. See `backend/README.md` for details.

### Deployment

The `deploy` directory contains a helm chart that can be used to stand up the
application in Kubernetes.

# Development
## Development Lifecycle

### Dependencies

* Postgres
* Docker / docker compose
* node.js

### CI/CD

CI/CD configuration can be found in the `gitlab-ci.yml` file, as well as the
[templates repository](https://gitlab.com/onestepprojects/ci-templates). The
`docker build` command may need to be updated if environment variables should be
included in the Docker image.

The below diagram depicts the CI/CD Pipeline process:

![Pipeline Diagram](docs/images/pipeline.png)

### Branching Model

We will be using a branching model as described in the example diagrams below.
Capstone students should do all work off of the `capstone-main` branch in each
repository. They should also format their branch names with the following
convention: `<type>/<username>/capstone/<description>`.

![General Branching Diagram](docs/images/branching.png)

![Example Branching Diagram](docs/images/example-branching.png)


## Additional Resources

* [Basic Git Tutorial](https://www.atlassian.com/git)
* [Interactive Git Branching Tutorial](https://learngitbranching.js.org/?locale=en_US)
* [Publishing multiple distribution channels](https://semantic-release.gitbook.io/semantic-release/recipes/release-workflow/distribution-channels)
* [Angular Commit Message Formatting](https://gist.github.com/brianclements/841ea7bffdb01346392c)
* [Organization Module](https://gitlab.com/onestepprojects/organization-module)
* [Ant Design](https://ant.design/)
