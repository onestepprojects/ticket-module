import Dashboard from './components/Dashboard/Dashboard'
import CompletedTickets from './components/TicketList/CompletedTickets'
import Tickets from './components/TicketList/Tickets'
import AdminUsers from './components/Admin/Users/AdminUsers'
import AdminSettings from './components/Admin/AdminSettings'
import Webhooks from './components/Webhooks/Webhooks'
import SearchCard from './components/Search/SearchCard'
import Error404 from './components/Error/Error404'
import {
  NotificationIcon,
  ShowTicket,
  EditTicket,
  UserHistory,
  OrganizationTickets,
} from './components'
import {
  TeamOutlined,
  DashboardOutlined,
  SettingOutlined,
  TableOutlined,
  SearchOutlined,
  EditOutlined,
  ClockCircleOutlined,
  SolutionOutlined,
  FileExclamationOutlined,
  FileDoneOutlined,
  ToolOutlined,
  HistoryOutlined,
  BellOutlined,
} from '@ant-design/icons'
import ActiveTickets from './components/TicketList/ActiveTickets'
import AssignedTickets from './components/TicketList/AssignedTickets'
import UnassignedTickets from './components/TicketList/UnassignedTickets'
import { ReactNode } from 'react'
import Settings from './components/Settings/Settings'
import AdminDashboard from './components/Admin/Dashboard/AdminDashboard'
import ShowNotifications from './components/Notification/ShowNotifications'

export interface Route {
  name: string
  path: string
  exact: boolean
  component: ReactNode
  sidebarPos: number
  id: number
  subMenu?: string
  subGroup?: number
  icon?: typeof DashboardOutlined | typeof BellOutlined
  admin?: boolean
}

// SidebarPos < 0 means don't add entry to sidebar
// On sidebar, elements with same sidebarPos are grouped together to form submenu
// Within submenus, elements are sorted by subGroup, with dividers in b/w groups
const routes: Omit<Route, 'id'>[] = [
  {
    name: 'Dashboard',
    path: '/',
    exact: true,
    icon: DashboardOutlined,
    admin: false,
    component: Dashboard,
    sidebarPos: 0,
  },
  {
    name: 'Search',
    path: '/search',
    exact: true,
    admin: false,
    icon: SearchOutlined,
    component: SearchCard,
    sidebarPos: 1,
  },
  {
    name: 'Table',
    path: '/tickets',
    exact: true,
    admin: false,
    icon: TableOutlined,
    component: Tickets,
    subMenu: 'Tickets',
    subGroup: 0,
    sidebarPos: 2,
  },
  {
    name: 'Active',
    path: '/tickets/active',
    exact: true,
    admin: false,
    component: ActiveTickets,
    icon: ClockCircleOutlined,
    subMenu: 'Tickets',
    subGroup: 1,
    sidebarPos: 2,
  },
  {
    name: 'Assigned',
    path: '/tickets/assigned',
    exact: true,
    admin: false,
    component: AssignedTickets,
    icon: SolutionOutlined,
    subMenu: 'Tickets',
    subGroup: 1,
    sidebarPos: 2,
  },
  {
    name: 'Unassigned',
    path: '/tickets/unassigned',
    exact: true,
    admin: false,
    component: UnassignedTickets,
    icon: FileExclamationOutlined,
    subMenu: 'Tickets',
    subGroup: 1,
    sidebarPos: 2,
  },
  {
    name: 'Completed',
    path: '/tickets/completed',
    exact: true,
    admin: false,
    icon: FileDoneOutlined,
    component: CompletedTickets,
    subMenu: 'Tickets',
    subGroup: 2,
    sidebarPos: 2,
  },
  {
    name: 'Notifications',
    path: '/notifications',
    exact: true,
    admin: false,
    icon: NotificationIcon, // BellOutline,
    component: ShowNotifications,
    sidebarPos: 3,
  },
  {
    name: 'History',
    path: '/history',
    exact: true,
    admin: false,
    icon: HistoryOutlined,
    component: UserHistory,
    sidebarPos: 4,
  },
  {
    name: 'Settings',
    path: '/settings',
    exact: true,
    admin: false,
    icon: SettingOutlined,
    component: Settings,
    sidebarPos: 5,
  },
  // Defauls, not on sidebar
  {
    name: 'Ticket',
    path: '/tickets/:ticketId',
    exact: true,
    admin: false,
    icon: TableOutlined,
    component: ShowTicket,
    sidebarPos: -1,
  },
  {
    name: 'Organization Tickets',
    path: '/organizations/:orgId/tickets',
    exact: true,
    admin: false,
    icon: TableOutlined,
    component: OrganizationTickets,
    sidebarPos: -1,
  },
  {
    name: 'Edit Ticket',
    path: '/tickets/:ticketId/edit',
    exact: true,
    admin: false,
    icon: EditOutlined,
    component: EditTicket,
    sidebarPos: -1,
  },
  {
    name: 'Webhooks',
    path: '/webhooks',
    exact: true,
    admin: false, // XXX: True??
    component: Webhooks,
    sidebarPos: -1,
  },

  // Admin routes
  {
    name: 'Admin Dashboard',
    path: '/admin/dashboard',
    exact: true,
    admin: true,
    component: AdminDashboard,
    icon: ToolOutlined,
    sidebarPos: 0,
  },
  {
    name: 'Users',
    path: '/admin/users',
    exact: true,
    admin: true,
    component: AdminUsers,
    icon: TeamOutlined,
    sidebarPos: 1,
  },
  {
    name: 'Settings',
    path: '/admin/settings',
    exact: true,
    admin: true,
    component: AdminSettings,
    icon: SettingOutlined,
    sidebarPos: 2,
  },
  // Unknown Routes
  {
    name: 'Error404',
    path: '/',
    exact: false,
    component: Error404,
    sidebarPos: -Infinity,
  },
]

routes.sort((a, b) => {
  return Number(a.sidebarPos - b.sidebarPos > 0)
})
/* console.log(routes); */

export default routes.map((route, idx) => ({ ...route, id: idx }))
