import React, { FC } from 'react'
import { Checkbox, Divider, Form, Table, Typography } from 'antd'
import { Settings, TicketNotificationSettings, WatchedNotificationSettings } from '../../store'
import styles from './settings.module.css'
import { Paths } from '../../util'
import classnames from 'classnames'

export interface NotificationSettingsFormProps {
  settings: Settings
}

const typedPath = (paths: Paths<Settings>): Paths<Settings> => paths

const renderCell = (val: boolean, record: { name }, name) => (
  <Form.Item
    name={typedPath(['notifications', record.name, name])}
    getValueFromEvent={(e) => e.target.checked}
    valuePropName='checked'
  >
    <Checkbox defaultChecked={val} />
  </Form.Item>
)

const getColumns = (
  titles: string[],
  keys: (keyof TicketNotificationSettings | keyof WatchedNotificationSettings)[],
  rowName?: boolean
) =>
  titles.map((title: string, idx: number) => ({
    title,
    dataIndex: keys[idx],
    key: keys[idx],
    render:
      !rowName || idx > 0
        ? (val: boolean, record) => renderCell(val, record, keys[idx])
        : undefined,
  }))

const columnsCurrent = getColumns(
  ['is modified', 'changes status', 'is assigned', 'is commented on'],
  ['ticketUpdate', 'ticketStatusChange', 'ticketAssignment', 'ticketComment'],
  /* rowName= */ false
)

const columnsNew = getColumns(
  ['Tags', 'Organizations', 'Requested Roles'],
  ['withTags', 'withOrgs', 'withRoles'],
  /* rowName= */ false
)

const NotificationSettingsForm: FC<NotificationSettingsFormProps> = (props) => {
  const { settings } = props
  if (!settings) return null

  const { notifications } = settings
  const dataCurrent = [{ name: 'onSubscribed', ...notifications.onSubscribed }]
  const dataNew = [{ name: 'onWatched', ...notifications.onWatched }]

  return (
    <div>
      <Divider orientation='left' className='!pb-2 !mb-0'>
        <Typography.Title level={5}>Subscribe to notifications</Typography.Title>
      </Divider>
      <Typography.Paragraph>
        Automatically subscribe to notifications for tickets:
      </Typography.Paragraph>
      <Form.Item
        name={typedPath(['subscribeOnCreate'])}
        label='I create'
        getValueFromEvent={(e) => e.target.checked}
        valuePropName='checked'
      >
        <Checkbox defaultChecked={settings.subscribeOnCreate} />
      </Form.Item>
      <Form.Item
        name={typedPath(['subscribeOnAssign'])}
        label="I'm assigned"
        getValueFromEvent={(e) => e.target.checked}
        valuePropName='checked'
      >
        <Checkbox defaultChecked={settings.subscribeOnAssign} />
      </Form.Item>

      <Divider orientation='left' className='!pb-2 !mb-0'>
        <Typography.Title level={5}>Notifications for subscribed tickets</Typography.Title>
      </Divider>
      <Typography.Paragraph>
        Receive notifications from tickets I'm subscribed to when the ticket:
      </Typography.Paragraph>
      <Table
        className={classnames('!max-w-[680px]', styles.settingsTable)}
        columns={columnsCurrent}
        dataSource={dataCurrent}
        pagination={false}
        rowKey='name'
      />

      <Divider orientation='left' className='!pt-2'>
        <Typography.Title level={5}>Notifications for new tickets</Typography.Title>
      </Divider>
      <Typography.Paragraph>
        Receive notifications for new tickets when they are created with things I'm watching:
      </Typography.Paragraph>
      <Table
        className='!max-w-md'
        columns={columnsNew}
        dataSource={dataNew}
        pagination={false}
        rowKey='name'
      />
    </div>
  )
}

export default NotificationSettingsForm
