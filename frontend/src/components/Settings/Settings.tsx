import React, { FC } from 'react'
import { Card, Tabs } from 'antd'
import NotificationSettings from './NotificationSettings'
import WatchedCard from '../Watch/WatchedCard'
import { SubscriptionsCard } from '../Subscriptions'
const { TabPane } = Tabs

export interface SettingsProps {}

const Settings: FC<SettingsProps> = (_props) => {
  return (
    <Card>
      <Tabs type='card'>
        <TabPane key={1} tab='Notifications'>
          <NotificationSettings />
        </TabPane>
        <TabPane key={2} tab='Watching'>
          <WatchedCard />
        </TabPane>
        <TabPane key={3} tab='Subscribed'>
          <SubscriptionsCard />
        </TabPane>
      </Tabs>
    </Card>
  )
}

export default Settings
