import React, { FC } from 'react'
import { Form, notification, Button, Card, Typography } from 'antd'
import { Settings, useGetSettingsQuery, useUpdateSettingsMutation } from '../../store'
import { useMemoizedFn } from 'ahooks'

import NotificationSettingsForm from './NotificationSettingsForm'

export interface NotificationSettingsProps {}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4, offset: 6, pull: 1 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12, offset: 4, pull: 5 },
  },
}

const NotificationSettings: FC<NotificationSettingsProps> = (_props) => {
  const { data: settings, isLoading } = useGetSettingsQuery()
  const [touched, setTouched] = React.useState(false)
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [form] = Form.useForm<Settings>()
  const [updateSettings] = useUpdateSettingsMutation()

  const handleSubmit = useMemoizedFn(async () => {
    try {
      const values = await form.validateFields()
      setIsSubmitting(true)
      await updateSettings(values).unwrap()
      setTouched(false)
      notification.success({ message: 'Settings updated' })
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  })

  React.useEffect(() => {
    form.setFieldsValue(settings)
  }, [settings])

  if (isLoading) return null
  return (
    <div className='p-1'>
      <Form
        form={form}
        onFieldsChange={() => setTouched(true)}
        onFinish={handleSubmit}
        {...formItemLayout}
      >
        <Card
          title={
            <div className='flex justify-between pr-10 pt-2'>
              <Typography.Title level={3} className='!mb-2'>
                Notifications Settings
              </Typography.Title>
              <Button type='primary' htmlType='submit' loading={isSubmitting} disabled={!touched}>
                Save Settings
              </Button>
            </div>
          }
        >
          <NotificationSettingsForm settings={settings} />
        </Card>
      </Form>
    </div>
  )
}

export default NotificationSettings
