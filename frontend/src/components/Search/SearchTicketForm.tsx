import React, { FC } from 'react'
import {
  Button,
  Checkbox,
  DatePicker,
  Divider,
  Form,
  FormInstance,
  FormProps,
  Input,
  Radio,
  Row,
  Space,
  Typography,
} from 'antd'
import OrganizationSelect from '../Input/OrganizationSelect'
import TagSelect from '../Input/TagSelect'
import TicketEnumCheckbox from '../Input/TicketEnumCheckbox'
import PersonSelect from '../Input/PersonSelect'
import { SearchTicketsFormInput, TagType } from '../../store'
import { capitalize } from 'lodash-es'

export interface SearchTicketFormProps extends FormProps {
  form: FormInstance<SearchTicketsFormInput>
  onFinish
  className?: string
  confirmLoading?: boolean
}

// enum inputs to add to form
const ticketEnums = ['status', 'priority', 'visibility'] as const
const ticketPersons = ['creator', 'assignees', 'contributors'] as const

const TicketEnumSelectors = () => {
  return (
    <>
      {ticketEnums.map((x) => (
        <Form.Item key={x} label={capitalize(x)} name={x}>
          <TicketEnumCheckbox type={x} />
        </Form.Item>
      ))}
    </>
  )
}

const TicketPersonSelectors = () => {
  return (
    <div className='search-persons'>
      {ticketPersons.map((x) => (
        <Form.Item key={x} label={capitalize(x)} name={x}>
          <PersonSelect disabled placeholder={`select ${x}...`} mode='multiple' />
        </Form.Item>
      ))}
    </div>
  )
}

const TagSelectors = () => {
  return (
    <div className='search-tags'>
      {Object.keys(TagType).map((x) => (
        <Form.Item
          key={x}
          label={capitalize(x)}
          name={x}
          valuePropName={x}
          getValueFromEvent={(e) => e.map((x: { label: string }) => x.label)}
        >
          <TagSelect type={[x]} placeholder={`select ${x}s...`} />
        </Form.Item>
      ))}
    </div>
  )
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4, offset: 6, pull: 5 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16, offset: 6, pull: 5 },
  },
}

const SearchTicketForm: FC<SearchTicketFormProps> = (props) => {
  const { form, confirmLoading, className, onFinish, ...formProps } = props
  const [isLiteral, setIsLiteral] = React.useState(form.getFieldValue('literal'))

  return (
    <div className={className}>
      <Divider orientation='left'>
        <Typography.Title level={4}>Search tickets</Typography.Title>
      </Divider>

      <Row className='mx-10'>
        <p>
          When literal is selected, text in ticket tickets/details is searched for exact matches of
          the input queries. Otherwise, queries support PostgreSql 'and' (&) and 'or' (|) operators.
          For further details, see{' '}
          <a href='https://www.prisma.io/docs/concepts/components/prisma-client/full-text-search'>
            full text searches
          </a>
        </p>
      </Row>

      <Form
        form={form}
        name='search-tickets'
        onFinish={onFinish}
        initialValues={{
          isAssigned: 'all',
        }}
        {...formItemLayout}
        {...formProps}
        layout='vertical'
      >
        <Form.Item label='Match titles' name='title'>
          <Input autoFocus placeholder='search for matches in titles...' />
        </Form.Item>

        <Form.Item label='Match details' name='detail'>
          <Input placeholder='search for matches in ticket bodies...' />
        </Form.Item>

        <Form.Item label=' ' colon={false} className='!mb-0'>
          <Space size={13}>
            <Form.Item name='literal' label='Literal search' valuePropName='checked'>
              <Checkbox onChange={(e) => setIsLiteral(e.target.checked)} value={true} />
            </Form.Item>

            {isLiteral && (
              <Form.Item name='caseSensitive' label='Case-sensitive' valuePropName='checked'>
                <Checkbox disabled={!isLiteral} value={true} />
              </Form.Item>
            )}
          </Space>
        </Form.Item>

        <Form.Item label='Created' className='!mb-0'>
          <Space size={13}>
            <Form.Item name='createdAfter' label='after'>
              <DatePicker />
            </Form.Item>
            <Form.Item name='createdBefore' label='before'>
              <DatePicker />
            </Form.Item>
          </Space>
        </Form.Item>

        <TicketEnumSelectors />

        <Form.Item name='isAssigned' label='Is Assigned?'>
          <Radio.Group>
            <Radio defaultChecked value={'all'}>
              All
            </Radio>
            <Radio value={false}>Unassigned</Radio>
            <Radio value={true}>Assigned</Radio>
          </Radio.Group>
        </Form.Item>

        <TagSelectors />

        <Form.Item label='Organization(s)' name='organizations'>
          <OrganizationSelect disabled />
        </Form.Item>

        <TicketPersonSelectors />

        <div className='search-controls flex justify-center gap-8'>
          <Button htmlType='submit' type='primary' loading={confirmLoading}>
            Submit
          </Button>
        </div>
      </Form>
    </div>
  )
}

export default SearchTicketForm
