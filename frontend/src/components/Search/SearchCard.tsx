import React, { FC } from 'react'
import { Card, Form, notification, Spin } from 'antd'
import { SearchTicketsFormInput, useLazySearchTicketsQuery } from '../../store'
import { useMemoizedFn } from 'ahooks'
import SearchTicketForm from './SearchTicketForm'
import SearchTicketResults from './SearchTicketResults'
import { processSearchForm } from '../../util/search'

export interface SearchCardProps {}

const SearchCard: FC<SearchCardProps> = (_props) => {
  const [confirmLoading, setConfirmLoading] = React.useState(false)
  const [form] = Form.useForm<Partial<SearchTicketsFormInput>>()
  const [getSearchResults, { data: tickets, isLoading }] = useLazySearchTicketsQuery()

  const handleSubmit = useMemoizedFn(async (values: SearchTicketsFormInput) => {
    setConfirmLoading(true)
    try {
      const vals = processSearchForm(values)
      await getSearchResults(vals).unwrap()
    } catch (err) {
      console.debug(err)
      notification.error({
        message: 'Search failed',
        description: err?.data?.message,
      })
    } finally {
      setConfirmLoading(false)
    }
  })
  /*
   *   const handleValidate = useMemoizedFn(async () => {
   *     try {
   *       return form.validateFields()
   *     } catch (err) {
   *       console.debug('Search form validation failed:', err)
   *     }
   *   })
   *  */
  return (
    <Card className='mx-8 flex flex-col'>
      <SearchTicketForm form={form} onFinish={handleSubmit} confirmLoading={confirmLoading} />

      {isLoading ? <Spin /> : <SearchTicketResults tickets={tickets} />}
    </Card>
  )
}

export default SearchCard
