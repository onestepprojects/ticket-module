import { List, ListProps } from 'antd'
import { truncate } from 'lodash-es'
import dayjs from 'dayjs'
import React, { FC } from 'react'
import { useHistory } from 'react-router-dom-v5'
import { Ticket } from '../../store'

export interface SearchTicketResultsProps extends ListProps<Ticket> {
  title?: string
  detail?: string
  tickets: Ticket[]
}

const SearchResults: FC<SearchTicketResultsProps> = (props) => {
  const { tickets, ...listProps } = props
  const history = useHistory()

  if (!tickets) return null
  return (
    <List
      header={`Found ${tickets.length} results`}
      pagination={{
        position: 'both',
        pageSize: 10,
        defaultCurrent: 1,
        defaultPageSize: 10,
      }}
      {...listProps}
      dataSource={tickets}
      renderItem={(ticket) => (
        <List.Item
          key={ticket.id}
          onClick={() => history.push(`/tickets/${ticket.id}`)}
          className='hover:bg-slate-100 cursor-pointer'
        >
          <List.Item.Meta
            title={
              <div className='flex justify-between'>
                <div>
                  <span className='italic'>[#{ticket.id}]</span> {ticket.title}
                </div>
                <div>
                  <span className='text-slate-600'>
                    {dayjs(ticket.createdAt).format('MMM DD, YYYY')}
                  </span>
                </div>
              </div>
            }
            description={truncate(ticket.detail, { length: 100 })}
          />
        </List.Item>
      )}
    />
  )
}

export default SearchResults
