import { PlusOutlined } from '@ant-design/icons'
import { useMemoizedFn } from 'ahooks'
import { Divider, Form, FormInstance, Row, Tag } from 'antd'
import { isEmpty } from 'lodash-es'
import React, { FC } from 'react'
import { WatchingData } from '.'
import {
  selectTags,
  Tag as TicketTag,
  useAppSelector,
  useGetTagsQuery,
  WatchingUpdate,
} from '../../store'
import { Paths } from '../../util'
import TagComplete from '../Input/TagComplete'

export interface WatchedTagsProps {
  form: FormInstance<WatchingData>
  setTouched: React.Dispatch<React.SetStateAction<boolean>>
  tagIds: number[]
}

const typedPath = (paths: Paths<WatchingUpdate>): Paths<WatchingUpdate> => paths

const WatchedTags: FC<WatchedTagsProps> = (props) => {
  const { tagIds, form, setTouched } = props
  const [tagFormVisible, setTagFormVisible] = React.useState(false)
  const [pending, setPending] = React.useState<WatchingData['tagIds']>({})
  const { data: _, isLoading: tagsLoading } = useGetTagsQuery()
  const tags = useAppSelector(selectTags)
  const ids = tagIds?.reduce((acc, id) => ({ ...acc, [id]: true }), {})

  React.useEffect(() => {
    setPending({})
  }, [tagIds])

  // add/remove tag from pending
  const updatePending = (adding: boolean, close: boolean, id: number) => {
    const cur = Object.assign({}, pending)

    if (close) {
      if (cur[id] === undefined) return
      delete cur[id]
    } else {
      if (cur[id] !== undefined) return
      cur[id] = adding
    }

    const fields = form.getFieldsValue()
    Object.assign(fields, { tagIds: cur })
    form.setFieldsValue(fields)
    setPending(cur)
    setTouched(!close || Object.keys(cur).length > 0)
  }

  const handleSelect = useMemoizedFn((tag: TicketTag) => {
    updatePending(true, /* close= */ false, tag.id)
  })

  if (tagsLoading) return null
  return (
    <div>
      <Divider orientation='left'>
        <span className='inline-flex items-center'>
          Tags{' '}
          <PlusOutlined
            onClick={() => setTagFormVisible(!tagFormVisible)}
            className='ml-2 cursor-pointer'
          />
        </span>
      </Divider>

      {/* empty items to hook up form */}
      <Form.Item name={typedPath(['tagIds'])} noStyle hidden />

      {tagFormVisible && (
        <TagComplete
          className='!mr-4 !mb-2'
          onSelectedTag={(tag) => handleSelect(tag)}
          onSelectedLabel={() => ''}
          exclude={Object.assign({}, ids, pending)}
          placeholder='add tags...'
        />
      )}

      <div className='!mb-4'>
        {!isEmpty(pending) && <span className='italic'>Pending Changes: </span>}
        {Object.entries(pending).map(([id, add]) => (
          <Tag
            key={id}
            className='!inline-flex items-center'
            closable
            onClose={() => updatePending(add, /* close= */ true, Number(id))}
            color={add ? 'green' : 'volcano'}
          >
            {tags[id].name}
          </Tag>
        ))}
      </div>

      <Row>
        {tagIds
          ?.filter((id) => pending[id] !== false)
          .map((id) => (
            <Tag
              key={id}
              className='!inline-flex items-center'
              color='geekblue'
              closable
              onClose={() => updatePending(false, /* close= */ false, id)}
            >
              {tags[id].name}
            </Tag>
          ))}
      </Row>
    </div>
  )
}

export default WatchedTags
