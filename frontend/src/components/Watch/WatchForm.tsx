import React, { FC } from 'react'
import { Button, Form, FormProps, Input, notification } from 'antd'
import { WatchingUpdate } from '../../store'
import { useMemoizedFn } from 'ahooks'

export interface WatchFormProps extends FormProps {
  onFinish
}

const WatchForm: FC<WatchFormProps> = (props) => {
  const { onFinish, ...formProps } = props
  const [form] = Form.useForm<WatchingUpdate>()
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const handleSubmit = useMemoizedFn(async () => {
    setIsSubmitting(true)
    try {
      const vals = await form.validateFields()
      await onFinish(vals).unwrap()
      notification.success({ message: 'Watched tags/orgs updated' })
    } catch (err) {
      notification.error({
        message: 'Failed to update watched',
        description: err?.data?.message,
      })
    } finally {
      setIsSubmitting(false)
    }
  })

  return (
    <Form form={form} onFinish={handleSubmit} colon={false} {...formProps}>
      <Form.Item name='tagIds' label='Tags'>
        <Input />
      </Form.Item>
      <Form.Item name='orgIds' label='Orgs'>
        <Input />
      </Form.Item>
      <Button type='primary' htmlType='submit' loading={isSubmitting}>
        Submit
      </Button>
    </Form>
  )
}

export default WatchForm
