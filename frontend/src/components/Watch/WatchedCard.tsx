import React, { FC } from 'react'
import { Button, Card, Form, Typography } from 'antd'
import { useGetWatchedQuery, useUpdateWatchedMutation } from '../../store'
import WatchedTags from './WatchedTags'
import WatchedOrgs from './WatchedOrgs'
import { useMemoizedFn } from 'ahooks'
import ErrorBoundary from 'antd/lib/alert/ErrorBoundary'
import { invertBy } from 'lodash-es'

export interface WatchedCardProps {}

export type WatchingData = {
  tagIds: { [key: number]: boolean }
  orgIds: { [key: string]: boolean }
}
const emptyUpdate: WatchingData = { orgIds: {}, tagIds: {} }

const WatchedCard: FC<WatchedCardProps> = () => {
  const [touched, setTouched] = React.useState(false)
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [form] = Form.useForm<WatchingData>()
  const { data: watching, isLoading } = useGetWatchedQuery()
  const [updatedWatched] = useUpdateWatchedMutation()

  const handleSubmit = useMemoizedFn(async () => {
    setIsSubmitting(true)
    try {
      const vals = await form.validateFields()
      const tids = invertBy(vals.tagIds)
      await updatedWatched({
        tagIds: {
          add: tids.true?.map(Number),
          remove: tids.false?.map(Number),
        },
      }).unwrap()
      setTouched(false)
      form.resetFields()
      form.setFieldsValue(emptyUpdate)
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  })

  if (isLoading) return null
  return (
    <Form form={form} preserve={true} onFinish={handleSubmit}>
      <Card
        title={
          <div className='flex justify-between pr-10 pt-2'>
            <Typography.Title level={4} className='!mb-2'>
              Manage watched items
            </Typography.Title>
            <Button type='primary' htmlType='submit' loading={isSubmitting} disabled={!touched}>
              Save Settings
            </Button>
          </div>
        }
      >
        <WatchedTags tagIds={watching?.tagIds} form={form} setTouched={setTouched} />

        <ErrorBoundary>
          <WatchedOrgs orgIds={watching?.orgIds} form={form} setTouched={setTouched} />
        </ErrorBoundary>
      </Card>
    </Form>
  )
}

export default WatchedCard
