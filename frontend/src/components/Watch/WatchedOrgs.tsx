import { PlusOutlined } from '@ant-design/icons'
import { Divider, Form, FormInstance, Tag } from 'antd'
import React, { FC } from 'react'
import { selectUuid, useGetOrganizationRolesByPersonQuery, WatchingUpdate } from '../../store'

export interface WatchedOrgProps {
  form: FormInstance<WatchingUpdate>
  setTouched: React.Dispatch<React.SetStateAction<boolean>>
  orgIds: string[]
}

const WatchedOrgs: FC<WatchedOrgProps> = (props) => {
  const { orgIds, form, setTouched } = props // eslint-disable-line
  const [orgFormVisible, setOrgFormVisible] = React.useState(false)

  const uuid = selectUuid()
  if (!uuid) return null
  /* XXX: replace with useGetOrganizationsQuery when enpoint works again */
  const { data: orgsData, isLoading: orgsLoading } = useGetOrganizationRolesByPersonQuery(uuid)

  if (orgsLoading) return null

  const orgs = orgsData.ids.reduce(
    (acc, id) => ({
      ...acc,
      [id]: orgsData.entities[id],
    }),
    {}
  )

  return (
    <div className='w-100'>
      <Divider orientation='left'>
        <span className='inline-flex items-center'>
          Organizations{' '}
          <PlusOutlined
            onClick={() => setOrgFormVisible(!orgFormVisible)}
            className='ml-2 cursor-pointer'
          />
        </span>
      </Divider>

      {orgFormVisible && <Form.Item name='orgIds'></Form.Item>}

      {orgIds.map((id) => (
        <Tag key={id} color='volcano'>
          {orgs[id]?.name ?? id}
        </Tag>
      ))}
    </div>
  )
}

export default WatchedOrgs
