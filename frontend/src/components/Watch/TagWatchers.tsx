import { Card, Col, Row, Spin, Tag, Typography } from 'antd'
import React, { FC } from 'react'
import { useLazyGetTagWatchersQuery } from '../../store/services/watchingApi'
import TagComplete from '../Input/TagComplete'

export interface TagWatchersProps {}

const TagWatchers: FC<TagWatchersProps> = () => {
  const [getTagWatchers, { data: watchers, isLoading }] = useLazyGetTagWatchersQuery()
  const [tag, setTag] = React.useState(null)

  React.useEffect(() => {
    if (!tag) return
    getTagWatchers(tag.id)
  }, [tag])

  return (
    <Card>
      <Row className='items-center mb-1'>
        <Col span={3}>
          <span className='pr-2'>Tag</span>
        </Col>
        <TagComplete className='!pr-2' style={{ width: 160 }} onSelectedTag={setTag} />
      </Row>
      <Row>
        <Col span={3}>
          <Typography.Text className='pr-1'>Watchers({watchers?.length}):</Typography.Text>
        </Col>
        <Col>
          {isLoading ? (
            <Spin />
          ) : (
            watchers?.map(({ id, name }) => (
              <Tag key={id} color='geekblue'>
                {name}
              </Tag>
            ))
          )}
        </Col>
      </Row>
    </Card>
  )
}

export default TagWatchers
