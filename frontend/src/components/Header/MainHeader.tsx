import React, { FC } from 'react'
import { Layout } from 'antd'
import styles from './header.module.css'
import classnames from 'classnames'
import { DefaultLayoutProps } from '../Layout'
import NotificationPopover from '../Notification/NotificationPopover'

export type MainHeaderProps = Omit<DefaultLayoutProps, 'children'>

const MainHeader: FC<MainHeaderProps> = (_props) => {
  const { Header } = Layout
  return (
    <Header
      id='main-header'
      className={classnames(styles.header, 'shadow-md', {
        [styles.fixed]: true,
        [styles.collapsed]: false,
      })}
    >
      <div className='logo' />
      <div className={styles.rightContainer}>
        <NotificationPopover getPopupContainer={() => document.querySelector('#main-header')} />
      </div>
    </Header>
  )
}

export default MainHeader
