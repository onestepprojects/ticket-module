import React, { FC } from 'react'
import { Link } from 'react-router-dom-v5'
import { useTable, useFilters, useGlobalFilter, usePagination } from 'react-table'
import { MarkdownPreview } from '..'
import { Ticket } from '../../store'

const high = 'bg-red-100 text-red-800'
const low = 'bg-blue-100 text-blue-800'
const normal = 'bg-green-100 text-green-800'

const defaultColumns: { Header; accessor?; width?; id; Cell? }[] = [
  {
    Header: 'No.',
    accessor: 'id',
    width: 10,
    id: 'id',
  },
  {
    Header: 'Priority',
    accessor: 'priority',
    id: 'priority',
    // eslint-disable-next-line
    Cell: ({ value }) => {
      const badge = value === 'Low' ? low : value === 'Normal' ? normal : high
      return (
        <>
          <span
            className={`inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium ${badge}`}
          >
            {value}
          </span>
        </>
      )
    },
  },
  {
    Header: 'Title',
    accessor: 'title',
    id: 'Title',
    // eslint-disable-next-line
    Cell: ({ value }) => {
      return (
        <div className='truncate'>
          <MarkdownPreview data={value} />
        </div>
      )
    },
  },
  {
    Header: '',
    id: 'actions',
    // eslint-disable-next-line
    Cell: ({ row }) => {
      return (
        <>
          {/* eslint-disable-next-line */}
          <Link to={`/tickets/${row.cells[0].value}`}>View</Link>
        </>
      )
    },
  },
]

const DefaultColumnFilter: FC<{ column }> = ({ column: { filterValue, setFilter } }) => {
  return (
    <input
      className='shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md'
      type='text'
      value={filterValue || ''}
      onChange={(e) => {
        setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
      }}
      placeholder='Type to filter'
    />
  )
}

const TicketTable: FC<{ data: Ticket[]; columns?: Array<object> }> = ({ data, columns }) => {
  if (!data) return null

  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      // fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) =>
        rows.filter((row) => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue).toLowerCase().startsWith(String(filterValue).toLowerCase())
            : true
        }),
    }),
    []
  )

  const defaultColumn = React.useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
    }),
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    canPreviousPage,
    canNextPage,
    /* pageCount,
     * gotoPage, */
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex: _, pageSize },
  } = useTable(
    {
      columns: columns || defaultColumns,
      data,
      defaultColumn, // Be sure to pass the defaultColumn option
      filterTypes,
      initialState: {
        pageIndex: 0,
      },
    },
    useFilters, // useFilters!
    useGlobalFilter,
    usePagination
  )

  return (
    <div className='overflow-x-auto md:-mx-6 lg:-mx-8'>
      <div className='py-2 align-middle inline-block min-w-full md:px-6 lg:px-8'>
        <div className='shadow overflow-hidden border-b border-gray-200 md:rounded-lg'>
          <table {...getTableProps()} className='min-w-full divide-y divide-gray-200'>
            <thead className='bg-gray-50'>
              {headerGroups.map((headerGroup) => (
                <tr
                  {...headerGroup.getHeaderGroupProps()}
                  key={headerGroup.headers.map((header) => header.id)}
                >
                  {headerGroup.headers.map((column) =>
                    column.hideHeader === false ? null : (
                      <th
                        {...column.getHeaderProps()}
                        className='px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider'
                      >
                        {column.render('Header')}
                        <div>{column.canFilter ? column.render('Filter') : null}</div>
                      </th>
                    )
                  )}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {page.map((row) => {
                prepareRow(row)
                return (
                  <tr {...row.getRowProps()} className='bg-white'>
                    {row.cells.map((cell) => (
                      <td
                        className='px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900'
                        {...cell.getCellProps()}
                      >
                        {cell.render('Cell')}
                      </td>
                    ))}
                  </tr>
                )
              })}
            </tbody>
          </table>

          <nav
            className='bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6'
            aria-label='Pagination'
          >
            <div className='hidden sm:block'>
              <div className='flex flex-row flex-nowrap w-full space-x-2'>
                <p htmlFor='location' className='block text-sm font-medium text-gray-700 mt-4'>
                  Show
                </p>
                <select
                  id='location'
                  name='location'
                  className='block w-full pl-3 pr-10 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md'
                  value={pageSize}
                  onChange={(e) => {
                    setPageSize(Number(e.target.value))
                  }}
                >
                  {[10, 20, 30, 40, 50].map((pageSize) => (
                    <option key={pageSize} value={pageSize}>
                      {pageSize}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className='flex-1 flex justify-between sm:justify-end'>
              <button
                className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
                type='button'
                onClick={() => previousPage()}
                disabled={!canPreviousPage}
              >
                Previous
              </button>
              <button
                className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
                type='button'
                onClick={() => nextPage()}
                disabled={!canNextPage}
              >
                Next
              </button>
            </div>
          </nav>
        </div>
      </div>
    </div>
  )
}
export default TicketTable
