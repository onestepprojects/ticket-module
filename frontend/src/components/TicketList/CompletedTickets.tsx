import React from 'react'
import TicketTable from './TicketTable'
import {
  selectAllTickets,
  ticketsByStatus,
  TicketStatus,
  useAppSelector,
  useGetTicketsQuery,
} from '../../store'
import { Spin } from 'antd'

const CompletedTickets = () => {
  const { data: _, isLoading } = useGetTicketsQuery()
  const tickets = ticketsByStatus(TicketStatus.completed, useAppSelector(selectAllTickets))

  if (isLoading) return <Spin />
  return (
    <div className='hidden sm:block'>
      <TicketTable data={tickets} />
    </div>
  )
}

export default CompletedTickets
