import React, { FC } from 'react'
import {
  ticketsByStatus,
  selectAllTickets,
  useAppSelector,
  useGetTicketsQuery,
  TicketStatus,
} from '../../store'
import TicketTable from './TicketTable'
import { Spin } from 'antd'

const ActiveTickets: FC = (_props) => {
  const { data: _tickets, isLoading } = useGetTicketsQuery()
  const tickets = ticketsByStatus(TicketStatus.open, useAppSelector(selectAllTickets))

  if (isLoading) return <Spin />
  return (
    <div className='hidden sm:block'>
      <TicketTable data={tickets} />
    </div>
  )
}
export default ActiveTickets
