import React, { FC } from 'react'
import { selectAllTickets, useAppSelector, useGetTicketsQuery } from '../../store'
import TicketTable from './TicketTable'
import { Spin } from 'antd'

export const Tickets: FC = () => {
  const { data: _, isLoading } = useGetTicketsQuery()
  const tickets = useAppSelector(selectAllTickets)

  if (isLoading) return <Spin />
  return (
    <div className='hidden sm:block'>
      <TicketTable data={tickets} />
    </div>
  )
}
export default Tickets
