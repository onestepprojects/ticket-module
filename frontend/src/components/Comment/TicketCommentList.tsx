import React, { FC } from 'react'
import dayjs from 'dayjs'
import { useGetCommentsForTicketQuery } from '../../store/services/commentApi'
import { Button, List, ListProps } from 'antd'
import { Comment } from '@ant-design/compatible'
import {
  type Comment as CommentType,
  selectPersons,
  useAppSelector,
  useGetPersonsQuery,
} from '../../store'
import classnames from 'classnames'
import TicketCommentForm from '../Forms/TicketCommentForm'
import MarkdownPreview from '../Markdown/MarkdownPreview'

export interface TicketCommentListProps extends ListProps<CommentType> {
  ticketId: number
  className?: string
}

const TicketCommentList: FC<TicketCommentListProps> = (props) => {
  const { ticketId, className, id, ...listProps } = props
  const [expanded, setExpanded] = React.useState(true)
  const { data: comments, isLoading } = useGetCommentsForTicketQuery(ticketId)
  const { data: _, isLoading: isLoadingPersons } = useGetPersonsQuery()
  const persons = useAppSelector(selectPersons)

  if (isLoading || isLoadingPersons || !persons || !comments) return null

  // should temp fix issue
  const personsEmpty = Object.keys(persons).length < 1
  const url = 'https://cdn.onlinewebfonts.com/svg/img_181369.png'
  // removed persons[userId].name

  return (
    <div id={id} className={classnames('ticket-comments', className)}>
      <TicketCommentForm ticketId={ticketId} />
      {/* <pre>{comments.length && JSON.stringify(comments)}</pre> */}
      <List
        header={
          <div className='flex justify-between fw-full'>
            <span>{comments.length} replies</span>
            <Button type='link' onClick={() => setExpanded(!expanded)}>
              {comments?.length > 0 && expanded ? 'hide' : 'show'}
            </Button>
          </div>
        }
        itemLayout='horizontal'
        dataSource={comments}
        renderItem={({ comment, userId, createdAt }) =>
          expanded && (
            <li>
              <Comment
                content={<MarkdownPreview data={comment} />}
                author={personsEmpty ? 'Author PlaceHolder' : persons[userId].name}
                avatar={personsEmpty ? url : persons[userId]?.profilePicture?.url}
                datetime={dayjs(createdAt).fromNow()}
              />
            </li>
          )
        }
        {...listProps}
      />
    </div>
  )
}

export default TicketCommentList
