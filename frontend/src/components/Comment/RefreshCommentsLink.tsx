import { notification, Spin } from 'antd'
import React, { FC } from 'react'
import { useLocation } from 'react-router-dom-v5'
import { useLazyGetCommentsForTicketQuery } from '../../store'

export interface RefreshCommentsLinkProps {
  className?: string
}

const pathRegex = /\/tickets\/(\d+)/

const getTicketId = (path?: string) => {
  if (!path) return undefined
  const match = pathRegex.exec(path)
  return match ? match[1] : undefined
}

export const RefreshCommentsLink: FC<RefreshCommentsLinkProps> = (props) => {
  const { className } = props
  const location = useLocation()
  const [refreshComments, { data: _ }] = useLazyGetCommentsForTicketQuery()
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const handleSubmit = async (ticketId: number) => {
    setIsSubmitting(true)
    try {
      await refreshComments(ticketId, false).unwrap()
      notification.success({
        message: `Comments updated for ticket ${ticketId}`,
      })
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  }

  return isSubmitting ? (
    <Spin />
  ) : (
    <a
      className={className}
      onClick={() => {
        const ticketId = getTicketId(location.pathname)
        if (ticketId) handleSubmit(Number(ticketId))
        else
          notification.warning({
            message: 'Must be on a ticket page to refresh its comments',
          })
      }}
      {...props}
    >
      Refresh Comments
    </a>
  )
}

export default RefreshCommentsLink
