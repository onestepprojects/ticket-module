import React, { FC } from 'react'
import classnames from 'classnames'
import { Card, CardProps } from 'antd'
import {
  useCreateWebhookMutation,
  useDeleteWebhookMutation,
  useGetWebooksQuery,
  useUpdateWebhookMutation,
} from '../../store'

export interface WebhooksProps extends CardProps {
  className?: string
}

export const Webhooks: FC<WebhooksProps> = (props) => {
  const { className, ...cardProps } = props
  const { data: webhooks, isLoading } = useGetWebooksQuery()
  const [updateWebhook] = useUpdateWebhookMutation() // eslint-disable-line
  const [createWebhook] = useCreateWebhookMutation() // eslint-disable-line
  const [deleteWebhook] = useDeleteWebhookMutation() // eslint-disable-line

  if (isLoading || !webhooks) return null
  return <Card className={classnames('', className)} title='Webhooks' {...cardProps}></Card>
}
export default Webhooks
