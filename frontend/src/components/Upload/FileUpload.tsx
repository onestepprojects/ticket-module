import React, { FC } from 'react'
/* import { UploadOutlined } from '@ant-design/icons' */
import FileList from './FileList'
import { useUploadFileMutation } from '../../store'

export interface FileUploadProps {}

const FileUpload: FC<FileUploadProps> = () => {
  const [uploaded, setUploaded] = React.useState([])
  const [uploadFile] = useUploadFileMutation() // eslint-disable-line

  return (
    <div>
      <FileList uploaded={uploaded} setUploaded={setUploaded} />
    </div>
  )
}

export default FileUpload
