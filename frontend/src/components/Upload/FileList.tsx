import React, { FC } from 'react'
/* import fileDownload from 'js-file-download' */
import { useGetUserFilesQuery } from '../../store'

export interface FileListProps {
  uploaded: string[]
  setUploaded: React.Dispatch<React.SetStateAction<string[]>>
}

export const FileList: FC<FileListProps> = (props) => {
  const { uploaded, setUploaded } = props // eslint-disable-line
  const { data: files, isLoading } = useGetUserFilesQuery()

  if (isLoading || !files) return null
  return <div></div>
}
export default FileList
