import React, { FC } from 'react'
import { StatusBadgeProps } from './StatusRibbon'
import classnames from 'classnames'

const StatusBadge: FC<StatusBadgeProps> = ({ status, className }) => {
  return (
    <span
      className={classnames(
        'inline-flex items-center px-2.5 py-0.5 rounded-full h-6 text-md font-medium text-white',
        status === 'open' ? 'bg-red-500' : status === 'completed' ? 'bg-green-500' : 'bg-gray-500',
        className
      )}
    >
      {status}
    </span>
  )
}

export default StatusBadge
