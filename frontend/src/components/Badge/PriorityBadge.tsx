import React, { FC } from 'react'
import { type TicketPriority } from '../../store'
import { Tag } from 'antd'

export interface PriorityBadgeProps {
  priority: TicketPriority
  className?: string
}

const PriorityBadge: FC<PriorityBadgeProps> = (props) => {
  const { priority, className } = props

  return (
    <Tag
      className={className}
      color={priority === 'low' ? 'geekblue' : priority === 'normal' ? 'green' : 'volcano'}
    >
      {priority}
    </Tag>
  )
}

export default PriorityBadge
