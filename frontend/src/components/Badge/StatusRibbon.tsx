import { Badge } from 'antd'
import React, { FC, PropsWithChildren } from 'react'
import { type TicketStatus } from '../../store'

export interface StatusBadgeProps {
  status: TicketStatus
  className?: string
}

const StatusRibbon: FC<PropsWithChildren<StatusBadgeProps>> = (props) => {
  const { status, children, className } = props

  return (
    <Badge.Ribbon
      text={status}
      color={status === 'open' ? 'red' : status === 'completed' ? 'green' : 'gray'}
      className={className}
    >
      {children}
    </Badge.Ribbon>
  )
}

export default StatusRibbon
