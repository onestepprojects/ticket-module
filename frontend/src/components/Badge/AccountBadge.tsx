import { UserOutlined } from '@ant-design/icons'
import React, { FC } from 'react'

export interface AccountBadgeProps {
  isAdmin?: boolean
}

export const AccountBadge: FC<AccountBadgeProps> = ({ isAdmin }) => (
  <span className='text-green-400 flex items-center'>
    <UserOutlined className='!text-green-500 text-[23px] mr-1.5' />
    {isAdmin ? 'Admin' : 'user'}
  </span>
)

export default AccountBadge
