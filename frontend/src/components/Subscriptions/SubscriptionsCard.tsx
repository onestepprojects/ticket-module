import { useMemoizedFn } from 'ahooks'
import { Button, Card, Checkbox, Form, List, Typography } from 'antd'
import ErrorBoundary from 'antd/lib/alert/ErrorBoundary'
import React, { FC } from 'react'
import {
  useGetSubscriptionsQuery,
  useGetTicketsQuery,
  useUnsubscribeFromTicketsMutation,
} from '../../store'
import SubscriptionItem from './SubscriptionItem'

export interface SubscriptionsCardProps {}

export interface SubscriptionForm {
  [_: string]: boolean
}

export const SubscriptionsCard: FC<SubscriptionsCardProps> = (_props) => {
  const { data: subs, isLoading: loadingSubs } = useGetSubscriptionsQuery()
  const { data: tickets, isLoading: loadingTickets } = useGetTicketsQuery()
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [touched, setTouched] = React.useState(false)
  const [numSelected, setNumSelected] = React.useState(0)
  const [unsubscribe] = useUnsubscribeFromTicketsMutation()
  const [form] = Form.useForm<SubscriptionForm>()

  const handleSubmit = useMemoizedFn(async () => {
    try {
      const fields = await form.validateFields()
      const ticketIds = Object.entries(fields)
        .filter(([_k, v]) => v)
        .map(([k, ..._]) => Number(k))
      setIsSubmitting(true)
      await unsubscribe({ ticketIds }).unwrap()
      setTouched(false)
      setNumSelected(0)
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  })

  const handleSelect = () => {
    if (!numSelected) {
      form.setFieldsValue(subs.ticketIds.reduce((acc, id) => ({ ...acc, [id]: true }), {}))
      setNumSelected(subs.ticketIds.length)
      setTouched(true)
    } else {
      form.resetFields()
      setTouched(false)
      setNumSelected(0)
    }
  }

  if (loadingSubs || loadingTickets) return null
  const subbed = subs.ticketIds.map((id) => tickets.entities[id]).filter((x) => x)

  return (
    <Form
      form={form}
      onFieldsChange={(_, fields) => {
        setNumSelected(fields.filter(({ value }) => value).length)
        setTouched(true)
      }}
      onFinish={handleSubmit}
    >
      <Card
        title={
          <div className='flex justify-between pr-10 pt-2'>
            <Typography.Title level={4} className='!mb-2'>
              Manage subscriptions
            </Typography.Title>
            <Button type='primary' htmlType='submit' loading={isSubmitting} disabled={!touched}>
              Unsubscribe
            </Button>
          </div>
        }
      >
        <ErrorBoundary>
          <List
            header={
              <div className='flex justify-between mr-3'>
                <div>{subbed.length} subscribed tickets</div>
                <div>
                  <span className='mr-3'>
                    {!numSelected ? 'Select all' : `${numSelected} selected`}
                  </span>
                  <Checkbox checked={numSelected > 0} onClick={handleSelect} />
                </div>
              </div>
            }
            dataSource={subbed}
            renderItem={(ticket) => <SubscriptionItem ticket={ticket} />}
          />
        </ErrorBoundary>
      </Card>
    </Form>
  )
}

export default SubscriptionsCard
