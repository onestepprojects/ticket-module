import React, { FC } from 'react'
import { Checkbox, Col, Form, List, Row } from 'antd'
import { Ticket } from '../../store'

export interface SubscriptionItemProps {
  ticket: Ticket
}

export const SubscriptionItem: FC<SubscriptionItemProps> = (props) => {
  const { ticket } = props

  if (!ticket) return null
  return (
    <List.Item
      key={ticket.id}
      className='!p-1'
      actions={[
        <Form.Item
          name={ticket.id}
          getValueFromEvent={(e) => e.target.checked}
          valuePropName='checked'
          className='!p-0 !m-0'
        >
          <Checkbox defaultChecked={false} />
        </Form.Item>,
      ]}
    >
      <List.Item.Meta
        title={
          <Row>
            <Col span={2}>
              <span className='italic text-slate-400'>#{ticket.id}</span>
            </Col>
            {ticket.title}
          </Row>
        }
      />
    </List.Item>
  )
}

export default SubscriptionItem
