import { Card, Typography } from 'antd'
import { capitalize } from 'lodash-es'
import React, { FC } from 'react'
import { useParams } from 'react-router-dom-v5'
import {
  selectUserOrgById,
  selectUuid,
  skipToken,
  useGetOrganizationRolesByPersonQuery,
  useGetOrganizationTicketsQuery,
} from '../../store'
import TicketTable from '../Dashboard/TicketTable'

export interface OrganizationTicketsProps {}

export const OrganizationTickets: FC<OrganizationTicketsProps> = () => {
  const { orgId } = useParams<{ orgId: string }>()
  const userId = selectUuid()
  const { data: tickets, isLoading } = useGetOrganizationTicketsQuery(orgId || skipToken)
  const { data: userOrgs, isLoading: orgsLoading } = useGetOrganizationRolesByPersonQuery(
    userId || skipToken
  )

  if (isLoading || orgsLoading || !tickets) return null
  const org = selectUserOrgById(orgId, userOrgs)
  return (
    <Card
      title={
        <Typography.Title level={4} className='!m-0'>
          {capitalize(org.name)} Tickets
        </Typography.Title>
      }
    >
      <TicketTable data={tickets} />
    </Card>
  )
}

export default OrganizationTickets
