import React, { FC } from 'react'
import { Form, ModalProps } from 'antd'
import { TicketFormData, useCreateTicketMutation } from '../../../store'
import { TicketModalForm } from '../Form'
import styles from './createTicketModal.module.css'

export interface CreateTicketModalProps extends ModalProps {
  visible: boolean
  setVisible: React.Dispatch<React.SetStateAction<boolean>>
}

export const CreateTicketModal: FC<CreateTicketModalProps> = (props) => {
  const [form] = Form.useForm<TicketFormData>()
  const [createTicket] = useCreateTicketMutation()

  return (
    <TicketModalForm
      form={form}
      className={styles.modal}
      onFinish={createTicket}
      formProps={{
        initialValues: {
          detail: '',
        },
      }}
      {...props}
    />
  )
}
export default CreateTicketModal
