import React, { FC } from 'react'
import { Card, FormInstance, FormProps, Modal, ModalProps, notification } from 'antd'
import { useMemoizedFn } from 'ahooks'
import { dirtyFields, processCreateTicketForm, processEditTicketForm } from '../../../util'
import { useHistory } from 'react-router-dom-v5'
import { capitalize } from 'lodash-es'
import { selectUuid, Ticket, TicketFormData } from '../../../store'
import TicketForm from './TicketForm'
import styles from './form.module.css'

export type TicketFormType = 'create' | 'edit'
export interface TicketModalFormProps extends ModalProps {
  form: FormInstance<TicketFormData>
  onFinish
  visible: boolean
  setVisible: React.Dispatch<React.SetStateAction<boolean>>
  formType?: TicketFormType
  formProps?: FormProps<TicketFormData>
  ticket?: Ticket
}

export const TicketModalForm: FC<TicketModalFormProps> = (props) => {
  const {
    form,
    onFinish,
    formProps,
    visible,
    setVisible,
    formType = 'create',
    ticket: origTicket,
    ...modalProps
  } = props
  const [confirmLoading, setConfirmLoading] = React.useState(false)
  const history = useHistory()
  const userId = selectUuid()
  if (!userId) return null

  const handleCancel = () => {
    if (formType === 'create') form.resetFields()
    setVisible(false)
    notification.warning({
      message: formType === 'create' ? 'Ticket cancelled' : 'Edit cancelled',
      icon: ' ',
    })
  }

  const handleSubmit = useMemoizedFn(async (ticket: Partial<TicketFormData>) => {
    setConfirmLoading(true)
    try {
      const data =
        formType === 'create'
          ? processCreateTicketForm(ticket)
          : processEditTicketForm(ticket, origTicket)
      if (!data || Object.keys(data).length === 0) {
        notification.warning({ message: 'No fields changed' })
      } else {
        /* console.debug('DEBUG: submit ticket data:', data) */
        const res = await onFinish({ userId, ...data }).unwrap()
        notification.success({
          message: `Ticket ${formType === 'create' ? 'created' : 'edited'}`,
          icon: ' ',
        })
        form.resetFields()
        if (formType === 'create') history.push(`/tickets/${res.id}`)
      }
    } catch (err) {
      console.error(`Ticket ${formType} error:`, err)
      notification.error({
        message: `Failed to ${formType} ticket`,
        description: err?.data?.message,
        icon: ' ',
      })
    } finally {
      setConfirmLoading(false)
      setVisible(false)
    }
  })

  const handleValidate = useMemoizedFn(async () => {
    try {
      if (formType === 'create') return form.validateFields()
      const fields = await dirtyFields(form)
      // return the tag fields that arent dirty so the changes to
      // all the tags can be determined later
      const untouchedTags = ['physicians', 'educators', 'topics']
        .filter((x) => !fields[x])
        .reduce(
          (acc, x) => ({
            ...acc,
            ...form.getFieldsValue([x]),
          }),
          {}
        )
      /* console.debug("DEBUG: untouchedTags:", untouchedTags);
       * console.debug("DEBUG: fields:", fields); */
      return {
        ...fields,
        ...untouchedTags,
      }
    } catch (err) {
      console.log('Form validation error:', err)
    }
  })

  return (
    <React.Fragment>
      <Modal
        title={`${capitalize(formType)} Ticket`}
        centered
        closable={false}
        maskClosable={false}
        visible={visible}
        width='100%'
        className={styles.ticketModalForm}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText='Submit'
        onOk={() => {
          handleValidate()
            .then((values) => handleSubmit(values))
            .catch((info) => console.log('Validation Failed:', info))
        }}
        {...modalProps}
      >
        <Card className='max-w-screen-lg rounded-xl'>
          <TicketForm form={form} {...formProps} />
        </Card>
      </Modal>
    </React.Fragment>
  )
}

export default TicketModalForm
