import React, { FC, useState } from 'react'
import { Card, Form, FormInstance, FormProps, Input, Select } from 'antd'
import TagSelect from '../../Input/TagSelect'
import OrganizationSelect from '../../Input/OrganizationSelect'
import TicketPriorityRadio from '../../Input/TicketPriorityRadio'
import TicketSelect from '../../Input/TicketSelect'
import { TicketEnums, TicketFormData, TicketVisibility } from '../../../store'
import MarkdownEditor from '../../Markdown/MarkdownEditor'
import styles from './form.module.css'
import { capitalize } from 'lodash-es'
import { TicketFormType } from '.'

export interface TicketFormProps extends FormProps {
  form: FormInstance<TicketFormData>
  className?: string
  formType?: TicketFormType
}

export const TicketForm: FC<TicketFormProps> = (props) => {
  const { form, formType = 'create', className, initialValues, ...formProps } = props
  const [detail, setDetail] = useState(form.getFieldValue(['detail']))
  // const [visibility, setVisibility] = React.useState(
  //   form.getFieldValue('visibility') ?? TicketVisibility.public
  // )
  const [orgs, setOrgs] = React.useState({})

  // Todo add style={{paddingBottom: "5px"}} to tagSelect?

  return (
    <div className={className}>
      <Form
        layout='vertical'
        form={form}
        name={`${formType}-ticket-form`}
        colon={false}
        initialValues={{
          priority: 'normal',
          status: 'open',
          visibility: 'public',
          detail: '',
          ...initialValues,
        }}
        className={styles.ticketSpecialities}
        {...formProps}
      >
        <Form.Item
          label='Title'
          name='title'
          rules={[{ required: true, message: 'Ticket title required' }]}
        >
          <Input />
        </Form.Item>

        {/* <Form.Item label='Visibility' name='visibility'>
          <Select
            defaultValue={visibility}
            onSelect={(val) => setVisibility(val)}
            options={Object.keys(TicketEnums.visibility).map((value) => ({
              label: capitalize(value),
              value,
            }))}
            className='!mt-[-4px]'
          />
        </Form.Item> */}

        <Form.Item
          label='Organization'
          name='organizationIds'
          // rules={[
          //   {
          //     required: visibility === TicketVisibility.organization,
          //     message: 'Organization required unless visibility is public',
          //   },
          // ]}
          hasFeedback
        >
          <OrganizationSelect
            onChange={(value, _opt) =>
              setOrgs(value.reduce((acc, val) => ({ ...acc, [val.value]: true }), {}))
            }
          />
        </Form.Item>

        <Form.Item label='Specialties' name='physicians'>
          <TagSelect type='physician' placeholder='select specialities...' />
        </Form.Item>

        {/* <Form.Item label='Educators' name='educators'>
          <TagSelect type='educator' placeholder='select specialities...' />
        </Form.Item> */}

        <Form.Item label='Subject' name='topics'>
          <TagSelect type='topic' placeholder='select subjects...' />
        </Form.Item>

        <Form.Item label='Priority' name='priority'>
          <TicketPriorityRadio buttonStyle='solid' />
        </Form.Item>

        {/* <Form.Item label='Referral URL' name='referralUrl'>
          <Input />
        </Form.Item> */}

        {/* <Form.Item label='Link tickets' name='linkedTo'>
          <TicketSelect orgs={orgs} />
        </Form.Item> */}

        <Form.Item
          label='Description'
          name='detail'
          rules={[{ required: true, message: 'Details required' }]}
          labelCol={{ span: 24 }}
        >
          <MarkdownEditor
            value={detail}
            placeholder='Add ticket description...'
            style={{ minHeight: '200px', width: '100%' }}
            onChange={(text) => setDetail(text)}
          />
        </Form.Item>
      </Form>
    </div>
  )
}
export default TicketForm
