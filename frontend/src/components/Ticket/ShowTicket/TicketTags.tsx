import React, { FC } from 'react'
import { Tag } from 'antd'
import { EntityId } from '@reduxjs/toolkit'
import { useGetTagsQuery } from '../../../store'
import { TagType } from '../../../store/types'
import classnames from 'classnames'

export interface TicketTagsProps {
  className?: string
  tagIds: EntityId[]
  expanded?: boolean
  tagType: TagType[]
  id?: string
}

const TicketTags: FC<TicketTagsProps> = (props) => {
  const { tagType = [], id, className, expanded, tagIds } = props
  if (!expanded) return null

  const { data: ticketTags, isLoading } = useGetTagsQuery(undefined, {
    selectFromResult: ({ data, ...rest }) => ({
      ...rest,
      data: (data?.entities && tagIds.map((id) => data.entities[id])) || [],
    }),
  })

  if (!ticketTags || isLoading) return null
  const tags = ticketTags.filter(({ type }) => tagType.indexOf(type) !== -1)
  return (
    <div id={id} className={classnames('!mt-2', className)}>
      {tags.map(({ name, id }) => (
        <Tag key={id} color='blue'>
          {name}
        </Tag>
      ))}
    </div>
  )
}

export default TicketTags
