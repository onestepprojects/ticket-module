import { Col, Row, Space, Typography } from 'antd'
import classnames from 'classnames'
import React, { FC } from 'react'
import { Ticket } from '../../../store'
import StatusBadge from '../../Badge/StatusBadge'
import SubscribeSwitch from '../../Button/SubscribeSwitch'

export interface TicketHeaderProps {
  ticket: Ticket
  className?: string
}

const TicketHeader: FC<TicketHeaderProps> = (props) => {
  const { ticket, className } = props
  return (
    <Row
      gutter={5}
      align='middle'
      className={classnames('pb-2 border-b-slate-200 border-b-[2px]', className)}
    >
      <Col flex={1} className='flex justify-start items-center'>
        <Row gutter={0} className='flex justify-start items-center'>
          <Space size='middle'>
            <Typography.Title level={4} className='!mb-0 !text-slate-700 italic'>
              Ticket #{ticket.id}
            </Typography.Title>
            <StatusBadge status={ticket.status} />
          </Space>
        </Row>
      </Col>
      <Col flex={4} className='flex justify-end items-center'>
        <Space>
          <SubscribeSwitch ticketId={ticket.id} />
        </Space>
      </Col>
    </Row>
  )
}

export default TicketHeader
