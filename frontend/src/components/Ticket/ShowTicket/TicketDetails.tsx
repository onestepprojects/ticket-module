import React, { FC } from 'react'
import { Ticket } from '../../../store'
import MarkdownPreview from '../../Markdown/MarkdownPreview'

export interface TicketDetailsProps {
  ticket: Ticket
}

const TicketDetails: FC<TicketDetailsProps> = (props) => {
  const { ticket } = props
  return <MarkdownPreview data={ticket.detail} />
}

export default TicketDetails
