import React, { FC } from 'react'
import { EntityId } from '@reduxjs/toolkit'
import { Col, List, Row } from 'antd'
import { Link } from 'react-router-dom-v5'
import { Ticket, useGetTicketsQuery } from '../../../store'
import { truncate } from 'lodash-es'

export interface TicketRelatedProps {
  ticketIds: EntityId[]
  className?: string
  expanded?: boolean
  id?: string
}

const TicketRelated: FC<TicketRelatedProps> = (props) => {
  const { id, className, expanded, ticketIds } = props
  if (!expanded) return null

  const { data: tickets, isLoading } = useGetTicketsQuery(undefined, {
    selectFromResult: ({ data, ...rest }) => ({
      ...rest,
      data:
        (data?.entities &&
          ticketIds.map(
            (id) =>
              data.entities[id] ?? {
                id,
                // user doesn't have access privelegdes to see linked ticket
                title: 'Not authorized',
                disabled: true,
              }
          )) ||
        [],
    }),
  })

  if (!tickets || isLoading) return null
  return (
    <div id='ticket-related' className={className}>
      <List
        dataSource={tickets as (Ticket & { disabled?: boolean })[]}
        renderItem={({ id, title, disabled }) => (
          <List.Item key={id} actions={[!disabled && <Link to={`/tickets/${id}`}>View</Link>]}>
            <Row className='!w-full'>
              <Col span={6} className='mr-2'>
                <span className='italic'>#{id}</span>
              </Col>
              <Col span={16}>{truncate(title)}</Col>
            </Row>
          </List.Item>
        )}
      />
    </div>
  )
}

export default TicketRelated
