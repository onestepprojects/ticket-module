import React, { FC } from 'react'
import { Ticket, selectIsAssigned, useAppSelector } from '../../../store'
import { parseReferral } from '../../../util/links'

export interface TicketReferralProps {
  ticket: Ticket
}

const TicketReferral: FC<TicketReferralProps> = ({ ticket }) => {
  const isAssigned = useAppSelector((state) => selectIsAssigned(state, ticket.id))
  if (!isAssigned) return null
  return (
    <>
      {ticket?.referralUrl?.length && isAssigned && (
        <div className='mb-2'>
          <span className='mr-2'>Reference Case:</span>
          <a href={parseReferral(ticket.referralUrl)}>{ticket.referralUrl}</a>
        </div>
      )}
    </>
  )
}

export default TicketReferral
