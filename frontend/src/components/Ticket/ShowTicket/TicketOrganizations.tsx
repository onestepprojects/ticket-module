import { EntityId } from '@reduxjs/toolkit'
import { Button, List, ListProps } from 'antd'
import ErrorBoundary from 'antd/lib/alert/ErrorBoundary'
import React, { FC } from 'react'
import { useHistory } from 'react-router-dom-v5'
import { Organization, useGetOrganizationsQuery } from '../../../store'

export interface TicketOrganizationsProps extends ListProps<Organization> {
  orgIds: EntityId[]
  expanded?: boolean
  id?: string
}

const TicketOrganizations: FC<TicketOrganizationsProps> = (props) => {
  const history = useHistory()
  const { orgIds, expanded, ...listProps } = props
  if (!expanded) return null

  const { data: orgs, isLoading } = useGetOrganizationsQuery(undefined, {
    selectFromResult: ({ data, ...rest }) => ({
      ...rest,
      data: (data?.entities && orgIds.map((id) => data.entities[id])) || [],
    }),
  })

  if (isLoading || !orgs) return null
  return (
    <ErrorBoundary>
      <List
        {...listProps}
        id='ticket-organizations'
        dataSource={orgs?.filter((x) => !!x)}
        renderItem={({ id, name }) => (
          <List.Item
            key={id}
            actions={[
              <Button type='link' onClick={() => history.push(`/organizations/${id}/tickets`)}>
                View tickets
              </Button>,
            ]}
          >
            {name}
          </List.Item>
        )}
      />
    </ErrorBoundary>
  )
}

export default TicketOrganizations
