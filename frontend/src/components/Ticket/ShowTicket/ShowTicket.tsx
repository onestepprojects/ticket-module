import React, { FC } from 'react'
import { useParams } from 'react-router-dom-v5'
import { Col, Row, Card, Divider, Space, Affix } from 'antd'
import { useGetTicketByIdQuery } from '../../../store'
import TicketHeader from './TicketHeader'
import TicketMeta from './TicketMeta'
import TicketTitle from './TicketTitle'
import TicketDetails from './TicketDetails'
import TicketCommentList from '../../Comment/TicketCommentList'
import UpdateStatusButton from '../../Button/UpdateStatusButton'
import LinkTicketButton from '../../Button/LinkTicketButton'
import { EditTicketButton } from '..'
import TicketReferral from './TicketReferral'
import styles from './showticket.module.css'
import classnames from 'classnames'
import ShowTicketToc from './ShowTicketToc'

export const ShowTicket: FC = (_props) => {
  const { ticketId } = useParams<{ ticketId: string }>()
  const { data: ticket, isLoading } = useGetTicketByIdQuery(Number(ticketId))

  if (isLoading || !ticket) return null
  return (
    <Card className={classnames(styles.showTicket, 'min-h-screen')}>
      <TicketHeader ticket={ticket} />

      <Row>
        <Col xxl={24} xl={24} lg={21} md={24} sm={24} xs={24} className='pt-3'>
          <Row className='row'>
            <Col xxl={17} xl={16} lg={24} md={24} sm={24} xs={24}>
              <TicketTitle ticket={ticket} />

              <Divider orientation='center' className='!mt-0 !mb-4'>
                <Space>
                  <EditTicketButton
                    ticket={ticket}
                    className='!rounded-full items-center min-w-[80px]'
                  />
                  <UpdateStatusButton
                    ticketId={ticket.id}
                    status={ticket.status}
                    authorId={ticket.userId}
                  />
                  <LinkTicketButton className='!rounded-full items-center min-w-[80px]' />
                </Space>
              </Divider>

              <TicketReferral ticket={ticket} />

              <TicketDetails ticket={ticket} />

              <Divider />

              <TicketCommentList id='ticket-comments' className='mt-10' ticketId={ticket.id} />
            </Col>

            <Col xxl={7} xl={8} lg={24} md={24} sm={24} xs={24} className='pl-3'>
              <TicketMeta ticket={ticket} />
            </Col>
          </Row>
        </Col>

        <Col xxl={24} xl={24} lg={3} md={24} sm={24} xs={24} className='main-menu pt-3'>
          <Affix offsetTop={60}>
            <ShowTicketToc />
            {/* <section className="main-menu-inner">{menuChild}</section> */}
          </Affix>
        </Col>
      </Row>
    </Card>
  )
}
export default ShowTicket
