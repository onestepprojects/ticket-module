import React, { FC } from 'react'
import { Card, Col, Row, Space, Typography } from 'antd'
import classnames from 'classnames'
import dayjs from 'dayjs'
import { selectPersonById, Ticket, useAppSelector } from '../../../store'
import PersonAvatar from '../../Person/PersonAvatar'
import TicketAssignees from './TicketAssignees'
const { Title, Link } = Typography
import styles from './showticket.module.css'

export interface TicketTitleProps {
  ticket: Ticket
  className?: string
}

const TicketTitle: FC<TicketTitleProps> = (props) => {
  const { ticket, className } = props
  if (!ticket) return null
  const { updatedAt, createdAt, userId } = ticket

  const creator = useAppSelector((state) => selectPersonById(state, userId))
  if (!creator) return null
  const { email, name } = creator

  return (
    <Row className={classnames('flex justify-between pb-2', className)}>
      <Col>
        <Typography>
          <Title level={3}>{ticket.title}</Title>

          <div>
            <PersonAvatar person={creator}>
              <span>
                {name}{' '}
                {email && (
                  <Link href={`mailto:${email}`} target='_blank'>
                    {email}
                  </Link>
                )}
              </span>
            </PersonAvatar>
          </div>

          <div className='inline-flex flex-col text-slate-550'>
            <span className='text-[12px] w-100'>
              Created {dayjs(createdAt).format('MMMM Do YYYY, h:mm A')}
            </span>
            <span className='text-[12px]'>Updated {dayjs(updatedAt).fromNow()}</span>
          </div>
        </Typography>
      </Col>

      <Col className={classnames('!bg-inherit !pd-0', styles.assignees)}>
        <Card>
          <Space direction='vertical'>
            <Card.Meta description='Assignees'></Card.Meta>
            <TicketAssignees ticketId={ticket?.id} assignedTo={ticket?.assignedTo} />
          </Space>
        </Card>
      </Col>
    </Row>
  )
}

export default TicketTitle
