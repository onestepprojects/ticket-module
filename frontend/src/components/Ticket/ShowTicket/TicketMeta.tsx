import React, { FC } from 'react'
import { Button, Card, Col, Descriptions, Divider } from 'antd'
import { Ticket } from '../../../store'
import classnames from 'classnames'
import { uniq } from 'lodash-es'
import styles from '../ticket.module.css'
import PriorityBadge from '../../Badge/PriorityBadge'
import TicketChangeLog from '../../Changelog/TicketChangeLog'
import TicketTags from './TicketTags'
import TicketRelated from './TicketRelated'
import TicketOrganizations from './TicketOrganizations'

export interface TicketMetaProps {
  ticket: Ticket
}

interface ExpandButtonProps {
  setExpanded: React.Dispatch<React.SetStateAction<boolean>>
  text: string
  expanded?: boolean
}

const ExpandButton: FC<ExpandButtonProps> = ({ expanded, setExpanded, text }) => {
  return (
    <div className='!inline-flex items-center w-full !justify-between border-b-[1px]'>
      <Col>
        <Divider orientation='left' className='!mb-2'>
          {text}
        </Divider>
      </Col>
      <Col>
        <Button type='link' onClick={() => setExpanded(!expanded)}>
          {expanded ? 'hide' : 'show'}
        </Button>
      </Col>
    </div>
  )
}

const TicketMeta: FC<TicketMetaProps> = ({ ticket }) => {
  const { id: ticketId, linkedTo, linkedBy, priority, visibility, organizations, tags } = ticket
  if (!ticket) return null
  const [orgsExpanded, setOrgsExpanded] = React.useState(organizations?.length > 0)
  const [relatedExpanded, setRelatedExpanded] = React.useState(
    linkedTo?.length > 0 || linkedBy?.length > 0
  )
  const [tagsExpanded, setTagsExpanded] = React.useState(tags?.length > 0)
  const [specsExpanded, setSpecsExpanded] = React.useState(tags?.length > 0)

  const anyExpanded = () => orgsExpanded || relatedExpanded || tagsExpanded || specsExpanded

  const toggleAll = () => {
    const dir = !anyExpanded()
    setOrgsExpanded(dir)
    setRelatedExpanded(dir)
    setTagsExpanded(dir)
    setSpecsExpanded(dir)
  }

  return (
    <>
      <Card className={classnames(styles.metaCard, 'shadow-md')}>
        <Descriptions size='small' layout='vertical' colon={false} column={26}>
          <Descriptions.Item label='Priority' span={13}>
            <PriorityBadge priority={priority} />
          </Descriptions.Item>
          <Descriptions.Item label='Visibility' span={13}>
            <PriorityBadge priority={visibility} />
          </Descriptions.Item>
        </Descriptions>

        <div className='flex flex-end justify-end mt-2 border-b-[1px]'>
          <Button onClick={() => toggleAll()} type='link'>
            {anyExpanded() ? 'Hide' : 'Expand'} all
          </Button>
        </div>

        <ExpandButton text='Organization' expanded={orgsExpanded} setExpanded={setOrgsExpanded} />
        <TicketOrganizations
          id='ticket-organizations'
          orgIds={organizations}
          expanded={orgsExpanded}
        />

        <ExpandButton text='Specialities' expanded={specsExpanded} setExpanded={setSpecsExpanded} />
        <TicketTags
          id='ticket-specialities'
          tagIds={tags}
          expanded={specsExpanded}
          tagType={['educator', 'physician']}
        />

        <ExpandButton
          text='Related Tickets'
          expanded={relatedExpanded}
          setExpanded={setRelatedExpanded}
        />
        <TicketRelated
          id='ticket-related'
          ticketIds={uniq(linkedTo.concat(linkedBy))}
          expanded={relatedExpanded}
        />

        <ExpandButton text='Topic Tags' expanded={tagsExpanded} setExpanded={setTagsExpanded} />
        <TicketTags id='ticket-tags' tagIds={tags} expanded={tagsExpanded} tagType={['topic']} />
      </Card>

      <Divider style={{ padding: 0, margin: '14px' }} />

      <TicketChangeLog id='ticket-changelog' ticketId={ticketId} className='shadow-md' />
    </>
  )
}

export default TicketMeta
