import React, { FC } from 'react'

export interface ShowTicketTocProps {}

const clearActiveToc = () => {
  ;[].forEach.call(document.querySelectorAll('.toc-affix li a'), (node) => {
    node.className = ''
  })
}

export const updateActiveToc = (id) => {
  const currentNode = document.querySelectorAll(`.toc-affix li a[href="#${id}"]`)[0]
  if (currentNode) {
    clearActiveToc()
    currentNode.className = 'current'
  }
}

export const ShowTicketToc: FC<ShowTicketTocProps> = () => {
  return (
    <div className='toc-affix border-l-[1px] text-slate-400 pl-1 ml-2'>
      <ul>
        <li>
          <a id='top' href='#'>
            Top
          </a>
        </li>
        <li>
          <a id='comments' href='#ticket-comments'>
            Comments
          </a>
        </li>
        <li>
          <a id='organizations' href='#ticket-organizations'>
            Organization
          </a>
        </li>
        <li>
          <a id='specialities' href='#ticket-specialities'>
            Specialities
          </a>
        </li>
        <li>
          <a id='related' href='#ticket-related'>
            Related
          </a>
        </li>
        <li>
          <a id='tags' href='#ticket-tags'>
            Tags
          </a>
        </li>
        <li>
          <a id='changelog' href='#ticket-changelog'>
            Changelog
          </a>
        </li>
      </ul>
    </div>
  )
}

export default ShowTicketToc
