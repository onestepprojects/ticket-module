import { EntityId } from '@reduxjs/toolkit'
import { Avatar, Tooltip } from 'antd'
import React, { FC, useRef } from 'react'
import { useGetPersonsQuery } from '../../../store'
import AssignSwitch from '../../Button/AssignSwitch'
import PersonAvatar from '../../Person/PersonAvatar'

export interface TicketAssigneesProps {
  ticketId: number
  assignedTo: EntityId[]
}

const TicketAssignees: FC<TicketAssigneesProps> = (props) => {
  const { assignedTo, ticketId } = props
  const { data: assignees, isLoading } = useGetPersonsQuery(undefined, {
    selectFromResult: ({ data, ...rest }) => ({
      ...rest,
      data: (data?.entities && assignedTo.map((id) => data.entities[id])) || [],
    }),
  })

  const $ref = useRef()
  if (!assignees || isLoading) return null
  return (
    <>
      <div id='ticket-assignees' style={{ width: '100%' }} className='!h-[38px]'>
        {!assignees?.length ? (
          <PersonAvatar person={null} showUnassigned={true} />
        ) : assignees.length === 1 ? (
          <PersonAvatar person={assignees[0]} />
        ) : (
          <Avatar.Group>
            {assignees.map((person, idx) => (
              <Tooltip ref={$ref} key={idx} title={person.name} placement='top' trigger='hover'>
                <PersonAvatar
                  key={person.uuid}
                  person={person}
                  inGroup={true}
                  className='!bg-zinc-200 cursor-pointer'
                />
              </Tooltip>
            ))}
          </Avatar.Group>
        )}
      </div>

      <AssignSwitch ticketId={ticketId} />
    </>
  )
}

export default TicketAssignees
