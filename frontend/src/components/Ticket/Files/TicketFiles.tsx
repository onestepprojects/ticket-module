import React, { FC } from 'react'
import { Card, CardProps } from 'antd'
import fileDownload from 'js-file-download'
import axios from 'axios'
import {
  useDeleteFileMutation,
  useGetTicketFilesQuery,
  useUploadFileMutation,
} from '../../../store'

export interface TicketFilesProps extends CardProps {
  ticketId: number
  uploaded: boolean
  setUploaded: React.Dispatch<React.SetStateAction<boolean>>
}

/* XXX: not being used atm */
export const TicketFiles: FC<TicketFilesProps> = (props) => {
  const { ticketId, uploaded, setUploaded, ...cardProps } = props // eslint-disable-line
  const { data: files, isLoading } = useGetTicketFilesQuery()
  const [deleteFile] = useDeleteFileMutation() // eslint-disable-line
  const [uploadFile] = useUploadFileMutation() // eslint-disable-line

  // eslint-disable-next-line
  const download = (file) => {
    const url = `/tickets/${ticketId}/files/download?filepath=${file.path}`
    const data = new FormData()
    axios
      .post(url, data, {
        headers: {
          'Content-Type': 'application/json',
        },
        responseType: 'blob',
      })
      .then((res) => {
        fileDownload(res.data, file.filename)
      })
  }

  if (isLoading || !files) return null
  return (
    <Card title='Ticket Files' {...cardProps}>
      <span className='red bold'>TODO: not being used atm</span>
    </Card>
  )
}

export default TicketFiles
