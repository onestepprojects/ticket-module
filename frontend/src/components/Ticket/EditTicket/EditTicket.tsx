import React, { FC } from 'react'
import { useMemoizedFn } from 'ahooks'
import { Form, notification } from 'antd'
import { useParams } from 'react-router-dom-v5'
import {
  TicketFormData,
  useAppSelector,
  useGetTicketByIdQuery,
  useUpdateTicketMutation,
} from '../../../store'
import { TicketForm } from '..'
import { dirtyFields, processEditTicketForm } from '../../../util'

export const EditTicket: FC = (_props) => {
  const { ticketId } = useParams<{ ticketId: string }>()
  const { data: origTicket, isLoading } = useGetTicketByIdQuery(Number(ticketId))
  const [updateTicket] = useUpdateTicketMutation()
  const [form] = Form.useForm<TicketFormData>()
  const [_confirmLoading, setConfirmLoading] = React.useState(false)
  const userId = useAppSelector((state) => state.auth.person.uuid)

  // eslint-disable-next-line
  const handleSubmit = useMemoizedFn(async (ticket: Partial<TicketFormData>) => {
    setConfirmLoading(true)
    try {
      await updateTicket({ userId, ...processEditTicketForm(ticket, origTicket) }).unwrap()
      notification.success({ message: 'Ticket updated' })
      form.resetFields()
    } catch (err) {
      console.log('Ticket update error', err)
      notification.error({
        message: 'Failed to update ticket',
        description: err?.data?.message,
      })
    } finally {
      setConfirmLoading(false)
    }
  })

  // eslint-disable-next-line
  const handleValidate = useMemoizedFn(async () => {
    try {
      return dirtyFields(form)
    } catch (err) {
      console.log('Form validation error:', err)
    }
  })

  if (isLoading || !origTicket) return null
  return <TicketForm form={form} formType='edit' initialValues={{ ...origTicket }} />
}

export default EditTicket
