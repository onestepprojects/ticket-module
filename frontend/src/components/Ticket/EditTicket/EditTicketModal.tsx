import React, { FC } from 'react'
import { Form, Spin } from 'antd'
import { CreateTicketModalProps, TicketModalForm } from '..'
import {
  Ticket,
  TicketEditFormData,
  TicketFormData,
  useGetTagsQuery,
  useUpdateTicketMutation,
} from '../../../store'
import { ticketToFormData } from '../../../util'

export interface EditTicketModalProps extends CreateTicketModalProps {
  ticket: Ticket
}

export const EditTicketModal: FC<EditTicketModalProps> = (props) => {
  const { ticket, ...modalProps } = props
  const [form] = Form.useForm<TicketFormData>()
  const { data: allTags, isLoading: loadingTags } = useGetTagsQuery()
  const [updateTicket] = useUpdateTicketMutation()

  React.useEffect(() => {
    if (!(ticket && allTags)) return
    const formData = ticketToFormData(ticket, allTags)
    form.setFieldsValue(formData)
  }, [ticket, allTags])

  const onFinish = (data: Partial<TicketEditFormData>) =>
    updateTicket({
      id: ticket.id,
      ...data,
    })

  if (!ticket || loadingTags) {
    return <Spin />
  }
  return (
    <TicketModalForm
      form={form}
      formType='edit'
      ticket={ticket}
      onFinish={onFinish}
      formProps={{
        initialValues: ticket,
      }}
      {...modalProps}
    />
  )
}

export default EditTicketModal
