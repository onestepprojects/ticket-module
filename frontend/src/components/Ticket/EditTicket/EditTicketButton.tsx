import { Button, ButtonProps } from 'antd'
import React, { FC } from 'react'
import { EditTicketModal } from '.'
import { selectUuid, useAppSelector, selectIsAssigned, Ticket } from '../../../store'

export interface EditTicketButtonProps extends ButtonProps {
  ticket: Ticket
}

export const EditTicketButton: FC<EditTicketButtonProps> = (props) => {
  const { ticket, ...buttonProps } = props
  const [visible, setVisible] = React.useState(false)

  // Only creator / assignees can edit for now
  const userId = selectUuid()
  const isAssigned = useAppSelector((state) => selectIsAssigned(state, ticket.id))

  const handleClick = () => setVisible(!visible)

  if (!userId) return null
  return (
    <>
      <EditTicketModal ticket={ticket} visible={visible} setVisible={setVisible} />
      <Button
        onClick={handleClick}
        {...buttonProps}
        disabled={ticket.userId !== userId && !isAssigned}
      >
        Edit
      </Button>
    </>
  )
}

export default EditTicketButton
