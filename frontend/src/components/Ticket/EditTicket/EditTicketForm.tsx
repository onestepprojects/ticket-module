import { Form, Spin } from 'antd'
import React, { FC, useEffect } from 'react'
import { useParams } from 'react-router-dom-v5'
import { TicketFormData, useGetTagsQuery, useGetTicketByIdQuery } from '../../../store'
import { TicketForm } from '..'
import { ticketToFormData } from '../../../util'

const EditTicketForm: FC = (_props) => {
  const { ticketId } = useParams<{ ticketId: string }>()
  const { data: ticket, isLoading } = useGetTicketByIdQuery(Number(ticketId))
  const { data: allTags, isLoading: loadingTags } = useGetTagsQuery()
  const [form] = Form.useForm<TicketFormData>()

  useEffect(() => {
    if (!(ticket && allTags)) return
    const formData = ticketToFormData(ticket, allTags)
    form.setFieldsValue(formData)
  }, [ticket, allTags])

  if (isLoading || loadingTags) return <Spin />
  return <TicketForm form={form} formType={'edit'} />
}

export default EditTicketForm
