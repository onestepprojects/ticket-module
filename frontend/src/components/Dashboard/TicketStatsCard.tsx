import { Card, CardProps, Space, Statistic } from 'antd'
import React, { FC } from 'react'
import { capitalize } from 'lodash-es'
import classnames from 'classnames'

export interface TicketStatsCardProps extends CardProps {
  fields: object
  title: string
  data
  className?: string
}

const TicketStatsCard: FC<TicketStatsCardProps> = (props) => {
  const { fields, title, data, className, ...cardProps } = props
  if (!data) return null
  return (
    <Card title={capitalize(title)} className={classnames('shadow-md', className)} {...cardProps}>
      <Space direction='horizontal' size='large'>
        {Object.keys(fields).map((x, idx) => (
          <Statistic key={idx} title={x} value={data[x]} />
        ))}
      </Space>
    </Card>
  )
}

export default TicketStatsCard
