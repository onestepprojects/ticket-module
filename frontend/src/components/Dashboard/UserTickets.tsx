import { Card, Tabs, Typography } from 'antd'
import classnames from 'classnames'
import React, { FC } from 'react'
import { useGetAssignedTicketsQuery, useGetTicketsCreatedByUserQuery } from '../../store'
import TicketTable from './TicketTable'
const { TabPane } = Tabs

export interface UserTicketsProps {
  className?: string
}

const UserTickets: FC<UserTicketsProps> = (props) => {
  const { className } = props
  const { data: assigned, isLoading: loadingAssigned } = useGetAssignedTicketsQuery({})
  const { data: created, isLoading: loadingCreated } = useGetTicketsCreatedByUserQuery({})

  if (loadingCreated || loadingAssigned) return null

  return (
    <Card
      className={classnames('shadow-md', className)}
      title={
        <Typography.Title level={4} className='!m-0'>
          My Tickets
        </Typography.Title>
      }
    >
      <Tabs type='card'>
        <TabPane key={1} tab='Created'>
          <TicketTable data={created} />
        </TabPane>
        <TabPane key={2} tab='Assigned'>
          <TicketTable data={assigned} />
        </TabPane>
        <TabPane key={3} tab='Contributed'>
          <div>Contributed to</div>
        </TabPane>
      </Tabs>
    </Card>
  )
}

export default UserTickets
