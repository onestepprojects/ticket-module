import React, { FC } from 'react'
import { TicketContainerProps } from '../../Container'
import TicketStats from './TicketStats'
import UserTickets from './UserTickets'
/* import styles from './dashboard.module.css' */

export interface DashboardProps extends Pick<TicketContainerProps, 'auth'> {
  isAdmin?: boolean
}

const Dashboard: FC<DashboardProps> = (props) => {
  const { isAdmin } = props // eslint-disable-line
  return (
    <div>
      <main className='p-1'>
        <TicketStats className='!mt-2' />
        <UserTickets className='!mt-3' />
      </main>
    </div>
  )
}

export default Dashboard
