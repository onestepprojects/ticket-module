import React, { FC } from 'react'
import { Card, Typography } from 'antd'
import PersonAvatar from '../Person/PersonAvatar'
import classnames from 'classnames'
import { DashboardProps } from '.'
import { AccountBadge } from '..'

export interface DashboardHeaderProps extends DashboardProps {
  className?: string
}

const DashboardHeader: FC<DashboardHeaderProps> = ({ auth, isAdmin, className = '' }) => {
  const hour = new Date().getHours()
  if (!auth) return <div></div>
  const { person } = auth
  return (
    <Card className={classnames('shadow-md', className)}>
      <Typography>
        <PersonAvatar person={person} className='!mr-1.5'>
          <div>
            <Typography.Title level={3} className='!mb-0'>
              Good {hour < 12 ? 'Morning' : 'Afternoon'}, {person.name}!
            </Typography.Title>
            <AccountBadge isAdmin={isAdmin} />
          </div>
        </PersonAvatar>
      </Typography>
    </Card>
  )
}

export default DashboardHeader
