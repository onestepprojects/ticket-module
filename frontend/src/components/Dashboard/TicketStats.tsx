import React, { FC } from 'react'
import { Card, Space, Typography } from 'antd'
import classnames from 'classnames'
import {
  TicketPriority,
  TicketStatus,
  useGetTicketsAggregateQuery,
  useGetTicketsByAssignedQuery,
} from '../../store'
import styles from './dashboard.module.css'
import TicketStatsCard from './TicketStatsCard'

export interface TicketStatsProps {
  className?: string
}

const aggregateDefault = (typ: string[]): object =>
  typ.reduce((acc, val) => ({ ...acc, [val]: 0 }), {})

const normalizeAggregate = (typ: string[], by: string, agg, field = '_count'): object =>
  agg?.reduce(
    (acc, x) => ({
      ...acc,
      [x[by]]: x[field],
    }),
    aggregateDefault(typ)
  )

const TicketStats: FC<TicketStatsProps> = (props) => {
  const { className } = props
  const { data: ticketByStatus, isLoading: loadingStatus } = useGetTicketsAggregateQuery({
    by: 'status',
    count: 'all',
  })
  const { data: ticketByPriority, isLoading: loadingPriority } = useGetTicketsAggregateQuery({
    by: 'priority',
    count: 'all',
  })
  const { data: ticketByAssigned, isLoading: loadingAssigned } = useGetTicketsByAssignedQuery({
    count: true,
  })
  if (loadingStatus || loadingPriority || loadingAssigned) return null

  return (
    <Card
      className={classnames('shadow-md', styles.ticketStats, className)}
      title={
        <Typography.Title level={4} className='!m-0'>
          Ticket Stats
        </Typography.Title>
      }
    >
      <Space direction='horizontal'>
        <TicketStatsCard
          key='status'
          fields={TicketStatus}
          title='status'
          data={normalizeAggregate(Object.keys(TicketStatus), 'status', ticketByStatus)}
          className='shadow-md'
        />
        <TicketStatsCard
          key='priority'
          fields={TicketPriority}
          title='priority'
          data={normalizeAggregate(Object.keys(TicketPriority), 'priority', ticketByPriority)}
          className='shadow-md'
        />
        <TicketStatsCard
          key='assigned'
          fields={{ assigned: 'assigned', unassigned: 'unassigned' }}
          title='Assigned'
          data={ticketByAssigned}
          className='shadow-md'
        />
      </Space>
    </Card>
  )
}

export default TicketStats
