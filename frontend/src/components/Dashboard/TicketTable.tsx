import { Table, Button, Input, Space } from 'antd'
import React, { FC } from 'react'
import { Ticket, TicketPriority, TicketStatus, TicketVisibility } from '../../store'
import dayjs from 'dayjs'
import { Link } from 'react-router-dom-v5'
import { capitalize } from 'lodash-es'
import { SearchOutlined } from '@ant-design/icons'
import Highlighter from 'react-highlight-words'

export interface TicketTableProps {
  data: Ticket[]
}

const sortAlpha = (key: string) => (a: Ticket, b: Ticket) => a[key].localeCompare(b[key])
const sortDate = (key: string) => (a: Ticket, b: Ticket) =>
  dayjs(a[key]).unix() - dayjs(b[key]).unix()

const TicketTable: FC<TicketTableProps> = (props) => {
  const { data } = props
  const [searchText, setSearchText] = React.useState('')
  const [searchedColumn, setSearchedColumn] = React.useState('')
  const $searchInput = React.useRef(null)

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm()
    setSearchText(selectedKeys[0])
    setSearchedColumn(dataIndex)
  }

  const handleReset = (clearFilters) => {
    clearFilters()
    setSearchText('')
    setSearchedColumn('')
  }

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={$searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type='primary'
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size='small'
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => {
              handleReset(clearFilters)
              // handleSearch(selectedKeys, confirm, dataIndex)
            }}
            size='small'
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type='link'
            size='small'
            onClick={() => {
              confirm({ closeDropdown: false })
              setSearchText(selectedKeys[0])
              setSearchedColumn(dataIndex)
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => $searchInput.current.select(), 100)
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  })

  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
      ...getColumnSearchProps('id'),
      render: (text, record) => (
        <Button type='link'>
          <Link to={`/tickets/${record.id}`}>{record.id}</Link>
        </Button>
      ),
      sorter: {
        compare: (a, b) => a.id - b.id,
        multiple: 1,
      },
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      ...getColumnSearchProps('title'),
      sorter: {
        compare: sortAlpha('title'),
        multiple: 2,
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      filters: Object.keys(TicketStatus).map((value) => ({
        text: <span>{capitalize(value)}</span>,
        value,
      })),
      onFilter: (val, rec) => rec.visibility === val,
    },
    {
      title: 'Priority',
      dataIndex: 'priority',
      key: 'priority',
      filters: Object.keys(TicketPriority).map((value) => ({
        text: <span>{capitalize(value)}</span>,
        value,
      })),
      onFilter: (val, rec) => rec.visibility === val,
    },
    {
      title: 'Visibility',
      dataIndex: 'visibility',
      key: 'visibility',
      filters: Object.keys(TicketVisibility).map((vis) => ({
        text: <span>{capitalize(vis)}</span>,
        value: vis,
      })),
      onFilter: (val, rec) => rec.visibility === val,
    },
    {
      title: 'Created',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text, record) => dayjs(record.createdAt).format('MM/DD/YYYY'),
      sorter: {
        compare: sortDate('createdAt'),
        multiple: 3,
      },
    },
    {
      title: 'Updated',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      render: (text, record) => dayjs(record.createdAt).fromNow(),
      sorter: {
        compare: sortDate('updatedAt'),
        multiple: 3,
      },
    },
  ]

  return <Table columns={columns} dataSource={data} rowKey={(record) => record.id} />
}

export default TicketTable
