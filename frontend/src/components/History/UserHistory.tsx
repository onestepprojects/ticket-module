import React, { FC } from 'react'
import { Card, CardProps, List, ListProps, Typography } from 'antd'
import dayjs from 'dayjs'
import { useHistory } from 'react-router-dom-v5'
import { TicketChange, useGetUserHistoryQuery, UserHistoryEntry } from '../../store'

export interface UserHistoryProps {
  cardProps?: CardProps
  listProps?: ListProps<UserHistoryEntry>
}

const activityInfo = ({ type, log }: UserHistoryEntry) => {
  switch (type) {
    case TicketChange.created:
      return `Created new ticket`
    case TicketChange.modified:
      return `Modified ticket`
    case TicketChange.statusChange:
      return `Changed ticket status to ${log}`
  }
}

export const UserHistory: FC<UserHistoryProps> = (props) => {
  const { cardProps, listProps } = props
  const { data: activity, isLoading } = useGetUserHistoryQuery({
    orderBy: '-createdAt',
  })
  const history = useHistory()

  if (isLoading || !activity) return null
  return (
    <Card {...cardProps}>
      <List
        header={<Typography.Title level={4}>History</Typography.Title>}
        dataSource={activity}
        pagination={{
          position: 'bottom',
          pageSize: 8,
          defaultCurrent: 1,
          defaultPageSize: 10,
        }}
        {...listProps}
        renderItem={(log) => (
          <List.Item
            key={log.id}
            className='!justify-between cursor-pointer hover:bg-slate-100'
            onClick={() => history.push(`/tickets/${log.ticketId}`)}
          >
            <List.Item.Meta
              title={`#${log.ticketId}: ${activityInfo(log)}`}
              description={dayjs(log.createdAt).fromNow()}
            />
          </List.Item>
        )}
      />
    </Card>
  )
}

export default UserHistory
