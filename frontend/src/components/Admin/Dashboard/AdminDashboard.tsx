import React, { FC } from 'react'
import { TicketContainerProps } from '../../../Container'
import TicketStats from '../../Dashboard/TicketStats'
/* import AdminControls from './AdminControls' */
import DevPanel from '../Dev/DevPanel'
import TagsCard from '../../Tag/TagsCard'
import TagWatchers from '../../Watch/TagWatchers'
import { Card, Tabs } from 'antd'
import classnames from 'classnames'
const { TabPane } = Tabs

export interface AdminDashboardProps extends Pick<TicketContainerProps, 'auth'> {
  className?: string
}

const AdminDashboard: FC<AdminDashboardProps> = (props) => {
  const { className } = props

  return (
    <div className={classnames('p-1', className)}>
      <DevPanel />

      {/* <AdminControls /> */}

      <TicketStats className='!mt-2' />

      <Card className='!mt-2'>
        <Tabs type='card'>
          <TabPane key={1} tab='Tags' className='min-h-[200px]'>
            <TagsCard />
          </TabPane>
          <TabPane key={2} tab='Watchers' className='min-h-[200px]'>
            <TagWatchers />
          </TabPane>
        </Tabs>
      </Card>
      <div className='flex sm:flex-row mt-5 flex-nowrap flex-col gap-4'></div>
    </div>
  )
}

export default AdminDashboard
