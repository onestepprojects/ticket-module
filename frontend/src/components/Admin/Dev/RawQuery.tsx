import { useMemoizedFn } from 'ahooks'
import { Button, Col, Form, notification, Row, Tag } from 'antd'
import TextArea from 'antd/lib/input/TextArea'
import React, { FC } from 'react'
import {
  useLazyGetSQLRawQuery,
  RawSQLQuery,
  selectUuid,
  useGetSQLTablesQuery,
} from '../../../store'

const RawQuery: FC = () => {
  const [form] = Form.useForm<RawSQLQuery>()
  const uuid = selectUuid()
  const [sql, setSql] = React.useState(`
select * from "User"
where (id = '${uuid}')`)
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [getRawQuery, { data, isLoading }] = useLazyGetSQLRawQuery()
  const { data: tables, isLoading: loadingTables } = useGetSQLTablesQuery()

  const handleSubmit = useMemoizedFn(async () => {
    setIsSubmitting(true)
    try {
      const vals = await form.validateFields()
      await getRawQuery(vals).unwrap()
    } catch (err) {
      notification.error({
        message: 'SQL query failed',
        description: err?.data.message,
      })
    } finally {
      setIsSubmitting(false)
    }
  })

  const tagTables = (tabs: string[]) =>
    tabs.map((t) => (
      <Tag
        key={t}
        className='cursor-pointer'
        onClick={(e) => {
          e.preventDefault()
          const val = `select * from "${t}" where`
          setSql(val)
          form.setFieldsValue({ sql: val })
        }}
      >
        {t}
      </Tag>
    ))

  if (!uuid || loadingTables) return null
  return (
    <div className='w-100 border-l-[1px] pl-3'>
      <div className='mb-2'>
        <Col className='flex'>
          <Row className='inline-flex'>
            <span className='font-bold italic'>Models</span>
            <Col>{tagTables(tables?.models)}</Col>
          </Row>
          <Row className='inline-flex border-l-[1px] pl-4'>
            <span className='font-bold italic'>Pivot Tables</span>
            <Col>{tagTables(tables?.pivots)}</Col>
          </Row>
        </Col>
      </div>
      <Form form={form} layout='vertical' onFinish={handleSubmit} initialValues={{ sql }}>
        <Col>
          <Form.Item label=' ' colon={false}>
            <div className='italic'>
              Prisma tables, and any columns with capital letters, must be double-quoted in queries
            </div>
          </Form.Item>
          <Form.Item name='sql' label='Raw (unsafe) SQL Query' rules={[{ required: true }]}>
            <TextArea
              className='w-full !min-h-[90px]'
              placeholder='raw SQL...'
              allowClear
              onChange={(e) => setSql(e.target.value)}
            >
              {sql}
            </TextArea>
          </Form.Item>
          <Button type='primary' htmlType='submit' loading={isSubmitting}>
            Submit
          </Button>
        </Col>
      </Form>

      {!isLoading && <pre>{JSON.stringify(data, null, 2)}</pre>}
    </div>
  )
}

export default RawQuery
