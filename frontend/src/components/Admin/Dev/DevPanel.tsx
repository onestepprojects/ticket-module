import { Card, Divider, Row, Space, Typography } from 'antd'
import classnames from 'classnames'
import React, { FC } from 'react'
import { Token } from '.'
import { RefreshNotificationsButton } from '../..'
import styles from './dev.module.css'
import RawQuery from './RawQuery'

const DevPanel: FC = () => {
  return (
    <Card
      className={classnames(styles.card, '!mt-1')}
      title={
        <Typography.Title className='!p-0 !m-0' level={5}>
          Dev tools
        </Typography.Title>
      }
    >
      <Divider orientation='center' className='!mt-0 !mb-6'>
        <Space>
          <RefreshNotificationsButton className='!rounded-full items-center min-w-[80px]' />
        </Space>
      </Divider>

      <Row className='flex-inline items-center'>
        <Space direction='horizontal'>
          <Token width={150} />

          <RawQuery />
        </Space>
      </Row>
    </Card>
  )
}

export default DevPanel
