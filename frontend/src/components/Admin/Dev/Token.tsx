import React, { FC } from 'react'
import { Button, Typography } from 'antd'
import { useAppSelector } from '../../../store'
const { Paragraph } = Typography

export interface TokenProps {
  width?: number
  className?: string
}

export const Token: FC<TokenProps> = (props) => {
  const { className = '', width = 200 } = props
  const token = useAppSelector((state) => state.auth.token)
  const [expanded, setExpanded] = React.useState(false)

  const tokenText = (expanded: boolean) => (
    <Paragraph
      style={{ width: expanded ? '100%' : width }}
      copyable
      onClick={() => setExpanded(!expanded)}
      className='cursor-pointer'
      ellipsis={{
        rows: expanded ? 20 : 1,
        expandable: !expanded,
        suffix: '',
        symbol: <span></span>,
        onExpand: () => setExpanded(true),
      }}
    >
      {token}
    </Paragraph>
  )

  return (
    <div className='inline-flex items-center'>
      <span className='mr-1'>Token:</span>
      <Typography className={className}>
        {expanded ? (
          <div>{tokenText(expanded)}</div>
        ) : (
          <Button style={{ whiteSpace: 'normal', wordWrap: 'break-word' }}>
            {tokenText(expanded)}
          </Button>
        )}
      </Typography>
      {expanded && (
        <Button type='primary' onClick={() => setExpanded(false)}>
          Close
        </Button>
      )}
    </div>
  )
}

export default Token
