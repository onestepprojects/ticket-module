import { Button, Card, Form } from 'antd'
import React, { FC } from 'react'
import UserTable from './UserTable'
import Amplify, { Auth } from 'aws-amplify'
import amplifyConfig from './aws-amplify.config.js'
import SignInModal from './SignInModal'

Amplify.configure(amplifyConfig)

const signIn = async ({ username, password: pw }) => {
  await Auth.signIn(username, pw)
}

const AdminUsers: FC = () => {
  const [visible, setVisible] = React.useState(false)
  const [form] = Form.useForm()
  /* const [user, setUser] = React.useState(null) */

  /* const getUser = async () => {
   *   return await Auth.currentAuthenticatedUser()
   * } */

  const signInButton = ({ name, email }) => (
    <Button
      type='primary'
      onClick={() => {
        form.setFieldsValue({ username: email })
        setVisible(true)
      }}
    >
      Login as {name}
    </Button>
  )

  return (
    <div>
      <SignInModal form={form} signIn={signIn} visible={visible} setVisible={setVisible} />
      <Card>
        <UserTable signInButton={signInButton} />
      </Card>
    </div>
  )
}

export default AdminUsers
