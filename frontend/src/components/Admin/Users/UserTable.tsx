import { Space, Table, Spin } from 'antd'
import React, { FC, ReactNode } from 'react'
import { useGetUsersQuery } from '../../../store'

export interface UserTableProps {
  signInButton: (record) => ReactNode
}

const UserTable: FC<UserTableProps> = (props) => {
  const { signInButton } = props
  const { data: users, isLoading } = useGetUsersQuery()
  if (!users) return null

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => <Space size='middle'>{signInButton(record)}</Space>,
    },
  ]
  if (isLoading) return <Spin />

  return <Table columns={columns} dataSource={users} rowKey={(record) => record.id} />
}

export default UserTable
