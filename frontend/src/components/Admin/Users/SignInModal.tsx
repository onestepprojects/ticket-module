import { useMemoizedFn } from 'ahooks'
import { Form, FormInstance, Input, Modal, notification } from 'antd'
import React, { FC } from 'react'

export interface SignInModalProps {
  signIn: any // eslint-disable-line
  initialValues?: any // eslint-disable-line
  visible: boolean
  setVisible: any // eslint-disable-line
  form: FormInstance
}

const SignInModal: FC<SignInModalProps> = ({
  form,
  signIn,
  initialValues,
  visible,
  setVisible,
}) => {
  const [confirmLoading, setConfirmLoading] = React.useState(false)
  const [errors, setErrors] = React.useState(null)

  const handleCancel = () => {
    form.resetFields()
    setErrors(null)
    setVisible(false)
  }

  const handleSubmit = useMemoizedFn(async (values) => {
    setConfirmLoading(true)
    setErrors(null)
    try {
      await signIn(values).unwrap()
      setConfirmLoading(false)
      setVisible(false)
    } catch (err) {
      console.dir(err)
      notification.error({
        message: 'failed to login',
        description: JSON.stringify(err),
      })
      setErrors(err)
      throw new Error(err)
    } finally {
      setConfirmLoading(false)
    }
  })

  const handleValidate = useMemoizedFn(async () => {
    return form.validateFields().then(async (values) => {
      await handleSubmit(values)
    })
  })

  return (
    <Modal
      title='signin'
      centered
      visible={visible}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      okText='Login'
      onOk={() => {
        handleValidate().catch((info) => console.log('Validate failed', info))
      }}
    >
      <Form form={form} initialValues={initialValues}>
        <Form.Item name='username' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name='password' rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        {errors && <span className='text-red-500'>Validation Error: {JSON.stringify(errors)}</span>}
      </Form>
    </Modal>
  )
}
export default SignInModal
