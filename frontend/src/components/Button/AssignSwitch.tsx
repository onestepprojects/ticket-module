import { useMemoizedFn } from 'ahooks'
import { notification, Switch } from 'antd'
import React, { FC } from 'react'
import {
  selectIsAssigned,
  useAppSelector,
  useAssignTicketsMutation,
  useUnassignTicketsMutation,
} from '../../store'

export interface AssignSwitchProps {
  ticketId: number
}

const AssignSwitch: FC<AssignSwitchProps> = (props) => {
  const { ticketId } = props
  const isAssigned = useAppSelector((state) => selectIsAssigned(state, ticketId))
  const body = { ticketIds: [ticketId] }

  const [assign] = useAssignTicketsMutation()
  const [unassign] = useUnassignTicketsMutation()

  const toggleAssignment = useMemoizedFn(async (val) => {
    if (val === isAssigned) return
    try {
      await (val ? assign(body) : unassign(body)).unwrap()
    } catch (err) {
      console.debug('Assignment error:', err)
      notification.error({
        message: 'Failed to assign',
        description: err?.data?.message,
      })
    }
  })

  return (
    <Switch
      checkedChildren='Assigned'
      unCheckedChildren='Assign'
      style={{ color: 'green' }}
      size='small'
      checked={isAssigned}
      onChange={toggleAssignment}
    />
  )
}

export default AssignSwitch
