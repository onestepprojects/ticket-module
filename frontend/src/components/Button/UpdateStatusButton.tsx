import React, { FC } from 'react'
import { Button, ButtonProps, Dropdown, Menu } from 'antd'
import { selectUuid, TicketStatus, useUpdateTicketStatusMutation } from '../../store'
import { useMemoizedFn } from 'ahooks'
import { CheckOutlined, CloseCircleOutlined, DownOutlined } from '@ant-design/icons'
import classnames from 'classnames'

export interface UpdateStatusButtonProps extends ButtonProps {
  ticketId: number
  authorId: string
  status: TicketStatus
}

const UpdateStatusButton: FC<UpdateStatusButtonProps> = (props) => {
  const { className, ticketId, authorId, status, ...buttonProps } = props
  const userId = selectUuid()
  const [updateStatus] = useUpdateTicketStatusMutation()
  const isDisabled = userId !== authorId

  const handleUpdate = useMemoizedFn(async (val: TicketStatus) => {
    if (isDisabled || val === status) return
    try {
      await updateStatus({ ticketId, status: TicketStatus[val] }).unwrap()
    } catch (err) {
      console.debug('Update ticket status error:', err)
    }
  })

  const resolveMenu = (
    <Menu>
      <Menu.Item key={0} onClick={() => handleUpdate('completed')}>
        <a type='text' className='flex items-center justify-center'>
          Complete <CheckOutlined className='ml-2' style={{ color: 'green' }} />
        </a>
      </Menu.Item>
      <Menu.Item key={1} onClick={() => handleUpdate('closed')}>
        <a type='text' className='flex items-center justify-center'>
          Close <CloseCircleOutlined className='ml-2' style={{ color: 'red' }} />
        </a>
      </Menu.Item>
    </Menu>
  )

  return status === 'closed' || status === 'completed' ? (
    <Button
      disabled={isDisabled}
      onClick={() => handleUpdate('open')}
      // type='dashed'
      className={classnames('!rounded-full', className)}
      {...buttonProps}
    >
      Re-open
    </Button>
  ) : (
    <Dropdown
      disabled={isDisabled}
      overlay={resolveMenu}
      trigger={['click']}
      className={classnames('!rounded-full', className)}
    >
      <Button
        // type='dashed'
        {...buttonProps}
      >
        <span>
          Resolve <DownOutlined />
        </span>
      </Button>
    </Dropdown>
  )
}

export default UpdateStatusButton
