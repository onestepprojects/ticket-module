import React, { FC } from 'react'
import { LinkOutlined } from '@ant-design/icons'
import { Button, ButtonProps } from 'antd'

export type LinkTicketButtonProps = ButtonProps

const LinkTicketButton: FC<LinkTicketButtonProps> = (props) => {
  return (
    <Button icon={<LinkOutlined />} {...props}>
      Link
    </Button>
  )
}

export default LinkTicketButton
