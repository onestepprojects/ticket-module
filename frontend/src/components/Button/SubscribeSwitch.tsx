import React, { FC } from 'react'
import { notification, Switch } from 'antd'
import {
  useAppSelector,
  useSubscribeToTicketsMutation,
  useUnsubscribeFromTicketsMutation,
  selectIsSubscribed,
  useGetSubscriptionsQuery,
} from '../../store'
import { useMemoizedFn } from 'ahooks'

export interface SubscribeSwitchProps {
  ticketId: number
}

const SubscribeSwitch: FC<SubscribeSwitchProps> = (props) => {
  const { ticketId } = props
  const { data: _, isLoading } = useGetSubscriptionsQuery()
  const [subscribe] = useSubscribeToTicketsMutation()
  const [unsubscribe] = useUnsubscribeFromTicketsMutation()

  const isSubscribed = useAppSelector((state) => selectIsSubscribed(state, ticketId))
  const body = { ticketIds: [ticketId] }

  const toggleSubscription = useMemoizedFn(async (val) => {
    if (val === isSubscribed) return
    try {
      await (val ? subscribe(body) : unsubscribe(body)).unwrap()
    } catch (err) {
      console.log('subscription error', err)
      notification.error({
        message: 'Failed to subscribed',
        description: err?.data?.message,
      })
    }
  })

  if (isLoading) return null
  return (
    <Switch
      checkedChildren='Subscribed'
      unCheckedChildren='Subscribe'
      checked={isSubscribed}
      onChange={toggleSubscription}
    />
  )
}

export default SubscribeSwitch
