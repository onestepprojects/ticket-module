import React, { FC } from 'react'
import { Link } from 'react-router-dom-v5'
import './style.css'

export interface Error404Props {
  error?: string
}

export const Error404: FC<Error404Props> = (props) => {
  const { error } = props
  const msg = error ?? "The Page can't be found"
  return (
    <div>
      <div id='notfound'>
        <div className='notfound'>
          <div className='notfound-404'>
            <h1>Oops!</h1>
            <h4>{`404 - ${msg}`}</h4>
          </div>
          <Link to={`/`}>Return To Dashboard</Link>
        </div>
      </div>
    </div>
  )
}

export default Error404
