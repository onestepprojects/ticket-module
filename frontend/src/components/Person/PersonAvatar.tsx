import React, { FC } from 'react'
import { Avatar, Space, AvatarProps } from 'antd'
import { Person } from '../../store'
import styles from './person.module.css'
import { UserOutlined } from '@ant-design/icons'
import classnames from 'classnames'

export interface PersonAvatarProps extends AvatarProps {
  person: Person
  showUnassigned?: boolean
  inGroup?: boolean
  className?: string
  /* [key: string]: any // pass to child nodes to support tooltips */
}

interface UnassignedProps extends AvatarProps {
  className?: string
}

interface DefaultAvatarProps extends AvatarProps {
  person?: Person
  className?: string
}

const defaultClasses = 'mr-2 w-5 !bg-zinc-200'

const Unassigned: FC<UnassignedProps> = ({ className = '', ...props }) => (
  <Avatar
    {...props}
    className={classnames(defaultClasses, styles.unassigned, className)}
    icon={<UserOutlined style={{ fontSize: 28 }} />}
  />
)

const DefaultAvatar: FC<DefaultAvatarProps> = ({ person, className = '', ...props }) => {
  const hasPic = !!person?.profilePicture?.url
  return (
    <Avatar
      {...props}
      className={classnames(defaultClasses, styles.avatarNoPic, className)}
      src={person?.profilePicture?.url}
      icon={!hasPic && <UserOutlined style={{ fontSize: 28 }} />}
    />
  )
}

interface AvatarLabelProps {
  person?: Person
  showUnassigned: boolean
}

const AvatarLabel: FC<AvatarLabelProps> = ({ person, showUnassigned }) => {
  if (!person && showUnassigned) return <span className='italic'>Unassigned</span>
  return <span>{person ? person.name : ''}</span>
}

const PersonAvatar: FC<PersonAvatarProps> = (props) => {
  const {
    person,
    showUnassigned = false,
    inGroup = false,
    className = '',
    children,
    ...avatarProps
  } = props

  return inGroup && person ? (
    <DefaultAvatar {...avatarProps} person={person} className={className} />
  ) : (
    <span className={'inline-flex items-center'}>
      <Space>
        {person ? (
          <DefaultAvatar {...avatarProps} person={person} className={className} />
        ) : (
          showUnassigned && <Unassigned {...avatarProps} className={className} />
        )}
        {children || <AvatarLabel person={person} showUnassigned={showUnassigned} />}
      </Space>
    </span>
  )
}

export default PersonAvatar
