import React, { FC } from 'react'
import { Card, Col, Row, Switch, Timeline, Typography } from 'antd'
import classnames from 'classnames'
import styles from './changelog.module.css'
import { ChangeLogEntry, TicketChange, useGetTicketChangeLogQuery } from '../../store'
import dayjs from 'dayjs'

export interface TicketChangeLogProps {
  ticketId: number
  className?: string
  id?: string
}

export const logColor = (type: TicketChange) => {
  switch (type) {
    case TicketChange.created:
      return 'green'
    case TicketChange.modified:
      return 'blue'
    case TicketChange.statusChange:
      return 'red'
  }
}

const logInfo = ({ type, user, log }: ChangeLogEntry) => {
  switch (type) {
    case TicketChange.created:
      return `Created by ${user}`
    case TicketChange.modified:
      return `Modified by ${user}`
    case TicketChange.statusChange:
      return `Status changed to ${log} by ${user}`
  }
}

const TicketChangeLog: FC<TicketChangeLogProps> = (props) => {
  const { ticketId, className, id } = props
  const { data: changelog, isLoading } = useGetTicketChangeLogQuery({ ticketId })
  const [fmtSince, setFmtSince] = React.useState(true)

  const changeFmt = (val: boolean) => setFmtSince(val)

  if (!changelog || isLoading) return null
  return (
    <Card
      title={
        <div className='flex justify-between'>
          <Typography.Title level={4}>Ticket History</Typography.Title>
          <Col>
            <div>Format date</div>
            <Switch
              checkedChildren='From now'
              unCheckedChildren='MM/DD/YY'
              checked={fmtSince}
              onChange={changeFmt}
            />
          </Col>
        </div>
      }
      className={classnames(className, styles.history)}
      id={id}
    >
      <Timeline>
        {changelog?.map((log) => (
          <Timeline.Item key={log.id} color={logColor(log.type)}>
            <Row className='!justify-between'>
              <div>{logInfo(log)}</div>
              <div>
                {fmtSince
                  ? dayjs(log.createdAt).fromNow()
                  : dayjs(log.createdAt).format('MM/DD/YY')}
              </div>
            </Row>
          </Timeline.Item>
        ))}
      </Timeline>
    </Card>
  )
}

export default TicketChangeLog
