import { BellOutlined, CheckOutlined, CloseOutlined, RightOutlined } from '@ant-design/icons'
import { Badge, Button, Divider, List, Popover, PopoverProps, Switch, Typography } from 'antd'
import dayjs from 'dayjs'
import React, { FC } from 'react'
import { useHistory } from 'react-router-dom-v5'
import {
  getUnreadNotifications,
  useGetNotificationsQuery,
  useMarkNotificationsMutation,
} from '../../store'
import styles from './notification.module.css'

export type NotificationPopoverProps = PopoverProps

const NotificationPopover: FC<NotificationPopoverProps> = (props) => {
  const { getPopupContainer, ...popoverProps } = props
  const { data: notifications, isLoading: loadingNotifs } = useGetNotificationsQuery({})
  const [markNotifications] = useMarkNotificationsMutation()
  const history = useHistory()
  const [unreadOnly, setUnreadOnly] = React.useState(true)

  const clearNotifications = () => markNotifications({ read: 'all' })

  const handleClick = (ticketId: number) => history.push(`/tickets/${ticketId}`)

  if (loadingNotifs) return null
  const numUnread = getUnreadNotifications(notifications)
  const notifs = unreadOnly ? notifications?.filter((notif) => notif.read === false) : notifications
  return (
    <Popover
      placement='bottomRight'
      trigger='click'
      key='notifications'
      overlayClassName={styles.notificationPopover}
      getPopupContainer={getPopupContainer}
      {...popoverProps}
      content={
        <div className={styles.notificationPopContent}>
          <Typography.Title className='!m-0 !p-0' level={4}>
            Notifications
          </Typography.Title>
          <List
            itemLayout='horizontal'
            dataSource={notifs}
            header={
              <div className='flex justify-between'>
                <div>
                  <span className='mr-2 text-xs'>Only show unread</span>
                  <Switch
                    checkedChildren={<CheckOutlined />}
                    unCheckedChildren={<CloseOutlined />}
                    checked={unreadOnly}
                    onChange={() => setUnreadOnly(!unreadOnly)}
                  />

                  <Divider className='!m-0 !p-0' type='vertical' />
                  <Button
                    type='link'
                    onClick={() =>
                      markNotifications({ [numUnread === 0 ? 'unread' : 'read']: 'all' })
                    }
                  >
                    Mark all {numUnread === 0 ? 'unread' : 'read'}
                  </Button>
                </div>
              </div>
            }
            locale={{
              emptyText: <span>You have viewed all notifications.</span>,
            }}
            renderItem={(notif) => (
              <List.Item
                onClick={() => handleClick(notif.ticketId)}
                className={styles.notificationPopItem}
              >
                <List.Item.Meta
                  title={notif.message}
                  description={dayjs(notif.createdAt).fromNow()}
                />
                <RightOutlined style={{ fontSize: 10, color: '#ccc' }} />
              </List.Item>
            )}
          />
          {notifs.length ? (
            <div onClick={clearNotifications} className={styles.clearButton}>
              Clear notifications
            </div>
          ) : null}
        </div>
      }
    >
      <Badge showZero={false} count={numUnread} offset={[-20, 15]} className={styles.iconButton}>
        <BellOutlined className={styles.iconFont} />
      </Badge>
    </Popover>
  )
}

export default NotificationPopover
