import { Typography } from 'antd'
import React, { FC } from 'react'
import { NotificationTypeProps } from '.'

export const CommentNotification: FC<NotificationTypeProps> = ({ notification, sender }) => (
  <Typography.Paragraph>
    {notification.message}
    <span className='italic pl-4'>- {sender?.name}</span>
  </Typography.Paragraph>
)

export default CommentNotification
