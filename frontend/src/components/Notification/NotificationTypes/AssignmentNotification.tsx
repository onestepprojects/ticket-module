import React, { FC } from 'react'
import { NotificationTypeProps } from '.'

export const AssignmentNotification: FC<NotificationTypeProps> = ({ notification, sender }) => (
  <span>
    {sender?.name} {notification.message}ed {notification.message === 'assign' ? 'to' : 'from'}{' '}
    ticket
  </span>
)

export default AssignmentNotification
