import React, { FC } from 'react'
import {
  AssignmentNotification,
  CommentNotification,
  NewTicketNotification,
  NotificationTypeProps,
  RewardNotification,
} from '.'
import { NotificationType } from '../../../store'

export const NotificationTypeBody: FC<NotificationTypeProps> = (props) => {
  const { notification } = props

  switch (notification.type) {
    case NotificationType.rewards:
      return <RewardNotification {...props} />
    case NotificationType.ticketComment:
      return <CommentNotification {...props} />
    case NotificationType.ticketStatusChange:
      return <CommentNotification {...props} />
    case NotificationType.ticketAssignment:
      return <AssignmentNotification {...props} />
    case NotificationType.ticketCreate:
      return <NewTicketNotification {...props} />
    default:
      return <span>{notification.message}</span>
  }
}

export default NotificationTypeBody
