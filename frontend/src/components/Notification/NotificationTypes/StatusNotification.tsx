import React, { FC } from 'react'
import { NotificationTypeProps } from '.'
import StatusBadge from '../../Badge/StatusBadge'

export const StatusNotification: FC<NotificationTypeProps> = ({ notification }) => (
  <span>
    Ticket status changed to <StatusBadge status={notification.status} className='text-sm h-5' />
  </span>
)

export default StatusNotification
