import React, { FC } from 'react'
import { Tag } from 'antd'
import {
  selectTagById,
  useAppSelector,
  selectUuid,
  useGetOrganizationRolesByPersonQuery,
  useGetWatchedQuery,
} from '../../../store'
import { NotificationTypeProps } from '.'

/**
 * Notifications for new tickets get tagged with the watched tags/organizations
 * that where added to the new ticket
 */
export const NewTicketNotification: FC<NotificationTypeProps> = (props) => {
  const { notification: notif } = props
  const uuid = selectUuid()
  if (!uuid) return null
  const { data: userOrgs, isLoading: loadingOrgs } = useGetOrganizationRolesByPersonQuery(uuid)
  const { data: watched, isLoading: loadingWatched } = useGetWatchedQuery()
  const tags = notif.tags?.map((id) => useAppSelector((state) => selectTagById(state, id))) ?? []

  if (loadingWatched || loadingOrgs) return null

  const watchedTags =
    tags && watched?.tagIds.length > 0
      ? tags.filter((tag) => watched.tagIds.indexOf(tag.id) !== -1)
      : undefined

  const watchedOrgs =
    (userOrgs && watched?.orgIds?.map((id) => userOrgs.entities[id]).filter((x) => !!x)) ||
    undefined

  return (
    <div>
      {watchedTags && (
        <div>
          <span className='italic pr-2'>tagged:</span>
          {watchedTags.map((tag) => (
            <Tag key={tag.id} color='blue'>
              {tag?.name}
            </Tag>
          ))}
        </div>
      )}
      {watchedOrgs && (
        <div>
          <span className='italic pr-2'>organization:</span>
          {watchedOrgs.map((org) => (
            <Tag key={org?.id} color='volcano'>
              {org.name}
            </Tag>
          ))}
        </div>
      )}
    </div>
  )
}

export default NewTicketNotification
