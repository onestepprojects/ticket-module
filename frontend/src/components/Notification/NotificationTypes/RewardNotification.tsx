import { Tag } from 'antd'
import React, { FC } from 'react'
import { NotificationTypeProps } from '.'

export const RewardNotification: FC<NotificationTypeProps> = ({ notification }) => {
  return notification.points > 0 ? (
    <span>
      Completed ticket <Tag color='green'>+{notification.points} points</Tag>
    </span>
  ) : (
    <span>
      Ticket re-opened <Tag color='volcano'>{notification.points} points</Tag>
    </span>
  )
}

export default RewardNotification
