import { Person, Notification } from '../../../store'

export interface NotificationTypeProps {
  notification: Notification
  sender?: Person
}

export * from './AssignmentNotification'
export * from './CommentNotification'
export * from './StatusNotification'
export * from './RewardNotification'
export * from './NotificationTypeTag'
export * from './NotificationTypeBody'
export * from './NewTicketNotification'
