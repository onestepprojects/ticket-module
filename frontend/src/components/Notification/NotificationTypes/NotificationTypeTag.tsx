import { Tag } from 'antd'
import React, { FC } from 'react'
import { NotificationType } from '../../../store'

// Notification [ color, event ]
export const notificationColorEvent = {
  [NotificationType.rewards]: ['gold', 'rewards'],
  [NotificationType.ticketUpdate]: ['geekblue', 'updated'],
  [NotificationType.ticketStatusChange]: ['purple', 'status'],
  [NotificationType.ticketAssignment]: ['volcano', 'assignment'],
  [NotificationType.ticketComment]: ['cyan', 'comment'],
  [NotificationType.ticketCreate]: ['green', 'created'],
  [NotificationType.withOrgs]: ['green', 'created'],
  [NotificationType.withTags]: ['green', 'created'],
  [NotificationType.withRoles]: ['green', 'created'],
  default: ['gray', 'unknown'],
}

export const NotificationTypeTag: FC<{ type: NotificationType }> = ({ type }) => {
  const [color, event] = notificationColorEvent[type] || notificationColorEvent.default
  return <Tag color={color}>{event}</Tag>
}
