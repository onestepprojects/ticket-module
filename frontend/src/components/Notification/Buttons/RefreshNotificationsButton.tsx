import { Button, ButtonProps, notification } from 'antd'
import React, { FC } from 'react'
import { useLazyGetNotificationsQuery } from '../../../store'

export interface RefreshNotificationsButtonProps extends ButtonProps {}

export const RefreshNotificationsButton: FC<RefreshNotificationsButtonProps> = (props) => {
  const [getNotifs, { data: _ }] = useLazyGetNotificationsQuery({})
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const handleSubmit = async () => {
    setIsSubmitting(true)
    try {
      await getNotifs({}, false).unwrap()
      notification.success({
        message: 'Notifications updated',
      })
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  }
  return (
    <Button {...props} onClick={() => handleSubmit()} loading={isSubmitting}>
      Refresh Notifications
    </Button>
  )
}

export default RefreshNotificationsButton
