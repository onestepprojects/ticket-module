import { notification, Spin } from 'antd'
import React, { FC } from 'react'
import { useLazyGetNotificationsQuery } from '../../../store'

export interface RefreshNotificationsLinkProps {
  className?: string
}

export const RefreshNotificationsLink: FC<RefreshNotificationsLinkProps> = (props) => {
  const { className } = props
  const [getNotifs, { data: _ }] = useLazyGetNotificationsQuery({})
  const [isSubmitting, setIsSubmitting] = React.useState(false)

  const handleSubmit = async () => {
    setIsSubmitting(true)
    try {
      await getNotifs({}, false).unwrap()
      notification.success({
        message: 'Notifications updated',
      })
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  }

  return isSubmitting ? (
    <Spin />
  ) : (
    <a className={className} {...props} onClick={() => handleSubmit()}>
      Refresh Notifications
    </a>
  )
}

export default RefreshNotificationsLink
