import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import { Button, Card, Divider, List, Switch, Typography } from 'antd'
import React, { FC } from 'react'
import {
  selectUuid,
  useDeleteReadNotificationsMutation,
  useGetNotificationsQuery,
  useGetPersonsQuery,
  useGetTagsQuery,
  useGetTicketsQuery,
  useMarkNotificationsMutation,
} from '../../store'
import NotificationItem from './NotificationItem'
import styles from './notification.module.css'
import classnames from 'classnames'

export interface ShowNotificationsProps {}

const ShowNotifications: FC<ShowNotificationsProps> = (_props) => {
  const uuid = selectUuid()
  if (!uuid) return null
  const { data: notifications, isLoading: loadingNotifs } = useGetNotificationsQuery({})
  const { data: _tags } = useGetTagsQuery()
  const { data: _persons } = useGetPersonsQuery()
  const { data: _tickets } = useGetTicketsQuery()
  const [markNotifications] = useMarkNotificationsMutation()
  const [deleteReadNotifications] = useDeleteReadNotificationsMutation()
  const [unreadOnly, setUnreadOnly] = React.useState(false)
  const numUnread = notifications?.reduce((acc, notif) => acc + Number(notif.read === false), 0)

  if (loadingNotifs) return null
  return (
    <Card className='!mt-2 h-full'>
      <Typography.Title level={4}>Notifications</Typography.Title>

      <List
        header={
          <div className='flex justify-between'>
            <div className='flex items-center'>
              <div className={classnames(styles.count, 'bg-slate-200')}>{numUnread}</div>
              <span> unread notification{numUnread !== 1 ? 's' : ''}</span>
            </div>
            <div>
              <span className='mr-3'>Only show unread</span>
              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                checked={unreadOnly}
                onChange={() => setUnreadOnly(!unreadOnly)}
              />

              <Divider className='!m-0 !p-0' type='vertical' />
              <Button type='link' onClick={() => deleteReadNotifications()}>
                Delete all read
              </Button>
              <Divider className='!m-0 !p-0' type='vertical' />
              <Button
                type='link'
                onClick={() => markNotifications({ [numUnread === 0 ? 'unread' : 'read']: 'all' })}
              >
                Mark all {numUnread === 0 ? 'unread' : 'read'}
              </Button>
            </div>
          </div>
        }
        itemLayout='horizontal'
      >
        {notifications?.map((notif) => (
          <NotificationItem key={notif.id} notification={notif} unreadOnly={unreadOnly} />
        ))}
      </List>

      {(notifications?.length === 0 || (unreadOnly && numUnread === 0)) && (
        <div className='!min-h-screen flex !justify-center !items-center pb-[100px]'>
          <span className='text-xl text-slate-500 text-center'>
            {notifications.length === 0 ? (
              <p>No notifications</p>
            ) : (
              <p>You've read all your notifications</p>
            )}
          </span>
        </div>
      )}
    </Card>
  )
}

export default ShowNotifications
