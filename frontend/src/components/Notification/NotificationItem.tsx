import React, { FC } from 'react'
import { Col, Divider, List, Row, Tooltip } from 'antd'
import { ListItemProps } from 'antd/lib/list'
import { InfoCircleOutlined, CheckCircleOutlined } from '@ant-design/icons'
import classnames from 'classnames'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import { Link } from 'react-router-dom-v5'
import {
  Notification,
  selectPersonById,
  selectTicketById,
  useAppSelector,
  useMarkNotificationsMutation,
} from '../../store'
import { truncate } from 'lodash-es'
import { NotificationTypeTag, NotificationTypeBody } from '.'

dayjs.extend(relativeTime)

export interface NotificationItemProps extends ListItemProps {
  notification: Notification
  unreadOnly: boolean
}

const NotificationItem: FC<NotificationItemProps> = (props) => {
  const { className, unreadOnly, notification: notif, ...itemProps } = props
  const sender = notif.senderId
    ? useAppSelector((state) => selectPersonById(state, notif.senderId))
    : undefined
  const ticket = useAppSelector((state) => selectTicketById(state, notif.ticketId))
  const [markNotification] = useMarkNotificationsMutation()

  const handleMark = () => markNotification({ [notif.read ? 'unread' : 'read']: [notif.id] })

  if (!notif || (unreadOnly && notif.read)) return null
  return (
    <List.Item
      key={notif.id}
      actions={[
        notif.ticketId ? <Link to={`/tickets/${notif.ticketId}`}>#{notif.ticketId}</Link> : null,
        <Tooltip title={notif.read ? 'Mark unread' : 'Mark read'}>
          {notif.read ? (
            <InfoCircleOutlined onClick={handleMark} />
          ) : (
            <CheckCircleOutlined onClick={handleMark} style={{ color: '#096dd9' }} />
          )}
        </Tooltip>,
      ]}
      className={classnames('items-center !pb-1', className)}
      {...itemProps}
    >
      <Row className='items-center'>
        <Col className='min-w-[120px] inline-flex items-center'>
          <List.Item.Meta
            className=''
            description={dayjs(notif.updatedAt).fromNow()}
            title={<NotificationTypeTag type={notif.type} />}
          />
          <Divider type='vertical' />
        </Col>

        <Col className='justify-items-start items-center content-between'>
          <Row className='pb-1'>
            <Link to={`/tickets/${notif.ticketId}`}>
              <span className='italic pr-2'>#{notif.ticketId}</span>
              {truncate(ticket?.title, { length: 50 })}
            </Link>
          </Row>
          <Row>
            <NotificationTypeBody notification={notif} sender={sender} />
          </Row>
        </Col>
      </Row>
    </List.Item>
  )
}

export default NotificationItem
