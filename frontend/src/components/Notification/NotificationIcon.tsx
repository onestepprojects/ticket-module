import React, { FC, ComponentProps } from 'react'
import { BellOutlined } from '@ant-design/icons'
import { Badge } from 'antd'
import { getUnreadNotifications, useGetNotificationsQuery } from '../../store'
import styles from './notification.module.css'
import classnames from 'classnames'

export interface NotificationIconProps extends ComponentProps<typeof BellOutlined> {
  className?: string
}

export const NotificationIcon: FC<NotificationIconProps> = (props) => {
  const { data: notifications, isLoading: loadingNotifs } = useGetNotificationsQuery({})
  const { className } = props
  if (loadingNotifs) return null
  const numUnread = getUnreadNotifications(notifications)
  return (
    <Badge
      showZero={false}
      count={numUnread}
      // offset={[-20, 15]}
      className={classnames(styles.iconBadge, 'ant-menu-item-icon')}
    >
      <BellOutlined
        // className={styles.iconFont}
        className={classnames('!leading-0 w-[48px]', className)}
        style={{ lineHeight: '0px' }}
        {...props}
      />
    </Badge>
  )
}

export default NotificationIcon
