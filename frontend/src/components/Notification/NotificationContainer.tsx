import React, { FC } from 'react'
import { useGetNotificationsQuery } from '../../store'

export interface NotificationContainerProps {
  children: React.ReactElement
}

const NotificationContainer: FC<NotificationContainerProps> = (props) => {
  const { children } = props
  /* maybe? open websocket to receive notification updates */
  const { data: notifs, isLoading } = useGetNotificationsQuery({})

  if (isLoading) return null
  return <>{React.cloneElement(children, { notifications: notifs })}</>
}

export default NotificationContainer
