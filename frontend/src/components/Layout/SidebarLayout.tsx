import React, { FC, Fragment } from 'react'
import { Link } from 'react-router-dom-v5'
import { Layout, Menu } from 'antd'
import { CreateTicketModal } from '../Ticket/CreateTicket'
import { DefaultLayoutProps } from '.'
import routes, { Route } from '../../routes'
import { CommentOutlined, NotificationOutlined, PlusOutlined } from '@ant-design/icons'
import { groupBy } from '../../util'
import classnames from 'classnames'
import styles from './layout.module.css'
import { RefreshNotificationsLink } from '..'
import RefreshCommentsLink from '../Comment/RefreshCommentsLink'
export type SidebarLayoutProps = DefaultLayoutProps

// Order of tabs in main sidebar
// Todo - fix text on subMenu, subMenu needs black text when (sideBar.collapsed === true)
// Todo - also need to fix radio buttons on some menus
const sidebarOrder = [
  { name: 'Dashboard' },
  { name: 'Search' },
  // { subMenu: 'Tickets', icon: TicketOutline },
  { name: 'Notifications' },
  { name: 'History' },
  { name: 'Divider' },
  { name: 'Settings' },
]

const tabGroups = groupBy<Route>(
  routes.filter((route) => route.sidebarPos >= 0 && !route.admin),
  (x) => x.subMenu ?? x.name
)

const adminRoutes = routes.filter((route) => route.sidebarPos >= 0 && route.admin)

// TODO: conditional styling needs fixed, sub-menu, versus name have
// a different primary color by default in antd (the colors seem OK? -nvp)
// eslint-disable-next-line
const menuItem = (route: Route, disabled = false, collapsed: boolean) => {
  return (
    <Menu.Item
      key={route.name + (route.admin ? '-admin' : '')}
      icon={route.icon && <route.icon />}
      disabled={disabled}
    >
      <Link className={collapsed ? '!text-white' : '!text-black'} to={route.path}>
        {route.name}
      </Link>
    </Menu.Item>
  )
}

const subMenuItem = (item, disabled = false, collapsed: boolean) => {
  const group = tabGroups[item.name].sort((a, b) => a?.subGroup - b?.subGroup)
  return (
    <Menu.SubMenu key={`submenu-${item.name}`} title={item.name} icon={item?.icon && <item.icon />}>
      {group.map((x, idx) => {
        if (idx > 0 && group[idx - 1]?.subGroup !== group[idx]?.subGroup)
          return (
            <Fragment key={idx}>
              <Menu.Divider key={idx} />
              {menuItem(x, disabled, collapsed)}
            </Fragment>
          )
        return menuItem(x, disabled, collapsed)
      })}
    </Menu.SubMenu>
  )
}

const tabList = (collapsed: boolean) => (
  <>
    {sidebarOrder.map(({ name, subMenu, icon }, idx) =>
      subMenu ? (
        subMenuItem({ name: subMenu, icon }, false, collapsed)
      ) : name === 'Divider' ? (
        <Menu.Divider key={idx} color='gray-8' />
      ) : (
        menuItem(tabGroups[name][0], false, collapsed)
      )
    )}
  </>
)

const adminTabs = (collapsed: boolean) => (
  <Menu.ItemGroup key='admin' title='Admin'>
    {adminRoutes.map((x) => menuItem(x, false, collapsed))}
  </Menu.ItemGroup>
)

// Extra menu items to help during development
const devTabs = (collapsed: boolean) => {
  const className = collapsed ? '!text-white' : '!text-black'
  return (
    <Menu.ItemGroup key='dev' title='Dev'>
      <Menu.Item key='refresh-notifications' icon={<NotificationOutlined />}>
        <RefreshNotificationsLink className={className} />
      </Menu.Item>
      <Menu.Item key='refresh-comments' icon={<CommentOutlined />}>
        <RefreshCommentsLink className={className} />
      </Menu.Item>
    </Menu.ItemGroup>
  )
}

// menu items not to select when clicked
const ignoreSelected = ['ticket-modal', 'refresh-notifications', 'refresh-comments']

const SidebarLayout: FC<SidebarLayoutProps> = (props) => {
  const { auth: _, isAdmin, children } = props
  const [visible, setVisible] = React.useState(false)
  const [collapsed, setCollapsed] = React.useState(true)
  const [selectedKeys, setSelectedKeys] = React.useState(['2'])
  const { Content, Sider } = Layout

  // don't select create ticket modal in menu
  const onSelect = ({ item: _item, key, selectedKeys: _selected }) => {
    if (ignoreSelected.indexOf(key) !== -1) return
    setSelectedKeys([key])
  }

  return (
    <Layout>
      <Layout className='content-layout'>
        <Content style={{ margin: '0 16px' }}>{children}</Content>
      </Layout>
      <CreateTicketModal visible={visible} setVisible={setVisible} maskClosable={true} />
      <Sider
        className={classnames(styles.collapsed, 'shadow-sm')}
        collapsible
        collapsed={collapsed}
        reverseArrow={true}
        onCollapse={(val) => setCollapsed(val)}
      >
        <Menu
          theme='light'
          mode='inline'
          defaultSelectedKeys={['Dashboard']}
          selectedKeys={selectedKeys}
          onSelect={onSelect}
          style={{ height: '100%', borderRight: 0 }}
        >
          <Menu.Item
            key='ticket-modal'
            icon={<PlusOutlined />}
            title='New Ticket'
            onClick={() => setVisible(true)}
          >
            New Ticket
          </Menu.Item>
          {tabList(collapsed)}
          {devTabs(collapsed)}
          {isAdmin && adminTabs(collapsed)}
        </Menu>
      </Sider>
    </Layout>
  )
}

export default SidebarLayout
