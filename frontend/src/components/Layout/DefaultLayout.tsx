import React, { FC } from 'react'
import { Layout } from 'antd'
import { TicketContainerProps } from '../../Container'
import SidebarLayout from './SidebarLayout'

export interface DefaultLayoutProps extends Pick<TicketContainerProps, 'auth'> {
  isAdmin: boolean
  children: React.ReactNode
}

export type DefaultTicketProps = Omit<DefaultLayoutProps, 'children'>

const DefaultLayout: FC<DefaultLayoutProps> = (props) => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <SidebarLayout {...props} />
    </Layout>
  )
}
export default DefaultLayout
