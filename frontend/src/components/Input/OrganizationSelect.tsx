import React, { FC } from 'react'
import { Select, SelectProps } from 'antd'
import {
  selectAllUserOrgs,
  useAppSelector,
  useGetOrganizationRolesByPersonQuery,
} from '../../store'

export type OrganizationSelectProps = SelectProps

const OrganizationSelect: FC<OrganizationSelectProps> = (props) => {
  const userId = useAppSelector((state) => state.auth.person.uuid)
  const { Option } = Select
  const { data: userOrgs, isLoading } = useGetOrganizationRolesByPersonQuery(userId)

  const orgs = userOrgs ? selectAllUserOrgs(userOrgs) : []

  return (
    <Select
      mode='multiple'
      placeholder='select organizations...'
      allowClear
      loading={isLoading}
      {...props}
    >
      {orgs?.map((org) => (
        <Option key={org.id} value={org.id}>
          {org.name}
        </Option>
      ))}
    </Select>
  )
}

export default OrganizationSelect
