import React, { FC } from 'react'
import { Select, SelectProps } from 'antd'
import { selectAllPersons, useAppSelector, useGetPersonsQuery } from '../../store'

export type PersonInputProps = SelectProps

const PersonSelect: FC<PersonInputProps> = (props) => {
  const { Option } = Select
  const { data: _, isLoading } = useGetPersonsQuery()
  const persons = useAppSelector(selectAllPersons)

  if (isLoading) return null

  return (
    <Select placeholder='select person' allowClear {...props}>
      {persons?.map((person) => (
        <Option key={person.uuid} value={person.uuid}>
          {person.name}
        </Option>
      ))}
    </Select>
  )
}

export default PersonSelect
