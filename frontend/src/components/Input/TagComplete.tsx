import { AutoComplete, AutoCompleteProps } from 'antd'
import { lowerCase } from 'lodash-es'
import React, { FC } from 'react'
import { selectTagsData, Tag, useAppSelector, useGetTagsQuery } from '../../store'
import { TagOption } from '../Tag'

export interface TagCompleteProps extends AutoCompleteProps {
  onSelectedTag?: (tag: Tag) => any // eslint-disable-line
  onSelectedLabel?: (label: string) => any // eslint-disable-line
  exclude?: { [key: number]: any } // eslint-disable-line
}

const TagComplete: FC<TagCompleteProps> = (props) => {
  const { exclude = {}, onSelectedTag, onSelectedLabel, ...acProps } = props
  const { data: _, isLoading } = useGetTagsQuery()
  const [value, setValue] = React.useState('')
  const tagsData = useAppSelector((state) => selectTagsData(state))
  const options = tagsData?.ids
    .filter((id) => exclude[Number(id)] === undefined)
    .map((id) => ({
      label: lowerCase(tagsData.entities[id].name),
      value: String(id),
    }))

  const onSelect = (text: string, option: TagOption) => {
    onSelectedTag && onSelectedTag(tagsData.entities[Number(option.value)])
    setValue(onSelectedLabel ? onSelectedLabel(option.label) : option.label)
  }
  const onChange = (text: string) => setValue(text)

  if (isLoading || !tagsData) return null
  return (
    <AutoComplete
      value={value}
      options={options}
      style={{ width: 200 }}
      placeholder='search tags...'
      filterOption={(input, option: TagOption) => option.label.indexOf(input) !== -1}
      onSelect={onSelect}
      onChange={onChange}
      {...acProps}
    />
  )
}

export default TagComplete
