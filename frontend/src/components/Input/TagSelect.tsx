import React, { FC } from 'react'
import { Select, SelectProps } from 'antd'
import { selectAllTags, TagType, useAppSelector, useGetTagsQuery } from '../../store'
import { isArray, isEmpty } from 'lodash-es'

export interface TagSelectProps extends SelectProps {
  /** Only select from tags of type. If empty or undefined, select from all tags */
  type?: TagType | TagType[]
}

const TagSelect: FC<TagSelectProps> = (props) => {
  const { type: tagType, ...selectProps } = props
  const { data: _ } = useGetTagsQuery()
  const tagsData = useAppSelector(selectAllTags)
  if (!tagsData) return null
  const types = !tagType || isArray(tagType) ? tagType : [tagType]
  const tags = isEmpty(tagType)
    ? tagsData
    : tagsData?.filter(({ type }) => types.indexOf(type) !== -1)
  return (
    <Select
      mode='multiple'
      allowClear
      placeholder='select related tags'
      disabled={!tags}
      {...selectProps}
    >
      {tags?.map((tag) => (
        <Select.Option key={tag.id} value={tag.id}>
          {tag.name}
        </Select.Option>
      ))}
    </Select>
  )
}

export default TagSelect
