import React, { FC } from 'react'
import { Checkbox } from 'antd'
import { CheckboxGroupProps } from 'antd/lib/checkbox'
import { TicketEnums } from '../../store'
import { capitalize } from 'lodash-es'

export interface TicketEnumCheckboxProps extends CheckboxGroupProps {
  type: keyof typeof TicketEnums
}

const TicketEnumCheckbox: FC<TicketEnumCheckboxProps> = (props) => {
  const { type, ...checkProps } = props

  return (
    <Checkbox.Group {...checkProps}>
      {Object.keys(TicketEnums[type]).map((value, idx) => (
        <Checkbox key={idx} value={value}>
          {capitalize(value)}
        </Checkbox>
      ))}
    </Checkbox.Group>
  )
}

export default TicketEnumCheckbox
