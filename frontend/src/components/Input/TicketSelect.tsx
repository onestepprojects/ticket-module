import React, { FC } from 'react'
import { Select, SelectProps } from 'antd'
import {
  selectAllTickets,
  selectAllUserOrgs,
  useAppSelector,
  useGetOrganizationRolesByPersonQuery,
  useGetTicketsQuery,
} from '../../store'
import { truncate } from 'lodash-es'

export interface TicketSelectProps extends SelectProps {
  /** Restrict ticket selection to organizations in hash */
  orgs?: { [key: string]: any } // eslint-disable-line
}

const TicketSelect: FC<TicketSelectProps> = (props) => {
  const { orgs, ...selectProps } = props
  const { Option } = Select
  const { data: _, isLoading } = useGetTicketsQuery()
  const ticketsData = useAppSelector(selectAllTickets)

  const userId = useAppSelector((state) => state.auth.person.uuid)
  const { data: userOrgs } = useGetOrganizationRolesByPersonQuery(userId)

  if (isLoading || !userOrgs) return null

  if (isLoading) return null
  function checkMatchingOrg(ticket) {
    if (ticket.organizations.length > 0) {
      return true
    }
    return false
  }

  let tickets =
    orgs != null ? ticketsData.filter((ticket) => checkMatchingOrg(ticket)) : ticketsData
  tickets = tickets.slice(0, 10)

  return (
    <Select mode='multiple' placeholder='select tickets...' allowClear {...selectProps}>
      {tickets?.map((ticket) => (
        <Option key={ticket.id} value={ticket.id}>
          Ticket {ticket.id}: {truncate(ticket.title)}
        </Option>
      ))}
    </Select>
  )
}

export default TicketSelect
