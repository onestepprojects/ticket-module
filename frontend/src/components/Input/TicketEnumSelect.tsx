import React, { FC } from 'react'
import { Select, SelectProps } from 'antd'
import { TicketEnums } from '../../store'
import { capitalize } from 'lodash-es'

export interface TicketEnumSelectProps extends SelectProps {
  type: keyof typeof TicketEnums
}

const TicketEnumSelect: FC<TicketEnumSelectProps> = (props) => {
  const { type, ...selectProps } = props
  const { Option } = Select

  return (
    <Select
      mode='multiple'
      placeholder={`select ${type}, leave blank for all`}
      allowClear
      {...selectProps}
    >
      {Object.keys(TicketEnums[type]).map((value, idx) => (
        <Option key={idx} value={value}>
          {capitalize(value)}
        </Option>
      ))}
    </Select>
  )
}

export default TicketEnumSelect
