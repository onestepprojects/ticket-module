import { Radio, RadioGroupProps, RadioProps } from 'antd'
import React, { FC } from 'react'
import { TicketPriority } from '../../store'
import { capitalize } from 'lodash-es'

export interface TicketPriorityRadioProps extends RadioGroupProps {
  buttonProps?: RadioProps
}

const TicketPriorityRadio: FC<TicketPriorityRadioProps> = (props) => {
  const { buttonProps, ...groupProps } = props
  return (
    <Radio.Group {...groupProps}>
      {Object.keys(TicketPriority).map((value) => (
        <Radio.Button key={value} {...buttonProps} value={value}>
          {capitalize(value)}
        </Radio.Button>
      ))}
    </Radio.Group>
  )
}

export default TicketPriorityRadio
