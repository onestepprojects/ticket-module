import classnames from 'classnames'
import React, { FC } from 'react'
import Spinner from './Spinner'

export interface LoadingContainerProps {
  children: React.ReactElement
  isUninitialized?: boolean
  isLoading?: boolean
  isSuccess?: boolean
  isError?: boolean
  isFetching?: boolean
  error?: { status?: string; error?: string }
  className?: string
}

const LoadingContainer: FC<LoadingContainerProps> = (props) => {
  const { children, className, error, isUninitialized, isLoading, isSuccess, isError, isFetching } =
    props

  let content: React.ReactElement
  if (isUninitialized) {
    content = null
  } else if (isLoading) {
    content = <Spinner text='Loading data...' />
  } else if (isError) {
    content = (
      <div className='min-h-screen flex flex-col justify-center items-center py-12 sm:px-6 lg:px-8'>
        <h2 className='text-2xl font-bold'> Error fetching data ... </h2>
        <div>{error?.status}: </div>
        <div>{error?.error}</div>
      </div>
    )
  } else if (isSuccess) {
    const classes = classnames(className, { disabled: isFetching })
    content = <div className={classes}>{children}</div>
  } else content = <div>What happened here?</div>

  return content
}
export default LoadingContainer
