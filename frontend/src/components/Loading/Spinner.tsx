import React, { FC } from 'react'
import './spinner.css'
/* https://redux.js.org/tutorials/essentials/part-8-rtk-query-advanced#streaming-cache-updates */

interface SpinnerProps {
  text?: string
  size?: string | number
}

const Spinner: FC<SpinnerProps> = ({ text = '', size = '5em' }) => {
  const header = text ? <h4>{text}</h4> : null
  return (
    <div className='spinner'>
      {header}
      <div className='loader' style={{ height: size, width: size }} />
    </div>
  )
}
export default Spinner
