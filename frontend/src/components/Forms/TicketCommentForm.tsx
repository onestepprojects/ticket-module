import React, { FC } from 'react'
import { Avatar, Button, Form, FormProps } from 'antd'
import {
  Comment,
  CommentData,
  selectPersonById,
  selectUuid,
  useAppSelector,
  useCreateCommentMutation,
} from '../../store'
import { useMemoizedFn } from 'ahooks'
import { useLocation } from 'react-router-dom-v5'
import MarkdownEditor from '../Markdown/MarkdownEditor'
import classnames from 'classnames'
import styles from './form.module.css'

export interface TicketCommentFormProps extends FormProps {
  ticketId: number
  className?: string
}

const placeholderUrl =
  'https://www.pngkey.com/png/full/115-1150152_default-profile-picture-avatar-png-green.png'

const TicketCommentForm: FC<TicketCommentFormProps> = (props) => {
  const { ticketId, className, ...formProps } = props
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [visible, setVisible] = React.useState(false)
  const [comment, setComment] = React.useState('')
  const [form] = Form.useForm<Comment>()
  const location = useLocation()

  const [createComment] = useCreateCommentMutation()
  const uuid = selectUuid()
  const person = useAppSelector((state) => selectPersonById(state, uuid))

  React.useEffect(() => {
    handleCancel()
  }, [location])

  const handleCancel = useMemoizedFn(() => {
    setComment('')
    form.setFieldsValue({ comment: '' })
    setVisible(false)
  })

  const handleValidate = useMemoizedFn(async () => {
    try {
      return form.validateFields()
    } catch (err) {
      console.debug('Comment form validation error:', err)
    }
  })

  const handleSubmit = useMemoizedFn(async (data: CommentData) => {
    setIsSubmitting(true)
    try {
      await handleValidate()
      await createComment({ userId: person.uuid, ticketId, ...data }).unwrap()
      handleCancel()
    } catch (err) {
      console.debug('Failed to create comment', err)
    } finally {
      setIsSubmitting(false)
    }
  })

  return (
    <div className={classnames(styles.commentInput, 'inline-flex !w-full flex-row align-center')}>
      <div className='mr-5'>
        <Avatar src={person?.profilePicture?.url ?? placeholderUrl} />
      </div>
      {visible ? (
        <Form
          name='comment-form'
          className={classnames('inline-block w-full', className)}
          form={form}
          onFinish={handleSubmit}
          colon={false}
          initialValues={{ comment }}
          {...formProps}
        >
          <Form.Item name='comment' rules={[{ required: true }]}>
            <MarkdownEditor
              value={comment}
              placeholder='Add a comment...'
              style={{ minHeight: '200px', width: '100%' }}
              onChange={(text) => setComment(text)}
            />
          </Form.Item>
          <div className='pt-0 inline-flex items-center comment-form-controls'>
            <Button type='primary' htmlType='submit' className='mr-1' loading={isSubmitting}>
              Post
            </Button>
            <Button onClick={handleCancel}>Cancel</Button>
          </div>
        </Form>
      ) : (
        <div onClick={() => setVisible(true)}>
          <input placeholder='Add a comment...' />
        </div>
      )}
    </div>
  )
}

export default TicketCommentForm
