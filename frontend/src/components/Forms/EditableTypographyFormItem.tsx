import React, { FC, ReactElement } from 'react'
import { Form, FormInstance, FormItemProps } from 'antd'

export interface EditableTypographyFormItemProps extends FormItemProps {
  children: ReactElement
  name: string
  form: FormInstance
}

/* Child is expected to be a Typography, or understand 'editable' prop
 * - 'name' is the associated form field
 */
const EditableTypographyFormItem: FC<EditableTypographyFormItemProps> = (props) => {
  const { children, form, name, ...itemProps } = props
  return (
    <Form.Item {...itemProps} name={name} style={{ margin: 0, padding: 0 }}>
      {React.cloneElement(children, {
        className: 'hover:bg-slate-200 !focus-visible:bg-inherit focus-within:bg-inherit',
        editable: {
          autosize: false,
          triggerType: ['text', 'icon'],
          onChange: async (newValue) => {
            if (newValue === form.getFieldValue(name)) return
            console.debug(`change ${form.getFieldValue(name)} => ${newValue}`)
            try {
              form.setFieldsValue({ [name]: newValue })
              await form.validateFields()
            } catch {}
          },
        },
      })}
    </Form.Item>
  )
}

export default EditableTypographyFormItem
