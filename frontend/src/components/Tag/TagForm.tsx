import { useMemoizedFn } from 'ahooks'
import { Button, Form, FormProps, Input, notification, Select } from 'antd'
import classnames from 'classnames'
import React, { FC } from 'react'
import { Tag, useCreateTagMutation } from '../../store'

export interface TagFormProps extends FormProps {
  types: string[]
  className?: string
}

const TagForm: FC<TagFormProps> = (props) => {
  const { types, className, ...formProps } = props
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [createTag] = useCreateTagMutation()
  const [form] = Form.useForm<Tag>()

  const handleSubmit = useMemoizedFn(async () => {
    setIsSubmitting(true)
    try {
      const vals = await form.validateFields()
      await createTag(vals).unwrap()
    } catch (err) {
      notification.error({
        message: 'Failed to create tag',
        description: err?.data?.message,
      })
    } finally {
      setIsSubmitting(false)
    }
  })

  return (
    <Form
      title='New Tag'
      className={classnames('!pt-2', className)}
      form={form}
      layout='inline'
      onFinish={handleSubmit}
      {...formProps}
    >
      <Form.Item name='name' label='New tag' rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item name='type' label='Type' rules={[{ required: true }]} className='!min-w-[90px]'>
        <Select
          className='min-w-[90px]'
          options={types?.map((typ) => ({ label: typ, value: typ }))}
        />
      </Form.Item>
      <Button htmlType='submit' type='primary' loading={isSubmitting}>
        Create
      </Button>
    </Form>
  )
}

export default TagForm
