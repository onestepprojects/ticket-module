import React, { FC } from 'react'
import { Affix, Button, Card, Col, List, Popconfirm, Row, Switch, Tag } from 'antd'
import { selectTagsData, useAppSelector, useDeleteTagMutation, useGetTagsQuery } from '../../store'
import { capitalize, isEmpty } from 'lodash-es'
import TagSearch from './TagSearch'
import { groupTags, tagTypes } from '../../util'
import { CheckOutlined, CloseOutlined, PlusOutlined } from '@ant-design/icons'
import TagForm from './TagForm'

export interface TagsCardProps {}

const colors = ['volcano', 'green', 'geekblue', 'purple', 'orange', 'red', 'cyan']

const TagsCard: FC<TagsCardProps> = (_props) => {
  const { data: _, isLoading } = useGetTagsQuery()
  const [deleteTag] = useDeleteTagMutation()
  const tagsData = useAppSelector((state) => selectTagsData(state))
  const [tags, setTags] = React.useState([])
  const [types, setTypes] = React.useState([])
  const [grouped, setGrouped] = React.useState(true)
  const [formVisible, setFormVisible] = React.useState(false)
  const $ref = React.useRef(null)

  React.useEffect(() => {
    if (!tagsData) return
    setTypes(tagTypes(tagsData))
    setTags(tagsData.ids.map((id) => tagsData.entities[id]))
  }, [tagsData])

  const TicketTag: FC<{ name: string; id: number; type: string }> = ({ name, id, type }) => (
    <Popconfirm
      title={`Permanently delete tag ${name}?`}
      onConfirm={() => deleteTag(id)}
      icon={<CloseOutlined />}
    >
      <Tag key={id} color={colors[types.indexOf(type)]}>
        <span>
          {name} <CloseOutlined className='!text-slate-400 cursor-pointer' />
        </span>
      </Tag>
    </Popconfirm>
  )

  if (isLoading) return null

  return (
    <Card title='Manage Tags'>
      <Row className='flex !justify-between'>
        <Affix ref={$ref} offsetTop={30}>
          <div className='inline-flex'>
            <TagSearch tags={tagsData} setResult={setTags} />
            <div className='pl-1'>
              <Button
                type='dashed'
                icon={<PlusOutlined />}
                onClick={() => setFormVisible(!formVisible)}
              >
                New Tag
              </Button>
            </div>
          </div>
        </Affix>

        <div>
          <span className='mr-3'>Group by type</span>
          <Switch
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
            checked={grouped}
            onChange={() => setGrouped(!grouped)}
            size='default'
          />
        </div>
      </Row>

      {formVisible && <TagForm types={types} />}

      <Row>
        {grouped ? (
          types?.map((typ) => {
            const groups = isEmpty(tags) ? undefined : groupTags(tags)
            return (
              <Col key={typ} flex={typ.length ?? 1}>
                <List
                  className='!pt-1'
                  header={capitalize(typ)}
                  dataSource={groups[typ] ?? []}
                  itemLayout='horizontal'
                  renderItem={(tag) => <TicketTag key={tag.id} {...tag} />}
                />
              </Col>
            )
          })
        ) : (
          <div className='pt-5'>
            {tags.map((tag) => (
              <TicketTag key={tag.id} {...tag} />
            ))}
          </div>
        )}
      </Row>
    </Card>
  )
}

export default TagsCard
