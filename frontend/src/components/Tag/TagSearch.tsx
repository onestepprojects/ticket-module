import React, { FC } from 'react'
import { EntityState } from '@reduxjs/toolkit'
import { AutoComplete } from 'antd'
import { lowerCase, debounce, isEmpty } from 'lodash-es'
import { Tag } from '../../store'

export interface TagSearchProps {
  tags: EntityState<Tag>
  setResult: React.Dispatch<React.SetStateAction<Tag[]>>
}

export type TagOption = { label: string; value: string }

const TagSearch: FC<TagSearchProps> = (props) => {
  const { tags, setResult } = props
  const [value, setValue] = React.useState('')
  const initialOpts: TagOption[] = tags?.ids.map((id) => ({
    label: lowerCase(tags.entities[id].name),
    value: String(id),
  }))
  const allTags = initialOpts.map(({ value }) => tags.entities[Number(value)])
  const [options, setOptions] = React.useState(initialOpts)

  const onSelect = (_data: string, option: TagOption) => {
    setValue(option.label)
    setResult([tags.entities[Number(option.value)]])
  }

  const onChange = (text: string, _option: TagOption) => {
    setValue(text.trimStart())
  }

  const handleSearch = (search: string) => {
    const text = search.trim()
    if (isEmpty(text)) {
      setOptions(initialOpts)
      setResult(allTags)
    } else {
      /* console.debug("DEBUG: searching tags:", text); */
      const opts = (
        !(text.startsWith(value) && text.length > value.length) ? initialOpts : options
      ).filter((opt) => opt.label.indexOf(text) !== -1)
      if (opts.length !== options.length) {
        setOptions(opts)
        setResult(opts.map(({ value }) => tags.entities[Number(value)]))
      }
    }
  }
  const onSearch = debounce(handleSearch, 300)

  if (!tags) return null
  return (
    <AutoComplete
      value={value}
      options={options}
      style={{ width: 200 }}
      placeholder='search tags...'
      onChange={onChange}
      onSelect={onSelect}
      onSearch={onSearch}
    />
  )
}

export default React.memo(TagSearch)
