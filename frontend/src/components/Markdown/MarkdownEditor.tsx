import React, { ForwardRefExoticComponent } from 'react'
import MdEditor, { MDEditorProps } from '@uiw/react-md-editor'
import classnames from 'classnames'
import remarkGfm from 'remark-gfm'
import rehypeSanitize from 'rehype-sanitize'

export interface MarkdownEditorProps extends MDEditorProps {
  className?: string
  placeholder?: string
}

const MarkdownEditor: ForwardRefExoticComponent<MarkdownEditorProps> = React.forwardRef(
  (props: MarkdownEditorProps, ref) => {
    const { className, placeholder, ...editorProps } = props
    return (
      <MdEditor
        ref={ref}
        className={classnames('markdown-editor', className)}
        autoFocus
        previewOptions={{
          rehypePlugins: [[rehypeSanitize]],
          remarkPlugins: [[remarkGfm]],
        }}
        textareaProps={{ placeholder }}
        preview='edit'
        {...editorProps}
      />
    )
  }
)

export default MarkdownEditor
