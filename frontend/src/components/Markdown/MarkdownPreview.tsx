import React, { FC } from 'react'
import MDEditor from '@uiw/react-md-editor'
import rehypeSanitize from 'rehype-sanitize'
import remarkGfm from 'remark-gfm'

interface MarkdownPreviewProps {
  data
}

export const MarkdownPreview: FC<MarkdownPreviewProps> = (props) => {
  const { data } = props
  return (
    <MDEditor.Markdown
      source={data}
      rehypePlugins={[[rehypeSanitize]]}
      remarkPlugins={[[remarkGfm]]}
    />
  )
}

export default MarkdownPreview
