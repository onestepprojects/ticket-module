import { EntityState } from '@reduxjs/toolkit'
import { groupBy } from 'lodash-es'
import { Tag } from '../store'

/** Unique tag types from entity state */
export const tagTypes = (tagsData: EntityState<Tag>): string[] => {
  const hash = {}
  Object.values(tagsData.entities).forEach(({ type }) => (hash[type] = 1))
  return Object.keys(hash)
}

export type GroupedTags = { [key: string]: Tag[] }

/** Group tags by type */
export const groupTags = (tags: Tag[]): GroupedTags => {
  return groupBy(tags, (x) => x?.type)
}
