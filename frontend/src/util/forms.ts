import { FormInstance } from 'antd'

// validate and return only dirty/touched fields in form
export const dirtyFields = async (form: FormInstance) =>
  form.validateFields(Object.keys(form.getFieldsValue()).filter(form.isFieldTouched))
