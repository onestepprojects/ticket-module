import { SearchTicketsFormInput, TicketPriority, TicketStatus, TicketVisibility } from '../store'

// dont send query parameters if all options are selected
// since that is default anyway
const processEnum = (cur: string[] | undefined, vals: readonly string[]) =>
  cur && cur.length && cur.length !== vals.length ? cur : undefined

/** Convert form inputs into those expected by query */
export const processSearchForm = (values: SearchTicketsFormInput) => {
  const {
    educator = [],
    physician = [],
    speciality: spec,
    isAssigned,
    literal,
    caseSensitive,
    status,
    visibility,
    priority,
    createdBefore,
    createdAfter,
    ...rest
  } = values

  const speciality = educator.concat(physician).concat(spec ?? [])
  return {
    ...rest,
    isAssigned: isAssigned === 'all' ? undefined : isAssigned,
    speciality: speciality.length ? speciality : undefined,
    literal: literal || undefined,
    caseSensitive: (literal && caseSensitive) || undefined,
    status: processEnum(status, TicketStatus),
    visibility: processEnum(visibility, TicketVisibility),
    priority: processEnum(priority, TicketPriority),
    createdBefore: (createdBefore && String(createdBefore)) || undefined,
    createdAfter: (createdAfter && String(createdAfter)) || undefined,
  }
}
