// eslint-disable-next-line
export const groupBy = function <T>(arr: T[], fn: string | ((x: T) => any)): object {
  return arr.map(typeof fn === 'function' ? fn : (val) => val[fn]).reduce((acc, val, i) => {
    acc[val] = (acc[val] || []).concat(arr[i])
    return acc
  }, {})
}
