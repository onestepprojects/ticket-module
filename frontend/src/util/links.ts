export const parseReferral = (link?: string) => {
  if (!link || link.length === 0 || link.indexOf('http://') === 0 || link.indexOf('https://') === 0)
    return link

  if (link.startsWith('/')) link = link.slice(1)
  return `${window.origin}/${link}`
}
