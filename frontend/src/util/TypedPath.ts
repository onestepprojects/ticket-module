// @onestepprojects/ogranization-module/frontend/src/utils/TypedPath.ts
type Cons<H, T> = T extends readonly any[] // eslint-disable-line
  ? ((h: H, ...t: T) => void) extends (...r: infer R) => void
    ? R
    : never
  : never

type Prev = [
  never,
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  // https://stackoverflow.com/questions/64356569/how-to-understand-0-1-2-0-in-type-definition-in-typescript
  ...0[]
]

/**
 * Get nested paths of an object in a typed-safe way.
 *
 *     Type User = {
 *       name: string;
 *       address: {
 *         home: string
 *       }
 *     }
 *
 *     Const paths: Paths<User> = ['name'] // correct
 *     const paths: Paths<User> = ['address', 'home'] // correct
 *     const paths: Paths<User> = ['address', 'home2'] // compilation error
 */
export type Paths<T, D extends number = 10> = [D] extends [never]
  ? never
  : T extends object
  ? {
      [K in keyof T]-?:
        | [K]
        | (Paths<T[K], Prev[D]> extends infer P ? (P extends [] ? never : Cons<K, P>) : never)
    }[keyof T]
  : []
