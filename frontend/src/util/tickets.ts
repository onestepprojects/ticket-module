import { EntityState } from '@reduxjs/toolkit'
import { groupTags } from '.'
import { Tag, Ticket, TicketEditFormData, TicketFormData, TicketUpdateRelation } from '../store'

export const cleanQuery = (obj: object) =>
  // eslint-disable-next-line
  Object.fromEntries(Object.entries(obj).filter(([_, v]) => v != undefined && v != null))

/**
 * Merge tag inputs to those array of tag ids
 */
const processTags = (values: Partial<TicketFormData>): Partial<TicketFormData> => {
  const { tagIds, topics, specialities, educators, physicians, ...data } = values
  return {
    ...data,
    tagIds: [tagIds, topics, specialities, educators, physicians].reduce(
      (acc, x) => (x ? acc.concat(x) : acc),
      []
    ),
  }
}

/**
 * Convert create ticket form inputs into those expected by query
 */
export const processCreateTicketForm = (
  values: Partial<TicketFormData>
): Partial<TicketFormData> => {
  return processTags(values)
}

/**
 * Group relations to update by those to add/remove
 */
function groupRelation<T>(newVals: T[], oldVals: T[]): TicketUpdateRelation<T> | undefined {
  if (!newVals) return undefined
  const add = newVals.filter((x) => oldVals.indexOf(x) === -1)
  const remove = oldVals.filter((x) => newVals.indexOf(x) === -1)
  return add.length === 0 && remove.length === 0
    ? undefined
    : {
        add,
        remove,
      }
}

/**
 * Convert edit ticket form inputs into those expected by query
 * Determine which relations need to be connected/disconnected
 */
export const processEditTicketForm = (
  values: Partial<TicketFormData>,
  ticket: Ticket
): Partial<TicketEditFormData> => {
  const { tagIds, linkedTo, organizationIds, ...data } = processTags(values)
  // const { tagIds: allTags } = processTags(values);
  // const oldVals = ticket?.tags?.filter(id => allTags.indexOf(id))

  return cleanQuery({
    ...data,
    tagIds: groupRelation<number>(tagIds, ticket?.tags ?? []),
    organizationIds: groupRelation<string>(organizationIds, ticket?.organizations ?? []),
    linkedTo: groupRelation<number>(linkedTo, ticket?.linkedTo ?? []),
  })
}

/**
 * Convert ticket object to ticket form data
 */
export const ticketToFormData = (
  ticket: Ticket,
  allTags: EntityState<Tag>
): Partial<TicketFormData> => {
  const { tags: tagIds, organizations, ...rest } = ticket
  // Split tags into types: educator, physician, topic, etc.
  const tags = tagIds?.map((id) => allTags.entities[id])
  const groupedTags = groupTags(tags)
  return {
    ...rest,
    ...Object.entries(groupedTags).reduce(
      (acc, [k, v]) => ({
        ...acc,
        [k + 's']: v.map(({ id }) => id),
      }),
      {}
    ),
    organizationIds: organizations,
  }
}
