import { createSelector, createSlice } from '@reduxjs/toolkit'
import { assignmentApi, subscriptionApi, userApi } from '..'
import type { User } from '../types'

type HashedIds = { [id: number | string]: string | number | boolean }
export interface UserState extends Omit<User, 'password'> {
  subscriptions?: HashedIds
  watchedOrgs?: HashedIds
  watchedTags?: HashedIds
  ticketsFor?: HashedIds
  ticketsBy?: HashedIds
  activity?: HashedIds
}

const hashFromObjects = (arr?: { [id: string]: number | string }[]) =>
  arr?.reduce((acc, { id }) => ({ ...acc, [id]: true }), {})

const hashFromArray = (arr: number[]) => arr.reduce((acc, id) => ({ ...acc, [id]: true }), {})

const initialState: UserState = {
  id: null,
  isAdmin: false,
}

export const userSlice = createSlice({
  name: 'user',
  initialState,

  reducers: {
    unsetUser: (state, _action) => {
      state = initialState // eslint-disable-line
    },
  },

  extraReducers: (builder) => {
    builder.addMatcher(userApi.endpoints.getUser.matchFulfilled, (state, { payload }) => {
      const { subscriptions, watchedOrgs, watchedTags, ticketsFor, ticketsBy, activity, ...user } =
        payload
      state.id = user.id
      state.isAdmin = user.isAdmin
      state.subscriptions = hashFromObjects(subscriptions)
      state.watchedOrgs = hashFromObjects(watchedOrgs)
      state.watchedTags = hashFromObjects(watchedTags)
      state.ticketsFor = hashFromObjects(ticketsFor)
      state.ticketsBy = hashFromObjects(ticketsBy)
      state.activity = hashFromObjects(activity)
    })

    // Subscriptions
    builder.addMatcher(
      subscriptionApi.endpoints.getSubscriptions.matchFulfilled,
      (state, { payload }) => {
        state.subscriptions = hashFromArray(payload.ticketIds)
      }
    )
    builder.addMatcher(
      subscriptionApi.endpoints.subscribeToTickets.matchFulfilled,
      (state, { payload }) => {
        state.subscriptions = hashFromArray(payload.ticketIds)
      }
    )
    builder.addMatcher(
      subscriptionApi.endpoints.unsubscribeFromTickets.matchFulfilled,
      (state, { payload }) => {
        state.subscriptions = hashFromArray(payload.ticketIds)
      }
    )

    // Assignments
    builder.addMatcher(
      assignmentApi.endpoints.assignTickets.matchFulfilled,
      (state, { payload }) => {
        state.ticketsFor = hashFromArray(payload.ticketIds)
      }
    )
    builder.addMatcher(
      assignmentApi.endpoints.unassignTickets.matchFulfilled,
      (state, { payload }) => {
        state.ticketsFor = hashFromArray(payload.ticketIds)
      }
    )
  },
})

export const selectIsAssigned = createSelector(
  [(state) => state.user?.ticketsFor, (state, ticketId) => ticketId],
  (ticketsFor, ticketId: number) => {
    // console.debug('DEBUG: selectIsAssigned')
    return ticketsFor[ticketId] === true
  }
)
