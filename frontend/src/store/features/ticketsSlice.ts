// import { createSlice, type PayloadAction } from '@reduxjs/toolkit'
// import type { Person } from '../types'
// import { ticketApi } from '../services/ticketApi';
// import { RootState } from '../store';

// interface AuthState {
//   token?: string
//   person?: Person
//   roles?: string[]
//   isAdmin?: boolean,
//   userId?: number,
// }

// const initialState: AuthState = {}

// export const authSlice = createSlice({
//   name: 'auth',
//   initialState,
//   reducers: {
//     setAuth: (state, action: PayloadAction<Pick<AuthState, 'token' | 'roles' | 'person'>>) => {
//       const { token, person, roles } = action.payload

//       // TODO: Consider remove this. RTK query read token from redux.
//       if (token) {
//         localStorage.setItem('authtoken', token)
//       }

//       state.token = token
//       state.roles = roles
//       state.person = person

//       state.isAdmin = roles?.includes('admin')
//     },
//     // XXX: better place to do this??? remove probably in favor of extraReducer
//     setUserId: (state, action: PayloadAction<Pick<AuthState, 'userId'>>) => {
//       const { userId } = action.payload
//       state.userId = userId;
//     }
//   },
//   // Update userId in auth state after api call complets
//   extraReducers: (builder) => {
//     builder.addMatcher(
//       ticketApi.endpoints.loginOrCreateUser.matchFulfilled,
//       (state, { payload }) => {
//         console.log('extraReducer:');
//         console.dir(state)
//         console.dir(payload.data)
//         state.userId = payload.data.id
//       }
//     )
//   }
// })
// export const currentUser = (state: RootState) => state.auth.userId;
