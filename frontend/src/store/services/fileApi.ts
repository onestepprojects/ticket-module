import type { UserFile, TicketFile } from '../types'
import { api, moduleUrl } from './api'

export const fileApi = api.injectEndpoints({
  overrideExisting: true,

  // XXX: not doing anything with files atm
  endpoints: (builder) => ({
    getUserFiles: builder.query<UserFile[], void>({
      query: () => ({
        url: moduleUrl(`files`),
        method: 'GET',
      }),
    }),

    getTicketFiles: builder.query<TicketFile[], void>({
      query: (ticketId) => ({
        url: moduleUrl(`tickets/${ticketId}/files`),
        method: 'GET',
      }),
    }),

    deleteFile: builder.mutation<TicketFile, number>({
      query: (id) => ({
        url: moduleUrl(`files/${id}`),
        method: 'DELETE',
      }),
    }),

    uploadFile: builder.mutation<UserFile[], Partial<UserFile>>({
      query: (body) => ({
        url: moduleUrl(`files`),
        method: 'POST',
        body,
      }),
    }),
  }),
})

export const {
  useGetUserFilesQuery,
  useGetTicketFilesQuery,
  useDeleteFileMutation,
  useUploadFileMutation,
} = fileApi
