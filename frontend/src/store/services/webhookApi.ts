import { Webhook } from '..'
import { api, moduleUrl } from './api'

export const webhookApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getWebooks: builder.query<Webhook, void>({
      query: () => ({
        url: moduleUrl(`webhooks`),
        method: 'GET',
      }),
      providesTags: ['Webhooks'],
    }),

    createWebhook: builder.mutation<Webhook, Partial<Webhook>>({
      query: (body) => ({
        url: moduleUrl('webhooks'),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Webhooks'],
    }),

    updateWebhook: builder.mutation<Webhook, Partial<Webhook>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`webhooks/${id}`),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Webhooks'],
    }),

    deleteWebhook: builder.mutation<Webhook, number>({
      query: (id) => ({
        url: moduleUrl(`webhooks/${id}`),
        method: 'DELETE',
      }),
      invalidatesTags: ['Webhooks'],
    }),
  }),
})

export const {
  useGetWebooksQuery,
  useDeleteWebhookMutation,
  useCreateWebhookMutation,
  useUpdateWebhookMutation,
} = webhookApi
