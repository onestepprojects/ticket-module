import { createEntityAdapter, createSelector, EntityState } from '@reduxjs/toolkit'
import { RootState } from '..'
import {
  Ticket,
  TicketFormData,
  TicketResponse,
  LinkTicketQuery,
  GroupByQuery,
  SelectManyQuery,
  UpdateTicketStatusQuery,
  TicketStatus,
  TicketEditFormData,
} from '../types'
import { groupByQuery, api, moduleUrl } from './api'

const ticketsAdapter = createEntityAdapter<Ticket>()
const initialState = ticketsAdapter.getInitialState()

export const ticketApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    createTicket: builder.mutation<TicketResponse, TicketFormData>({
      query: (body) => ({
        url: moduleUrl('tickets'),
        method: 'POST',
        body,
      }),
      invalidatesTags: [
        'Ticket',
        'UserHistory',
        'Subscription',
        // XXX: remove when done testing
        'Notification',
        { type: 'Ticket' as const, id: 'OPEN' },
      ],
    }),

    updateTicket: builder.mutation<TicketResponse, Partial<TicketEditFormData>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`tickets/${id}`),
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result, error, arg) => [
        'UserHistory',
        // XXX: remove when done testing
        'Notification',
        { type: 'Ticket', id: arg.id },
      ],
    }),

    updateTicketStatus: builder.mutation<TicketResponse, UpdateTicketStatusQuery>({
      query: ({ ticketId, ...body }) => ({
        url: moduleUrl(`tickets/${ticketId}/status`),
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result, error, arg) => [
        'UserHistory',
        // XXX: remove when done testing
        'Notification',
        { type: 'Ticket', id: arg.ticketId },
      ],
    }),

    // Link ticket to other tickets
    linkTickets: builder.mutation<TicketResponse, LinkTicketQuery>({
      query: ({ id, ...ids }) => ({
        url: moduleUrl(`tickets/${id}/link`),
        method: 'POST',
        body: ids,
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'Ticket', id: arg.id },
        ...arg.ids.map((id) => ({ type: 'Ticket' as const, id })),
      ],
    }),

    getTickets: builder.query<EntityState<Ticket>, void>({
      query: () => ({
        url: moduleUrl('tickets'),
        method: 'GET',
      }),
      providesTags: ['Ticket', { type: 'Ticket' as const, id: 'LIST' }],
      transformResponse: (response: Ticket[]) => {
        return ticketsAdapter.setAll(initialState, response)
      },
    }),

    getTicketById: builder.query<Ticket, number>({
      query: (id) => ({
        url: moduleUrl(`tickets/${id}`),
        method: 'GET',
      }),
      providesTags: (result, error, arg) => [{ type: 'Ticket' as const, id: Number(arg) }],
    }),

    getOpenTickets: builder.query<Ticket[], void>({
      query: () => ({
        url: moduleUrl('tickets/open'),
        method: 'GET',
      }),
      providesTags: (result = []) => [
        { type: 'Ticket' as const, id: 'LIST' },
        { type: 'Ticket' as const, id: 'OPEN' },
        ...result.map(({ id }) => ({ type: 'Ticket' as const, id })),
      ],
    }),

    getCompletedTickets: builder.query<Ticket[], SelectManyQuery | null>({
      query: (params) => ({
        url: moduleUrl('tickets/completed'),
        method: 'GET',
        params,
      }),
      providesTags: (result = []) => [
        { type: 'Ticket' as const, id: 'LIST' },
        { type: 'Ticket' as const, id: 'COMPLETED' },
        ...result.map(({ id }) => ({ type: 'Ticket' as const, id })),
      ],
    }),

    getTicketsCreatedByUser: builder.query<Ticket[], SelectManyQuery | null>({
      query: (params) => ({
        url: moduleUrl(`tickets/opened`),
        method: 'GET',
        params,
      }),
      providesTags: (result = []) => [...result.map(({ id }) => ({ type: 'Ticket' as const, id }))],
    }),

    getTicketsAggregate: builder.query<object[], GroupByQuery>({
      query: (params) => ({
        url: moduleUrl(`aggregate`),
        method: 'GET',
        params: groupByQuery(params),
      }),
      providesTags: ['Ticket', { type: 'Ticket' as const, id: 'LIST' }],
    }),

    getTicketsByAssigned: builder.query<object, object | null>({
      query: (params?) => ({
        url: moduleUrl('aggregate/by-assigned'),
        method: 'GET',
        params,
      }),
      providesTags: ['Assignment', 'Ticket', { type: 'Ticket' as const, id: 'LIST' }],
    }),
  }),
})

export const {
  useUpdateTicketMutation,
  useUpdateTicketStatusMutation,
  useLinkTicketsMutation,
  useGetTicketsQuery,
  useLazyGetTicketsQuery,
  useGetTicketByIdQuery,
  useGetOpenTicketsQuery,
  useCreateTicketMutation,
  useGetTicketsAggregateQuery,
  useGetTicketsByAssignedQuery,
  useGetTicketsCreatedByUserQuery,
} = ticketApi

export const selectTicketsResult = ticketApi.endpoints.getTickets.select()

const selectTicketsData = createSelector(selectTicketsResult, (ticketsResult) => ticketsResult.data)

export const {
  selectAll: selectAllTickets,
  // Note: this data doesn't have as detailed data as useGetTicketByIdQuery
  selectById: selectTicketById,
  selectEntities: selectTickets,
} = ticketsAdapter.getSelectors<RootState>((state) => selectTicketsData(state) ?? initialState)

// filter tickets by status
export const ticketsByStatus = (status: keyof typeof TicketStatus, tickets?: Ticket[]) => {
  if (!tickets) return []
  return tickets.filter((ticket) => ticket.status === status)
}
