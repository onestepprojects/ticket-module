import { createSelector } from '@reduxjs/toolkit'
import { RootState } from '..'
import { api, moduleUrl } from './api'

interface SubscriptionData {
  ticketIds: number[]
}
interface SubscriptionResponse {
  ticketIds: number[]
}

export const subscriptionApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getSubscriptions: builder.query<SubscriptionResponse, void>({
      query: () => ({
        url: moduleUrl(`subscriptions`),
        method: 'GET',
      }),
      providesTags: ['Subscription'],
    }),

    subscribeToTickets: builder.mutation<SubscriptionResponse, SubscriptionData>({
      query: (body) => ({
        url: moduleUrl(`subscriptions`),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Subscription'],
    }),

    unsubscribeFromTickets: builder.mutation<SubscriptionResponse, SubscriptionData>({
      query: (body) => ({
        url: moduleUrl(`subscriptions`),
        method: 'DELETE',
        body,
      }),
      invalidatesTags: ['Subscription'],
    }),
  }),
})

export const {
  useUnsubscribeFromTicketsMutation,
  useSubscribeToTicketsMutation,
  useGetSubscriptionsQuery,
} = subscriptionApi

export const selectSubscriptionsResult = subscriptionApi.endpoints.getSubscriptions.select()

const selectSubscriptionsData = createSelector(
  selectSubscriptionsResult,
  (subsResult) => subsResult.data
)

export const selectIsSubscribed = createSelector(
  [(state: RootState) => selectSubscriptionsData(state), (state, ticketId) => ticketId],
  (subscriptions, ticketId: number) => {
    // console.debug('DEBUG: selectIsSubscribed')
    return subscriptions?.ticketIds.indexOf(ticketId) !== -1
  }
)
