import { ChangeLogEntry, ChangeLogQuery } from '..'
import { api, moduleUrl } from './api'

export const changeLogApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getTicketChangeLog: builder.query<ChangeLogEntry[], ChangeLogQuery>({
      query: ({ ticketId, ...params }) => ({
        url: moduleUrl(`tickets/${ticketId}/changelog`),
        method: 'GET',
        params,
      }),
      providesTags: (result, error, arg) => [{ type: 'Ticket', id: arg.ticketId }],
    }),
  }),
})

export const { useGetTicketChangeLogQuery } = changeLogApi
