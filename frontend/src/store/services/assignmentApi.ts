import { SelectManyQuery, TicketResponse } from '..'
import { api, moduleUrl } from './api'

interface AssignmentData {
  ticketIds: number[]
}
interface AssignmentResponse {
  ticketIds: number[]
}

export const assignmentApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getAssignedTickets: builder.query<TicketResponse[], SelectManyQuery>({
      query: (params) => ({
        url: moduleUrl(`assignments`),
        method: 'GET',
        params,
      }),
      providesTags: (result = []) => [
        'Assignment',
        ...result.map(({ id }) => ({ type: 'Ticket' as const, id })),
      ],
    }),
    getAssignedTicketsIds: builder.query<TicketResponse[], void>({
      query: () => ({
        url: moduleUrl('assignments'),
        method: 'GET',
        params: { select: 'id' },
      }),
      providesTags: ['Assignment'],
    }),

    assignTickets: builder.mutation<AssignmentResponse, AssignmentData>({
      query: (body) => ({
        url: moduleUrl('assignments'),
        method: 'POST',
        body,
      }),
      invalidatesTags: (result, error, arg) => [
        'Assignment',
        'Subscription',
        // XXX: remove when done testing
        'Notification',
        ...arg.ticketIds.map((id) => ({ type: 'Ticket' as const, id })),
      ],
    }),

    unassignTickets: builder.mutation<AssignmentResponse, AssignmentData>({
      query: (body) => ({
        url: moduleUrl('assignments'),
        method: 'DELETE',
        body,
      }),
      invalidatesTags: (result, error, arg) => [
        'Assignment',
        'Subscription',
        // XXX: remove when done testing
        'Notification',
        ...arg.ticketIds.map((id) => ({ type: 'Ticket' as const, id })),
      ],
    }),
  }),
})

export const {
  useGetAssignedTicketsIdsQuery,
  useGetAssignedTicketsQuery,
  useAssignTicketsMutation,
  useUnassignTicketsMutation,
} = assignmentApi
