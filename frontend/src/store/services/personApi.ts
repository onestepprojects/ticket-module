import { createEntityAdapter, createSelector, EntityState } from '@reduxjs/toolkit'
import { RootState } from '..'
import type { Person } from '../types'
import { api, moduleUrl } from './api'

const personsAdapter = createEntityAdapter<Person>({
  selectId: (person) => person.uuid,
})

const initialState = personsAdapter.getInitialState()

export const personApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getPersons: builder.query<EntityState<Person>, void>({
      query: () => ({
        url: moduleUrl('persons', 'organization'),
        method: 'GET',
      }),
      transformResponse: (res: Person[]) => {
        return personsAdapter.setAll(initialState, res)
      },
    }),

    getPersonsOfOrganization: builder.query<EntityState<Person>, string>({
      query: (orgId) => ({
        url: moduleUrl(`persons/get-by-parent/${orgId}`, 'organization'),
        method: 'GET',
      }),
      transformResponse: (res: { data }) => res.data,
    }),
  }),
})

export const { useGetPersonsOfOrganizationQuery, useGetPersonsQuery } = personApi

export const selectPersonsResult = personApi.endpoints.getPersons.select()

const selectPersonsData = createSelector(selectPersonsResult, (personResult) => personResult.data)

export const {
  selectAll: selectAllPersons,
  selectById: selectPersonById,
  selectEntities: selectPersons,
} = personsAdapter.getSelectors<RootState>((state) => selectPersonsData(state) ?? initialState)

// export const selectPersonsByIds = createSelector(
//   selectPersons,
//   (persons, ids) => ids,
//   (persons, ids) => ids.map(id => persons[id]),
// )
