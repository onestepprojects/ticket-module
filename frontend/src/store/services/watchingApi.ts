import { OrgWatchers, TagWatchers, WatchingResponse, WatchingUpdate } from '../types'
import { api, moduleUrl } from './api'

export const watchingApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getTagWatchers: builder.query<TagWatchers, number>({
      query: (tagId) => ({
        url: moduleUrl(`tags/${tagId}/watchers`),
        method: 'GET',
      }),
      providesTags: (result, error, arg) => [{ type: 'WatchedTag' as const, id: arg }],
    }),

    getOrgWatchers: builder.query<OrgWatchers, string>({
      query: (orgId) => ({
        url: moduleUrl(`organizations/${orgId}/watchers`),
        method: 'GET',
      }),
      providesTags: (result, error, arg) => [{ type: 'WatchedOrg' as const, id: arg }],
    }),

    updateWatched: builder.mutation<WatchingResponse, WatchingUpdate>({
      query: (body) => ({
        url: moduleUrl('watching'),
        method: 'PUT',
        body,
      }),
      invalidatesTags: ['Watching'],
    }),

    getWatched: builder.query<WatchingResponse, void>({
      query: () => ({
        url: moduleUrl('watching'),
        method: 'GET',
      }),
      providesTags: ['Watching'],
    }),
  }),
})

export const {
  useLazyGetTagWatchersQuery,
  useLazyGetOrgWatchersQuery,
  useUpdateWatchedMutation,
  useGetWatchedQuery,
} = watchingApi
