import { RawSQLQuery } from '..'
import { api, moduleUrl } from './api'

export const adminApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getSQLRaw: builder.query<object, RawSQLQuery>({
      query: (params) => ({
        url: moduleUrl('admin/sql/query'),
        method: 'GET',
        params,
      }),
    }),

    getSQLTables: builder.query<{ pivots: string[]; models: string[] }, void>({
      query: () => ({
        url: moduleUrl('admin/sql/tables'),
        method: 'GET',
      }),
    }),
  }),
})

export const { useLazyGetSQLRawQuery, useGetSQLTablesQuery } = adminApi
