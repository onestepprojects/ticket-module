import { createEntityAdapter, createSelector, EntityState } from '@reduxjs/toolkit'
// import { lowerCase } from 'lodash-es'
import { RootState } from '..'
import type { Tag } from '../types'
import { api, moduleUrl } from './api'

const tagsAdapter = createEntityAdapter<Tag>()

const initialState = tagsAdapter.getInitialState()

export const tagApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getTags: builder.query<EntityState<Tag>, void>({
      query: () => ({
        url: moduleUrl('tags'),
        method: 'GET',
      }),
      providesTags: ['Tag'],
      transformResponse: (res: Tag[]) => {
        // res.forEach((tag, idx, arr) => {
        //   arr[idx] = { ...tag, value: tag.id, label: lowerCase(tag.name) }
        // })
        return tagsAdapter.setAll(initialState, res)
      },
    }),
    createTag: builder.mutation<Tag, Partial<Tag>>({
      query: (body) => ({
        url: moduleUrl('tags'),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Tag'],
    }),
    deleteTag: builder.mutation<Tag, number>({
      query: (id) => ({
        url: moduleUrl(`tags/${id}`),
        method: 'DELETE',
      }),
      invalidatesTags: ['Tag'],
    }),
    updateTag: builder.mutation<Tag, Partial<Tag>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`tags/${id}`),
        method: 'PUT',
        body,
      }),
      invalidatesTags: ['Tag'],
    }),
    getTagById: builder.query<Tag, string>({
      query: (id) => ({
        url: moduleUrl(`tags/${id}`),
        method: 'GET',
      }),
      providesTags: ['Tag'],
    }),
  }),
})

export const {
  useGetTagsQuery,
  useCreateTagMutation,
  useDeleteTagMutation,
  useUpdateTagMutation,
  useGetTagByIdQuery,
} = tagApi

export const selectTagsResult = tagApi.endpoints.getTags.select()

export const selectTagsData = createSelector(selectTagsResult, (tagsResult) => tagsResult.data)

export const {
  selectAll: selectAllTags,
  selectById: selectTagById,
  selectEntities: selectTags,
} = tagsAdapter.getSelectors<RootState>((state) => selectTagsData(state) ?? initialState)
