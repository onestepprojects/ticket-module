import { api, moduleUrl } from '.'
import { SearchTicketsQuery, Ticket } from '..'

export const searchApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    searchTickets: builder.query<Ticket[], SearchTicketsQuery>({
      query: (params) => ({
        url: moduleUrl('search'),
        method: 'GET',
        params,
      }),
      providesTags: ['Search'],
    }),
  }),
})

export const { useLazySearchTicketsQuery } = searchApi
