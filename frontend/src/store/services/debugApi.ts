import type { DebugSlowParams } from '../types'
import { api, moduleUrl } from './api'

export const debugApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getSlow: builder.query<object[], DebugSlowParams>({
      query: (params) => ({
        url: moduleUrl('debug/slow'),
        method: 'GET',
        params,
      }),
      providesTags: ['Debug'],
    }),
  }),
})

export const { useGetSlowQuery } = debugApi
