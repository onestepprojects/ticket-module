import type { Note } from '../types'
import { api, moduleUrl } from './api'

export const noteApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getNotes: builder.query<Note[], void>({
      query: () => ({
        url: moduleUrl('notes'),
        method: 'GET',
      }),
      providesTags: ['Note'],
    }),
    createNote: builder.mutation<Note, Partial<Note>>({
      query: (body) => ({
        url: moduleUrl('notes'),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Note'],
    }),
    deleteNote: builder.mutation<Note, number>({
      query: (id) => ({
        url: moduleUrl(`notes/${id}`),
        method: 'DELETE',
      }),
      invalidatesTags: ['Note'],
    }),
    updateNote: builder.mutation<Note, Partial<Note>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`notes/${id}`),
        method: 'PUT',
        body,
      }),
    }),
    getNoteById: builder.query<Note, string>({
      query: (id) => ({
        url: moduleUrl(`notes/${id}`),
        method: 'GET',
      }),
      providesTags: ['Note'],
    }),
  }),
})

export const {
  useGetNotesQuery,
  useCreateNoteMutation,
  useDeleteNoteMutation,
  useUpdateNoteMutation,
  useGetNoteByIdQuery,
} = noteApi
