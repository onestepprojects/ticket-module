import { createEntityAdapter, EntityState } from '@reduxjs/toolkit'
import { OrgRole, Organization, Ticket } from '../types'
import { api, moduleUrl } from './api'

const organizationsAdapter = createEntityAdapter<Organization>()
const initialState = organizationsAdapter.getInitialState()

const orgRolesAdapter = createEntityAdapter<OrgRole>()
const initialOrgRoles = orgRolesAdapter.getInitialState()

export const organizationApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getOrganizations: builder.query<EntityState<Organization>, void>({
      query: () => ({
        url: moduleUrl('organizations', 'organization'),
        method: 'GET',
      }),
      providesTags: ['Organization'],
      transformResponse: (response: Organization[]) => {
        return organizationsAdapter.setAll(initialState, response)
      },
    }),

    getOrganizationsByPerson: builder.query<Organization[], string>({
      query: (personId) => ({
        url: moduleUrl(`organizations/get-org-by-person/${personId}`, 'organization'),
        method: 'GET',
      }),
      providesTags: ['Organization'],
    }),

    getOrganizationRolesByPerson: builder.query<EntityState<OrgRole>, string>({
      query: (userId) => ({
        url: moduleUrl(`organizations/get-org-roles-by-person/${userId}`, 'organization'),
        method: 'GET',
      }),
      transformResponse: (response: OrgRole[]) => {
        return orgRolesAdapter.setAll(initialOrgRoles, response)
      },
      providesTags: ['Organization'],
    }),

    getOrganizationTickets: builder.query<Ticket[], string>({
      query: (orgId) => ({
        url: moduleUrl(`organizations/${orgId}/tickets`),
        method: 'GET',
      }),
      providesTags: (result = [], error, arg) => [
        { type: 'Ticket' as const, id: `OrgList-${arg}` },
        ...result.map(({ id }) => ({ type: 'Ticket' as const, id })),
      ],
    }),
  }),
})

export const {
  useGetOrganizationsQuery,
  useLazyGetOrganizationsQuery,
  useGetOrganizationsByPersonQuery,
  useGetOrganizationRolesByPersonQuery,
  useGetOrganizationTicketsQuery,
} = organizationApi

export const selectAllUserOrgs = (userOrgs: EntityState<OrgRole>) =>
  Object.values(userOrgs.entities)

export const selectUserOrgById = (orgId: string, userOrgs: EntityState<OrgRole>) =>
  userOrgs.entities[orgId]

// XXX: (5/3/22) organization selects no longer working since routes don't work
// export const selectOrganizationsResult = (
//   organizationApi.endpoints.getOrganizations.select()
// )

// const selectOrganizationsData = createSelector(
//   selectOrganizationsResult,
//   (organizationsResult) => organizationsResult.data
// )

// export const {
//   selectAll: selectAllOrganizations,
//   selectById: selectOrganizationById,
//   selectEntities: selectOrganizations,
// } = organizationsAdapter.getSelectors<RootState>(
//   (state) => selectOrganizationsData(state) ?? initialState
// )
