import type { FindManyNotificationsQuery, MarkNotificationBody, Notification } from '../types'
import { api, moduleUrl } from './api'

export const notificationApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getNotifications: builder.query<Notification[], FindManyNotificationsQuery>({
      query: (params) => ({
        url: moduleUrl('notifications'),
        method: 'GET',
        params,
      }),
      providesTags: (result, error, arg) => [
        'Notification',
        ...[
          Object.keys(arg).length > 0
            ? // invalidate 'Query' types, but not 'LIST' during optimistic update
              { type: 'Notification' as const, id: 'Query' }
            : { type: 'Notification' as const, id: 'LIST' },
        ],
      ],
    }),

    createNotification: builder.mutation<Notification, Partial<Notification>>({
      query: (body) => ({
        url: moduleUrl('notifications'),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Notification'],
    }),

    markNotifications: builder.mutation<Notification, MarkNotificationBody>({
      query: (body) => ({
        url: moduleUrl('notifications/mark'),
        method: 'PUT',
        body,
      }),
      // don't invalidate 'LIST'
      invalidatesTags: [{ type: 'Notification' as const, id: 'Query' }],
      // optimistic update to make page reponsive
      async onQueryStarted(body, { dispatch, queryFulfilled }) {
        const { read, unread } = body
        const patchResult = dispatch(
          notificationApi.util.updateQueryData('getNotifications', {}, (draft) => {
            if (read === 'all') draft.forEach((notif) => (notif.read = true))
            else if (unread === 'all') draft.forEach((notif) => (notif.read = false))
            else {
              read?.forEach((id) => {
                const notif = draft.find((notif) => notif.id === id)
                if (notif) notif.read = true
              })
              unread?.forEach((id) => {
                const notif = draft.find((notif) => notif.id === id)
                if (notif) notif.read = false
              })
            }
          })
        )
        try {
          await queryFulfilled
        } catch {
          patchResult.undo()
        }
      },
    }),

    deleteReadNotifications: builder.mutation<number, void>({
      query: () => ({
        url: moduleUrl('notifications/read'),
        method: 'DELETE',
      }),
      invalidatesTags: ['Notification'],
    }),

    deleteNotification: builder.mutation<Notification, number>({
      query: (id) => ({
        url: moduleUrl(`notifications/${id}`),
        method: 'DELETE',
      }),
      invalidatesTags: ['Notification'],
    }),

    updateNotification: builder.mutation<Notification, Partial<Notification>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`notifications/${id}`),
        method: 'PUT',
        body,
      }),
    }),

    getNotificationById: builder.query<Notification, string>({
      query: (id) => ({
        url: moduleUrl(`notifications/${id}`),
        method: 'GET',
      }),
      providesTags: ['Notification'],
    }),
  }),
})

export const {
  useGetNotificationsQuery,
  useLazyGetNotificationsQuery,
  useCreateNotificationMutation,
  useDeleteNotificationMutation,
  useUpdateNotificationMutation,
  useGetNotificationByIdQuery,
  useMarkNotificationsMutation,
  useDeleteReadNotificationsMutation,
} = notificationApi

// get the query results that will be updated optimistically
export const selectNotifications = notificationApi.endpoints.getNotifications.select({})

export const getUnreadNotifications = (notifs?: Notification[]) =>
  notifs?.reduce((acc, notif) => acc + Number(notif.read === false), 0) ?? 0
