import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { GroupByQuery, FindManyQuery } from '..'
import type { RootState } from '../store'

export const tagTypes = [
  'Person',
  'PersonRole',
  'Organization',
  'User',
  'Ticket',
  'Note',
  'Notification',
  'Tag',
  'Debug',
  'Subscription',
  'Assignment',
  'WatchedOrg',
  'WatchedTag',
  'Watching',
  'Comment',
  'Settings',
  'UserComments',
  'Search',
  'Webhooks',
  'UserHistory',
] as const

/**
 * Stringify findMany query arguments use a paramSerializer?
 *
 * @see https://redux-toolkit.js.org/rtk-query/api/fetchBaseQuery#setting-the-query-string
 */
export const findManyQuery = (params: FindManyQuery) => {
  const { where, cursor, ...rest } = params
  return {
    where: where ? JSON.stringify(where) : undefined,
    cursor: cursor ? JSON.stringify(cursor) : undefined,
    ...rest,
  }
}
export const groupByQuery = (params: GroupByQuery) => {
  const { where, ...rest } = params
  return {
    where: where ? JSON.stringify(where) : undefined,
    ...rest,
  }
}

export const capstoneUrl = process.env.REACT_APP_BASE_API_URL

/** Determine query URL */
export const moduleUrl = (url: string, module = 'ticket') => {
  // Backend creates proxy in development
  // To run one-step-ui locally, and proxy org service calls from ticket
  // service backend running locally:
  // - start ticket backend
  // - set REACT_APP_TICKET_USE_LOCAL=true in one-step-ui .env.development
  // - run one-step-ui
  const baseUrl = process.env.REACT_APP_TICKET_USE_LOCAL_PROXY ? 'https://localhost:5005' : ''
  // no 'ticket' prefix running local backend
  return process.env.REACT_APP_TICKET_USE_LOCAL_PROXY || process.env.NODE_ENV === 'development'
    ? module === 'ticket'
      ? `${baseUrl}/${url}`
      : `${baseUrl}/${module}/${url}`
    : // process.env.REACT_APP_TICKET_USE_REMOTE_PROXY -
    //   Use ticket service proxy in production
    // eg. capstone..org/organization => capstone...org/ticket/organization
    process.env.REACT_APP_TICKET_USE_REMOTE_PROXY
    ? `/ticket/${module}/${url}`
    : `/${module}/${url}` // otherwise, use onestep
}

export const fetchWithBQ = (baseUrl?: string) =>
  fetchBaseQuery({
    baseUrl:
      process.env.NODE_ENV === 'development'
        ? 'https://localhost:5005/' // backend server
        : baseUrl ?? capstoneUrl,

    // include cookies on every request
    credentials: 'include',

    prepareHeaders: (headers, api) => {
      const state = api.getState() as RootState
      const token = state.auth.token || localStorage.getItem('authtoken')
      const userId = state.user?.id

      if (token) {
        headers.set('Authorization', `Bearer ${token}`)
      }
      if (userId) {
        // console.log(`Setting userId = ${userId}`)
        headers.set('UserId', userId.toString())
      }

      return headers
    },
  })

/** Initialize an empty api service that we'll inject endpoints into later as needed */
export const api = createApi({
  baseQuery: fetchWithBQ(),

  endpoints: () => ({}),

  tagTypes,
})
