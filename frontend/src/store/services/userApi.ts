import type { User, UserResponse } from '../types'
import { api, moduleUrl } from './api'

export const userApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    loginOrCreateUser: builder.mutation<User, Partial<User>>({
      query: (body) => ({
        url: moduleUrl('users/login'),
        method: 'POST',
        body,
      }),
      invalidatesTags: ['User'],
    }),

    getCurrentUser: builder.query<UserResponse, void>({
      query: () => ({
        url: moduleUrl(`users/current`),
        method: 'GET',
      }),
      providesTags: ['User'],
    }),

    getUser: builder.query<UserResponse, string>({
      query: (userId) => ({
        url: moduleUrl(`admin/${userId}`),
        method: 'GET',
      }),
      providesTags: ['User'],
    }),

    getUsers: builder.query<User[], void>({
      query: () => ({
        url: moduleUrl('admin/users'),
        method: 'GET',
      }),
      providesTags: (result = []) => [
        { type: 'User', id: 'LIST' },
        ...result.map(({ id }) => ({ type: 'User' as const, id })),
      ],
    }),
  }),
})

export const {
  useLoginOrCreateUserMutation,
  useGetCurrentUserQuery,
  useGetUserQuery,
  useLazyGetUserQuery,
  useGetUsersQuery,
} = userApi
