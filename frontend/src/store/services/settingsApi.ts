import { Settings } from '../types'
import { api, moduleUrl } from './api'

export const settingsApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getSettings: builder.query<Settings, void>({
      query: () => ({
        url: moduleUrl('settings'),
        method: 'GET',
      }),
      providesTags: ['Settings'],
    }),

    updateSettings: builder.mutation<Settings, Partial<Settings>>({
      query: (body) => ({
        url: moduleUrl('settings'),
        method: 'PUT',
        body,
      }),
      invalidatesTags: ['Settings'],
    }),
  }),
})

export const { useGetSettingsQuery, useUpdateSettingsMutation } = settingsApi
