import type { Comment, CommentData } from '../types'
import { api, moduleUrl } from './api'

export const commentApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getCommentsForTicket: builder.query<Comment[], number>({
      query: (ticketId) => ({
        url: moduleUrl(`tickets/${ticketId}/comments`),
        method: 'GET',
      }),
      providesTags: (result = []) => [
        'Comment',
        ...result.map(({ id }) => ({ type: 'Comment' as const, id })),
      ],
    }),
    getComments: builder.query<Comment[], void>({
      query: () => ({
        url: moduleUrl(`comments`),
        method: 'GET',
      }),
      providesTags: (result = []) => [
        'Comment',
        { type: 'Comment' as const, id: 'LIST' },
        ...result.map(({ id }) => ({ type: 'Comment' as const, id })),
      ],
    }),
    createComment: builder.mutation<Comment, CommentData>({
      query: (body) => ({
        url: moduleUrl('comments'),
        method: 'POST',
        body,
      }),
      invalidatesTags: (result, error, arg) => [
        'Comment',
        // XXX: remove when done testing
        'Notification',
        { type: 'Ticket' as const, id: Number(arg.ticketId) },
        { type: 'UserComments' as const, id: arg.userId },
      ],
    }),
    deleteComment: builder.mutation<Comment, number>({
      query: (id) => ({
        url: moduleUrl(`comments/${id}`),
        method: 'DELETE',
      }),
      invalidatesTags: (result, error, arg) => [
        'Comment',
        { type: 'Ticket' as const, id: Number(arg) },
        { type: 'UserComments' as const, id: result?.userId },
      ],
    }),
    updateComment: builder.mutation<Comment, Partial<Comment>>({
      query: ({ id, ...body }) => ({
        url: moduleUrl(`comments/${id}`),
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result, error, arg) => [{ type: 'Comment' as const, id: Number(arg.id) }],
    }),
    getCommentById: builder.query<Comment, string>({
      query: (id) => ({
        url: moduleUrl(`comments/${id}`),
        method: 'GET',
      }),
      providesTags: (result, error, arg) => [{ type: 'Comment' as const, id: Number(arg) }],
    }),
  }),
})

export const {
  useGetCommentsForTicketQuery,
  useLazyGetCommentsForTicketQuery,
  useGetCommentsQuery,
  useCreateCommentMutation,
  useDeleteCommentMutation,
  useUpdateCommentMutation,
  useGetCommentByIdQuery,
} = commentApi
