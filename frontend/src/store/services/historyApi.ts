import { UserHistoryEntry, UserHistoryQuery } from '..'
import { api, moduleUrl } from './api'

export const historyApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getUserHistory: builder.query<UserHistoryEntry[], UserHistoryQuery | undefined>({
      query: (params?) => ({
        url: moduleUrl(`history`),
        method: 'GET',
        params,
      }),
      providesTags: ['UserHistory'],
    }),
  }),
})

export const { useGetUserHistoryQuery } = historyApi
