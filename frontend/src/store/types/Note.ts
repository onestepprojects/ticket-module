export interface Note {
  id: number
  userId: string
  createdAt: string
  updatedAt: string
  note: string
  ticketId: number
}
