/** @see backend/src/interfaces/settings.interface.ts */
export interface TicketNotificationSettings {
  ticketUpdate: boolean
  ticketStatusChange: boolean
  ticketAssignment: boolean
  ticketComment: boolean
}

export interface WatchedNotificationSettings {
  withTags: boolean
  withOrgs: boolean
  withRoles: boolean
}

export interface NotificationSettings {
  onSubscribed: TicketNotificationSettings
  onWatched: WatchedNotificationSettings
}

export interface Settings {
  /**
   * Subscribe to tickets when they are created
   */
  subscribeOnCreate: boolean
  /**
   * Subscribe to/unsubscribe from tickets when assigned/unassigned
   */
  subscribeOnAssign: boolean
  notifications: NotificationSettings
}
