/** @see backend/interaces/queries.interface.ts */
export interface PaginationQuery {
  skip?: number
  take?: number
}

export interface BaseManyQuery extends PaginationQuery {
  count?
  orderBy?: string
  createdBefore?: string
  createdAfter?: string
}

export interface SelectManyQuery extends BaseManyQuery {
  select?: string
}

export interface FindManyQuery extends SelectManyQuery {
  userId?: string
  cursor?: object
  where?: object
}

export interface GroupByQuery extends PaginationQuery {
  by: string
  count?: 'all' | null | string
  orderBy?: string
  where?: object
}
