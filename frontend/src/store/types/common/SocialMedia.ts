export interface SocialMedia {
  twitter: string
  facebook: string
  linkedin: string
  youtube: string
}
