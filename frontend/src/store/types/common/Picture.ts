export interface Picture {
  name: string
  size: number
  format: string
  body: string
  caption: string
  bucketName: string
  creator: string
  url: string
}
