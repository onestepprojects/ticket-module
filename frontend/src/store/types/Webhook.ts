/**
 * @see backend WebhookType in schema / compiled '@prisma/client'
 */
export const WebhookType = {
  ticketCreate: 'ticketCreate',
  ticketStatusChange: 'ticketStatusChange',
}

export type WebhookType = typeof WebhookType[keyof typeof WebhookType]

export interface Webhook {
  id?: number
  createdAt?: Date
  updatedAt?: Date
  createdBy?: string
  name: string
  type: WebhookType
  url?: string
  email?: string
  active?: boolean
  secret?: string
}
