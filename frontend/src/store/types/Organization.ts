import type { Address, Picture, SocialMedia } from './common'

export interface Organization {
  id: string
  type: string
  supplier: boolean
  name: string
  logo: Picture
  email: string
  phones: {
    phone: string
  }
  website: string
  address: Address
  creatorUUID: string
  statusLabel: string
  statusLastActive: string
  tagline: string
  description: {
    description: string
    serviceArea: { latitude: number; longitude: number }[]
    projectTypes: string[]
  }
  requesterRoles: string[]
  isActive: boolean
  socialMedia: SocialMedia
}

// GET org/organizations/get-org-roles-by-person/{personId}
export interface OrgRole {
  id: string
  name: string
  roles: string[]
}
