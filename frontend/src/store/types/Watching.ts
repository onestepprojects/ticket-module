import { Organization, User } from '.'

/** @see backend/src/interfaces/watching.interface.ts */
export interface WatchingResponse {
  orgIds: string[]
  tagIds: number[]
}

export interface WatchingUpdate {
  orgIds?: {
    add?: string[]
    remove?: string[]
  }
  tagIds?: {
    add?: number[]
    remove?: number[]
  }
}

export type TagWatchers = Partial<User>[]
export type OrgWatchers = Partial<Organization>[]
