import { BaseManyQuery } from '.'

/**
 * Ticket changelogs
 *
 * @see backend/src/prisma/prisma.schema TicketChangeLog
 * @see backend/src/interfaces/changelogs.interface.ts
 */
export const TicketChange = {
  created: 'created',
  modified: 'modified',
  statusChange: 'statusChange',
}

export type TicketChange = typeof TicketChange[keyof typeof TicketChange]

export interface TicketChangeLog {
  id: number
  type: TicketChange
  createdAt: Date
  userId: string
  ticketId: number
  log?: string
}

export interface ChangeLogEntry extends TicketChangeLog {
  user: string
}

/** Query parameters for GET request to retrieve changelog for ticket */
export interface ChangeLogQuery extends BaseManyQuery {
  ticketId: number
  /** Changelog entry types to get */
  types?: TicketChange[]
}
