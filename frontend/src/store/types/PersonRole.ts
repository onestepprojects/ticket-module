export interface PersonRole {
  name: string
  roles: string[]
}
