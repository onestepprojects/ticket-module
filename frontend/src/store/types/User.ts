export interface User {
  id: string
  email?: string
  roles?: string[]
  name?: string
  // XXX: remove these eventually
  token?: string
  isAdmin: boolean
  password: '1234' | string
}

export interface UserResponse extends Omit<User, 'password'> {
  subscriptions?: { id: number }[]
  watchedOrgs?: { id: string }[]
  watchedTags?: { id: number }[]
  ticketsFor?: { id: number }[]
  ticketsBy?: { id: number }[]
  activity?: { id: number }[]
}
