import { BaseManyQuery, TicketStatus } from '.'

/** @see backend/src/interfaces/notifications.interface.ts */
export type TicketEvent =
  | 'ticketUpdate'
  | 'ticketStatusChange'
  | 'ticketAssignment'
  | 'ticketComment'
  | 'ticketCreate'

export type NewTicketEvent = 'withTags' | 'withOrgs' | 'withRoles'
export type SubscribeEvent = 'subscribeOnCreate' | 'subscribeOnAssign'

/**
 * @see NotificationType in backend '@prisma/client'
 */
export const NotificationType = {
  ticketCreate: 'ticketCreate',
  ticketUpdate: 'ticketUpdate',
  ticketAssignment: 'ticketAssignment',
  ticketStatusChange: 'ticketStatusChange',
  ticketComment: 'ticketComment',
  withTags: 'withTags',
  withOrgs: 'withOrgs',
  rewards: 'rewards',
  withRoles: 'withRoles',
  message: 'message',
  user: 'user',
  organization: 'organization',
}
export type NotificationType = typeof NotificationType[keyof typeof NotificationType]

export interface Notification {
  id: number
  type: NotificationType
  message: string
  points?: number
  status?: keyof typeof TicketStatus
  createdAt?: string
  updatedAt?: string
  senderId?: string
  ticketId?: number
  read?: boolean
  tags?: number[]
  organizations?: string[]
}

export interface MarkNotificationBody {
  read?: 'all' | number[]
  unread?: 'all' | number[]
}

export interface FindManyNotificationsQuery extends BaseManyQuery {
  read?: boolean
  days?: number
}
