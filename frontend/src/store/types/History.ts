/**
 * User history / activity
 *
 * @see backend/src/prisma/prisma.schema TicketChangeLog
 * @see backend/src/interfaces/history.interface.ts
 */

import { ChangeLogEntry, ChangeLogQuery } from '.'

export type UserHistoryEntry = ChangeLogEntry

export interface UserHistoryQuery extends Omit<ChangeLogQuery, 'ticketId'> {
  ticketIds?: number[]
}
