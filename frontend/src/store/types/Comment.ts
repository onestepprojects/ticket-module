export interface Comment {
  id: number
  createdAt?: string
  updatedAt?: string
  userId: string
  ticketId: number
  comment: string
}

export type CommentData = Omit<Comment, 'id' | 'createdAt' | 'updatedAt'>
