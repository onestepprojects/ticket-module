export interface UserFile {
  id?: number
  userId?: string
  createdAt?: Date
  filename: string
  path: string
}

export interface TicketFile {
  id?: number
  ticketId?: number
  createdAt?: Date
  filename: string
  path: string
}
