import { FindManyQuery, TagTypes } from '.'
import { TicketChangeLog } from './ChangeLog'

/**
 * @see associated types in backend '@prisma/client'
 */
export const TicketStatus = {
  open: 'open',
  closed: 'closed',
  completed: 'completed',
}
export type TicketStatus = typeof TicketStatus[keyof typeof TicketStatus]

export const TicketPriority = {
  low: 'low',
  normal: 'normal',
  high: 'high',
}
export type TicketPriority = typeof TicketPriority[keyof typeof TicketPriority]

export const TicketVisibility = {
  restricted: 'restricted',
  public: 'public',
  organization: 'organization',
  private: 'private',
}
export type TicketVisibility = typeof TicketVisibility[keyof typeof TicketVisibility]

export const TicketEnums = {
  status: TicketStatus,
  priority: TicketPriority,
  visibility: TicketVisibility,
}

export interface Ticket {
  createdAt?: string
  updatedAt?: string
  id: number
  userId: string
  title: string
  detail: string
  changelog?: TicketChangeLog[]
  status: TicketStatus
  priority: TicketPriority
  visibility: TicketVisibility
  referralUrl?: string
  files?: number[]
  tags?: number[]
  organizations?: string[]
  linkedTo?: number[]
  linkedBy?: number[]
  assignedTo?: string[]
}

export interface TicketFormData extends Omit<Ticket, 'tags' | 'organizations'> {
  topics?: number[]
  physicians?: number[]
  educators?: number[]
  tagIds?: number[]
  organizationIds?: string[]
}

export interface TicketUpdateRelation<T> {
  add?: T[]
  remove?: T[]
}

export interface TicketEditFormData extends Omit<Ticket, 'tags' | 'organizations' | 'linkedTo'> {
  tagIds?: TicketUpdateRelation<number>
  organizationIds?: TicketUpdateRelation<string>
  linkedTo?: TicketUpdateRelation<number>
}

/** @see backend/src/interfaces/tickets.interface.ts */
export interface TicketResponse
  extends Omit<
    Ticket,
    'files' | 'tags' | 'organizations' | 'linkedTo' | 'linkedBy' | 'assignedTo' | 'changelog'
  > {
  files: number[]
  tags: number[]
  organizations: string[]
  linkedTo: number[]
  linkedBy: number[]
  assignedTo: string[]
  contributors: string[]
  points: number
}

export interface LinkTicketQuery {
  id: number
  ids: number[]
}

export interface FindManyTicketQuery extends FindManyQuery {
  isAssigned?: boolean
  status?: TicketStatus[]
  priority?: TicketPriority[]
  visibility?: TicketVisibility[]
  speciality?: string[]
  topic?: string[]
}

/** TODO: add support for searching within these */
export interface TicketSearchRelations {
  organizations?: string[]
  creators?: string[]
  assignees?: string[]
  contributors?: string[]
}

/** Search query parameters for tickets */
export interface SearchTicketsQuery extends FindManyTicketQuery {
  title?: string
  detail?: string
  literal?: boolean
  caseSensitive?: boolean
}

// input fields for search form
interface SearchTicketsInputs extends Omit<SearchTicketsQuery, 'isAssigned'> {
  isAssigned?: 'all' | boolean
}
export type SearchTicketsFormInput = SearchTicketsInputs & {
  [_ in typeof TagTypes[number]]?: string[]
}

export interface UpdateTicketStatusQuery {
  ticketId: number
  status: keyof typeof TicketStatus
}
