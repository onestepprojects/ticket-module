import type { Address, Picture, SocialMedia } from './common'

export interface Person {
  uuid: string
  name: string
  currentAddress: Address
  email: string
  phones: {
    phone: string
  }
  birthdate: string
  profilePicture: Picture
  lastActivity: string
  history: any[] // eslint-disable-line
  homeAddress: Address
  ecUUID: string
  ecName: any // eslint-disable-line
  ecPhones: {
    phone: string
  }
  ecAddress: Address
  paymentAccount: {
    payID: string
    blockchainAddress: string
    status: string
    assets: any // eslint-disable-line
  }
  location: any // eslint-disable-line
  gender: string
  socialMedia: SocialMedia
}
