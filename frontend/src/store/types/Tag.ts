// @see schema.prisma TagType
// @see backend/src/interfaces/tags.interface.ts

export const TagType = {
  topic: 'topic',
  educator: 'educator',
  physician: 'physician',
}

export type TagType = typeof TagType[keyof typeof TagType]

export interface Tag {
  id: number
  createdAt: string
  updatedAt: string
  name: string
  type: TagType
}
