import '@uiw/react-md-editor/markdown-editor.css'
import '@uiw/react-markdown-preview/markdown.css'
import './App.css'
import React, { FC } from 'react'
import { BrowserRouter } from 'react-router-dom-v5'
import { Provider } from 'react-redux'
import { store } from './store'
import { TicketContainer, TicketContainerProps } from './Container'

export interface TicketModuleProps extends TicketContainerProps {
  /** Module router base name. */
  mountPath: string
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  authHelper?: any // eslint-disable-line
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  person?: any // eslint-disable-line
}

const TicketModule: FC<TicketModuleProps> = ({ mountPath, ...restProps }) => {
  return (
    <BrowserRouter basename={mountPath}>
      <Provider store={store}>
        <TicketContainer {...restProps} />
      </Provider>
    </BrowserRouter>
  )
}

export default TicketModule
