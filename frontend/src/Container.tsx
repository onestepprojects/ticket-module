// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect } from 'react'
import { Route, Switch, RouteComponentProps } from 'react-router-dom-v5'
import DefaultLayout from './components/Layout/DefaultLayout'
import routes from './routes'
import { useAppDispatch, authSlice, type Person, userSlice, useLazyGetUserQuery } from './store'
import { notification, Spin, Typography } from 'antd'

export interface TicketContainerProps extends RouteComponentProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

notification.config({
  placement: 'bottomRight',
  bottom: 50,
  duration: 3,
  rtl: true,
})

export const TicketContainer: FC<TicketContainerProps> = ({ auth }) => {
  // TODO: Remove this after refactor all components
  // const mountPath = ''
  const dispatch = useAppDispatch()
  const [getUser, { data: user }] = useLazyGetUserQuery()
  const [isAdmin, setIsAdmin] = React.useState(false)
  const [activeRoutes, setActiveRoutes] = React.useState([]) // eslint-disable-line

  // Create associated with AWS user in backend
  /* const [loginUser, { isLoading: _ }] = useLoginOrCreateUserMutation() */

  useEffect(() => {
    if (!auth) return
    dispatch(authSlice.actions.setAuth(auth))
    // XXX: Uncomment eventually to enforce admin status
    // setIsAdmin(auth.roles.includes('admin'))
    setIsAdmin(true)
    setActiveRoutes(isAdmin ? routes : routes.filter((route) => !route.admin))
  }, [auth])

  useEffect(async () => {
    if (!auth) {
      dispatch(userSlice.actions.unsetUser({}))
    } else if (!user) {
      const res = await getUser(auth.person.uuid).unwrap()
      if (!res) {
        console.log('Error setting user:', res)
      }
      /* const { person, roles, token } = auth
       * loginUser({
       *   id: auth.person.uuid,
       *   name: person.name,
       *   email: person.email,
       *   password: '1234',
       *   isAdmin: true,
       *   token,
       *   roles,
       * }) */
    } // else console.dir(user)
  }, [user, auth])

  if (!(auth && user))
    return (
      <div className='!min-h-screen flex !justify-center !items-center pb-[100px]'>
        <div className='grid grid-cols-1 place-content-center'>
          <Typography.Title level={4}>Loading ticket module</Typography.Title>
          <Spin />
        </div>
      </div>
    )

  return (
    /* className='ui container' */
    <div className=''>
      <DefaultLayout auth={auth} isAdmin={isAdmin}>
        <React.StrictMode>
          <Switch>
            {routes.map((route, idx) => (
              <Route
                key={idx}
                exact={route.exact}
                path={route.path}
                component={(props) => <route.component auth={auth} isAdmin={isAdmin} {...props} />}
              />
            ))}
          </Switch>
        </React.StrictMode>
      </DefaultLayout>
    </div>
  )
}
