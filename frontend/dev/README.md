Running this makefile should launch a patched version of one-step-ui with our
ticket-module embedded (with an extra 'Ticket' button on the main sidebar) and a
route to '/tickets'.

# Setup

The default paths should work, but can be adjusted by copying
`example.envvars.make` to `envvars.make` and tweaking them.

The `DOTENV` needs to point to a file with site environment variables (eg.
`.env`).

This runs on linux, but may work as long as you have `make`, YMMV.

# Run

The makefile does the following:

1.  clone one-step-ui
2.  copies in `DOTENV`
3.  applies a patch to make changes to source code
4.  builds our ticket-module to a module needed by one-step-ui
5.  links the module to one-step-ui
6.  runs one-step-ui

To run:

    make
