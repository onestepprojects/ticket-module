# Use absolute paths!
# it's interpreted by make, so $(CURDIR) refers to this directory

# Path to ticket-module fronted directory
TICKET_FRONTEND_DIR = $(CURDIR)/..
# Path to install one-step-ui
ONESTEPUI_DIR       = $(CURDIR)/one-step-ui
# Path to .env
DOTENV              = $(TICKET_FRONTEND_DIR)/.env
