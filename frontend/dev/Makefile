SHELL = /bin/bash

-include envvars.make

# Path to ticket-module directory
TICKET_FRONTEND_DIR ?= $(CURDIR)/..
# Path to install one-step-ui
ONESTEPUI_DIR       ?= $(CURDIR)/one-step-ui
# Path to .env
DOTENV              ?= $(TICKET_FRONTEND_DIR)/.env

TICKET_FRONTEND_DIST = $(TICKET_FRONTEND_DIR)/dist
ONESTEPUI_DIST       = ${ONESTEPUI_DIR}/node_modules/@onestepprojects/ticket-module/dist

all: run
	@

${ONESTEPUI_DIR}:
	git clone git@gitlab.com:onestepprojects/one-step-ui "${ONESTEPUI_DIR}"

${ONESTEPUI_DIR}/.env.development: ${ONESTEPUI_DIR}
	[ -e "${DOTENV}" ] || { echo ".env not found: ${DOTENV}" && exit 1; }
	cp "${DOTENV}" "$@"

# Apply changes
patch: ${ONESTEPUI_DIR}
	cd "${ONESTEPUI_DIR}" &&                            \
	git apply --ignore-space-change --ignore-whitespace \
		"$(CURDIR)/patch.diff" 2>/dev/null || true

${ONESTEPUI_DIR}/node_modules: ${ONESTEPUI_DIR}/.env.development patch
	cd "${ONESTEPUI_DIR}" && npm i --force

${TICKET_FRONTEND_DIST}:
	[ -e "${TICKET_FRONTEND_DIR}" ] || {               \
		echo "${TICKET_FRONTEND_DIR} not found" && \
		exit 1;                                    \
	}
	cd "${TICKET_FRONTEND_DIR}" && npx vite build --target modules;

${ONESTEPUI_DIST}: ${TICKET_FRONTEND_DIST} ${ONESTEPUI_DIR}/node_modules
	[ -e "${TICKET_FRONTEND_DIST}" ] || {               \
		echo "${TICKET_FRONTEND_DIST} not found" && \
		exit 1;                                     \
	}
	ln -sfn "${TICKET_FRONTEND_DIST}" "${ONESTEPUI_DIST}";

patch.diff: ${ONESTEPUI_DIR}
	cd "${ONESTEPUI_DIR}" && \
	git add -A &&            \
	git diff HEAD --relative ':(exclude)package-lock.json' > "$(CURDIR)/$@"

# Install one-step-ui, patch it, install it, create dist, link it,
# and run one-step-ui with ticket-module
run: ${ONESTEPUI_DIST}
	cd "${ONESTEPUI_DIR}" && npm run start
