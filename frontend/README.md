# Ticket Module

## env file

Create a `.env.development.local` file under `frontend/`, and fill it from the
latest content from the Slack channel #https://onestep-relief.slack.com/archives/C01TXR5R895

## Dev Setup

### Start

Run `npm run dev` to start the standalone app.

### Backend

To start the backend, go to `../backend-express` and follow the README to
configure environment variables and start it with `docker-compose up`. That
will start a backend server listening on http://localhost:5005 with its
documented routes at http://localhost:5005/docs

### Https

`devcert` should configure the proper certificates necessary to develop on https
at localhost in chrome and firefox - this happens when `vite` starts the server
(ie. `npm run dev`).

### Chrome

If using chrome to develop, and the login to AWS isn't showing up, you
may need to tell chrome to allow popups and redirects in the site settings,

    chrome://settings/content/siteDetails?site=https%3A%2F%2Flocalhost%3A3000%2F

_This is probably unnecessary_: to allow invalid certificates on localhost. Go to
`chrome://flags` and enable "Allow invalid certificates for resources loaded
from localhost".

### React/redux devtools

To add react developer tools to the browser:
Install `npm -g react-devtools` globally, and add the chrome/firefox react
developer tools extension. Then a panel will be available in the browser
console to watch React components.

To add redux devtools, install the relevant browser extension for
https://github.com/reduxjs/redux-devtools

## Developing with one-step-ui webpack dev server

Add dev domain name to `/etc/hosts`.

```sh
# /etc/hosts
127.0.0.1 dev.onestepprojects.org
```

Install dependencies and start module in watch mode.

```sh
# ticket-module/frontend
npm install
npm start
```

Link module and start app dev server.

```sh
# one-step-ui
npm install
npm link ../ticket-module/frontend
HTTPS=1 npm start
```

Open https://dev.onestepprojects.org:3000/ in your browser.

## Common Issues

### Vite

If there is an error when building about maximum call stack being exceeded, you
may need to ensure there is a `.env` file in this directory (eg. copy
`.env.development.local`) to `.env`. This is an active issue on vite
<https://github.com/vitejs/vite/issues/6933>.

## Available Scripts

In the project directory, you can run:

### Install: `npm i`

To install all the dependencies, if you are hitting the error that
`@onestepprojects/authentication-helper` package cannot be fetched via the
public npm registry, you will need to add the private registry in your `.npmrc`
file.

Add this line in your `.npmrc` file:

```
@onestepprojects:registry=https://gitlab.com/api/v4/packages/npm/
```

If you don't want to change the npm registry for all your other projects on your
machine, you can create a new `.npmrc` file inside the frontend project root,
and it's effective for this project only.

### Dev: `npm run dev`

It will start a vite dev server and listens on <https://localhost:3000/>.

It supports fast live reloading, which is very friendly for development.

TODO: Following parts will be updated later

### Build: `npm start`

Runs the app in the development mode.\ Open
[http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\ See the section about
[running tests](https://facebook.github.io/create-react-app/docs/running-tests)
for more information.

### `npm run build`

Builds the app for production to the `build` folder.\ It correctly bundles React
in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\ Your app is ready
to be deployed!

See the section about
[deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can
`eject` at any time. This command will remove the single build dependency from
your project.

Instead, it will copy all the configuration files and the transitive
dependencies (webpack, Babel, ESLint, etc) right into your project so you have
full control over them. All of the commands except `eject` will still work, but
they will point to the copied scripts so you can tweak them. At this point
you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for
small and middle deployments, and you shouldn’t feel obligated to use this
feature. However we understand that this tool wouldn’t be useful if you couldn’t
customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App
documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here:
[https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here:
[https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here:
[https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here:
[https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here:
[https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here:
[https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

# Getting Started with Create React App

This project was bootstrapped with [Create React
App](https://github.com/facebook/create-react-app).
